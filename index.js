/** @format */
import { AppRegistry } from 'react-native';
import Main from './Components/Main.js';
import { name as appName } from './app.json';
//import Test from './Components/Test';

AppRegistry.registerComponent(appName, () => Main);
