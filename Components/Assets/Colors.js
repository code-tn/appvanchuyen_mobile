const Colors = {
    mainColor: '#328513',
    subColor: '#23b42d',
    blackColor: '#333',
    activeColor: '#206606'
};
export default Colors;
