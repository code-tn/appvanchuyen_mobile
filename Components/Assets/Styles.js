import { StyleSheet, Platform, StatusBar, Dimensions } from 'react-native';
import Colors from './Colors';

const { height } = Dimensions.get('window');
export default StyleSheet.create({
  paddingTopAndroid: {
    paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
    fontFamily: 'Helvetica Neue'
  },
  text: {
    marginBottom: 10
  },
  input: {
    backgroundColor: '#fff',
    color: Colors.blackColor,
    marginBottom: 10,
    height: 40,
    paddingLeft: 12,
    paddingRight: 12,
    fontSize: 13,
    borderRadius: 3,
    borderWidth: 1,
    borderColor: '#c8c8c8',
  },
  error: {
    borderColor: 'red'
  },
  textarea: {
    backgroundColor: '#fff',
    color: Colors.blackColor,
    marginBottom: 10,
    paddingLeft: 12,
    paddingRight: 12,
    fontSize: 13,
    borderRadius: 3,
    borderWidth: 1,
    borderColor: '#c8c8c8',
    height: 80
  },
  select: {
    backgroundColor: '#fff',
    color: Colors.blackColor,
    marginBottom: 10,
    height: 40,
    paddingLeft: 12,
    paddingRight: 12,
    fontSize: 13,
    borderRadius: 3,
    borderWidth: 1,
    borderColor: '#c8c8c8',
  },
  btnSubmit: {
    height: 40,
    fontSize: 13,
    backgroundColor: Colors.mainColor,
    color: '#fff',
    paddingLeft: 10,
    paddingRight: 10
  },
  modalDialog: {
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    padding: 10,
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  modalBody: {
    backgroundColor: '#fff',
  },
  modalHeader: {
    paddingRight: 10,
    backgroundColor: Colors.mainColor,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'space-between',
  },
  modalTitle: {
    color: '#fff', 
    marginTop: 12 
  },
  btnCloseModal: {
    fontSize: 13,
    color: '#fff',
    padding: 3,
    height: 'auto',
  },
  errorAlert: {
    color: 'red',
    fontSize: 12,
    marginBottom: 5
  },
  topBar: {
    backgroundColor: Colors.mainColor,
    color: '#fff',
    height: height / 13,
    fontSize: 20
  },
  fontFace: {
    fontFamily: 'Helvetica Neue'
  },
  content: {
    backgroundColor: '#eee'
  },
  link: {
    color: Colors.subColor,
    borderBottomWidth: 1,
    borderColor: Colors.subColor
  },
  btn: {
    height: 'auto'
  },
  btn__text: {
    paddingLeft: 12,
    paddingRight: 0,
  },
  btn__icon: {
    fontSize: 17, 
    marginRight: 12, 
    marginLeft: 12,
  }
});
