import React, { Component } from 'react';
import { StyleSheet, StatusBar, Platform, View } from 'react-native';
import { Header, Body, Title } from 'native-base';
import Colors from '../Assets/Colors';

class TopBarOnlyTitle extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        const { title } = this.props;
        return (
            <Header style={styles.topBar} androidStatusBarColor={Colors.activeColor} iosBarStyle='light-content'>
                <Body style={styles.body}>
                    <Title style={styles.topBarTitle}>{title}</Title>
                </Body>
            </Header>
        );
    }
}
const styles = StyleSheet.create({
    topBar: {
        backgroundColor: Colors.mainColor,
        color: '#fff',
    },
    body: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row',
    },
    topBarTitle: {
        color: '#fff',
        fontSize: 17,
        fontFamily: 'Helvetica Neue',
    }
});
export default TopBarOnlyTitle;
