import React, { Component } from 'react';
import { View, ActivityIndicator, StyleSheet, Text, Dimensions } from 'react-native';
import Colors from '../Assets/Colors';

const { height } = Dimensions.get('window');
class Loading extends Component {
    render() {
        let loading_text = 'Đang tải...';
        if (typeof this.props.loadingText !== 'undefined') {
            loading_text = this.props.loadingText;
        }
        return (
            <View style={loadingStyle.container}>
                <ActivityIndicator size="large" color={Colors.mainColor}/>
                <Text>{loading_text}</Text>
            </View>
        );
    }
}
const loadingStyle = StyleSheet.create({
    container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        zIndex: 99,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    }
});
export default Loading;
