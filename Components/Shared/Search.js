import React, { Component } from 'react';
import { View, StyleSheet, TextInput } from 'react-native';
import { Button, Icon } from 'native-base';
import Colors from '../Assets/Colors';

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword: ''
        };
    }
    render() {
        const {
            searchBox,
            formBox,
            searchInput,
            btnSearch,
            btnSearchIcon
        } = SearchStyles;
        return (
            <View style={searchBox}>
                <View style={formBox}>
                    <TextInput 
                        style={searchInput} 
                        onChangeText={(text) => this.setState({ keyword: text })}
                        placeholder='Tìm kiếm...'
                    />
                    <Button style={btnSearch}>
                        <Icon type='FontAwesome' name='search' style={btnSearchIcon} />
                    </Button>
                </View>
            </View>
        );
    }
}
const SearchStyles = StyleSheet.create({
    searchBox: {
        padding: 10,
        backgroundColor: '#eee'
    },
    formBox: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    searchInput: {
        borderWidth: 1,
        borderColor: '#eee',
        height: 35,
        paddingVertical: 0,
        paddingHorizontal: 8,
        backgroundColor: '#fff',
        flexGrow: 1,
        borderRadius: 2,
    },
    btnSearch: {
        backgroundColor: Colors.subColor,
        height: 35,
        marginLeft: 5
    },
    btnSearchIcon: {
        color: '#fff',
        fontWeight: 'normal',
        marginLeft: 10,
        marginRight: 10,
        fontSize: 15,
    }
});
export default Search;
