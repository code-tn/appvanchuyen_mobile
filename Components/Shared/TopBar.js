import React, { Component } from 'react';
import { StyleSheet, View, Platform, StatusBar } from 'react-native';
import { Header, Left, Button, Icon, Body, Title, Right } from 'native-base';
import Colors from '../Assets/Colors';

export default class TopBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        const { navigation, title = 'Title' } = this.props;
        const {
            topBar,
            topBarTitle
        } = styles;
        return (
            <Header style={topBar} androidStatusBarColor={Colors.activeColor} iosBarStyle='light-content'>
                <Left style={{ flex: 1 }}>
                    <Button transparent onPress={() => navigation.goBack()}>
                        <Icon name='arrow-back' style={{ color: '#fff' }} />
                    </Button>
                </Left>
                <Body style={{ flex: 8 }}>
                    <Title style={topBarTitle}>{title}</Title>
                </Body>
                <Right style={{ flex: 1 }} />
            </Header>
        );
    }
}
const styles = StyleSheet.create({
    topBar: {
        fontFamily: 'Helvetica Neue',
        backgroundColor: Colors.mainColor,
        color: '#fff',
    },
    topBarTitle: {
        fontSize: 17,
        fontFamily: 'Helvetica Neue',
        width: '100%',
        textAlign: 'center',
        color: '#fff'

    }
});
