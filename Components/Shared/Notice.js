import React, { PureComponent } from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Container, Button, Text } from 'native-base';
import TopBarOnlyTitle from './TopBarOnlyTitle';
import Styles from '../Assets/Styles';
import noticeIcon from './images/promotion.png';

class Notice extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    
    render_confirm() {
        let callback = this.props.navigation.getParam('callback');
        if(typeof callback !== 'function') return;
        return (
            <Button
                style={Styles.btnSubmit}
                onPress={() => {
                    callback();
                }}
            >
                <Text>OK</Text>
            </Button>
        );
    }
    render() {
        const {
            icon,
            contentBox,
            textStyle
        } = noticeStyles;
        let msg = '';
        if (typeof this.props.msg !== 'undefined' && msg == '')
            msg = this.props.msg;
        if(typeof this.props.navigation.getParam('msg') !== 'undefined' && msg == '')
            msg = this.props.navigation.getParam('msg');
        if(msg == ''){
            this.props.navigation.goBack();
        }
        return (
            <Container>
                <TopBarOnlyTitle title='Thông báo'/>
                <View style={contentBox}>
                    <Image source={noticeIcon} style={icon}/>
                    <Text style={textStyle}>{msg}</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                        {this.render_confirm()}
                    </View>
                </View>
            </Container>
        );
    }
}

const noticeStyles = StyleSheet.create({
    contentBox: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        backgroundColor: '#eee',
        flexGrow: 1,
        padding: 10
    },
    icon: {
        width: 128,
        height: 128,
        resizeMode: 'contain'
    },
    textStyle: {
        fontSize: 15,
        textAlign: 'center',
        marginVertical: 10,
    }
});
export default Notice;
