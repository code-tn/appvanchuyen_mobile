import React, { Component } from 'react';
import { View, StyleSheet, ActivityIndicator, Alert } from 'react-native';
import { Container, Content, Button, Text } from 'native-base';
import RadioGroup from 'react-native-radio-buttons-group';
import TopBar from '../Shared/TopBar';
import Styles from '../Assets/Styles';
import BankForm from './Partials/BankForm';
import Colors from '../Assets/Colors';
import { updateUserBankInfo } from '../../Data/userRepo';

class EditBankInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSource: undefined,
            loading: false,
            sending: false,
            data: ''
        };
    }

    componentDidMount() {
        const dataSource = this.props.navigation.getParam('dataSource', undefined);
        if (dataSource == undefined) {
            this._alert('Thông tin ngân hàng', 'Có lỗi xảy ra trong quá trình kết nối, vui lòng thử lại sau!');
            this.props.navigation.goBack();
        }
        let lichDoiSoat = '';
        if (dataSource.lich_doisoat) {
            dataSource.lich_doisoat.map((item, index) => {
                lichDoiSoat += item;
                lichDoiSoat += index < dataSource.lich_doisoat.length - 1 ? ',' : '';
            });
        }
        this.setState({
            dataSource: dataSource,
            data: [
                {
                    label: 'Đối soát 1 lần/ngày trong giờ hành chính',
                    value: '2,3,4,5,6,7,8',
                    selected: lichDoiSoat === '2,3,4,5,6,7,8' ? true : false
                },
                {
                    label: 'Đối soát 2 lần/tuần vào thứ 4 và thứ 6',
                    value: '4,6',
                    selected: lichDoiSoat === '4,6' ? true : false
                },
                {
                    label: 'Đối soát 3 lần/tuần vào thứ 2/4/6',
                    value: '2,4,6',
                    selected: lichDoiSoat === '2,4,6' ? true : false
                },
                {
                    label: 'Đối soát 1 lần/tuần vào thứ 6',
                    value: '6',
                    selected: lichDoiSoat === '6' ? true : false
                }
            ],
        })
    }
    _alert(title, content) {
        return (
            Alert.alert(
                title,
                content.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            )
        );
    }
    _submitForm() {
        const tknh = this.state.dataSource.tai_khoan_ngan_hang;
        if (this.state.dataSource.tai_khoan_ngan_hang === null || this.state.dataSource.tai_khoan_ngan_hang === undefined) {
            this._alert('Lỗi', 'Vui lòng nhập đầy đủ thông tin');
            return;
        }
        if (tknh.chutaikhoan === "" || tknh.chutaikhoan === undefined) {
            this._alert('Lỗi', 'Vui lòng nhập thông tin chủ tài khoản');
            return;
        }
        if (tknh.so_taikhoan === "" || tknh.so_taikhoan === undefined) {
            this._alert('Lỗi', 'Vui lòng nhập số tài khoản');
            return;
        }
        if (tknh.nganhang === "" || tknh.nganhang === undefined) {
            this._alert('Lỗi', 'Vui lòng nhập tên ngân hàng');
            return;
        }
        if (tknh.chinhanh === "" || tknh.chinhanh === undefined) {
            this._alert('Lỗi', 'Vui lòng nhập thông tin chi nhánh');
            return;
        }
        this.setState({ sending: true });
        let bankInfo = this.state.dataSource.tai_khoan_ngan_hang;
        let lichDoiSoat = this.state.data.find(e => e.selected == true);
        bankInfo = Object.assign(bankInfo, { lich_doisoat: lichDoiSoat.value });
        setTimeout(async () => {
            await updateUserBankInfo(bankInfo, res => {
                if (res.success) {
                    this.setState({ sending: false });
                    Alert.alert(
                        'Thông báo',
                        'Lưu thông tin thành công',
                        [
                            {
                                text: 'OK', onPress: () => {
                                    if (typeof this.props.navigation.state.params.onGoBack === 'function')
                                        this.props.navigation.state.params.onGoBack(bankInfo);
                                    this.props.navigation.goBack();
                                }
                            },
                        ],
                        { cancelable: false },
                    );
                } else {
                    this.setState({ sending: false });
                    this._alert('Lỗi', res.message);
                }
            }, error => {
                this._alert('Lỗi', error.toString());
                this.setState({ sending: false });
            });
        });

    }

    render() {
        const { navigation } = this.props;
        const {
            btnSubmit,
            fontFace,
        } = Styles;
        if (typeof this.state.dataSource === "undefined")
            return <View />;

        return (
            <Container style={fontFace}>
                <TopBar navigation={navigation} title='Thông tin ngân hàng và đối soát' />
                <Content padder style={{ backgroundColor: '#eee' }}>
                    <Text style={editBankInfoStyles.sectionTitle}>Thông tin ngân hàng</Text>
                    <BankForm dataSource={this.state.dataSource.tai_khoan_ngan_hang}
                        onFormChange={(dataSource) => {
                            var bankInfo = dataSource;
                            if (this.state.dataSource.tai_khoan_ngan_hang !== undefined) {
                                bankInfo = Object.assign(this.state.dataSource.tai_khoan_ngan_hang, dataSource);
                            }
                            this.setState({
                                dataSource: Object.assign(this.state.dataSource, {
                                    tai_khoan_ngan_hang: bankInfo
                                })
                            });
                        }}
                    />
                    <Text style={editBankInfoStyles.sectionTitle}>Thông tin đối soát</Text>
                    <View style={{ alignItems: 'flex-start' }}>
                        <RadioGroup
                            radioButtons={this.state.data}
                            onPress={data => this.setState({ data })}
                        />
                    </View>
                    <View>
                        <Button
                            block
                            style={btnSubmit}
                            onPress={() => this._submitForm()}
                        >
                            {this.state.sending ? <ActivityIndicator size="small" color="#fff" /> : null}
                            <Text style={{ color: '#fff' }}>Lưu thay đổi</Text>
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }

}

const editBankInfoStyles = StyleSheet.create({
    sectionTitle: {
        color: Colors.blackColor,
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 10
    },
});
export default EditBankInfo;
