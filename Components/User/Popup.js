import React, { Component } from 'react';
import { View, Modal, Image, StyleSheet, AsyncStorage, Alert } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { Text, Icon } from 'native-base';
import Swiper from 'react-native-swiper';
import Styles from '../Assets/Styles';
import Colors from '../Assets/Colors';
import { getPopup } from '../../Data/userRepo';

class Popup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            popupList: '',
        };
    }
    async componentDidMount() {
        await this._checkPopup();
    }

    _checkPopup = async () => {
        const date = new Date();
        const currentDate = Number(date);
        const nextDate = date.setDate(date.getDate() + 7);
        const checkIsShow = await this._retrieveData();
        if (!checkIsShow) {
            await this._getPopup();
            this._storeData(nextDate);
            return;
        }
        if (currentDate > checkIsShow) {
            await this._getPopup();
            this._storeData(nextDate);
            return
        }
    }
    _getPopup = async () => {
        await getPopup(res => {
            if (res.success) {
                this.setState({ popupList: res.data, modalVisible: true });
            } else {
                Alert.alert(
                    'Lỗi',
                    res.message,
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
            }
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
        })
    }
    _setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    _storeData = async (endDay) => {
        try {
            await AsyncStorage.setItem('@PopupIsShow', JSON.stringify(endDay));
        } catch (error) {
        }
    };
    _setDate() {
        const date = new Date();
        const prevDate = date.setDate(date.getDate() - 8);
        this._storeData(prevDate);
    }
    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('@PopupIsShow');
            if (value !== null) {
                return value;
            } else {
                return false;
            }
        } catch (error) {
            // Error retrieving data
            return false;
        }
    };

    render() {
        const {
        } = Styles;
        const {
            imageBox,
            imageStyle,
        } = popupStyle;
        return (

            <View>
                <Modal
                    animationType="slide"
                    visible={this.state.modalVisible}
                    transparent
                    onRequestClose={() => { return null; }}
                >
                    <SafeAreaView style={{ flex: 1 }}>
                        <View style={{ flex: 1, padding: 10, backgroundColor: 'rgba(0, 0, 0, 0.5)' }}>
                            <View style={{ backgroundColor: Colors.subColor, padding: 5 }}>
                                <Text style={{ color: '#fff' }} onPress={() => this._setModalVisible(false)}>
                                    Đóng{' '}
                                    <Icon type='FontAwesome' name='times-circle' style={{ color: '#fff', fontSize: 16 }} />
                                </Text>
                            </View>
                            <View style={{ padding: 10, flex: 1, backgroundColor: '#fff' }}>
                                <View style={imageBox}>
                                    {this.state.popupList.length > 0 && this.state.popupList !== '' ? (
                                        <Swiper>
                                            {
                                                this.state.popupList.map((item) => {
                                                    return (
                                                        <View key={item.id}>
                                                            <Image
                                                                style={imageStyle}
                                                                source={{ uri: item.image }}
                                                            />
                                                        </View>
                                                    )
                                                })
                                            }
                                        </Swiper>
                                    ) : null}
                                </View>
                            </View>
                        </View>
                    </SafeAreaView>
                </Modal>
            </View>
        );
    }
}
const popupStyle = StyleSheet.create({
    imageBox: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignContent: 'center',
        width: '100%',
        flex: 1,
        backgroundColor: '#fff',
        overflow: 'hidden',
    },
    imageStyle: {
        width: '100%',
        resizeMode: 'contain',
        height: '100%',
    }
});
export default Popup;
