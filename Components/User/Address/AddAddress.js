import React, { Component } from 'react';
import { Alert } from 'react-native';
import {
    Container,
    Content,
    Picker,
    Form,
    Item,
    Input,
    Label,
    Button,
    Icon,
    Text,
} from 'native-base';
import Styles from '../../Assets/Styles';
import TopBar from '../../Shared/TopBar';
import { createCustomerBookmark, updateCustomerBookmark } from '../../../Data/userRepo';
import { Auth } from '../../../Networking/auth';
import { getProvinceList, getDistrictByProvinceID, getWardByDistrictID } from '../../../Data/locationRepo';
import Loading from '../../Shared/Loading';
import AddAddressStyles from '../Assets/AddAddressStyles';
import Colors from '../../Assets/Colors';

export default class AddAddress extends Component {

    constructor(props) {
        super(props);

        const dataSource = {
            ten_goinho: '',
            hoten_nguoinhan: '',
            so_dienthoai: '',
            quocgia: 'VN',
            tinh: '77',
            huyen: '',
            phuong: '',
            chitiet: '',
            id_taikhoan: '',
            loai_diachi: ''
        };
        const dataSource_ = this.props.navigation.getParam('dataSource', undefined);
        this.state = {
            title: '',
            provinceList: '',
            districtList: '',
            wardList: '',
            loading: false,
            dataSource: Object.assign(dataSource, dataSource_)
        };
    }
    _alert(title, content) {
        return (
            Alert.alert(
                title,
                content.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            )
        );
    }
    componentWillMount() {
        let sloai_diachi = this.props.navigation.getParam('loai_diachi', 'nguoinhan');
        let stitle = sloai_diachi == 'nguoinhan' ? 'Người nhận' : 'Người gửi';
        this.setState({
            title: stitle,
            btn_text: 'Lưu',
            dataSource: { ...this.state.dataSource, loai_diachi: sloai_diachi }
        });
    }

    componentDidMount = async () => {
        await getProvinceList(async (res) => {
            if (res.success) {
                this.setState({
                    provinceList: res.data
                });
            } else {
                this._alert('Lỗi', res.message);
            }
        }, error => {
            this._alert('Lỗi', error);
        });
        if (this.state.provinceList !== '' && typeof this.state.provinceList !== 'string') {
            await getDistrictByProvinceID('77', responseJson => {
                if (responseJson.success) {
                    this.setState({ districtList: responseJson.data });
                } else {
                    this._alert('Lỗi', responseJson.message);
                }
            }, e => {
                this._alert('Lỗi', e);
            });
        }
        if (this.state.districtList !== '' && typeof this.state.districtList !== 'string') {
            await getWardByDistrictID(this.state.districtList[0].maqh, responseJson => {
                if (responseJson.success) {
                    this.setState({ wardList: responseJson.data });
                } else {
                    this._alert('Lỗi', responseJson.message);
                }
            }, e => {
                this._alert('Lỗi', e);
            });
        }
        
        if (typeof this.state.dataSource !== 'undefined' 
                && this.state.dataSource.tinh !== '' 
                    && this.state.dataSource.huyen !== '' 
                        && this.state.dataSource.phuong !== '') {
            await getDistrictByProvinceID(this.state.dataSource.tinh, res => {
                if (res.success) {
                    this.setState({
                        districtList: res.data,
                        dataSource: Object.assign(this.state.dataSource, {
                            tinh: this.state.dataSource.tinh,
                            huyen: this.state.dataSource.huyen !== null ? this.state.dataSource.huyen : ''
                        }),
                    });
                }
            });
            await getWardByDistrictID(this.state.dataSource.huyen, res => {
                if (res.success) {
                    this.setState({
                        wardList: res.data,
                        dataSource: Object.assign(this.state.dataSource, {
                            huyen: this.state.dataSource.huyen,
                            phuong: this.state.dataSource.phuong !== null ? this.state.dataSource.phuong : ''
                        }),
                    });
                }
            });
        } else {
            this.setState({
                dataSource: Object.assign(this.state.dataSource, {
                    tinh: '77',
                    huyen: this.state.districtList[0].maqh,
                    phuong: this.state.wardList[0].xaid,
                })
            });
        }
    }

    onChangeInput(data) {
        this.setState({
            dataSource: data
        });
    }

    async onProvinceListChanged(province_id) {
        await getDistrictByProvinceID(province_id, res => {
            if (res.success) {
                this.setState({
                    dataSource: Object.assign(this.state.dataSource, {
                        tinh: province_id,
                        huyen: res.data[0].maqh
                    }),
                    districtList: res.data
                });
            } else {
                this._alert('Lỗi', res.message);
            }
        }, error => {
            this._alert('Lỗi', error);
        });
        await getWardByDistrictID(this.state.districtList[0].maqh, res => {
            if (res.success) {
                this.setState({
                    wardList: res.data,
                    dataSource: {
                        ...this.state.dataSource,
                        phuong: res.data[0].xaid,
                    }
                });
            } else {
                this._alert('Lỗi', res.message);
            }
        });
    }

    async onDistrictListChanged(district_id) {
        await getWardByDistrictID(district_id, res => {
            if (res.success) {
                this.setState({
                    wardList: res.data,
                    dataSource: {
                        ...this.state.dataSource,
                        huyen: district_id,
                        phuong: res.data[0].xaid,
                    }
                });
            } else {
                this._alert('Lỗi', res.message);
            }
        });
    }

    isOnEdit() {
        return typeof this.state.dataSource.id !== 'undefined';
    }

    async formSubmit() {
        if (this.state.loading) {
            this._alert('Lỗi', 'Bạn thao tác quá nhanh!');
            return;
        }
        if (this.state.dataSource.tinh == '') {
            Alert.alert(
                'Lỗi',
                'Vui lòng chọn Tỉnh/ Thành phố',
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
            return;
        }
        if (this.state.dataSource.huyen == '') {
            Alert.alert(
                'Lỗi',
                'Vui lòng chọn quận/ huyện',
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
            return;
        }
        // Block all input
        this.setState({ loading: true });
        if (this.isOnEdit()) {
            await updateCustomerBookmark(this.state.dataSource, res => {
                if (res.success) {
                    this.props.navigation.state.params.onGoBack();
                    Alert.alert(
                        'Thông báo',
                        'Cập nhật địa chỉ thành công!',
                        [
                            { text: 'OK' },
                        ],
                        { cancelable: false },
                    );
                    this.props.navigation.goBack();
                } else {
                    this._alert('Lỗi', res.message);
                }
            }, error => {
                this._alert('Lỗi', error);
            });
        } else {
            let user = await Auth.currentUser();
            await createCustomerBookmark(Object.assign(this.state.dataSource, { id_taikhoan: user.id }),
                res => {
                    if (res.success) {
                        this.props.navigation.state.params.onGoBack();
                        Alert.alert(
                            'Thông báo',
                            'Tạo địa chỉ thành công!',
                            [
                                { text: 'OK' },
                            ],
                            { cancelable: false },
                        );
                        this.props.navigation.goBack();
                    } else {
                        this._alert('Lỗi', res.message);
                    }
                }, error => {
                    this._alert('Lỗi', error);
                });
            this.setState({
                loading: false,
            });
        }
    }

    render_form() {
        let { title, loading } = this.state;
        let oldState = this.state.dataSource;
        let { ten_goinho, hoten_nguoinhan, so_dienthoai, quocgia, phuong, chitiet } = this.state.dataSource;

        let btn_text = loading ? 'Đang xử lý...' : 'Lưu';
        const {
            formItem,
            lableStyle,
        } = AddAddressStyles;
        const {
            input
        } = Styles;

        return (
            <Form>
                {/* <Button onPress={() => this._alert('a', 'a')}><Text>Test</Text></Button> */}
                <Item stackedLabel style={formItem}>
                    <Label style={lableStyle}>Tên gợi nhớ</Label>
                    <Input
                        autoCapitalize='words'
                        disabled={loading}
                        onChangeText={val => this.onChangeInput({
                            ...oldState,
                            ten_goinho: val
                        })}
                        value={ten_goinho}
                        style={input}
                    />
                </Item>
                <Item stackedLabel style={formItem}>
                    <Label style={lableStyle}>Họ tên {title}</Label>
                    <Input
                        autoCapitalize='words'
                        disabled={loading}
                        onChangeText={val => this.onChangeInput({
                            ...oldState,
                            hoten_nguoinhan: val
                        })}
                        value={hoten_nguoinhan}
                        style={input}
                    />
                </Item>
                <Item stackedLabel style={formItem}>
                    <Label style={lableStyle}>Số điện thoại</Label>
                    <Input
                        disabled={loading}
                        onChangeText={val => this.onChangeInput({
                            ...oldState,
                            so_dienthoai: val
                        })}
                        value={so_dienthoai}
                        style={input}
                        keyboardType='phone-pad'
                    />
                </Item>
                {typeof this.state.provinceList !== 'string' ? (
                    <Item picker style={formItem}>
                        <Label style={[lableStyle, { paddingTop: 10, width: 90 }]}>Tỉnh/ TP</Label>
                        <Picker
                            block disabled={loading}
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            iosHeader='Tỉnh/ TP'
                            style={[{ width: undefined }]}
                            placeholder="Chọn Tỉnh/ Thành phố"
                            placeholderStyle={{ color: Colors.blackColor }}
                            placeholderIconColor={Colors.blackColor}
                            selectedValue={this.state.dataSource.tinh}
                            onValueChange={val => this.onProvinceListChanged(val)}
                        >
                            {this.state.provinceList.map(item => {
                                return <Picker.Item label={item.name} key={item.matp} value={item.matp} />;
                            })}
                        </Picker>
                    </Item>
                ) : <Text>Không có dữ liệu Tỉnh/ Thành phố</Text>}
                {typeof this.state.districtList !== 'string' ? (
                    <Item picker style={formItem}>
                        <Label style={[lableStyle, { paddingTop: 10, width: 90 }]}>Quận/ huyện</Label>
                        <Picker block disabled={loading}
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            style={{ width: undefined }}
                            iosHeader='Quận/ Huyện'
                            placeholder="Chọn Quận/ Huyện"
                            placeholderStyle={{ color: Colors.blackColor }}
                            placeholderIconColor={Colors.blackColor}
                            selectedValue={this.state.dataSource.huyen}
                            onValueChange={val => this.onDistrictListChanged(val)}
                        >
                            {this.state.districtList.map(item => {
                                return <Picker.Item label={item.name} key={item.maqh} value={item.maqh} />;
                            })}
                        </Picker>
                    </Item>
                ) : <Text>Không có dữ liệu Quận/ Huyện</Text>}
                {typeof this.state.wardList !== 'string' ? (
                    <Item picker style={formItem}>
                        <Label style={[lableStyle, { paddingTop: 10, width: 90 }]}>Phường/ Xã</Label>
                        <Picker block disabled={loading}
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            style={{ width: undefined }}
                            iosHeader='Phường/ Xã'
                            placeholder="Chọn Phường/ Xã"
                            placeholderStyle={{ color: Colors.blackColor }}
                            placeholderIconColor={Colors.blackColor}
                            selectedValue={this.state.dataSource.phuong}
                            onValueChange={val => this.onChangeInput({ ...oldState, phuong: val })}
                        >
                            {this.state.wardList.map(item => {
                                return <Picker.Item label={item.name} key={item.xaid} value={item.xaid} />;
                            })}
                        </Picker>
                    </Item>
                ) : <Text>Không có dữ liệu Phường/ Xã</Text>}

                {/* <Item stackedLabel style={formItem}>
                    <Label style={lableStyle}>Phường</Label>
                    <Input
                        disabled={loading}
                        onChangeText={val => this.onChangeInput({ ...oldState, phuong: val })}
                        value={phuong}
                        style={input}
                    />
                </Item> */}
                <Item stackedLabel style={formItem}>
                    <Label style={lableStyle}>Địa chỉ/ Số nhà</Label>
                    <Input
                        disabled={loading}
                        onChangeText={val => this.onChangeInput({ ...oldState, chitiet: val })}
                        value={chitiet}
                        style={input}
                    />
                </Item>
                <Button
                    style={Styles.btnSubmit}
                    block
                    onPress={() => this.formSubmit()}
                >
                    <Text>{btn_text}</Text>
                </Button>
            </Form>
        );
    }

    render() {
        if (this.state.provinceList) {
            return (
                <Container style={Styles.fontFace}>
                    <TopBar title={this.isOnEdit() ? 'Cập nhập địa chỉ' : 'Thêm địa chỉ'} navigation={this.props.navigation} />
                    <Content style={Styles.content} padder>
                        {
                            this.state.provinceList ? this.render_form() : null
                        }
                    </Content>
                </Container>
            );
        } else {
            return <Loading />;
        }

    }
}
