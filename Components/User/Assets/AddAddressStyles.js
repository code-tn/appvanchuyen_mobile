import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    formItem: {
        marginLeft: 0,
        marginRight: 0,
        // backgroundColor: '#fff',
        marginTop: 0,
        marginBottom: 5,
        paddingBottom: 0,
        paddingRight: 0,
    },
    lableStyle: {
        fontSize: 14,
        paddingTop: 0,
        paddingBottom: 10
    },
    formInput: {
        height: 40,
        backgroundColor: '#fff',
        paddingLeft: 12,
        paddingRight: 12,
    }
});
