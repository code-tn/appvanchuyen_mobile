
import { StyleSheet } from 'react-native';
import Colors from '../../Assets/Colors';

export default StyleSheet.create({
    title: {
        color: Colors.blackColor,
        marginBottom: 10,
        marginTop: 10,
        fontWeight: '500',
    },
    listItem: {
        backgroundColor: '#fff',
        paddingHorizontal: 10,
        paddingVertical: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        fontSize: 13,
        fontWeight: '300',
        borderTopWidth: 1,
        borderTopColor: '#dad9d7',
        color: Colors.blackColor
    },
    listItemTitle: {
        flexGrow: 1,
        color: Colors.blackColor
    },
    listItemValue: {
        color: Colors.blackColor
    },
    listItemIcon: {
        fontSize: 14,
        fontWeight: 'normal',
    },
    fontMedium: {
        fontWeight: '500'
    },
    borderNone: {
        borderTopWidth: 0,
    }

});
