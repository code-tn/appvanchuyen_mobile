import { StyleSheet } from 'react-native';
import Colors from '../../Assets/Colors';

const {
    mainColor,
    blackColor
} = Colors;
const UserInfoStyles = StyleSheet.create({
    content: {
        backgroundColor: '#eee',
    },
    infoBox: {
        marginBottom: 15,
        backgroundColor: '#fff',
        color: 'red',
        borderWidth: 1,
        borderColor: '#d3d3d3',
        borderRadius: 5,
        overflow: 'hidden'
    },
    topBox: {
        paddingLeft: 15,
        backgroundColor: '#f6f7fa',
        position: 'relative'
    },
    btnEdit: {
        paddingHorizontal: 0,
        flexShrink: 0,
        marginLeft: 5,
        position: 'absolute',
        top: 3,
        right: 0,
        height: 'auto'
    },
    btnEditText: {
        fontSize: 15,
        color: mainColor,
        borderBottomWidth: 1,
        borderBottomColor: mainColor,
        flexGrow: 0,
    },
    btnEditIcon: {
        marginLeft: 5,
        color: mainColor,
        fontSize: 14,
    },
    boxTitle: {
        color: blackColor,
        fontSize: 13,
        fontWeight: '500',
        paddingVertical: 10,
        flexGrow: 1,
        paddingRight: 47
    },
    infoItem: {
        width: '100%',
        borderTopWidth: 1,
        borderTopColor: '#d3d3d3',
        paddingHorizontal: 12,
        paddingVertical: 6,
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    infoItemColumn: {
        paddingHorizontal: 12,
        paddingVertical: 6,
        flexDirection: 'column',
        borderTopWidth: 1,
        borderTopColor: '#d3d3d3',
    },
    infoText: {
        fontSize: 13,
        color: blackColor,
    },
    iconStyle: {
        width: 27,
        height: 27,
        resizeMode: 'contain',
        marginRight: 10,
    },
    flexSpaceBetween: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        borderTopWidth: 1,
        borderTopColor: '#d3d3d3',
        paddingHorizontal: 12,
        paddingVertical: 6,
    },
    buttonBox: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnLogout: {
        borderBottomWidth: 1,
        borderBottomColor: blackColor,
        height: 'auto',
        paddingVertical: 3,
        color: blackColor
    }
});
export default UserInfoStyles;
