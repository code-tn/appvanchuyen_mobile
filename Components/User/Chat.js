import React, { Component } from 'react';
import { View, StyleSheet, TextInput } from 'react-native';
import { Container, Footer, Content, Button, Icon } from 'native-base';
import TopBar from '../Shared/TopBar';
import Colors from '../Assets/Colors';

class Chat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: ''
        };
    }

    render() {
        const {
            chatBox,
            messageInput,
            btnSend,
        } = chatStyles;
        const {
            message
        } = this.state;
        const { navigation } = this.props;
        const titleTabBar = navigation.getParam('titleTabBar', 'Chat');
        
        return (
            <Container>
                <TopBar navigation={navigation} title={titleTabBar} />
                <Content />
                <Footer style={{ height: 'auto' }}>
                    <View style={chatBox}>
                        <TextInput
                            style={messageInput}
                            value={message}
                            onChange={text => this.setState({ message: text })} 
                        />
                        <Button style={btnSend}>
                            <Icon 
                                style={{ marginLeft: 0, marginRight: 0, fontSize: 17 }} 
                                type='FontAwesome' 
                                name='paper-plane' 
                            />
                        </Button>
                    </View>
                </Footer>
            </Container>
        );
    }
}
const chatStyles = StyleSheet.create({
    chatBox: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        padding: 10,
        width: '100%'
    },
    messageInput: {
        height: 40,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: '#b2b2b2',
        flexGrow: 1,
        paddingHorizontal: 15,
    },
    btnSend: {
        height: 40,
        width: 40,
        borderRadius: 20,
        marginLeft: 5,
        backgroundColor: Colors.subColor,
        padding: 0,
        alignItems: 'center',
        justifyContent: 'center',
    }
});
export default Chat;
