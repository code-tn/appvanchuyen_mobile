import React, { Component } from 'react';
import { View, TextInput } from 'react-native';
import Styles from '../../Assets/Styles';

class UserForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            shopName: '',
            phoneNumber: '',
            emailAddress: '',
            password: '',
            rePassword: '',
        };
    }

    render() {
        const {
            input,
        } = Styles;
        const {
            shopName,
            phoneNumber,
            emailAddress,
            password,
            rePassword,
        } = this.state;
        return (
            <View>
                <TextInput
                    value={shopName}
                    onChange={text => this.setState({ shopName: text })}
                    placeholder='Tên cửa hàng/shop'
                    style={input}
                />
                <TextInput
                    value={phoneNumber}
                    onChange={text => this.setState({ phoneNumber: text })}
                    placeholder='Điện thoại liên hệ'
                    style={input}
                />
                <TextInput
                    value={emailAddress}
                    onChange={text => this.setState({ emailAddress: text })}
                    placeholder='Email'
                    style={input}
                />
                <TextInput
                    secureTextEntry
                    value={password}
                    onChange={text => this.setState({ password: text })}
                    placeholder='Mật khẩu'
                    style={input}
                />
                <TextInput
                    secureTextEntry
                    value={rePassword}
                    onChange={text => this.setState({ rePassword: text })}
                    placeholder='Xác nhận mật khẩu'
                    style={input}
                />
            </View>
        );
    }
}

export default UserForm;
