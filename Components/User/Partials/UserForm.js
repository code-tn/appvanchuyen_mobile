import React, { Component } from 'react';
import { View, TextInput } from 'react-native';
import Styles from '../../Assets/Styles';

class UserForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            taikhoan: '',
            hoten: '',
            email: '',
            dienthoai: '',
            gioitinh: 0,
            so_cmnd: '',
            matkhau: '',
            rePassword: '',
            shopName: ''
        };
    }

    componentWillMount() {
        // Nếu có tham số parent ( prop) thì đăng ký state
        if (typeof this.props.parent !== 'undefined') {
            if (typeof this.props.dataSource !== 'undefined') {
                let dataSource = {
                    taikhoan: '',
                    hoten: '',
                    email: '',
                    dienthoai: '',
                    gioitinh: '0',
                    so_cmnd: '',
                    matkhau: '',
                    rePassword: '',
                    ten_cuahang: ''
                }
                this.props.parent.setState({
                    dataSource: Object.assign(this.props.parent.state.dataSource, dataSource)
                });
            }
        }
    }

    inputValue(name, defaultValue = '') {
        if (typeof this.props.parent === 'undefined') return defaultValue;
        if (typeof this.props.parent.state.dataSource === 'undefined') return defaultValue;
        let parent = this.props.parent;
        let dataSource = parent.state.dataSource;
        if (typeof dataSource[name] === 'undefined') return defaultValue;
        return dataSource[name];
    }

    _onInputChange(data) {
        if (typeof this.props.parent === 'undefined') return '';
        if (typeof this.props.parent.state.dataSource === 'undefined') return '';
        let parent = this.props.parent;
        let dataSource = parent.state.dataSource;
        let new_dataSource = Object.assign(dataSource, data);
        parent.setState({ dataSource: new_dataSource });
    }

    render() {
        const { input } = Styles;
        return (
            <View>
                <TextInput
                    autoCapitalize='none'
                    value={this.inputValue('taikhoan')}
                    onChangeText={(text) => this._onInputChange({ taikhoan: text })}
                    placeholder='Tên tài khoản'
                    style={input}
                />
                <TextInput
                    autoCapitalize='words'
                    value={this.inputValue('hoten')}
                    onChangeText={(text) => this._onInputChange({ hoten: text })}
                    placeholder='Họ tên'
                    style={input}
                />
                <TextInput
                    autoCapitalize='none'
                    value={this.inputValue('email')}
                    onChangeText={(text) => this._onInputChange({ email: text })}
                    placeholder='Email'
                    style={input}
                    keyboardType='email-address'
                />
                <TextInput
                    value={this.inputValue('dienthoai')}
                    onChangeText={(text) => this._onInputChange({ dienthoai: text })}
                    placeholder='Điện thoại liên hệ'
                    style={input}
                    keyboardType='phone-pad'
                />
                <TextInput
                    autoCapitalize='words'
                    value={this.inputValue('ten_cuahang')}
                    onChangeText={(text) => this._onInputChange({ ten_cuahang: text })}
                    placeholder='Tên cửa hàng/shop'
                    style={input}
                />
            </View>
        );
    }
}

export default UserForm;
