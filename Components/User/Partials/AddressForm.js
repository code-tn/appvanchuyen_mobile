import React, { Component } from 'react';
import {
    View,
    TextInput,
    Dimensions,
    StyleSheet,
} from 'react-native';
import { getProvinceList, getDistrictByProvinceID } from '../../../Data/locationRepo';
import {
    Text, 
    Icon,
    Item, 
    Label, 
    Picker
} from 'native-base';
import Styles from '../../Assets/Styles';
import Colors from '../../Assets/Colors';

const Screen = Dimensions.get('screen');

class AddressForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mainAddress: '',
            modalVisible: false,
            provinceList: '',
            districtList: '',
        };
    }
    componentDidMount() {
        this._getProviceList();
    }

    /*
    | ACTION METHODS -------------------------------------------------------------------------------
     */

    _onInputChange(data) {
        if (typeof this.props.parent === 'undefined') return '';
        if (typeof this.props.parent.state.dataSource === 'undefined') return '';
        let parent = this.props.parent;
        let dataSource = parent.state.dataSource;
        let new_dataSource = Object.assign(dataSource, data);
        parent.setState({ dataSource: new_dataSource });
    }

    inputValue(name, defaultValue = '') {
        if (typeof this.props.parent === 'undefined') return defaultValue;
        if (typeof this.props.parent.state.dataSource === 'undefined') return defaultValue;
        let parent = this.props.parent;
        let dataSource = parent.state.dataSource;
        if (typeof dataSource[name] === 'undefined') return defaultValue;
        return dataSource[name];
    }

    _setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    _chooseMainAddress(strProvince, strDistrict, strWard) {
        this.setState({
            mainAddress: strWard + ', ' + strDistrict + ', ' + strProvince
        });
        this._onInputChange({ tinh: strProvince });
        this._onInputChange({ huyen: strDistrict });
        this._onInputChange({ phuong: strWard });
        this._setModalVisible(false);
    }
    _getProviceList = async() => {
        await getProvinceList(res => {
            this.setState({ provinceList: res.data });
            this._onInputChange({tinh: this.state.provinceList[0].matp});
        });
        await getDistrictByProvinceID(this.state.provinceList[0].matp, res => {
            this.setState({
                districtList: res.data,
            });
            this._onInputChange({huyen: this.state.districtList[0].maqh});
        });
    }
    render() {
        const { input, btnCloseModal, modalDialog, modalBody, modalHeader, fontFace } = Styles;
        const {
            desc, mainLocation, collapseHeader, collapseHeaderBox, collapseHeaderText,
            collapseHeaderIcon, collapseBody, collapseBodyText } = addresFormStyles;
        return (
            <View style={fontFace}>
                <TextInput
                    value={this.inputValue('chitiet')}
                    onChangeText={text => this._onInputChange({ chitiet: text })}
                    placeholder='Số nhà, hẻm, ngõ, ngách, tòa nhà'
                    style={input}
                />
                <TextInput
                    value={this.inputValue('phuong')}
                    onChangeText={text => this._onInputChange({ phuong: text })}
                    placeholder='Phường/ Xã'
                    style={input}
                />
                <Item picker>
                    <Label>Tỉnh/ TP</Label>
                    {this.state.provinceList ?
                        (
                            <Picker block
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                style={{ width: undefined }}
                                iosHeader='Tỉnh/ TP'
                                placeholder="Chọn tỉnh/ thành phố"
                                placeholderStyle={{ color: Colors.blackColor }}
                                placeholderIconColor={Colors.blackColor}
                                selectedValue={this.inputValue('tinh')}
                                onValueChange={async (val) => {
                                    await getDistrictByProvinceID(val, res => {
                                        this.setState({
                                            districtList: res.data,
                                        });
                                        this._onInputChange({ tinh: val });
                                    });
                                }}
                            >
                                {this.state.provinceList.map(item => {
                                    return <Picker.Item label={item.name} key={item.matp} value={item.matp} />
                                })}
                            </Picker>
                        ) : <Text>Vui lòng chọn Tỉnh/ TP</Text>
                    }
                </Item>

                {
                    this.state.districtList ?
                        (
                            <Item picker style={{ marginBottom: 10 }}>
                                <Label>Quận/ huyện</Label>
                                <Picker block
                                    mode="dropdown"
                                    iosIcon={<Icon name="arrow-down" />}
                                    style={{ width: undefined }}
                                    iosHeader='Quận/ Huyện'
                                    placeholder="Chọn quận/ huyện"
                                    placeholderStyle={{ color: Colors.blackColor }}
                                    placeholderIconColor={Colors.blackColor}
                                    selectedValue={this.inputValue('huyen')}
                                    onValueChange={val => {
                                        this._onInputChange({ huyen: val });
                                    }}
                                >
                                    {this.state.districtList.map(item => {
                                        return <Picker.Item label={item.name} key={item.maqh} value={item.maqh} />
                                    })}
                                </Picker>
                            </Item>
                        ) : (
                            <View />
                        )
                }
            </View>
        );
    }
}

const addresFormStyles = StyleSheet.create({
    desc: {
        color: '#838383',
        fontSize: 13,
        lineHeight: 19,
        fontStyle: 'italic',
        paddingBottom: 10
    },
    mainLocation: {
        fontSize: 13,
        lineHeight: 19,
        backgroundColor: Colors.mainColor,
        color: '#fff',
        padding: 5,
    },
    collapseHeader: {
        fontSize: 14,
        lineHeight: 20,
        color: Colors.blackColor,
        fontWeight: '600',
        paddingVertical: 3,
        borderBottomWidth: 1,
        borderBottomColor: '#eee',
    },
    collapseHeaderBox: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        fontWeight: 'normal',
        color: '#333'
    },
    collapseHeaderIcon: {
        fontSize: 14,
        lineHeight: 20,
        fontWeight: 'normal',
        color: '#333'
    },
    collapseHeaderText: {
        fontSize: 14,
        lineHeight: 20,
        color: '#333',
        fontWeight: 'normal',
    },
    collapseBody: {
        paddingLeft: 8,
    },
    collapseBodyText: {
        fontSize: 13,
        lineHeight: 19,
        color: '#333',
        paddingVertical: 2,
        borderBottomWidth: 1,
        borderBottomColor: '#eee',
    }
});
export default AddressForm;
