import React, { Component } from 'react';
import { View, TextInput } from 'react-native';
import Styles from '../../Assets/Styles';

class NewPassword extends Component {
    _onInputChange(data) {
        if (typeof this.props.parent === 'undefined') return '';
        if (typeof this.props.parent.state.dataSource === 'undefined') return '';
        let parent = this.props.parent;
        let dataSource = parent.state.dataSource;
        let new_dataSource = Object.assign(dataSource, data);
        parent.setState({ dataSource: new_dataSource });
    }

    render() {
        const { input } = Styles;
        return (
            <View>
                <TextInput
                    secureTextEntry
                    onChangeText={(text) => this._onInputChange({matkhau: text})}
                    style={input}
                    placeholder='Mật khẩu'
                />
                <TextInput
                    secureTextEntry
                    onChangeText={(text) => this._onInputChange({ matkhau_confirmation: text })}
                    style={input}
                    placeholder='Xác nhận mật khẩu'
                />
            </View>
        );
    }
}
export default NewPassword;
