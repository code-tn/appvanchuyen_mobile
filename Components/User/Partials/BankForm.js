import React, {Component} from 'react';
import {View, TextInput} from 'react-native';
import Styles from '../../Assets/Styles';

class BankForm extends Component {

    constructor(props){
        super(props);
        const importDemo = typeof props.demo !== "undefined";
        let dataSource = {
            chutaikhoan: importDemo ? 'CAO TRIEU BINH' : '',
            nganhang: importDemo ? 'VIETCOMBANK' : '',
            so_taikhoan: importDemo ? '000426647232' : '',
            chinhanh: importDemo ? 'Quận 1' : '',
        }
        if(typeof props.dataSource !== "undefined"){
            dataSource.chutaikhoan = props.dataSource.chutaikhoan;
            dataSource.nganhang = props.dataSource.nganhang;
            dataSource.so_taikhoan = props.dataSource.so_taikhoan;
            dataSource.chinhanh = props.dataSource.chinhanh;
        }
        this.state = {
            dataSource: dataSource
        };
    }

    componentWillMount() {
        // Nếu có tham số parent ( prop) thì đăng ký state
        // if (typeof this.props.parent !== 'undefined') {
        //     if (typeof this.props.parent.state.dataSource !== 'undefined') {
        //         let importDemo = typeof this.props.demo !== 'undefined';
        //         let dataSource = {
        //             chutaikhoan: importDemo ? 'CAO TRIEU BINH' : '',
        //             nganhang: importDemo ? 'VIETCOMBANK' : '',
        //             so_taikhoan: importDemo ? '000426647232' : '',
        //             chinhanh: importDemo ? 'Quận 1' : '',
        //         }
        //         this.props.parent.setState({
        //             dataSource: Object.assign(this.props.parent.state.dataSource, dataSource)
        //         });
        //     }
        // }
    }

    _onInputChange(data) {
        let new_dataSource = Object.assign(this.state.dataSource, data);
        if(typeof this.props.onFormChange === "function"){
            this.props.onFormChange(new_dataSource);
        }
    }

    inputValue(name, defaultValue = '') {
        if (typeof this.props.dataSource === 'undefined') return defaultValue;
        let dataSource = this.props.dataSource;
        if (typeof dataSource[name] === 'undefined') return defaultValue;
        return dataSource[name];
    }

    render() {
        const {input} = Styles;
        return (
            <View>
                <TextInput
                    value={this.inputValue('chutaikhoan')}
                    onChangeText={text => this._onInputChange({chutaikhoan: text})}
                    placeholder='Chủ tài khoản ngân hàng'
                    style={input}
                />
                <TextInput
                    value={this.inputValue('so_taikhoan')}
                    onChangeText={text => this._onInputChange({so_taikhoan: text})}
                    placeholder='Số tài khoản'
                    style={input}
                    keyboardType='numeric'
                />
                <TextInput
                    value={this.inputValue('nganhang')}
                    onChangeText={text => this._onInputChange({nganhang: text})}
                    placeholder='Tên ngân hàng'
                    style={input}
                />
                <TextInput
                    value={this.inputValue('chinhanh')}
                    onChangeText={text => this._onInputChange({chinhanh: text})}
                    placeholder='Tên chi nhánh'
                    style={input}
                />
            </View>
        );
    }
}

export default BankForm;
