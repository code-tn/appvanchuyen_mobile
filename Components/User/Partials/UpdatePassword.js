import React, { Component } from 'react';
import { TextInput } from 'react-native';
import { Container, Content, Button, Text } from 'native-base';
import TopBar from '../../Shared/TopBar';
import Styles from '../../Assets/Styles';

class UpdatePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            oldpassword: '',
            password: '',
            repassword: '',
        };
    }
    _submit = () => {
        console.log('submit');
    }
    render() {
        const {
            content,
            input,
            btnSubmit
        } = Styles;
        return (
            <Container>
                <TopBar title='Mật khẩu mới' />
                <Content padder style={content}>
                    <TextInput
                        secureTextEntry
                        onChangeText={(text) => this.setState({ oldpassword: text })}
                        style={input}
                        placeholder='Mật khẩu cũ'
                    />
                    <TextInput
                        secureTextEntry
                        onChangeText={(text) => this.setState({ password: text })}
                        style={input}
                        placeholder='Mật khẩu'
                    />
                    <TextInput
                        secureTextEntry
                        onChangeText={(text) => this.setState({ repassword: text })}
                        style={input}
                        placeholder='Xác nhận mật khẩu'
                    />
                    <Button
                        block
                        style={btnSubmit}
                        onPress={() => this._submit()}
                    >
                        <Text>Xác nhận</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}
export default UpdatePassword;
