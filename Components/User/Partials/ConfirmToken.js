import React, { Component } from 'react';
import { TextInput } from 'react-native';
import { Container, Content, Button, Text } from 'native-base';
import TopBar from '../../Shared/TopBar';
import Styles from '../../Assets/Styles';

class ConfirmToken extends Component {
  constructor(props) {
    super(props);
    this.state = {
      strToken: ''
    };
  }
  _submit = () => {
    console.log('submit');
  }
  render() {
    const {
      content,
      input,
      btnSubmit
    } = Styles;
    return (
      <Container>
        <TopBar title='Xác nhận' />
        <Content padder style={content}>
          <TextInput
            onChangeText={(text) => this.setState({ strToken: text })} 
            style={input}
            placeholder='Nhập mã xác nhận'
          />
          <Button 
            block
            style={btnSubmit}
            onPress={() => this._submit()}
          >
            <Text>Xác nhận</Text>  
          </Button>
        </Content>
      </Container>
    );
  }
}
export default ConfirmToken;
