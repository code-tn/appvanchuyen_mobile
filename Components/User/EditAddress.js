import React, { Component } from 'react';
import { Alert, ListView, TouchableOpacity } from 'react-native';
import { Container, Content, Button, Icon, List, ListItem, Text, Fab } from 'native-base';
import TopBar from '../Shared/TopBar';
import Styles from '../Assets/Styles';

import { getCustomerBookmark, deleteCustomerBookmark } from '../../Data/userRepo';
import { Auth } from '../../Networking/auth';

class EditAddress extends Component {

    constructor(props) {
        super(props);
        this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            active: true,
            user: '',
            loading: true,
            listViewData: '',
            responseJson: ''
        };
    }

    loadData() {
        let user_id = this.state.user.id;
        setTimeout(async () => {
            await getCustomerBookmark({ id_khachhang: user_id }, (rJson) => {

                this.setState({
                    responseJson: rJson,
                    listViewData: rJson.data,
                    loading: false
                });
            }, (error) => {
                Alert.alert(
                    'Lỗi',
                    error.toString(),
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
            });
        }, 1);
    }

    async componentDidMount() {
        let user = await Auth.currentUser();
        this.setState({ ...this.state, user: user });
        this.loadData();
    }

    _onDeleteRow(id) {
        deleteCustomerBookmark(id, (res) => {
            if (res.success) {
                this.loadData();
            } else {
                Alert.alert(
                    'Lỗi',
                    res.message,
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
            }
        });

        // rowMap[`${secId}${rowId}`].props.closeRow();
        // const newData = [...this.state.listViewData];
        // newData.splice(rowId, 1);
        // this.setState({listViewData: newData});
    }

    render_list_item(item) {
        return (
            <ListItem>
                <TouchableOpacity onPress={() => {
                    alert('Tên: '.item.hoten_nguoinhan);
                    
                }}>
                    <Text>{item.ten_goinho + ' - ' + item.so_dienthoai} </Text>
                </TouchableOpacity>
            </ListItem>
        )
    }

    render_list() {
        if (this.state.loading)
            return <Text>Đang tải danh sách địa chỉ...</Text>

        if (typeof this.state.listViewData !== 'object')
            return <Text>Không có dữ liệu!</Text>

        return (
            <List
                rightOpenValue={-75}
                dataSource={this.ds.cloneWithRows(this.state.listViewData)}
                renderRow={item => this.render_list_item(item)}
                renderRightHiddenRow={(data) =>
                    <Button full danger onPress={_ => this._onDeleteRow(data.id)}>
                        <Icon active name="trash" />
                    </Button>}
            />
        )
    }

    render() {
        return (<Text></Text>);
        const { navigation } = this.props;
        const { fontFace } = Styles;

        return (
            <Container style={fontFace}>
                <TopBar navigation={navigation} title='Địa chỉ lấy hàng' />
                <Content padder style={{ backgroundColor: '#eee' }}>
                    {this.render_list()}
                </Content>

                <Fab
                    active={this.state.active}
                    direction="up"
                    containerStyle={{}}
                    style={{ backgroundColor: '#5067FF' }}
                    position="bottomRight"
                    onPress={() => {
                        this.props.navigation.navigate('AddAdderssScreen', {
                            onGoBack: () => this.loadData(),
                            loai_diachi: 'nguoigui'
                        });
                    }}>
                    <Icon name="add" />

                </Fab>
            </Container>
        );
    }
}

export default EditAddress;
