import React, { Component } from 'react';
import { TextInput, ActivityIndicator, AsyncStorage, BackHandler, Alert, Platform, StatusBar } from 'react-native';
import { Button, Container, Content, Text } from 'native-base';
import Styles from '../../Assets/Styles';
import TopBarOnlyTitle from '../../Shared/TopBarOnlyTitle';
import { ScreenManager } from '../../../System/Screen';
import Colors from '../../Assets/Colors';
import { requestForgotPassword, changeForgotPassword } from '../../../Data/userRepo';


class ConfirmResetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            matkhau: '',
            matkhau_confirmation: '',
            code: '',
            sending: false,
            resend: false,
            hasError: false,
            errorString: ''
        };
    }
    async componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this._handleBackButton);
        try {
            const currentEmail = await AsyncStorage.getItem('@ResetEmail');
            if (currentEmail !== null) {
                this.setState({ email: currentEmail });
                await ScreenManager.setInitScreen('ConfirmResetPasswordScreen');
            }
        } catch (error) {
            // Error retrieving data
            Alert.alert(
                'Lỗi',
                'Đã có lỗi xảy ra, vui lòng thử lại sau!',
                [
                    { text: 'OK', onPress: () => this.props.navigation.navigate('LoginScreen') },
                ],
                { cancelable: false },
            );
        }
    }
    _storeData = async () => {
        try {
            await AsyncStorage.setItem('@ResetEmail', '');
        } catch (error) {
            // Error saving data
        }
    };
    _handleBackButton() {
        return true;
    }
    _resend = async () => {
        this.setState({ resend: true });
        const data = {
            email: this.state.email
        };
        await requestForgotPassword(data, (res) => {
            if (res.success) {
                const stringAlert = 'Đã gửi lại mã xác thực. Vui lòng kiểm tra email ' + this.state.email + ' để nhận mã xác thực lấy lại mật khẩu.';
                Alert.alert(
                    'Thông báo',
                    stringAlert,
                    [
                        {
                            text: 'OK', onPress: () => {
                                this.setState({ resend: false });
                            }
                        },
                    ],
                    { cancelable: false }
                );
            } else {
                Alert.alert(
                    'Lỗi',
                    res.errors.email[0].toString(),
                    [
                        { text: 'OK', onPress: () => this.setState({ resend: false }) },
                    ],
                    { cancelable: false }
                );
            }
        }, (e) => {
            this.setState({ resend: false });
        });
    }
    _alert(title, content) {
        return (
            Alert.alert(
                title,
                content.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            )
        );
    }
    _submit = async () => {
        // Validation 
        if (this.state.code === '') {
            this.setState({ hasError: true });
            return;
        }
        if (this.state.matkhau === '') {
            this.setState({ hasError: true });
            return;
        }
        if (this.state.matkhau.length < 8) {
            this._alert('Lỗi', 'Mật khẩu phải bao gồm 8 ký tự');
            this.setState({ hasError: true });
            return;
        }
        if (this.state.matkhau_confirmation === '') {
            this.setState({ hasError: true });
            return;
        }
        if (this.state.matkhau_confirmation !== '' && this.state.matkhau !== this.state.matkhau_confirmation) {
            this.setState({ hasError: true, errorString: 'Nhập lại mật khẩu không trùng khớp' });
            return;
        }

        this.setState({ sending: true });

        const data = {
            email: this.state.email,
            matkhau: this.state.matkhau,
            matkhau_confirmation: this.state.matkhau_confirmation,
            code: this.state.code
        }
        await changeForgotPassword(data, (res) => {
            if (res.success) {
                Alert.alert(
                    'Thông báo',
                    'Đã cập nhật mật khẩu thành công!',
                    [
                        {
                            text: 'OK', onPress: async () => {
                                this.setState({ sending: false });
                                await this._storeData();
                                await ScreenManager.setInitScreen('LoginScreen');
                                this.props.navigation.navigate('LoginScreen');
                            }
                        },
                    ],
                    { cancelable: false }
                );
            } else {
                Alert.alert(
                    'Lỗi',
                    res.message,
                    [
                        {
                            text: 'OK', onPress: () => {
                                this.setState({ sending: false });
                            }
                        },
                    ],
                    { cancelable: false }
                );
            }
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    {
                        text: 'OK', onPress: () => {
                            this.setState({ sending: false });
                        }
                    },
                ],
                { cancelable: false }
            );
        });
    }
    render() {
        const { input, error, errorAlert } = Styles;
        const {
            code,
            matkhau,
            matkhau_confirmation,
            hasError,
            errorString,
            sending,
            resend } = this.state;
        return (
            <Container style={Styles.fontFace}>
                <View 
                    style={{                 
                        height: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight,
                        backgroundColor: Colors.activeColor
                    }}
                />
                <TopBarOnlyTitle title='Thay đổi mật khẩu' />
                <Content style={Styles.content} padder>
                    <Text style={{ marginBottom: 10, fontSize: 14 }}>
                        Mã xác thực đã được gửi đến email {this.state.email} của bạn.
                        Kiểm tra email và sử dụng mã xác thực để đặt lại mật khẩu.
                    </Text>
                    <Text style={{ marginBottom: 10, fontSize: 14 }}>
                        Bạn chưa nhận được email?
                        <Text
                            onPress={() => this._resend()}
                            style={{ marginBottom: 10, fontSize: 14, color: Colors.subColor }}
                        >
                            {resend ? ' Đang gửi...' : ' Gửi lại!'}
                        </Text>
                    </Text>
                    <TextInput
                        onChangeText={(text) => this.setState({ code: text })}
                        style={[input, { ...hasError && code === '' ? error : null }]}
                        placeholder='Mã xác thực'
                    />
                    <TextInput
                        secureTextEntry
                        onChangeText={(text) => this.setState({ matkhau: text })}
                        style={input}
                        placeholder='Mật khẩu mới'
                        style={[input, { ...hasError && matkhau === '' || matkhau !== '' && matkhau.length < 8 ? error : null }]}
                    />
                    {hasError && matkhau !== '' && matkhau.length < 8 ? (
                        <Text style={errorAlert}>Mật khẩu phải có ít nhất 8 ký tự</Text>
                    ) : null}
                    <TextInput
                        secureTextEntry
                        onChangeText={(text) => this.setState({ matkhau_confirmation: text })}
                        style={[input, { ...hasError && matkhau_confirmation === '' || matkhau_confirmation !== matkhau ? error : null }]}
                        placeholder='Xác nhận mật khẩu mới'
                    />
                    {error && matkhau_confirmation !== matkhau ? (
                        <Text style={errorAlert}>{errorString}</Text>
                    ) : null}
                    <Button
                        style={Styles.btnSubmit}
                        block
                        onPress={() => this._submit()}
                    >
                        {sending ? <ActivityIndicator size='small' color='#fff' /> : null}
                        <Text>Xác nhận</Text>
                    </Button>
                </Content>
            </Container>

        );
    }
}
export default ConfirmResetPassword;
