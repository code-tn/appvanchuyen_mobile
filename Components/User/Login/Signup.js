import React, { Component } from 'react';
import { View, SafeAreaView, StatusBar, Alert, ActivityIndicator, Platform } from 'react-native';
import { Container, Form, Content, Text, Button } from 'native-base';
import TopBar from '../../Shared/TopBar';
import Styles from '../../Assets/Styles';
import Colors from '../../Assets/Colors';
import SignupStyles from './Assets/SignupStyles';
import UserForm from '../Partials/UserForm';
import { ApiConfig } from '../../../config/api';
import ComPassword from '../Partials/NewPassword';

export default class Signup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            dataSource: {}
        };
    }
    
    componentWillMount() {
        this.setState({
            dataSource: {
                // shop infomation
                // mainAddress: '',
                // tinh: '',
                // huyen: '',
                // phuong: '',
                ten_cuahang: '',
                // user infomation
                taikhoan: '',
                hoten: '',
                email: '',
                dienthoai: '',
                // gioitinh: 0,
                // so_cmnd: '',
                // chitiet: '',
                // security infomation
                matkhau: '',
                matkhau_confirmation: '',
                // bank infomation
                // chutaikhoan: '',
                // so_taikhoan: '',
                // nganhang: '',
                // chinhanh: ''
            }
        });
    }
    waringNotice(message) {
        return (
            Alert.alert(
                'Lỗi',
                message,
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            )
        );
    }
    async _onSubmitForm() {
        
        const re = /^\w+$/;
        if (this.state.dataSource.taikhoan === '') {
            this.waringNotice('Bạn chưa nhập tên tài khoản');
            return;
        }
        if(!re.test(this.state.dataSource.taikhoan)) {
            this.waringNotice('Tên người dùng chỉ được bao gồm chữ cái, số và dấu gạch dưới!');
            return;
        }
        if (this.state.dataSource.hoten === '') {
            this.waringNotice('Bạn chưa nhập tên người dùng');
            return;
        }
        if (this.state.dataSource.email === '') {
            this.waringNotice('Bạn chưa nhập tài khoản email');
            return;
        }
        if (this.state.dataSource.dienthoai === '') {
            this.waringNotice('Bạn chưa nhập số điện thoại');
            return;
        }
        if (this.state.dataSource.ten_cuahang === '') {
            this.waringNotice('Bạn chưa nhập tên cửa hàng');
            return;
        }
        if (this.state.dataSource.matkhau === '') {
            this.waringNotice('Bạn chưa nhập mật khẩu');
            return;
        }
        if (this.state.dataSource.matkhau_confirmation === '') {
            this.waringNotice('Bạn chưa nhập xác nhận mật khẩu');
            return;
        }
        
        this.setState({ isLoading: true });
        await fetch(ApiConfig.api_url + 'api/v1/signup', {
            method: 'POST', 
            headers: ApiConfig.xhr_header,
            body: JSON.stringify(this.state.dataSource)
        })
            .then((responseJson) => responseJson.json())
            .then((res) => {
                if (!res.success) {
                    Alert.alert(
                        'Lỗi',
                        res.message,
                        [
                            { text: 'OK' },
                        ],
                        { cancelable: false },
                    );
                } else {
                    this.props.navigation.navigate('NoticeScreen', {
                        msg: 'Đăng ký thành công.',
                        callback: () => {
                            this.props.navigation.navigate('LoginScreen');
                        }
                    });
                }
            }).catch((e) => {
                Alert.alert(
                    'Lỗi',
                    e.toString(),
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
                return;
            });
        this.setState({ isLoading: false });
    }


    render() {
        const { navigation } = this.props;
        const {
            sectionTitle,
        } = SignupStyles;
        const {
            btnSubmit,
            fontFace,
            content,
        } = Styles;
        return (
            <Container style={fontFace}>
                <TopBar navigation={navigation} title='Đăng ký dịch vụ' />
                <SafeAreaView style={{ flex: 1 }}>
                    <Content padder style={content}>
                        <Form>
                            <Text style={sectionTitle}>Thông tin cửa hàng</Text>
                            <UserForm parent={this} />
                            {/* <AddressForm parent={this} /> */}
                            <Text style={sectionTitle}>Bảo mật</Text>
                            <ComPassword parent={this} />

                            {/*
                            <Text style={sectionTitle}> Thông tin đối soát chuyển khoản </Text> 
                            <BankForm dataSource={{
                                chutaikhoan: dataSource.chutaikhoan,
                                nganhang: dataSource.nganhang,
                                so_taikhoan: dataSource.so_taikhoan,
                                chinhanh: dataSource.chinhanh
                            }}
                                onFormChange={(dataSource) => {
                                    this.setState({
                                        dataSource: Object.assign(this.state.dataSource, dataSource)
                                    });
                                }}
                            /> */}
                        </Form>
                    </Content>
                    <View style={{ padding: 10, backgroundColor: '#eee' }}>
                        <Button block style={btnSubmit} onPress={() => this._onSubmitForm()}>
                            {this.state.isLoading ? <ActivityIndicator size='small' color='#fff' /> : null}
                            <Text>Đăng ký ngay</Text>
                        </Button>
                    </View>
                </SafeAreaView>
            </Container>
            
        );
    }
}
