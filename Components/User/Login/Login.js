import React, { Component } from 'react';
import {
    View,
    Image,
    ImageBackground,
    ActivityIndicator,
    SafeAreaView,
    StatusBar,
    Platform,
    Alert,
    AsyncStorage
} from 'react-native';
import { withNavigationFocus } from 'react-navigation';
import { Container, Content, Form, Input, Button, Text, Footer } from 'native-base';
import mainBg from './Assets/Images/bg.jpg';
import logo from './Assets/Images/logo.png';
import LoginStyles from './Assets/LoginStyles';
import Colors from '../../Assets/Colors';
import { Auth } from '../../../Networking/auth';
import Loading from '../../Shared/Loading';
import { ScreenManager } from '../../../System/Screen';
import FbLogin from './FbLogin';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            token: '',
            input: {
                btn_login: 'Đăng nhập'
            },
            isLoading: false,
            loggedIn: false,
            isSubmit: false,
        };
    }


    async componentWillMount() {
        await ScreenManager.setInitScreen('LoginScreen');
        let remember = await Auth.currentUser();
        if (remember !== null) {
            this.setState({ loggedIn: true });
        }
    }
    async _onLogin() {
        if (this.state.username.length === 0 || this.state.password.length === 0) {
            Alert.alert(
                'Lỗi',
                'Vui lòng điền đầy đủ thông tin',
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
            return;
        }
        this.setState({ input: { btn_login: 'Đang xử lý...' } });
        this.setState({ isSubmit: true });

        Auth.login(this.state.username, this.state.password, (d) => {
            this._onLoginCallback(d);
            this.setState({ isSubmit: false });
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
            this.setState({ isSubmit: false });
        });
        this.setState({ input: { btn_login: 'Đăng nhập' } });
    }

    async _onLoginCallback(responseJson) {
        if (responseJson.success) {
            // Async Storage - User
            if (responseJson.data.trangthai === 'chuakichhoat') {
                Alert.alert(
                    'Thông báo',
                    'Bạn chưa xác thực email để kích hoạt tài khoản',
                    [
                        {
                            text: 'Xác thực email', onPress: () => this.props.navigation.navigate('ConfirmEmailScreen', {
                                id_khachhang: responseJson.data.id,
                                email_khachhang: responseJson.data.email
                            })
                        },
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
                return;
            }
            await AsyncStorage.setItem('@User', JSON.stringify(responseJson.data));
            this.props.navigation.navigate('StatisticsScreen');
        } else {
            Alert.alert(
                'Lỗi',
                responseJson.message,
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
        }
    }
    showLoading(state = true) {
        this.setState({ isLoading: state });
    }
    render() {
        if (this.state.loggedIn) {
            this.props.navigation.navigate('StatisticsScreen');
            return (<View />);
        }

        if (this.state.isLoading) {
            return (<Loading />);
        }
        const {
            container,
            mainContent,
            formBox,
            inputStyle,
            inputPassword,
            bottomForm,
            btnForget,
            btnSignup,
            btnSubmit,
            btnText,
            formGroup,
            imgLogo
        } = LoginStyles;
        return (
            <Container style={container}>
                <StatusBar barStyle="dark-content" backgroundColor='#fff' animated />
                <ImageBackground
                    source={mainBg}
                    style={{ flex: 1 }}
                >
                    <SafeAreaView style={{ flex: 1 }}>
                        <Content style={mainContent}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <Image
                                    style={imgLogo}
                                    source={logo}
                                />
                            </View>
                            <Form style={formBox}>
                                <FbLogin navigation={this.props.navigation} />
                                <Input
                                    autoCapitalize='none'
                                    onChangeText={(username) => this.setState({ username })}
                                    style={inputStyle}
                                    placeholder='Nhập Email...'
                                    value={this.state.username}
                                    keyboardType='email-address'
                                />
                                <View style={formGroup}>
                                    <Input
                                        autoCapitalize='none'
                                        onChangeText={(password) => this.setState({ password })}
                                        secureTextEntry
                                        style={inputPassword}
                                        placeholder='Mật khẩu'
                                        value={this.state.password}
                                    />
                                    <Text
                                        style={btnForget}
                                        onPress={() => this.props.navigation.navigate('ResetScreen')}
                                    >
                                        Quên mật khẩu?
                                    </Text>
                                </View>

                                <View style={bottomForm}>
                                    <Button
                                        success style={btnSubmit}
                                        onPress={() => {
                                            this._onLogin();
                                        }}
                                    >
                                        {this.state.isSubmit ? <ActivityIndicator size='small' color='#fff' /> : null}
                                        <Text style={btnText}> {this.state.input.btn_login} </Text>
                                    </Button>

                                    <Button
                                        danger
                                        style={btnSignup}
                                        onPress={() => this.props.navigation.navigate('SignupScreen')}
                                    >
                                        <Text style={btnText}>Đăng ký tài khoản</Text>
                                    </Button>
                                </View>
                            </Form>
                        </Content>
                        <Footer
                            transparent
                            style={{
                                backgroundColor: 'transparent',
                                elevation: 0,
                                height: 26,
                                borderTopWidth: 0,
                            }}
                        >
                            <View
                                style={{
                                    paddingHorizontal: 10,
                                    justifyContent: 'center',
                                    flexDirection: 'row',
                                    alignItems: 'flex-end',
                                    paddingBottom: 8
                                }}
                            >
                                <Text
                                    style={{
                                        color: Colors.blackColor,
                                        borderBottomWidth: 1,
                                        borderBottomColor: Colors.blackColor,
                                        fontSize: 14,
                                    }}
                                    onPress={() => this.props.navigation.navigate('StaffLoginScreen')}
                                >
                                    Đăng nhập tài khoản NV?
                            </Text>
                            </View>
                        </Footer>
                    </SafeAreaView>
                </ImageBackground>
            </Container>
        );
    }
}
export default withNavigationFocus(Login);