import React, { Component } from 'react';
import { View, AsyncStorage, Alert } from 'react-native';
import { Button, Text, Icon } from 'native-base';
import { signupFacebook } from '../../../Data/userRepo';
import { Auth } from '../../../Networking/auth';
//fblogin
import {
    LoginManager,
    GraphRequest,
    GraphRequestManager,
} from "react-native-fbsdk";

class FbLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataFB: ''
        };
    }
    _storeData = async () => {
        try {
            await AsyncStorage.setItem('@LoginFacebook', 'Login by facebook');
        } catch (error) {
            // Error saving data
        }
    };
    _getInfoFBLogin() {
        return new Promise((resolve, reject) => {
            const infoRequest = new GraphRequest(
                '/me?fields=id,name,email',
                null,
                (error, result) => {
                    if (error) {
                        Alert.alert(
                            'Lỗi',
                            'Error fetching data: ' + error.toString(),
                            [
                                { text: 'OK' },
                            ],
                            { cancelable: false },
                        );
                        reject(error);
                    } else {
                        resolve(result);
                    }
                },
            );
            new GraphRequestManager().addRequest(infoRequest).start();
        });
    }
    async _checkLogin() {
        const info = await this._getInfoFBLogin();
        if (typeof info !== 'undefined' && info.email === undefined) {
            Alert.alert(
                'Lỗi',
                'Không thể truy cập địa chỉ email từ tài khoản Facebook này.\n' +
                'Ứng dụng cần lưu trữ địa chỉ email của bạn.\n' +
                'Kiểm tra lại thông tin tài khoản Facebook của bạn để đảm bảo có thể sử dụng tính năng này!',
                [
                    { text: 'OK', onPress: () => { LoginManager.logOut(); } },
                ],
                { cancelable: false },
            );
            return;
        }
        let data = null;
        if (typeof info !== 'undefined') {
            data = {
                hoten: info.name,
                email: info.email,
                id_fb: info.id,
            };
        }
        await Auth.getUserInfoFB(data.id_fb, async (res) => {
            if (res.success) {
                await this._storeData();
                await AsyncStorage.setItem('@User', JSON.stringify(res.data));
                this.props.navigation.navigate('StatisticsScreen');
            } else {
                await signupFacebook(data, async (res) => {
                    if (res.success) {
                        await Auth.getUserInfoFB(data.id_fb, async (res) => {
                            if (res.success) {
                                await this._storeData();
                                await AsyncStorage.setItem('@User', JSON.stringify(res.data));
                                this.props.navigation.navigate('StatisticsScreen');
                            } else {
                                Alert.alert(
                                    'Lỗi',
                                    res.message,
                                    [
                                        { text: 'OK' },
                                    ],
                                    { cancelable: false },
                                );
                                return;
                            }
                        }, (e) => {
                            Alert.alert(
                                'Lỗi',
                                e.toString(),
                                [
                                    { text: 'OK' },
                                ],
                                { cancelable: false },
                            );
                        });
                    } else {
                        Alert.alert(
                            'Lỗi',
                            res.message,
                            [
                                { text: 'OK' },
                            ],
                            { cancelable: false },
                        );
                    }
                }, (e) => {
                    Alert.alert(
                        'Lỗi',
                        e.toString(),
                        [
                            { text: 'OK' },
                        ],
                        { cancelable: false },
                    );
                });
            }
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
        });
    }
    _fbLogin() {
        LoginManager.logInWithReadPermissions(["public_profile", "email"]).then(
            (result) => {
                if (result.isCancelled) {
                    console.log("Login cancelled");
                } else {
                    // login facebook success
                    // check account info
                    this._checkLogin();
                }
            },
            (error) => {
                console.log("Login fail with error: " + error);
            }
        );
    }
    render() {
        return (
            <View style={{ marginBottom: 20 }}>
                <Button
                    iconLeft
                    block
                    style={{ height: 'auto', backgroundColor: '#4f64a7' }}
                    onPress={() => this._fbLogin()}
                >
                    <Icon type='FontAwesome' name='facebook-official' style={{ marginLeft: 12 }} />
                    <Text>Đăng nhập bằng Facebook</Text>
                </Button>
            </View>
        );
    }
}

export default FbLogin;
