import { StyleSheet, Dimensions } from 'react-native';
import Colors from '../../../Assets/Colors';

const { height, width } = Dimensions.get('window');
const LoginStyles = StyleSheet.create({
    container: {
        fontFamily: 'Helvetica Neue'
    },
    inputStyle: {
        backgroundColor: '#fff',
        color: Colors.blackColor,
        marginBottom: 15,
        height: 40,
        paddingLeft: 15,
        paddingRight: 15,
        fontSize: 13,
        borderRadius: 5,
    },
    inputPassword: {
        backgroundColor: '#fff',
        color: Colors.blackColor,
        marginBottom: 10,
        height: 40,
        paddingLeft: 15,
        paddingRight: 120,
        fontSize: 13,
        borderRadius: 5,
    },
    btnForget: {
        color: Colors.blackColor,
        padding: 0,
        position: 'absolute',
        top: 9,
        right: 10,
        fontSize: 13,
    },
    mainContent: {
        paddingTop: 10,
    },
    formBox: {
        padding: 10,
    },
    bottomForm: {
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    btnSubmit: {
        borderRadius: 5,
        // height: "auto",
        backgroundColor: Colors.subColor,
    },
    btnSignup: {
        backgroundColor: '#f84a39',
        // height: "auto",
        borderRadius: 5,
    },
    btnText: {
        // fontSize: 13, 
        marginLeft: 0, 
        paddingLeft: 10, 
        paddingRight: 10,
        paddingVertical: 2,
    },
    imgLogo: {
        width: width * 0.85,
        height: height / 5,
        resizeMode: 'contain',
    }
});

export default LoginStyles;
