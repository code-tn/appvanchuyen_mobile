import { StyleSheet } from 'react-native';
import Colors from '../../../Assets/Colors';

const ResetPasswordStyles = StyleSheet.create({
    content: {
        backgroundColor: '#eee',
    },
    title: {
        fontWeight: 'bold',
        color: Colors.blackColor,
        textAlign: 'center',
        marginBottom: 10,
    },
    buttonBox: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    description: {
        fontSize: 14,
        marginBottom: 5
    },
    btnSubmit: {
        backgroundColor: Colors.mainColor,
    }
});

export default ResetPasswordStyles;
