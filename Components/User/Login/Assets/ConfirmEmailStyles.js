import { StyleSheet } from 'react-native';
import Colors from '../../../Assets/Colors';

const ConfirmEmailStyle = StyleSheet.create({
    content: {
        flex: 1,
        padding: 10,
        backgroundColor: '#eee'
    },
    formBox: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#fff',
        padding: 10, 
        // flexWrap: 'nowrap',
    },
    formBoxLeft: {
        marginTop: 5,
        flexGrow: 1,
    },
    wrapper: {
        paddingRight: 5,
        flexDirection: 'row',
    },
    btnConfirm: {
        marginBottom: 10,
        height: 40,
        paddingLeft: 2,
        paddingRight: 2,
        fontSize: 14,
        borderRadius: 3,
        marginLeft: 3,
        flexShrink: 0,
    },
    btnReSend: {
        backgroundColor: '#dad9d7',
        marginBottom: 10,
        height: 40,
        paddingLeft: 2,
        paddingRight: 2,
        fontSize: 14,
        borderRadius: 3,
        marginTop: 4
    },
    heading: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: Colors.mainColor,
        padding: 10,
    },
    headingTitle: {
        color: '#fff',
        fontSize: 15
    },
    headingButton: {
        color: '#fff',
        borderBottomWidth: 1,
        borderBottomColor: '#fff'
    },
    desc: {
        fontSize: 14,
        marginBottom: 10,
        textAlign: 'center'
    }
});

export default ConfirmEmailStyle;
