import { StyleSheet, Dimensions } from 'react-native';
import Colors from '../../../Assets/Colors';

const screen = Dimensions.get('window');
const SignupStyles = StyleSheet.create({
    content: {
        backgroundColor: '#eee',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 10,
        fontFamily: 'Helvetica Neue'
    },
    sectionTitle: {
        color: Colors.blackColor,
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 10
    },
    modalAddress: {
        justifyContent: 'center',
        width: screen.width - 50,
    },
    btnClose: {
        fontSize: 14,
        color: '#fff'
    },
    modalHeader: {
        padding: 10,
        backgroundColor: Colors.mainColor,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    openModal: {
        backgroundColor: '#fff',
        color: Colors.blackColor,
        marginBottom: 10,
        height: 40,
        paddingLeft: 12,
        paddingRight: 12,
        fontSize: 14,
        borderRadius: 3,
        lineHeight: 40,
    }
});

export default SignupStyles;
