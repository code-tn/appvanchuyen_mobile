import React, { Component } from 'react';
import { View, Alert, TextInput, ActivityIndicator, AsyncStorage, StatusBar, Platform } from 'react-native';
import { Container, Text, Content, Form, Button } from 'native-base';

import TopBar from '../../Shared/TopBar';
import Styles from '../../Assets/Styles';
import ResetPasswordStyles from './Assets/ResetPasswordStyles';
import { requestForgotPassword } from '../../../Data/userRepo';
import Colors from '../../Assets/Colors';

export default class ResetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            hasError: false,
            errorString: '',
            sending: false,
        };
    }
    _invalidEmail(text) {
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (reg.test(text)) {
            return true;
        }
        return false;
    }
    _storeData = async () => {
        try {
            await AsyncStorage.setItem('@ResetEmail', this.state.email.toString());
        } catch (error) {
            // Error saving data
            Alert.alert(
                'Lỗi',
                'Đã có lỗi xảy ra, vui lòng thử lại sau!',
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
        }
    };
    _submitForm = async () => {
        if (this.state.email === '') {
            this.setState({ hasError: true, errorString: 'Vui lòng nhập email của bạn' });
            return;
        }
        if (!this._invalidEmail(this.state.email)) {
            this.setState({ hasError: true, errorString: 'Email không hợp lệ' });
            return;
        }
        this.setState({ sending: true });
        const data = {
            email: this.state.email
        };
        await requestForgotPassword(data, (res) => {
            if (res.success) {
                const stringAlert = 'Đã gửi yêu cầu khôi phục mật khẩu thành công. Vui lòng kiểm tra email ' + this.state.email + ' để nhận mã xác thực lấy lại mật khẩu.';
                Alert.alert(
                    'Thông báo',
                    stringAlert,
                    [
                        {
                            text: 'OK', onPress: () => {
                                this._storeData();
                                this.props.navigation.navigate('ConfirmResetPasswordScreen');
                            }
                        },
                    ],
                    { cancelable: false }
                );
            } else {
                Alert.alert(
                    'Lỗi',
                    res.errors.email[0].toString(),
                    [
                        { text: 'OK', onPress: () => this.setState({ sending: false }) },
                    ],
                    { cancelable: false }
                );
            }
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    { text: 'OK', onPress: () => this.setState({ sending: false }) },
                ],
                { cancelable: false }
            );
        });
    }
    render() {
        const { navigation } = this.props;

        const {
            fontFace,
            input,
            btnSubmit,
            errorAlert,
        } = Styles;

        const {
            content,
            description,
            buttonBox,
        } = ResetPasswordStyles;
        const { email, hasError, errorString } = this.state;
        return (
            <Container style={fontFace}>
                <TopBar navigation={navigation} title="Quên mật khẩu" />
                <Content padder style={content}>
                    <Text style={description}>Vui lòng điền địa chỉ Email tài khoản của bạn</Text>
                    <Form>
                        <TextInput
                            autoCapitalize='none'
                            style={input}
                            placeholder="Nhập Email của bạn"
                            value={email}
                            onChangeText={text => this.setState({ email: text })}
                            keyboardType='email-address'
                        />
                        {hasError ?
                            <Text style={errorAlert}>{errorString}</Text> : null
                        }
                        <View style={buttonBox}>
                            <Button style={btnSubmit} onPress={() => this._submitForm()}>
                                {this.state.sending ? <ActivityIndicator size="small" color="#fff" /> : null}
                                <Text>Gửi yêu cầu</Text>
                            </Button>
                        </View>
                    </Form>
                </Content>
            </Container>
        );
    }
}
