import React, { Component } from 'react';
import {
    Image,
    ImageBackground,
    View,
    ActivityIndicator,
    SafeAreaView,
    StatusBar,
    Platform,
    Alert,
} from 'react-native';
import {
    Container,
    Content,
    Form,
    Input,
    Button,
    Text,
    Footer,
} from 'native-base';
import mainBg from './Assets/Images/bg-staff.jpg';
import logo from './Assets/Images/logo.png';
import LoginStyles from './Assets/LoginStyles';
import ComLoading from '../../Shared/Loading';
import Colors from '../../Assets/Colors';
import { AuthShiper } from '../../../Networking/auth';
import { ScreenManager } from '../../../System/Screen';

export default class StaffLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            token: '',
            input: {
                btn_login: 'Đăng nhập'
            },
            isLoading: false,
            isLogin: false,
        };
    }

    async componentDidMount() {
        await ScreenManager.setInitScreen('StaffLoginScreen');
    }

    async _onLogin() {
        if (
            this.state.username.length === 0 ||
            this.state.password.length === 0
        ) {
            Alert.alert(
                'Lỗi',
                'Vui lòng điền đầy đủ thông tin!',
                [
                    {text: 'OK'},
                ],
                {cancelable: false},
            );
            return;
        }

        this.setState({ isLogin: true });

        AuthShiper.login(this.state.username, this.state.password, (responseJson) => {
            this._onLoginCallback(responseJson);
            this.setState({ isLogin: false });
        }, (e) => {
            this.setState({ isLogin: false });            
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    {text: 'OK'},
                ],
                {cancelable: false},
            );
        });
    }

    _onLoginCallback(responseJson) {
        let label = {
            dangnhapthanhcong: 'Đăng nhập thành công!',
            saimatkhau: 'Sai mật khẩu, vui lòng thử lại!',
            taikhoanbikhoa: 'Tài khoản bị khóa!',
            taikhoanchuakichhoat: 'Tài khoản chưa được kích hoạt!'
        };

        let res_token = (typeof responseJson.token !== 'undefined') ? responseJson.token : null;
        let res_user = (typeof responseJson.data !== 'undefined') ? responseJson.data : null;
        let login_state = responseJson.success;
        let login_message = label.dangnhapthanhcong;
        if (!responseJson.success) {
            login_state = false;
            login_message = label.saimatkhau;
            Alert.alert(
                'Thông báo',
                responseJson.message,
                [
                    {text: 'OK'},
                ],
                {cancelable: false},
            );
            return;
        }

        if (login_state
            && typeof res_user.trangthai !== 'undefined'
            && res_user.trangthai !== 'dakichhoat') {
            login_message = label.taikhoanchuakichhoat;
            login_state = false;


            Alert.alert(
                'Thông báo',
                login_message,
                [
                    {text: 'OK'},
                ],
                {cancelable: false},
            );
            return;
        }

        // REDIRECT IF EVERYTHING IS OKEY
        if (login_state && res_user.nhiemvu === 'shipper') {
            this.props.navigation.navigate('ShipperInfoScreen', {
                token: res_token,
                user: res_user
            });
        }
        if (login_state && res_user.nhiemvu === 'stocker') {
            this.props.navigation.navigate('StockImportRequestScreen', {
                token: res_token,
                user: res_user
            });
        }
    }
    showLoading(state = true) {
        this.setState({ isLoading: state });
    }

    render() {

        if (this.state.isLoading) {
            return (
                <ComLoading />
            );
        }
        const {
            container,
            mainContent,
            formBox,
            inputStyle,
            inputPassword,
            bottomForm,
            btnForget,
            btnText,
            btnSubmit,
            formGroup,
            imgLogo
        } = LoginStyles;
        return (
            <Container style={container}>
                <StatusBar barStyle="dark-content" backgroundColor='#fff' animated />
                <ImageBackground
                    source={mainBg}
                    style={{ flex: 1 }}
                >
                    <SafeAreaView style={{ flex: 1 }}>
                        <Content style={mainContent}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <Image
                                    style={imgLogo}
                                    source={logo}
                                />
                            </View>
                            <Form style={formBox}>
                                <Input
                                    autoCapitalize='none'
                                    onChangeText={(username) => this.setState({ username })}
                                    style={inputStyle}
                                    placeholder='Số điện thoại'
                                    value={this.state.username}
                                    keyboardType='phone-pad'
                                />
                                <View style={formGroup}>
                                    <Input
                                        autoCapitalize='none'
                                        onChangeText={(password) => this.setState({ password })}
                                        secureTextEntry
                                        style={[inputPassword, {paddingRight: 15}]}
                                        placeholder='Mật khẩu'
                                        value={this.state.password}
                                    />
                                    
                                </View>

                                <View style={bottomForm}>
                                    <Button
                                        success style={btnSubmit} onPress={() => this._onLogin()}
                                    >
                                        {this.state.isLogin ? <ActivityIndicator size='small' color='#fff' /> : null}
                                        <Text style={btnText}> {this.state.input.btn_login} </Text>
                                    </Button>
                                    {/* <Text
                                        style={btnForget}
                                        onPress={() => this.props.navigation.navigate('ResetScreen')}
                                    >
                                        Quên mật khẩu?
                                    </Text> */}
                                </View>

                            </Form>
                        </Content>
                        <Footer
                            transparent
                            style={{
                                backgroundColor: 'transparent',
                                elevation: 0,
                                height: 26,
                                borderTopWidth: 0,
                            }}
                        >
                            <View
                                style={{
                                    paddingHorizontal: 10,
                                    justifyContent: 'center',
                                    flexDirection: 'row',
                                    alignItems: 'flex-end',
                                    paddingBottom: 8
                                }}
                            >
                                <Text
                                    style={{
                                        color: Colors.blackColor,
                                        borderBottomWidth: 1,
                                        borderBottomColor: Colors.blackColor,
                                        fontSize: 14,
                                        height: 'auto',
                                        paddingBottom: 0,
                                    }}
                                    onPress={() => this.props.navigation.navigate('LoginScreen')}
                                >
                                    Đăng nhập tài khoản Shop?
                            </Text>
                            </View>
                        </Footer>
                    </SafeAreaView>
                </ImageBackground>
            </Container>
        );
    }   
}
