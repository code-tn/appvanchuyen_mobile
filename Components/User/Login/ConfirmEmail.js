import React, { Component } from 'react';
import { View, TextInput, Modal, Alert, BackHandler } from 'react-native';
import { Container, Text, Button, Icon, Form, Content } from 'native-base';
import Styles from '../../Assets/Styles';
import ConfirmEmailStyle from './Assets/ConfirmEmailStyles';
import TopBarOnlyTitle from '../../Shared/TopBarOnlyTitle';
import Colors from '../../Assets/Colors';
import { requestForgotPassword, confirmEmailToken } from '../../../Data/userRepo';

class ConfirmEmail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            otpEmail: '',
            reEmail: '',
            hasError: false,
            errorString: '',
            modalVisible: false,
            email: '',
            resend: false,
        };
    }
    _setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }
    _invalidEmail(text) {
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (reg.test(text)) {
            return true;
        }
        return false;
    }

    _resetEmail() {
        if (this.state.reEmail !== '' && this._invalidEmail(this.state.reEmail)) {
            const stringAlert = 'Cập nhật email thành công.';
            Alert.alert(
                'Thông báo',
                stringAlert,
                [
                    { text: 'Đóng', onPress: () => this._setModalVisible(!this.state.modalVisible) },
                ],
                { cancelable: false }
            );
        } else if (this.state.reEmail === '') {
            this.setState({ hasError: true, errorString: 'Vui lòng nhập email của bạn' });
            return;
        } else if (!this._invalidEmail(this.state.reEmail)) {
            this.setState({ hasError: true, errorString: 'Email không hợp lệ' });
            return;
        }
    }
    _confirmOTP = async () => {
        const data = {
            email: this.state.email,
            code: this.state.otpEmail
        }
        await confirmEmailToken(data, res => {
            if (res.success) {
                Alert.alert(
                    'Thông báo',
                    'Xác nhận Email thành công',
                    [
                        { text: 'OK', onPress: ()=> { this.props.navigation.navigate('LoginScreen') } },
                    ],
                    { cancelable: false }
                );
            } else {
                Alert.alert(
                    'Lỗi',
                    res.message,
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false }
                );
            }
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false }
            );
        })
    }
    _resendConfirmOTP() {
        Alert.alert(
            'Thông báo',
            'Đã gửi lại mã xác nhận qua email của bạn.',
            [
                { text: 'Đóng' },
            ],
            { cancelable: false }
        );
    }
    _resend = async() => {
        this.setState({ resend: true });
        const data = {
            email: this.state.email
        };
        await requestForgotPassword(data, (res) => {
            if (res.success) {
                const stringAlert = 'Đã gửi lại mã xác thực. Vui lòng kiểm tra email ' + this.state.email + ' để nhận mã xác thực lấy lại mật khẩu.';
                Alert.alert(
                    'Thông báo',
                    stringAlert,
                    [
                        {
                            text: 'OK', onPress: () => {
                                this.setState({ resend: false });
                            }
                        },
                    ],
                    { cancelable: false }
                );
            } else {
                Alert.alert(
                    'Lỗi',
                    res.errors.email[0].toString(),
                    [
                        { text: 'OK', onPress: () => this.setState({ resend: false }) },
                    ],
                    { cancelable: false }
                );
            }
        }, (e) => {
            this.setState({ resend: false });
        });
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', () => { return true; });
    }
    componentDidMount() {
        this.setState({email: this.props.navigation.getParam('email_khachhang')});
    }

    render() {
        const {
            fontFace,
            input,
        } = Styles;
        const {
            content,
            formBox,
            formBoxLeft,
            btnConfirm,
            btnReSend,
            heading,
            headingTitle,
            wrapper,
        } = ConfirmEmailStyle;
        const { otpEmail } = this.state;
        return (
            <Container style={fontFace}>
                <View 
                    style={{                 
                        height: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight,
                        backgroundColor: Colors.activeColor
                    }}
                />
                <TopBarOnlyTitle title='Xác nhận email' />
                <Content style={{ backgroundColor: '#eee' }}>
                    <View style={content}>
                        <View style={heading}>
                            <Text style={headingTitle}>XÁC NHẬN EMAIL</Text>
                        </View>
                        <View style={{ padding: 10, backgroundColor: '#fff'}}>
                            <Text>Mã xác thực đã được gửi về email <Text style={{ color: Colors.mainColor }}>{this.state.email}</Text> của bạn. Vui lòng kiểm tra và nhập vào ô ở dưới để tiến hành xác thực.</Text>
                        </View>
                        <Form style={formBox}>
                            <View style={formBoxLeft}>
                                <View style={wrapper}>
                                    <TextInput
                                        style={[input, {flexGrow: 1, width: 'auto'}]}
                                        placeholder="Nhập mã xác nhận"
                                        value={otpEmail}
                                        onChangeText={text => this.setState({ otpEmail: text })}
                                    />
                                    <Button
                                        success
                                        style={btnConfirm}
                                        onPress={this._confirmOTP.bind(this)}
                                    >
                                        <Text>OK</Text>
                                    </Button>
                                </View>
                            </View>
                            <Button
                                light
                                style={btnReSend}
                                onPress={() => this._resend()}
                            >
                                <Text uppercase={false} style={{ paddingLeft: 5, paddingRight: 5 }}> {this.state.resend ?'Đang gửi...' : 'Gửi lại mã'} </Text>
                            </Button>
                        </Form>
                    </View>
                </Content>
            </Container>
        );
    }
}
export default ConfirmEmail;

