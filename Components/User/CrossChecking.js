import React, { Component } from 'react';
import { Alert, FlatList, View, ActivityIndicator, RefreshControl, StyleSheet } from 'react-native';
import { Container, Text, Picker, Icon } from 'native-base';
import TopBarOnlyTitle from '../Shared/TopBarOnlyTitle';
import Loading from '../Shared/Loading';
import { getCrossCheckingHistory } from '../../Data/userRepo';
import { Auth } from '../../Networking/auth';
import Colors from '../Assets/Colors';
import FormatCurrency from '../Assets/FormatCurrency';

class CrossChecking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            userInfo: '',
            dataSource: '',
            data: [],
            refreshing: false,
            loadmore: false,
            sortBy: 'all'
        };
    }
    async componentDidMount() {
        const user = await Auth.currentUser();
        this.setState({ userInfo: user });
        const params = {
            id_khachhang: this.state.userInfo.id,
            sortby: this.state.sortBy
        };
        await getCrossCheckingHistory(params, null, (res) => {
            if (res.success) {
                this.setState({
                    dataSource: res.data,
                    data: res.data.data,
                    isLoading: false
                });
            } else {
                Alert.alert(
                    'Thông báo',
                    res.message,
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    isLoading: false
                });
            }
        }, (e) => {
            Alert.alert(
                'Thông báo',
                e.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
            this.setState({
                isLoading: false
            });
        });
    }
    async onEndReached() {
        if (this.state.dataSource.current_page < this.state.dataSource.last_page) {
            this.setState({ loadmore: true });
            const params = {
                id_khachhang: this.state.userInfo.id,
                sortby: this.state.sortBy,
            };
            await getCrossCheckingHistory(params, this.state.dataSource.next_page_url, (res) => {
                if (res.success) {
                    this.setState({
                        dataSource: res.data,
                        data: [...this.state.data, ...res.data.data],
                        refreshing: false
                    });
                } else {
                    Alert.alert(
                        'Thông báo',
                        res.message,
                        [
                            { text: 'OK' },
                        ],
                        { cancelable: false },
                    );
                    this.setState({
                        refreshing: false
                    });
                }
            }, (e) => {
                Alert.alert(
                    'Thông báo',
                    e.toString(),
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    refreshing: false
                });
            });
        }
    }
    _onRefresh = async () => {
        this.setState({ refreshing: true, sortBy: 'all' });
        const params = {
            id_khachhang: this.state.userInfo.id,
            sortby: 'all'
        };
        await getCrossCheckingHistory(params, null, (res) => {
            if (res.success) {
                this.setState({
                    dataSource: res.data,
                    data: res.data.data,
                    refreshing: false
                });
            } else {
                Alert.alert(
                    'Thông báo',
                    res.message,
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    refreshing: false
                });
            }
        }, (e) => {
            Alert.alert(
                'Thông báo',
                e.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
            this.setState({
                refreshing: false
            });
        });
    }

    async _onValueChange(value) {
        await this.setState({
            sortBy: value,
            refreshing: true
        });
        const params = {
            id_khachhang: this.state.userInfo.id,
            sortby: this.state.sortBy
        };
        await getCrossCheckingHistory(params, null, (res) => {
            if (res.success) {
                this.setState({
                    dataSource: res.data,
                    data: res.data.data,
                    refreshing: false
                });
            } else {
                Alert.alert(
                    'Thông báo',
                    res.message,
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
                this.setState({
                    refreshing: false
                });
            }
        }, (e) => {
            Alert.alert(
                'Thông báo',
                e.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
            this.setState({
                refreshing: false
            });
        });
    }
    render() {
        const {
            topBar
        } = crossCheckingStyles;
        if (this.state.isLoading) return <Loading />;
        return (
            <Container>
                <TopBarOnlyTitle title='Lịch sử giao dịch' />
                <View style={topBar}>
                    <Text>Thời gian </Text>
                    <Picker
                        note
                        mode='dropdown'
                        style={{ width: 120 }}
                        selectedValue={this.state.sortBy}
                        onValueChange={this._onValueChange.bind(this)}
                    >
                        <Picker.Item label='Tất cả' value='all' />
                        <Picker.Item label='Tuần hiện tại' value='this-week' />
                        <Picker.Item label='Tháng hiện tại' value='this-month' />
                        <Picker.Item label='Tháng hiện tại và tháng trước' value='this-month-and-last-month' />
                        <Picker.Item label='Tháng hiện tại và 2 tháng trước' value='this-montn-and-recent-two-months' />
                    </Picker>
                </View>
                <View style={{ flex: 1, padding: 10 }}>
                    {this.state.dataSource !== '' && this.state.data.length === 0 ? <Text>Không có dữ liệu</Text> : null}
                    <FlatList
                        style={{ flex: 1 }}
                        data={this.state.data}
                        renderItem={({ item }) =>
                            (
                                <View style={{ marginBottom: 15, flexDirection: 'row' }}>
                                    <View 
                                        style={{ 
                                            backgroundColor: Colors.mainColor, 
                                            marginRight: 10, 
                                            padding: 10, 
                                            flexDirection: 'column', 
                                            alignItems: 'center', 
                                            justifyContent: 'center' 
                                        }}
                                    >
                                        <Icon type='AntDesign' name='wallet' style={{ color: '#fff' }} />
                                    </View>
                                    <View>
                                        <Text>Tổng tiền: <Text style={{ color: Colors.mainColor, fontWeight: '600' }}>{FormatCurrency(parseFloat(item.tongtien))}</Text> VNĐ</Text>
                                        <Text>Ghi chú: {item.ghichu}</Text>
                                        <Text>Ngày tạo: {item.created_at}</Text>
                                    </View>
                                </View>
                            )
                        }
                        keyExtractor={(item) => item.id.toString()}
                        onEndReached={this.onEndReached.bind(this)}
                        onEndReachedThreshold={0.1}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this._onRefresh}
                            />
                        }
                    />
                    {this.state.loadmore ?
                        <View style={{ paddingTop: 5 }}>
                            <ActivityIndicator size='small' color={Colors.mainColor} />
                        </View>
                        : null}
                </View>
            </Container>
        );
    }
}
const crossCheckingStyles = StyleSheet.create({
    topBar: {
        paddingHorizontal: 10, 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        borderBottomWidth: 1, 
        borderBottomColor: '#f1f1f1'
    }
});
export default CrossChecking;
