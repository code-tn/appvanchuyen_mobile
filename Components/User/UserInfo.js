import React, { Component } from 'react';
import { View, Text, Image, RefreshControl, AsyncStorage, Modal, TextInput, ActivityIndicator, Alert } from 'react-native';
import {
    Container,
    Icon,
    Button,
    Content,
} from 'native-base';
import Styles from '../Assets/Styles';
import Loading from '../Shared/Loading';
import TopBarOnlyTitle from '../Shared/TopBarOnlyTitle';
import UserInfoStyles from './Assets/UserInfoStyles';

import { Auth } from '../../Networking/auth';
import { refreshUserStorage, updateUserPassword } from '../../Data/userRepo';
import { LoginManager } from 'react-native-fbsdk';
//icon
import iconHome from './Assets/Images/icon_1.png';
import iconKey from './Assets/Images/key.png';
import iconPhone from './Assets/Images/icon_2.png';
import iconMail from './Assets/Images/icon_3.png';
import iconUser from './Assets/Images/icon_4.png';
import iconBankNumber from './Assets/Images/icon_5.png';
import iconBankName from './Assets/Images/icon_6.png';
import iconBankBranch from './Assets/Images/icon_7.png';
import iconDS from './Assets/Images/icon_8.png';

class UserInfo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            dataSource: null,
            subStatus: false,
            refreshing: false,
            modalVisible: false,
            matkhau: '',
            matkhau_confirmation: '',
            isSubmit: false,
        };
    }

    async componentDidMount() {
        await this.loadData();
    }

    _storeData = async () => {
        try {
            await AsyncStorage.setItem('@LoginFacebook', '');
        } catch (error) {
            // Error saving data
        }
    };
    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('@LoginFacebook');
            if (value !== null) {
                return value;
            } else {
                return false;
            }
        } catch (error) {
            // Error retrieving data
            return false;
        }
    };

    _onRefresh = async () => {
        this.setState({ refreshing: true });
        await this.loadData();
    }

    loadData() {
        setTimeout(async () => {
            let user = await Auth.currentUser();
            this.setState({
                dataSource: user,
                isLoading: false,
                refreshing: false
            });
        }, 500);
    }
    updatePassword = async () => {
        this.setState({ isSubmit: true });
        const params = {
            id_khachhang: this.state.dataSource.id,
            matkhau: this.state.matkhau,
            matkhau_confirmation: this.state.matkhau_confirmation,
        }
        await updateUserPassword(params, (res) => {
            if (res.success) {
                Alert.alert(
                    'Thông báo',
                    'Đã cập nhật mật khẩu',
                    [
                        {   
                            text: 'OK', 
                            onPress: () => {
                                this.setState({ modalVisible: false, isSubmit: false })
                            }
                        },
                    ],
                    { cancelable: false },
                );
            } else {
                Alert.alert(
                    'Lỗi',
                    res.massage,
                    [
                        { text: 'OK'},
                    ],
                    { cancelable: false },
                );
            }
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    { text: 'OK'},
                ],
                { cancelable: false },
            );
        });
    }

    _setSubStatus(value) {
        this.setState({ subStatus: value });
    }

    render_personalSection() {
        const {
            infoBox, topBox, boxTitle, btnEdit, btnEditText, btnEditIcon, infoItem, infoText, iconStyle
        } = UserInfoStyles;
        const {
            modalDialog,
            modalBody,
            modalHeader,
            input,
            btnSubmit,
            btn__icon,
            btn__text
        } = Styles;
        const { dataSource } = this.state;

        let listItem = (item) => {
            return (
                <View style={infoItem}>
                    <Image style={iconStyle} source={item.icon} />
                    <Text style={infoText}>{item.name}</Text>
                </View>
            )
        }
        let contentPersonal = (data) => {
            if (typeof data !== 'object') {
                return (<Text>Đang tải...</Text>)
            }
            return (
                <View>
                    <Modal
                        animationType="slide"
                        transparent
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                            console.log('close');
                        }}
                    >
                        <View style={modalDialog}>
                            <View style={[modalHeader, { justifyContent: 'space-between', alignItems: 'center' }]}>
                                <Text style={{ paddingHorizontal: 10, paddingVertical: 5, color: '#fff' }} uppercase>Cập nhật mật khẩu</Text>
                                <Button
                                    transparent
                                    iconRight
                                    onPress={() => { this.setState({ modalVisible: false }) }}
                                >
                                    <Text uppercase={false} style={[btn__text, { color: '#fff', fontSize: 15 }]}>Đóng</Text>
                                    <Icon style={[btn__icon, { color: '#fff' }]} type='FontAwesome' name='times-circle' />
                                </Button>
                            </View>
                            <View style={modalBody}>
                                <View style={{ padding: 10 }}>
                                    <TextInput
                                        secureTextEntry
                                        onChangeText={(text) => this.setState({ matkhau: text })}
                                        style={input}
                                        placeholder='Mật khẩu'
                                        autoCapitalize='none'
                                    />
                                    <TextInput
                                        autoCapitalize='none'
                                        secureTextEntry
                                        onChangeText={(text) => this.setState({ matkhau_confirmation: text })}
                                        style={input}
                                        placeholder='Xác nhận mật khẩu'
                                    />
                                    <Button
                                        block
                                        style={btnSubmit}
                                        onPress={() => this.updatePassword()}
                                    >
                                        {this.state.isSubmit ? <ActivityIndicator size='small' color='#fff' /> : null}
                                        <Text style={{ color: '#fff' }}>Xác nhận</Text>
                                    </Button>
                                </View>
                            </View>
                        </View>
                    </Modal>
                    {listItem({ name: dataSource.hoten, icon: iconHome })}
                    {listItem({ name: dataSource.dienthoai, icon: iconPhone })}
                    {listItem({ name: dataSource.email, icon: iconMail })}

                    <View style={infoItem}>
                        <Image style={iconStyle} source={iconKey} />
                        <Text style={infoText} onPress={() => { this.setState({ modalVisible: true, matkhau: '', matkhau_confirmation: '' }) }}>Đổi mật khẩu</Text>
                    </View>
                </View>
            )
        }
        let onEditUserInfo = () => {
            this.props.navigation.navigate('EditUserInfoScreen', {
                onGoBack: () => {
                    setTimeout(async () =>
                        await refreshUserStorage()
                        , 100);
                    setTimeout(async () =>
                        await this._onRefresh()
                        , 600);
                }
            })
        }
        return (
            <View style={infoBox}>
                <View style={topBox}>
                    <Text style={boxTitle}>THÔNG TIN CƠ BẢN</Text>
                    <Button transparent iconRight style={btnEdit} onPress={() => onEditUserInfo()}>
                        <Text style={btnEditText}>Sửa</Text>
                        <Icon style={btnEditIcon} type='FontAwesome' name='angle-right' />
                    </Button>
                </View>

                {contentPersonal(dataSource)}
            </View>
        )
    }

    render_addressSection() {
        const {
            infoBox, topBox, boxTitle, btnEdit, btnEditText, btnEditIcon
        } = UserInfoStyles;

        return (
            <View>
                <View style={infoBox}>
                    <View style={topBox}>
                        <Text style={boxTitle}>ĐỊA CHỈ LẤY HÀNG</Text>
                        <Button
                            transparent iconRight style={btnEdit}
                            onPress={() => this.props.navigation.navigate('AdderssListScreen', {
                                loai_diachi: 'nguoigui'
                            })}
                        >
                            <Text style={btnEditText}>Xem</Text>
                            <Icon style={btnEditIcon} type='FontAwesome' name='angle-right' />
                        </Button>
                    </View>
                </View>

                <View style={infoBox}>
                    <View style={topBox}>
                        <Text style={boxTitle}>DANH BẠ NGƯỜI NHẬN</Text>
                        <Button
                            transparent iconRight style={btnEdit}
                            onPress={() => this.props.navigation.navigate('AdderssListScreen', {
                                loai_diachi: 'nguoinhan'
                            })}
                        >
                            <Text style={btnEditText}>Xem</Text>
                            <Icon style={btnEditIcon} type='FontAwesome' name='angle-right' />
                        </Button>
                    </View>
                </View>
            </View>
        );
    }

    render_bankSection() {
        const {
            infoBox, topBox, boxTitle, btnEdit, btnEditText, btnEditIcon, infoItem, infoText, iconStyle
        } = UserInfoStyles;
        const { dataSource } = this.state;

        const bankInfo = dataSource.tai_khoan_ngan_hang;
        let lichDoiSoat = '';
        if (dataSource.lich_doisoat !== null && dataSource.lich_doisoat !== '') {
            if (dataSource.lich_doisoat.length === 7) {
                lichDoiSoat = 'Đối soát 1 lần/ngày trong giờ hành chính';
            } else {
                lichDoiSoat += 'Lịch đối soát: ';
                dataSource.lich_doisoat.map((item, index) => {
                    lichDoiSoat += (item === '8' ? 'CN' : 'Thứ ' + item);
                    lichDoiSoat += index < (dataSource.lich_doisoat.length - 1) ? ', ' : '';
                });
            }
        }
        return (
            <View style={infoBox}>
                <View style={topBox}>
                    <Text style={boxTitle}>THÔNG TIN NGÂN HÀNG & ĐỐI SOÁT</Text>
                    <Button transparent iconRight style={btnEdit}
                        onPress={() => this.props.navigation.navigate('EditBankInfoScreen', {
                            dataSource: this.state.dataSource,
                            onGoBack: (bankInfo) => {
                                this.setState({
                                    dataSource: Object.assign(this.state.dataSource, {
                                        tai_khoan_ngan_hang: bankInfo
                                    })
                                });
                                setTimeout(async () =>
                                    await refreshUserStorage()
                                    , 100);
                                setTimeout(async () =>
                                    await this._onRefresh()
                                    , 600);
                            }
                        })}
                    >
                        <Text style={btnEditText}>Sửa</Text>
                        <Icon style={btnEditIcon} type='FontAwesome' name='angle-right' />
                    </Button>
                </View>
                {bankInfo === null || bankInfo === undefined ? <View /> : (
                    <View>
                        <View style={infoItem}>
                            <Image style={iconStyle} source={iconUser} />
                            <Text style={infoText}>{bankInfo.chutaikhoan}</Text>
                        </View>
                        <View style={infoItem}>
                            <Image style={iconStyle} source={iconBankNumber} />
                            <Text style={infoText}>{bankInfo.so_taikhoan}</Text>
                        </View>
                        <View style={infoItem}>
                            <Image style={iconStyle} source={iconBankName} />
                            <Text style={infoText}>{bankInfo.nganhang}</Text>
                        </View>
                        <View style={infoItem}>
                            <Image style={iconStyle} source={iconBankBranch} />
                            <Text style={infoText}>{bankInfo.chinhanh}</Text>
                        </View>
                    </View>
                )
                }
                <View style={infoItem}>
                    <Image style={iconStyle} source={iconDS} />
                    <Text style={infoText}>{lichDoiSoat}</Text>
                </View>
            </View>
        )
    }

    async _logout() {
        let checkFbLogin = null;
        checkFbLogin = await this._retrieveData();
        if (checkFbLogin !== null && checkFbLogin !== false) {
            await this._storeData();
            LoginManager.logOut();
        }
        await Auth.logout((e) => {
            // console.log(e);
            Alert.alert(
                'Thông báo',
                e.toString(),
                [
                    {   
                        text: 'OK',
                    },
                ],
                { cancelable: false },
            );
            return;
        });
        this.props.navigation.navigate('LoginScreen');
    }

    render() {
        const { fontFace } = Styles;
        const {
            content,
            buttonBox,
            btnLogout
        } = UserInfoStyles;
        if (this.state.isLoading) {
            return (
                <Loading loadingText="Đang tải thông tin..." />
            );
        }

        return (
            <Container style={[fontFace]}>
                <TopBarOnlyTitle title='Thông tin shop' />
                <Content
                    padder
                    style={content}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                >

                    {this.render_personalSection()}

                    {this.render_bankSection()}

                    {this.render_addressSection()}

                    {/* <View style={infoBox}>
                        <View style={topBox}>
                            <Text style={boxTitle}>
                                CÀI ĐẶT THÔNG BÁO
                            </Text>
                        </View>
                        <View style={flexSpaceBetween}>
                            <Text style={infoText}>
                                Nhận tin nhắn xác nhận lấy hàng
                            </Text>
                            <Switch 
                                value={subStatus}
                                onValueChange={(value) => this._setSubStatus(value)} 
                            />
                        </View>
                    </View> */}
                    <View style={buttonBox}>
                        <Button
                            style={btnLogout} transparent onPress={() => this._logout()}
                        >
                            <Text style={{ fontSize: 15 }}>Đăng xuất</Text>
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }
}

export default UserInfo;
