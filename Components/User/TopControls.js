import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';
import { Button, Icon } from 'native-base';
import TopControlsStyles from '../Assets/TopControlsStyles';

class TopControls extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword: ''
        };
    }

    render() {
        const {
            controlBox,
            btnColtrol,
            btnText,
            btnIcon,
            header,
            formSearch,
            searchInput,
            btnSearch,
            btnIconSearch,
        } = TopControlsStyles;
        const {
            keyword
        } = this.state;
        const { navigation } = this.props;

        return (
            <View style={header}>
                <View
                    style={
                        {
                            width: '100%',
                            flexDirection: 'column',
                            alignItems: 'center',
                            justifyContent: 'center',
                            shadowColor: 'black',
                            shadowOffset: { width: 0, height: 2 },
                            shadowOpacity: 0.2,
                            shadowRadius: 1.2,
                        }
                    }
                >
                    <View style={controlBox}>
                        <View style={{ flex: 1 }}>
                            <View style={formSearch}>
                                <TextInput
                                    style={searchInput}
                                    value={keyword}
                                    placeholder='Tìm kiếm...'
                                />
                                <Button style={btnSearch}>
                                    <Icon style={btnIconSearch} type='FontAwesome' name='search' />
                                </Button>
                            </View>
                        </View>
                        <Button
                            iconRight
                            transparent
                            style={btnColtrol}
                            onPress={() => navigation.navigate('CreateOrderScreen')}
                        >
                            <Text style={btnText}>Tạo đơn</Text>
                            <Icon
                                type='FontAwesome'
                                name='plus'
                                style={btnIcon}
                            />
                        </Button>
                    </View>
                </View>
            </View>
        );
    }
}

export default TopControls;
