import React, { Component } from 'react';
import { View, Text, RefreshControl } from 'react-native';
import { Container, Content, Icon } from 'native-base';
import {
  Collapse,
  CollapseHeader,
  CollapseBody,
} from 'accordion-collapse-react-native';
import TopBarOnlyTitle from '../Shared/TopBarOnlyTitle';
import Styles from '../Assets/Styles';
import CashStyles from './Assets/CashStyles';
import { getCashFollow } from '../../Data/userRepo';
import { Auth } from '../../Networking/auth';
import Loading from '../Shared/Loading';
import FormatCurrency from '../Assets/FormatCurrency';

class CashFlow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shopInfo: '',
      dataSource: '',
      isLoading: true,
      refreshing: false
    };
  }
  componentDidMount = async () => {
    await this._getUserInfo();
    await this._getCashFollow();
  }
  _getUserInfo = async () => {
    let user = await Auth.currentUser();
    this.setState({ shopInfo: user });
  }
  _getCashFollow = async () => {
    await getCashFollow(this.state.shopInfo.id, (res) => {
      if (res.success) {
        this.setState({
          dataSource: res.data,
          isLoading: false,
          refreshing: false,
        });
      }
    }, (e) => {
      this.setState({
        isLoading: false,
        refreshing: false,
      });
    });
  }
  _onRefresh = () => {
    this.setState({ refreshing: true });
    this._getCashFollow();
  }
  render() {
    const {
      content,
      fontFace
    } = Styles;
    const {
      title,
      listItem,
      listItemTitle,
      listItemValue,
      listItemIcon,
      collapseBody,
      fontMedium,
      borderNone
    } = CashStyles;
    if (this.state.isLoading) {
      return <Loading />;
    }
    const {
      count,
      total,
      total_formatted,
      next
    } = this.state.dataSource;
    return (
      <Container style={fontFace}>
        <TopBarOnlyTitle title='Quản lý dòng tiền' />
        <Content
          padder
          style={content}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
        >
          <Text style={[title, { marginTop: 0 }]}> PHIÊN ĐỐI SOÁT SẮP TỚI </Text>
          <View style={[listItem, borderNone]}>
            <Text style={listItemTitle}>Tiền thu hộ</Text>
            <Text style={listItemValue}>{FormatCurrency(next.total)} đ</Text>
          </View>
          {/* <Collapse>
            <CollapseHeader>
              <View style={listItem}>
                <Text style={listItemTitle}>Tổng phí dịch vụ</Text>
                <Text style={listItemValue}>0 đ <Icon style={listItemIcon} type='FontAwesome' name='angle-right' />
                </Text>
              </View>
            </CollapseHeader>
            <CollapseBody style={collapseBody}>
              <View style={[listItem, { paddingLeft: 20 }]}>
                <Text style={listItemTitle}>Phí giao hàng</Text>
                <Text style={listItemValue}>0 đ</Text>
              </View>
              <View style={[listItem, { paddingLeft: 20 }]}>
                <Text style={listItemTitle}>Phí chuyển hoàn</Text>
                <Text style={listItemValue}>0 đ</Text>
              </View>
              <View style={[listItem, { paddingLeft: 20 }]}>
                <Text style={listItemTitle}>Phí bảo hiểm</Text>
                <Text style={listItemValue}>0 đ</Text>
              </View>
              <View style={[listItem, { paddingLeft: 20 }]}>
                <Text style={listItemTitle}>Phí chuyển khoản</Text>
                <Text style={listItemValue}>0 đ</Text>
              </View>
            </CollapseBody>
          </Collapse>
          <View style={listItem}>
            <Text style={listItemTitle}>Tiền đối soát</Text>
            <Text style={listItemValue}>0 đ</Text>
          </View> */}
          <View style={listItem}>
            <Text style={listItemTitle}>Số đơn hàng</Text>
            <Text style={listItemValue}>{next.count}</Text>
          </View>

          <Text style={title}> LỊCH SỬ </Text>

          <View>
            <View style={[listItem, borderNone]}>
              <Text style={[listItemTitle, fontMedium]}>Tổng {count} phiên đổi soát</Text>
            </View>
            <View style={[listItem, borderNone]}>
              <Text style={listItemTitle}>Thu hộ</Text>
              <Text style={listItemValue}>{total_formatted}</Text>
            </View>
            {/* <View style={[listItem, borderNone]}>
              <Text style={listItemTitle}>Phí</Text>
              <Text style={listItemValue}>0</Text>
            </View>
            <View style={[listItem, borderNone]}>
              <Text style={listItemTitle}>Tiền đối soát</Text>
              <Text style={listItemValue}>0</Text>
            </View> */}
          </View>

          {/* <View style={{ borderTopWidth: 1, borderTopColor: '#dad9d7' }}>
            <View style={[listItem, borderNone]}>
              <Text style={[listItemTitle, fontMedium]}>Tổng 0 phiên bồi hoàn</Text>
            </View>
            <View style={[listItem, borderNone]}>
              <Text style={listItemTitle}>Thu hộ</Text>
              <Text style={listItemValue}>0</Text>
            </View>
            <View style={[listItem, borderNone]}>
              <Text style={listItemTitle}>Phí</Text>
              <Text style={listItemValue}>0</Text>
            </View>
            <View style={[listItem, borderNone]}>
              <Text style={listItemTitle}>Tiền đối soát</Text>
              <Text style={listItemValue}>0</Text>
            </View>
          </View> */}
        </Content>
      </Container>
    );
  }
}

export default CashFlow;
