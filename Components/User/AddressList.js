import React, { Component } from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, RefreshControl, Platform, Alert } from 'react-native';
import { Container, Content, Icon, Text, Fab } from 'native-base';
import TopBar from '../Shared/TopBar';
import Styles from '../Assets/Styles';
import Swipeout from 'react-native-swipeout';
import Loading from '../Shared/Loading';
import { getCustomerBookmark, deleteCustomerBookmark } from '../../Data/userRepo';
import { Auth } from '../../Networking/auth';
import Colors from '../Assets/Colors';

class AddressListItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const swipeSettings = {
            autoClose: true,
            onClose: (secId, rowId, direction) => {
                if (this.state.activeRowKey != null) {
                    this.setState({ activeRowKey: null });
                }
            },
            onOpen: (sectId, rowId, direction) => {
                this.setState({ activeRowKey: this.props.data.item.id });
            },
            right: [
                {
                    onPress: () => {
                        const itemID = this.state.activeRowKey;
                        
                        Alert.alert(
                            'Thông báo',
                            'Bạn muốn xoá liên hệ này?',
                            [
                                {
                                    text: 'No',
                                    style: 'cancel'
                                },
                                {
                                    text: 'Yes', onPress: () => {
                                        this.props.parent._onDeleteRow(itemID);
                                    }
                                },
                            ],
                            { cancelable: true }
                        );
                    },
                    text: 'Delete', type: 'delete'
                }
            ],
            rowId: this.props.index,
            sectionId: 1
        };
        const { container, name, phone } = AddressListItemStyle;
        const { hoten_nguoinhan, so_dienthoai, chitiet } = this.props.data.item;
        return (
            <Swipeout {...swipeSettings} style={{ backgroundColor: '#ddd' }}>
                <TouchableOpacity onPress={() => {
                    Alert.alert(
                        'Danh bạ địa chỉ',
                        `Họ tên: ${hoten_nguoinhan}
                        \nSố điện thoại: ${so_dienthoai}
                        \nĐịa chỉ: ${chitiet}`,
                        [
                            {
                                text: 'Chỉnh sửa', onPress: () => {
                                    if (typeof this.props.onEdit === 'function') {
                                        this.props.onEdit(this.props.data.item);
                                    }
                                }, style: 'cancel'
                            },
                            { text: 'Đóng', style: 'cancel' }
                        ]
                    );
                }}>
                    <View style={container}>
                        <Text style={name}>{this.props.data.item.ten_goinho}</Text>
                        {/* <Text style={name}>{this.props.data.item.hoten_nguoinhan}</Text> */}
                        <Text style={phone}>{this.props.data.item.so_dienthoai}</Text>
                    </View>
                </TouchableOpacity>
            </Swipeout>
        );
    }
}

const AddressListItemStyle = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#fff',
        padding: 10,
        borderBottomColor: '#ccc',
        borderBottomWidth: 2,
        // marginTop: 5,
        // margin: 5,
        // marginBottom: 0,
    },
    name: { flex: 6, flexDirection: 'column' },
    phone: { flex: 4, flexDirection: 'column', textAlign: 'right' }
});

class AddressList extends Component {

    constructor(props) {
        super(props);
        const sloai_diachi = this.props.navigation.getParam('loai_diachi', 'nguoinhan');
        this.state = {
            active: true,
            user: '',
            loading: true,
            listViewData: '',
            loai_diachi: sloai_diachi,
            activeRowKey: null,
            refreshing: false
        };
    }


    loadData() {
        let user_id = this.state.user.id;
        setTimeout(async () => {
            await getCustomerBookmark({
                id_khachhang: user_id,
                loai_diachi: this.state.loai_diachi
            }, (rJson) => {
                if (rJson.success) {
                    this.setState({
                        listViewData: rJson.data,
                        loading: false,
                        refreshing: false 
                    });
                } else {
                    this.setState({
                        loading: false,
                        refreshing: false 
                    });
                }
            }, (error) => {
                Alert.alert(
                    'Lỗi',
                    error.toString(),
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
                this.setState({ refreshing: false });
            });
        }, 1);
    }

    async componentDidMount() {
        let user = await Auth.currentUser();
        this.setState({ ...this.state, user: user });
        this.loadData();
    }

    async _onDeleteRow(id) {
        await deleteCustomerBookmark(id, (res) => {
            if (res.success) {
                this._onRefresh();
            } else {
                Alert.alert(
                    'Lỗi',
                    res.message,
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
            }
        });
    }

    render_list() {
        if (this.state.loading) {
            return <Loading loadingText='Tải danh sách địa chỉ...' />;
        }

        if (typeof this.state.listViewData !== 'object') {
            return <Text style={{ textAlign: 'center' }}>Không có dữ liệu! </Text>;
        }

        return (
            <FlatList
                data={this.state.listViewData}
                renderItem={
                    row =>
                        <AddressListItem
                            key={row}
                            index={row.index}
                            data={row}
                            parent={this}
                            onDeleteItem={(id) => this._onDeleteRow(id)}
                            onRefresh={() => this.loadData()}
                            loai_diachi={this.state.loai_diachi}
                            onEdit={(item) => {
                                this.props.navigation.navigate('AddAdderssScreen', {
                                    onGoBack: () => this.loadData(),
                                    loai_diachi: this.state.loai_diachi,
                                    dataSource: item
                                });
                            }}
                        />
                }
                keyExtractor={item => String(item.id)}
            />
        );
    }
    _onRefresh = () => {
        this.setState({ refreshing: true });
        this.loadData();
    }
    render() {
        const { navigation } = this.props;
        const { fontFace } = Styles;
        const TopBarTitle = this.state.loai_diachi == 'nguoinhan' ? 'Danh bạ người nhận' : 'Địa chỉ lấy hàng';
        if (this.state.loading) {
            return <Loading loadingText={'Đang tải...'} />;
        }

        return (
            <Container style={fontFace}>
                <TopBar navigation={navigation} title={TopBarTitle} />
                <Content
                    padder
                    style={{ backgroundColor: '#eee' }}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                >
                    {this.render_list()}
                </Content>

                <Fab
                    active={this.state.active}
                    direction="up"
                    style={{ backgroundColor: Colors.subColor, paddingTop: Platform.OS==="ios" ? 10 : 0 }}
                    position="bottomRight"
                    onPress={() => {
                        this.props.navigation.navigate('AddAdderssScreen', {
                            onGoBack: () => this.loadData(),
                            loai_diachi: this.state.loai_diachi
                        });
                    }}
                >
                    <Icon name="add" type='Ionicons' style={{ fontSize: 32 }} />

                </Fab>
            </Container>
        );
    }
}

export default AddressList;

const Style = StyleSheet.create({
    ListItem: {
        paddingLeft: 10
    }
});
