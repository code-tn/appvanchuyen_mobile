import React, { Component } from 'react';
import { View, Image, RefreshControl, Alert, Platform, StatusBar } from 'react-native';
import { Container, Content, Text, Button, Icon } from 'native-base';
import Styles from '../../Assets/Styles';
import UserInfoStyles from '../Assets/UserInfoStyles';
import TopBarOnlyTitle from '../../Shared/TopBarOnlyTitle';
import FormatCurrency from '../../Assets/FormatCurrency';
//icon
import iconHome from '../Assets/Images/icon_1.png';
import iconPhone from '../Assets/Images/icon_2.png';
import iconMail from '../Assets/Images/icon_3.png';
import iconMoney from '../Assets/Images/icon_8.png';
import iconMarker from '../Assets/Images/icon_9.png';
import Colors from '../../Assets/Colors';
import { AuthShiper } from '../../../Networking/auth';
import Loading from '../../Shared/Loading';

class ShipperInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSource: '',
            userID: '',
            refreshing: false,
            dataShipper: '',
            isLoading: true,
        };
    }
    async componentWillMount() {
        const currentUser = await AuthShiper.currentUser();
        if (currentUser) {
            this.setState({ dataSource: currentUser });
        }
        await this._getShipperInfo();
    }
    
    _alert(title, content) {
        return (
            Alert.alert(
                title,
                content.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            )
        );
    }
    async _getShipperInfo() {
        await AuthShiper.getShipperInfo(this.state.dataSource.id, (res) => {
            if (res.success && res.data !== undefined) {
                this.setState({
                    dataShipper: res.data,
                    isLoading: false,
                    refreshing: false
                });
            }
        }, (e) => {
            this._alert('Lỗi', e.toString());
            this.setState({
                isLoading: false,
                refreshing: false
            });
        });
    }
    async _logout() {
        await AuthShiper.logout(() => {
            this.props.navigation.navigate('StaffLoginScreen');
        }, () => {
            this._alert('Lỗi', 'Mạng chậm, không thể đăng xuất!');
        });
    }
    _onRefresh = async () => {
        this.setState({ refreshing: true });
        await this._getShipperInfo();
    }

    render() {
        const {
            infoBox,
            topBox,
            boxTitle,
            infoItem,
            infoText,
            iconStyle,
            buttonBox,
            btnLogout
        } = UserInfoStyles;
        const { dataShipper } = this.state;
        if (this.state.isLoading) {
            return <Loading />;
        }
        return (
            <Container style={Styles.fontFace}>
                <TopBarOnlyTitle title='Thông tin Shipper' />
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end', padding: 10 }}>
                    <Button
                        success
                        iconRight
                        onPress={() => this.props.navigation.navigate('CreateDepositNoteScreen', {
                            sodu_taikhoan: dataShipper.sodu_taikhoan,
                            id_vanchuyen: dataShipper.id
                        })}
                        style={{
                            height: 'auto',
                            paddingHorizontal: 15,
                            paddingTop: 10,
                            paddingBottom: 10,
                        }}
                    >
                        <Text style={{ paddingRight: 5, paddingLeft: 0 }}>Đóng Tiền</Text>
                        <Icon type='FontAwesome' name='money' style={{ fontSize: 15, marginRight: 0, }} />
                    </Button>
                </View>
                <Content
                    style={Styles.content}
                    padder
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                >
                    <View style={infoBox}>
                        <View style={topBox}>
                            <Text style={boxTitle}>
                                THÔNG TIN CƠ BẢN
                            </Text>
                        </View>
                        <View style={infoItem}>
                            <Image
                                style={iconStyle}
                                source={iconHome}
                            />
                            <Text style={infoText}>
                                Họ tên: {dataShipper.surname}
                            </Text>
                        </View>
                        <View style={infoItem}>
                            <Image
                                style={iconStyle}
                                source={iconPhone}
                            />
                            <Text style={infoText}>
                                Điện thoại: {dataShipper.sodienthoai}
                            </Text>
                        </View>
                        <View style={infoItem}>
                            <Image
                                style={iconStyle}
                                source={iconMail}
                            />
                            <Text style={infoText}>
                                Email: {dataShipper.email}
                            </Text>
                        </View>
                        <View style={infoItem}>
                            <Image
                                style={iconStyle}
                                source={iconMarker}
                            />
                            <Text style={infoText}>
                                Khu vực hoạt động: {dataShipper.khuvuc_giaohang_formatted}
                            </Text>
                        </View>

                        <View style={infoItem}>
                            <Image
                                style={iconStyle}
                                source={iconMoney}
                            />
                            <Text style={infoText}>
                                Tiền nộp công ty: <Text style={[infoText, { color: Colors.mainColor, fontWeight: 'bold' }]}>{FormatCurrency(dataShipper.sodu_taikhoan)} đ</Text>
                            </Text>
                        </View>

                        <View style={infoItem}>
                            <Image
                                style={iconStyle}
                                source={iconMoney}
                            />
                            <Text style={infoText}>
                                Lương hiện tại: <Text style={[infoText, { color: Colors.mainColor, fontWeight: 'bold' }]}>{FormatCurrency(dataShipper.salary)} đ</Text>
                            </Text>
                        </View>
                    </View>
                    <View style={buttonBox}>
                        <Button
                            style={btnLogout}
                            transparent
                            onPress={() => this._logout()}
                        >
                            <Text uppercase={false} style={{ color: Colors.blackColor, fontSize: 15 }}>Đăng xuất</Text>
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }
}

export default ShipperInfo;
