import React, { Component } from 'react';
import { View, Image, Alert } from 'react-native';
import { Container, Content, Text, Button } from 'native-base';
import Styles from '../../Assets/Styles';
import UserInfoStyles from '../Assets/UserInfoStyles';
import TopBarOnlyTitle from '../../Shared/TopBarOnlyTitle';
//icon
import iconHome from '../Assets/Images/icon_1.png';
import iconPhone from '../Assets/Images/icon_2.png';
import iconMail from '../Assets/Images/icon_3.png';
import Colors from '../../Assets/Colors';
import { AuthShiper } from '../../../Networking/auth';

class StockerInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSource: '',
            userID: ''
        };
    }

    async componentWillMount() {
        const currentUser = await AuthShiper.currentUser();
        if (currentUser) {
            this.setState({ dataSource: currentUser });
        }
    }

    async _logout() {
        await AuthShiper.logout(() => {
            this.props.navigation.navigate('StaffLoginScreen');
        }, () => {
            Alert.alert(
                'Lỗi',
                'Mạng chậm, không thể đăng xuất!',
                [
                    {
                        text: 'Cancel',
                        style: 'cancel',
                    },
                ],
                { cancelable: false },
            );
        });
    }
    render() {
        const {
            infoBox,
            topBox,
            boxTitle,
            infoItem,
            infoText,
            iconStyle,
            buttonBox,
            btnLogout
        } = UserInfoStyles;
        const { dataSource } = this.state;
        return (
            <Container style={Styles.fontFace}>
                <TopBarOnlyTitle title='Thông tin thủ kho' />
                <Content style={Styles.content} padder>
                    <View style={infoBox}>
                        <View style={topBox}>
                            <Text style={boxTitle}>
                                THÔNG TIN CƠ BẢN
                            </Text>
                        </View>
                        <View style={infoItem}>
                            <Image
                                style={iconStyle}
                                source={iconHome}
                            />
                            <Text style={infoText}>
                                Họ tên: {dataSource.surname}
                            </Text>
                        </View>
                        <View style={infoItem}>
                            <Image
                                style={iconStyle}
                                source={iconPhone}
                            />
                            <Text style={infoText}>
                                Điện thoại: {dataSource.sodienthoai}
                            </Text>
                        </View>
                        <View style={infoItem}>
                            <Image
                                style={iconStyle}
                                source={iconMail}
                            />
                            <Text style={infoText}>
                                Email: {dataSource.email}
                            </Text>
                        </View>
                    </View>
                    <View style={buttonBox}>
                        <Button
                            style={btnLogout}
                            transparent
                            onPress={() => this._logout()}
                        >
                            <Text uppercase={false} style={{ color: Colors.blackColor, fontSize: 14 }}>Đăng xuất</Text>
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }
}

export default StockerInfo;
