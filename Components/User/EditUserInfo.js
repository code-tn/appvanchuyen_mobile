import React, { Component } from 'react';
import { View, Alert } from 'react-native';
import { Container, Content, Button, Text } from 'native-base';
import TopBar from '../Shared/TopBar';
import UserForm from './Partials/UserForm';
import Styles from '../Assets/Styles';

import { Auth } from '../../Networking/auth';

class EditUserInfo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            dataSource: null
        };
    }

    componentDidMount() {
        setTimeout(async () => {
            this.setState({
                dataSource: await Auth.currentUser(),
                loading: false
            });
        }, 3000);
    }

    async _onSubmitForm() {
        const { dataSource } = this.state;
        const res = await Auth.updateProfile(null, {
            ten_cuahang: dataSource.ten_cuahang,
            email: dataSource.email,
            dienthoai: dataSource.dienthoai,
            so_cmnd: dataSource.so_cmnd,
            hoten: dataSource.hoten,
            gioitinh: dataSource.gioitinh
        });
        if (res.success) {
            Alert.alert(
                'Thông báo',
                'Cập nhật thông tin thành công!',
                [
                    {
                        text: 'OK', 
                        onPress: () => {
                            this.props.navigation.state.params.onGoBack();
                            this.props.navigation.goBack();
                        }
                    },
                ],
                { cancelable: false },
            );
        } else {
            Alert.alert(
                'Lỗi',
                res.message,
                [
                    {
                        text: 'OK', 
                        onPress: () => {
                        }
                    },
                ],
                { cancelable: false },
            );
        }
    }

    render() {
        const { navigation } = this.props;
        const { loading } = this.state;
        const { btnSubmit, fontFace } = Styles;
        return (
            <Container style={fontFace}>
                <TopBar navigation={navigation} title='Sửa thông tin' />
                <Content padder style={{ backgroundColor: '#eee' }}>
                    {
                        loading ?
                            (<Text>Đang tải...</Text>) :
                            (
                                <View>
                                    <UserForm parent={this} />
                                    <View>
                                        <Button block style={btnSubmit} onPress={() => this._onSubmitForm()} >
                                            <Text style={{ color: '#fff' }}>Lưu thay đổi</Text>
                                        </Button>
                                    </View>
                                </View>
                            )
                    }
                </Content>
            </Container>
        );
    }
}

export default EditUserInfo;
