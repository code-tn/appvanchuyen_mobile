import React, { Component } from 'react';
import { View, FlatList, RefreshControl, ActivityIndicator, Alert, Platform, StatusBar } from 'react-native';
import {
    Container,
    Text,
} from 'native-base';
import Styles from '../../Assets/Styles';
import TopBarOnlyTitle from '../../Shared/TopBarOnlyTitle';
import BillItem from './partials/BillItem';
import Loading from '../../Shared/Loading';
import { Order } from '../../../Networking/order';
import Colors from '../../Assets/Colors';

class StockImportRequest extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSource: '',
            dataList: [],
            refreshing: false,
            isLoading: true,
            loadmore: false,
        };
    }

    componentDidMount() {
        this._getNewImportStockNote();
    }
    async _getNewImportStockNote(params = null) {
        await Order.getStockImportRequest(params, (responseJson) => {
            if (responseJson.success && typeof responseJson.data !== 'undefined') {
                let data = null;
                if (params !== null) {
                    data = [...this.state.dataList, ...responseJson.data.data];
                } else {
                    data = responseJson.data.data
                }
                this.setState({
                    dataSource: responseJson.data,
                    dataList: data,
                    isLoading: false,
                    refreshing: false,
                    loadmore: false,
                });
            } else {
                Alert.alert(
                    'Lỗi',
                    responseJson.message,
                    [
                        {text: 'OK'},
                    ],
                    {cancelable: false},
                );
            }
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    {text: 'OK'},
                ],
                {cancelable: false},
            );
            this.setState({
                isLoading: false,
                loadmore: false,
            });
        });
    }

    _onRefresh = async() => {
        this.setState({
            refreshing: true,
            dataList: []
        });
        await this._getNewImportStockNote();
    }
    _afterAction = async() => {
        this.setState({
            refreshing: true,
        });
        await this._getNewImportStockNote();
    }
    handleLoadMore = async() => {
        if (this.state.dataSource.current_page < this.state.dataSource.last_page) {
            this.setState({ loadmore: true });
            const params = {
                request_url: this.state.dataSource.next_page_url
            }
            await this._getNewImportStockNote(params);
        }

    }
    render() {
        const {
            fontFace,
            content
        } = Styles;
        const {
            dataSource,
            dataList,
            isLoading
        } = this.state;
        const { navigation } = this.props;
        if (isLoading) {
            return <Loading />;
        }
        return (
            <Container style={fontFace}>
                <TopBarOnlyTitle title='Yêu cầu nhập kho' />
                <View style={[content, { flex: 1, padding: 10 }]}>
                    {(dataSource.data === null || 
                        typeof dataSource.data === 'undefined' || 
                        dataSource.data.length === 0) || 
                        this.state.refreshing ? 
                        <Text style={{ fontSize: 14, textAlign: 'center', marginBottom: 10, }}> {this.state.refreshing ? 'Đang tải danh sách...' : 'Không có phiếu nhập nào'} </Text>
                    : null}
                    <FlatList
                        style={{ flex: 1 }}
                        const
                        data={dataList}
                        renderItem={({ item }) =>
                            <BillItem
                                navigation={navigation}
                                order={item}
                                confirmAction={true}
                                afterAction={() => this._afterAction()}
                            />
                        }
                        keyExtractor={(item) => item.id.toString()}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={0.1}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this._onRefresh}
                            />
                        }
                    />
                    {this.state.loadmore ? <ActivityIndicator size='small' color={Colors.mainColor} /> : null}
                </View>
            </Container>
        );
    }
}
export default StockImportRequest;
