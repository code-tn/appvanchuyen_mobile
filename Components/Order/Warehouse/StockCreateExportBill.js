import React, { Component } from 'react';
import { View, Alert, TextInput, ActivityIndicator } from 'react-native';
import { Button, Text, Container, Content } from 'native-base';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import Colors from '../../Assets/Colors';
import Styles from '../../Assets/Styles';
import TopBar from '../../Shared/TopBar';
import { Order } from '../../../Networking/order';


class StockCreateExportBill extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            id_donhang: '',
            id_vanchuyen: '',
            id_kho: '',
            id_thukho: '',
            tinhtrang: 16,
            ngaytao: '',
            ghichu: '',
            isLoading: false,

            shipperList: '',
        };
    }
    componentDidMount = async () => {
        await this.setState({ id_donhang: this.props.navigation.getParam('id_donhang') });
        await this._getShipperList();
    }

    warningAlert(message) {
        return (
            Alert.alert(
                'Lỗi',
                message,
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            )
        );
    }
    _getShipperList = async () => {
        await Order.getShippersByAddressOrder(this.state.id_donhang, (res) => {
            if (res.success && typeof res.data !== 'undefined') {
                this.setState({
                    shipperList: res.data
                });
            } else {
                this.warningAlert(res.message);
            }
        }, (e) => {
            this.warningAlert(e.toString());
        });
    }
    _onSelectedItemsChange = (selectedItems) => {
        this.setState({ id_vanchuyen: selectedItems });
    };
    _submit = () => {
        if (this.state.id_vanchuyen === '') {
            Alert.alert(
                'Thông báo',
                'Bạn chưa chọn NV Giao hàng',
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
        } else {
            this._confirmSubmit();
        }
    }
    _confirmSubmit = async () => {
        this.setState({ isLoading: true });
        const data = {
            id_donhang: this.state.id_donhang,
            id_vanchuyen: this.state.id_vanchuyen[0],
            id_kho: 1,
            tinhtrang: 16,
            ghichu: this.state.ghichu
        };
        await Order.createStockExportRequest(data, (res) => {
            if (res.success) {
                Alert.alert(
                    'Thông báo',
                    'Tạo phiếu xuất kho thành công',
                    [
                        {
                            text: 'OK',
                            onPress: () => {
                                this.props.navigation.state.params.onNavigationBack();
                                this.props.navigation.goBack();
                            }
                        },
                    ],
                    { cancelable: false },
                );
                this.setState({ isLoading: false });
            } else {
                Alert.alert('Thông báo',
                    res.message,
                    [{ text: 'Ok', style: 'cancel' }],
                    { cancelable: false }
                );
            }
        }, (e) => {
            Alert.alert('Thông báo',
                e.toString(),
                [{ text: 'Ok', style: 'cancel' }],
                { cancelable: false }
            );
            this.setState({ isLoading: false });
        });
    }
    render() {
        const {
            textarea,
            btnSubmit,
            fontFace,
            content,
        } = Styles;
        const {
            ghichu,
        } = this.state;
        const items = [
            {
                surname: 'Shipper nhận hàng',
                id: 0,
                // these are the children or 'sub items'
                children: this.state.shipperList.shipper_nhan
            },
            {
                surname: 'Shipper giao hàng',
                id: 1,
                // these are the children or 'sub items'
                children: this.state.shipperList.shipper_giao
            }
        ];
        return (
            <Container style={fontFace}>
                <TopBar title='Tạo phiếu xuất kho' navigation={this.props.navigation} />
                <Content style={content} padder>
                    <Text style={{ marginBottom: 5 }}>Mã vận đơn: #{this.state.id_donhang}</Text>
                    <View style={{ backgroundColor: '#fff', borderWidth: 1, borderColor: '#c8c8c8', borderRadius: 3, marginBottom: 10, height: 60, overflow: 'hidden' }}>
                        <SectionedMultiSelect
                            items={items}
                            uniqueKey='id'
                            displayKey='surname'
                            selectText='NV giao hàng'
                            showDropDowns
                            onSelectedItemsChange={this._onSelectedItemsChange}
                            selectedItems={this.state.id_vanchuyen}
                            single
                            readOnlyHeadings
                            subKey="children"
                            confirmText='Đóng'
                            searchPlaceholderText='Tìm kiếm NV giao hàng...'
                            noResultsComponent={
                                <Text style={{ textAlign: 'center', color: Colors.blackColor, fontSize: 14, paddingVertical: 4 }}>Không tìm thấy...</Text>
                            }
                        />
                    </View>
                    <TextInput
                        style={textarea}
                        placeholder='Ghi chú...'
                        multiline
                        textAlignVertical='top'
                        numberOfLines={4}
                        onChangeText={(text) => this.setState({ ghichu: text })}
                        value={ghichu}
                    />
                    <Button
                        block
                        style={btnSubmit}
                        onPress={() => this._submit()}
                    >
                        {this.state.isLoading ? <ActivityIndicator size='small' color='#fff' /> : null}
                        <Text>Xác nhận</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

export default StockCreateExportBill;
