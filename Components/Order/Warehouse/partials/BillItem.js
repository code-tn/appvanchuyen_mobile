import React, { Component } from 'react';
import { View, TouchableOpacity, Alert, ActivityIndicator } from 'react-native';
import { Text, Button, Icon } from 'native-base';
import BillItemStyles from '../Assets/BillItemStyles';
import Colors from '../../../Assets/Colors';
import { Order } from '../../../../Networking/order';

class BillItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false,
            isLoading: false
        };
    }
    componentDidMount() {
        this.setState({
            action: this.props.confirmAction
        });
    }
    _confirmSubmit = () => {
        Alert.alert(
            'Thông báo',
            'Xác nhận yêu cầu nhập kho',
            [
                { text: 'OK', onPress: () => this._submit() },
                {
                    text: 'Hủy',
                    style: 'cancel',
                },
            ],
            { cancelable: false },
        );
    }
    _submit = async() => {
        this.setState({ isLoading: true });
        const data = {
            id_vanchuyen: this.props.order.id_vanchuyen,
            id_donhang: this.props.order.id_donhang,
            id_kho: 1,
            tinhtrang: this.props.order.tinhtrang === "18" ? 19 : 15
        }
        await Order.confirmImportStockRequest(this.props.order.id, data, (res)=>{
            if(res.success && res.data !== undefined) {
                Alert.alert(
                    'Xác nhận',
                    'Xác nhận nhập kho thành công',
                    [
                        { text: 'OK', onPress: () => 
                            {
                                this.props.afterAction();
                                this.setState({ isLoading: false });
                            }
                        },
                    ],
                    { cancelable: false },
                );
            } else {
                Alert.alert('Thông báo',
                    'Không thể kết nối tới máy chủ, vui lòng thử lại sau!',
                    [{ text: 'Ok', style: 'cancel' }],
                    { cancelable: false }
                );
                this.setState({ isLoading: false });
            }
        }, (e) => {
            Alert.alert('Thông báo',
                'Không thể kết nối tới máy chủ, vui lòng thử lại sau!',
                [{ text: 'Ok', style: 'cancel' }],
                { cancelable: false }
            );
            this.setState({ isLoading: false });
        })
    }
    render() {
        const {
            id,
            id_donhang,
            ten_trangthai,
            ghichu,
            ten_vanchuyen,
            updated_at,
        } = this.props.order;
        const {
            billItem,
            idStyle,
            text,
            line
        } = BillItemStyles;
        return (
            <View style={billItem}>
                <Text style={text}> Mã phiếu: <Text style={idStyle}>#{id}</Text></Text>
                <Text style={text}> Mã vận đơn: <Text style={{ color: Colors.subColor }}>#{id_donhang}</Text></Text>
                <Text style={text}> NV Giao hàng: {ten_vanchuyen} </Text>
                <Text style={text}> Ghi chú: {ghichu} </Text>
                <Text style={[text, line]}> Tình trạng: <Text style={[text, { color: Colors.subColor, fontWeight: 'bold' }]}>{ten_trangthai}</Text> </Text>
                <Text style={[text, line]}> Cập nhật: {updated_at}</Text>
                {
                    this.state.action ? (
                        <View style={[line, { flexDirection: 'row', justifyContent: 'flex-end' }]}>
                            <Button
                                success
                                iconRight
                                style={{ height: 'auto' }}
                                onPress={() => this._confirmSubmit()}
                            >   
                                {this.state.isLoading ? <ActivityIndicator size='small' color='#fff' /> : null}
                                <Text uppercase={false} style={{ fontSize: 15, paddingRight: 4, paddingLeft: 10 }}>
                                    Xác nhận
                                </Text>
                                <Icon type='FontAwesome' name='check-circle' style={{ fontSize: 15, marginRight: 10, }} />
                            </Button>
                        </View>
                    ) : null
                }
                
            </View>
        );
    }
}

export default BillItem;
