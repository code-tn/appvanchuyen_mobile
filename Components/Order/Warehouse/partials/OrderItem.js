import React, { PureComponent } from 'react';
import { View, Image } from 'react-native';
import { Text, Button, Icon } from 'native-base';
import truckIcon from '../Assets/Images/truck.png';
import OrderItemStyles from '../Assets/OrderItemStyles';
import Styles from '../../../Assets/Styles';
import Colors from '../../../Assets/Colors';

class OrderItem extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        const {
            boldStyle,
            line,
            orderItem,
            orderItemCode,
            orderItemCodeIcon,
            orderItemCodeText,
            orderItemText,
            orderItemControl,
        } = OrderItemStyles;
        const {
            id,
            ten_kienhang,
            trangthai,
            ten_trangthai,
            ghichu,
            created_at,
            hoantrahang,
        } = this.props.order;
        const { navigation } = this.props;
        const {
            btn,
            btn__icon,
            btn__text
        } = Styles;
        let tinh, phuong, huyen, chitiet, ttNguoiNhan;
        if (typeof this.props.order.diachi_nguoinhan !== 'undefined') {
            ttNguoiNhan = this.props.order.diachi_nguoinhan[0];
            chitiet = ttNguoiNhan.chitiet + ', ';
            tinh = ttNguoiNhan.tinhthanhpho[0].name;
            huyen = ttNguoiNhan.quanhuyen[0].name;
            phuong = ttNguoiNhan.xaphuong[0].name;
        } else {
            chitiet = '';
            tinh = this.props.order.tinhthanhpho;
            huyen = this.props.order.quanhuyen;
            phuong = this.props.order.phuong;
        }
        return (
            <View style={orderItem}>
                <View style={orderItemCode}>
                    <Image source={truckIcon} style={orderItemCodeIcon} />
                    <Text style={orderItemCodeText}>#{id}</Text>
                </View>
                {typeof hoantrahang !== 'undefined' && hoantrahang === 1 ?
                    <Text style={[orderItemText, { color: '#ca0e00' }]}>
                        <Icon type='FontAwesome' name='refresh' style={{ color: '#ca0e00', fontSize: 17 }}></Icon>
                        {' '}Khách yêu cầu hoàn trả
                </Text> : null}
                <Text style={orderItemText}>Địa chỉ: {chitiet} {phuong}, {huyen}, {tinh}</Text>
                <Text style={orderItemText}>Ngày tạo: {created_at}</Text>
                <Text style={orderItemText}>Ghi chú: {ghichu}</Text>
                <Text style={[orderItemText, line]}>
                    Tên kiện hàng: {ten_kienhang}
                </Text>
                <Text style={[orderItemText, boldStyle, line]}>
                    Trang thái: {ten_trangthai}
                </Text>
                <View
                    style={
                        [
                            orderItemControl, line,
                            {
                                paddingTop: 10,
                                flexDirection: 'row',
                                justifyContent: 'flex-end',
                                alignItems: 'center',
                            }
                        ]
                    }
                >
                    {trangthai === '16' ? (
                        null
                    ) : (
                            <Button
                                success
                                iconRight
                                transparent
                                style={btn}
                                onPress={() => this.props.navigation.navigate('StockCreateExportBillScreen', {
                                    id_donhang: this.props.order.id,
                                    onNavigationBack: this.props.parent._onRefresh
                                })}
                            >
                                <Text style={btn__text} uppercase={false} >Xuất Kho</Text>
                                <Icon name='add' style={btn__icon} />
                            </Button>
                        )}
                    <Button
                        success
                        iconRight
                        transparent
                        style={[btn, { marginLeft: 10 }]}
                        onPress={() => navigation.navigate('StockOrderDetailScreen', {
                            idOrder: id
                        })}
                    >
                        <Text uppercase={false} style={btn__text}>Xem chi tiết</Text>
                        <Icon type='FontAwesome' name='eye' style={btn__icon} />
                    </Button>
                </View>
            </View>
        );
    }
}

export default OrderItem;
