import React, { Component } from 'react';
import { FlatList, RefreshControl, ActivityIndicator, View, Alert } from 'react-native';
import {
    Container,
    Text
} from 'native-base';
import Styles from '../../Assets/Styles';
import TopBarOnlyTitle from '../../Shared/TopBarOnlyTitle';
import Search from '../../Shared/Search';
import BillItem from './partials/BillItem';
import Loading from '../../Shared/Loading';
import { Order } from '../../../Networking/order';
import Colors from '../../Assets/Colors';

class StockImportBills extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSource: '',
            dataList: [],
            refreshing: false,
            isLoading: true,
            loadmore: false,

        };
    }

    componentDidMount() {
        this._getImportedStockBills();
    }
    _getImportedStockBills = async (params = null) => {
        await Order.getStockImportedNote(params, (responseJson) => {
            if (responseJson.success && typeof responseJson.data !== 'undefined') {
                let data = null;
                if (params !== null) {
                    data = [...this.state.dataList, ...responseJson.data.data];
                } else {
                    data = responseJson.data.data
                }
                this.setState({
                    dataSource: responseJson.data,
                    dataList: data,
                    isLoading: false,
                    refreshing: false,
                    loadmore: false
                });
            } else {
                Alert.alert(
                    'Lỗi',
                    responseJson.message,
                    [
                        { text: 'OK'},
                    ],
                    { cancelable: false },
                );
            }
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    { text: 'OK'},
                ],
                { cancelable: false },
            );
            this.setState({
                isLoading: false,
                refreshing: false,
                loadmore: false
            });
        });
    }
    _onRefresh = () => {
        this.setState({ refreshing: true, dataList: [] });
        this._getImportedStockBills();
    }
    handleLoadMore = async () => {
        if (this.state.dataSource.current_page < this.state.dataSource.last_page) {
            this.setState({ loadmore: true });
            const params = {
                request_url: this.state.dataSource.next_page_url
            }
            await this._getImportedStockBills(params);
        }

    }
    render() {
        const {
            fontFace,
            content
        } = Styles;
        const {
            dataSource,
            dataList,
            isLoading
        } = this.state;
        const { navigation } = this.props;
        if (isLoading) {
            return <Loading />;
        }
        return (
            <Container style={[fontFace, { backgroundColor: '#eee' }]}>
                <TopBarOnlyTitle title='Phiếu nhập kho' />
                <View style={[content, { flex: 1, padding: 10 }]}>
                    {(dataSource.data === null || typeof dataSource.data === 'undefined' || dataSource.data.length === 0) || this.state.refreshing ?
                        <Text style={{ fontSize: 14, textAlign: 'center', marginBottom: 10, }}> {this.state.refreshing ? 'Đang tải danh sách...' : 'Không có phiếu nhập nào'} </Text>
                        : null}
                    <FlatList
                        style={{ flex: 1 }}
                        const
                        data={dataList}
                        renderItem={({ item }) => <BillItem navigation={navigation} order={item} />}
                        keyExtractor={(item) => item.id.toString()}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={0.1}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this._onRefresh}
                            />
                        }
                    />
                    {this.state.loadmore ? <ActivityIndicator color={Colors.mainColor} size='small' /> : null}
                </View>
            </Container>
        );
    }
}
export default StockImportBills;
