import React, { Component } from 'react';
import { View, FlatList, RefreshControl, ActivityIndicator, Alert } from 'react-native';
import {
    Container,
    Text
} from 'native-base';
import Styles from '../../Assets/Styles';
import TopBarOnlyTitle from '../../Shared/TopBarOnlyTitle';
import Search from '../../Shared/Search';
import BillItem from './partials/BillItem';
import Loading from '../../Shared/Loading';
import { Order } from '../../../Networking/order';
import Colors from '../../Assets/Colors';

class StockExportBills extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSource: '',
            dataList: [],
            refreshing: false,
            isLoading: true,
            loadmore: false,
            
        };
    }

    componentDidMount() {
        this._getExportStockBills();
    }
    
    warningAlert(message) {
        return (
            Alert.alert(
                'Lỗi',
                message,
                [
                    {text: 'OK'},
                ],
                {cancelable: false},
            )
        );
    }
    _getExportStockBills = async(params = null) => {
        await Order.getStockExportNote(params, (responseJson) => {
            if (responseJson.success && typeof responseJson.data !== 'undefined') {
                let data = null;
                if (params !== null) {
                    data = [...this.state.dataList, ...responseJson.data.data];
                } else {
                    data = responseJson.data.data
                }
                this.setState({ 
                    dataSource: responseJson.data, 
                    dataList: data,
                    isLoading: false, 
                    refreshing: false,
                    loadmore: false
                });
            }
        }, (e) => {
            this.warningAlert(e.toString());
            this.setState({
                isLoading: false,
                refreshing: false,
                loadmore: false
            });
        });
    }
    _onRefresh = () => {
        this.setState({ refreshing: true, dataList: [] });
        this._getExportStockBills();
    }
    handleLoadMore = async () => {
        if (this.state.dataSource.current_page < this.state.dataSource.last_page) {
            this.setState({ loadmore: true });
            const params = {
                request_url: this.state.dataSource.next_page_url
            }
            await this._getExportStockBills(params);
        }

    }
    render() {
        const {
            fontFace,
            content
        } = Styles;
        const {
            dataSource,
            dataList,
            isLoading
        } = this.state;
        const { navigation } = this.props;
        if (isLoading) {
            return <Loading />;
        }
        return (
            <Container style={[fontFace, { backgroundColor: '#eee' }]}>
                <TopBarOnlyTitle title='Phiếu Xuất kho' />
                <View style={[content, { flex: 1, padding: 10 }]}>
                    {(dataSource.data === null || typeof dataSource.data === 'undefined' || dataSource.data.length === 0) || this.state.refreshing ? 
                        <Text style={{ fontSize: 14, textAlign: 'center', marginBottom: 10, }}> {this.state.refreshing ? 'Đang tải danh sách...' : 'Không có phiếu xuất kho nào'} </Text>
                    : null }
                    <FlatList
                        const
                        data={dataList}
                        renderItem={({ item }) => <BillItem navigation={navigation} order={item} />}
                        keyExtractor={(item) => item.id.toString()}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={0.1}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this._onRefresh}
                            />
                        }
                    />
                    {this.state.loadmore ? <ActivityIndicator color={Colors.mainColor} size='small' /> : null}
                </View>
            </Container>
        );
    }
}
export default StockExportBills;
