import React, { Component } from 'react';
import { RefreshControl, SafeAreaView } from 'react-native';
import { Button, Container, Content, Icon, Text, View } from 'native-base';
import TopBar from '../../Shared/TopBar';
import Styles from '../../Assets/Styles';
import OrderDetailContent from '../Partials/OrderDetailContent';
import Loading from '../../Shared/Loading';
import { Order } from '../../../Networking/order';
import { AuthShiper } from '../../../Networking/auth';
import Colors from '../../Assets/Colors';

class StockOrderDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            _order_id: null,
            dataSource: '',
            dataReceiptNote: '',
            dataDeliveryNote: '',
            dataRefundNote: '',
            dataStockInputNote: '',
            userType: '',
            idShipper: '',
            isLoading: true,
            fab_status: false,
            id_vanchuyen: '',
            refreshing: false,
        };
    }
    componentWillMount() {
        this.setState({
            _order_id: this.props.navigation.getParam('idOrder')
        });
    }
    async componentDidMount() {
        await this.setState({ isLoading: true });
        // LOAD ORDER DETAIL
        await this._loadOrderDetail();
        // LOAD RECEIPT BILLS
        await this._loadOrderReceiptBills();
        // LOAD DELIVER BILLS
        await this._loadOrderDeliveryBills();
        // LOAD REFUND BILLS
        await this._loadOrderRefundBills();
        // LOAD STOCK INPUT BILLS
        await this._loadStockInputBills();
        //get id shipper
        await this._getShipperID();

        //this.setState({ isLoading: false });
    }

    _onRefresh = async() => {
        this.setState({ refreshing: true });
        await this._loadOrderDetail();
        await this._loadOrderReceiptBills();
        await this._loadOrderRefundBills();
        await this._loadStockInputBills();
        await this.setState({ refreshing: false });

    };

    async _loadOrderDetail(){
        await Order.getDetail(this.state._order_id, (responseJson) => {
            if (typeof responseJson.data !== 'undefined') {
                this.setState({
                    refreshing: false, 
                    dataSource: responseJson.data 
                });
            }
        });
    }

    async _loadOrderReceiptBills(){
        await Order.getOrderReceiptBills(this.state._order_id, (responseJson) => {
            if (typeof responseJson.data !== 'undefined') {
                this.setState({ dataReceiptNote: responseJson.data });
            }
        });
    }

    async _loadOrderDeliveryBills(){
        await Order.getOrderDeliveryBills(this.state._order_id, (responseJson) => {
            if (typeof responseJson.data !== 'undefined') {
                this.setState({ dataDeliveryNote: responseJson.data });
            }
        });
    }

    async _loadOrderRefundBills(){
        await Order.getOrderRefundBills(this.state._order_id, (responseJson) => {
            if (typeof responseJson.data !== 'undefined') {
                this.setState({ dataRefundNote: responseJson.data });
            }
        });
    }

    async _loadStockInputBills(){
        await Order.getStockInputNote(this.state._order_id, (responseJson) => {
            if (typeof responseJson.data !== 'undefined') {
                this.setState({ dataStockInputNote: responseJson.data, isLoading: false });
            }
        });
    }

    async _getShipperID(){
        const currentUser = await AuthShiper.currentUser();
        if (currentUser) {
            this.setState({ id_vanchuyen: currentUser.id });
        }
    }
    
    
    render_content() {
        return (
            <Content 
                padder 
                style={Styles.content}  
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh}
                    />
                }
            >
                <OrderDetailContent
                    dataSource={this.state.dataSource}
                    dataReceiptNote={this.state.dataReceiptNote}
                    dataDeliveryNote={this.state.dataDeliveryNote}
                    dataRefundNote={this.state.dataRefundNote}
                />
            </Content>
        );
    }
    render() {
        // SHOW LOADING SPINNER
        if (this.state.isLoading) {
            return (<Loading/>);
        }
        return (
            <Container>
                <TopBar title='Chi tiết đơn hàng' navigation={this.props.navigation}/>
                <View style={{ paddingHorizontal: 10, paddingVertical: 7, backgroundColor: '#fff', borderBottomWidth: 2, borderBottomColor: '#eee', flexDirection: 'row', justifyContent: 'flex-end', }}>
                    { this.state.dataSource.trangthai === "16" && this.state.dataSource !== '' ? 
                        (
                            <View>
                                <Text>Đang chờ xuất kho <Icon type='FontAwesome' name='clock-o' style={{ fontSize: 16, color: Colors.mainColor }} /></Text>
                            </View>
                        ) : 
                    (
                        <Button
                            danger
                            iconRight
                            style={{ height: 'auto' }}
                            onPress={() => this.props.navigation.navigate('StockCreateExportBillScreen', {
                                id_donhang: this.state.dataSource.id,
                                onNavigationBack: this._onRefresh
                            })}
                        >
                            <Text style={{ fontSize: 14, paddingLeft: 10, paddingRight: 5 }}>Xuất Kho</Text>
                            <Icon name='add' style={{ fontSize: 18, marginRight: 10, }} />
                        </Button>
                    ) }
                    
                </View>
                <SafeAreaView style={{ flex:1 }}>
                    {this.render_content()}
                </SafeAreaView>
            </Container>
        );
    }
}

export default StockOrderDetails;
