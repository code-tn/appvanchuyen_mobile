import React, { Component } from 'react';
import { View, FlatList, RefreshControl, ActivityIndicator, Alert, Modal } from 'react-native';
import {
    Container,
    Text,
    Button,
    Icon,
    Item,
    Picker,
    Label
} from 'native-base';
import OrderItem from './partials/OrderItem';
import TopBarOnlyTitle from '../../Shared/TopBarOnlyTitle';
import Styles from '../../Assets/Styles';
import { Order } from '../../../Networking/order';
import Loading from '../../../Components/Shared/Loading';
import Colors from '../../Assets/Colors';
import { getProvinceList, getDistrictByProvinceID, getWardByDistrictID } from '../../../Data/locationRepo';
import { searchStockerOrder } from '../../../Data/orderRepo';

class StockOrders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoadMore: false,
            id_vanchuyen: '',
            data: '',
            dataList: [],
            isLoading: true,
            refreshing: false,
            modalVisible: false,
            //
            isFilter: false,
            isSelect: false,
            provinceList: '',
            districtList: '',
            wardList: '',
            filter: {
                province: '77',
                district: '',
                ward: ''
            }
        };
    }
    async componentDidMount() {
        this.setState({ id_vanchuyen: 2 });
        await this._loadStockOrders();
        await this.loadLocationSelect();
    }
    async _loadStockOrders(params = null) {
        await Order.getStockOrders(params, (responseJson) => {
            if (responseJson.success && typeof responseJson.data !== 'undefined') {
                let data = null;
                if (params !== null) {
                    data = [...this.state.dataList, ...responseJson.data.data];
                } else {
                    data = responseJson.data.data
                }
                this.setState({
                    data: responseJson.data,
                    dataList: data,
                    isLoading: false,
                });
            } else {
                Alert.alert(
                    'Lỗi',
                    responseJson.message,
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
            }
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
            this.setState({
                isLoading: false,
            });
        });
    }

    _onRefresh = async () => {
        this.setState({ refreshing: true, dataList: [], isFilter: false, });
        await this._loadStockOrders();
        this.setState({ refreshing: false });
    };
    async loadLocationSelect() {
        await getProvinceList(res => {
            if (res.success) {
                this.setState({ provinceList: res.data });
            }
        });
        await getDistrictByProvinceID(this.state.filter.province, res => {
            if (res.success) {
                this.setState({
                    districtList: res.data,
                    filter: {
                        ...this.state.filter,
                        district: res.data[0].maqh
                    },
                });
            }
        });
        await getWardByDistrictID(this.state.districtList[0].maqh, res => {
            if (res.success) {
                this.setState({
                    wardList: res.data,
                    filter: {
                        ...this.state.filter,
                        ward: res.data[0].xaid
                    },
                });
            }
        });
    }
    warningAlert(message) {
        return (
            Alert.alert(
                'Lỗi',
                message,
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            )
        );
    }
    filterOrder = async () => {
        this.setState({ refreshing: true, dataList: [], modalVisible: false });
        if (this.state.filter.district === '') {
            this.warningAlert('Chưa chọn Quận/ Huyện hoặc đã có lỗi xảy ra.\nVui lòng thử lại sau ít phút.');
            return;
        }
        if (this.state.filter.ward === '') {
            this.warningAlert('Chưa chọn Phường/ Xã hoặc đã có lỗi xảy ra.\nVui lòng thử lại sau ít phút.');
            return;
        }
        await searchStockerOrder(this.state.filter, null, (res) => {
            if (res.success) {
                this.setState({
                    data: res.data,
                    dataList: res.data.data,
                    isFilter: true,
                    refreshing: false,
                });
            } else {
                this.warningAlert(res.massage);
                this.setState({
                    refreshing: false,
                });
            }
        }, (e) => {
            this.warningAlert(e.toString());
            this.setState({
                refreshing: false,
            });
        });
    }
    loadMoreFilter = async (params, request_url) => {
        this.setState({ isLoadMore: true });
        await searchStockerOrder(params, request_url, (res) => {
            if (res.success) {
                this.setState({
                    data: res.data,
                    dataList: [...this.state.dataList, ...responseJson.data.data],
                });
            } else {
                this.warningAlert(res.massage);
                this.setState({
                    isLoadMore: false,
                });
            }
        }, (e) => {
            this.warningAlert(e.toString());
            this.setState({
                isLoadMore: false,
            });
        });
    }
    handleLoadMore = async () => {
        if (this.state.data.current_page < this.state.data.last_page) {
            this.setState({ isLoadMore: true });
            if (this.state.isFilter) {
                const params = this.state.filter;
                this.loadMoreFilter(params, this.state.data.next_page_url);
            } else {
                let params = {
                    request_url: this.state.data.next_page_url
                };
                await this._loadStockOrders(params);
            }
            this.setState({ isLoadMore: false });
        }
    }
    renderFilter() {
        const {
            modalBody,
            modalDialog,
            modalHeader,
            btn__icon,
            btn__text,
            btnSubmit
        } = Styles;
        return (
            <Modal
                animationType="slide"
                transparent
                visible={this.state.modalVisible}
                onRequestClose={() => {
                    console.log('close');
                }}
            >
                <View style={modalDialog}>
                    <View style={[modalHeader, { justifyContent: 'space-between', alignItems: 'center' }]}>
                        <Text style={{ paddingHorizontal: 10, paddingVertical: 5, color: '#fff' }} uppercase>Tìm kiếm đơn hàng</Text>
                        <Button
                            transparent
                            iconRight
                            onPress={() => { this.setState({ modalVisible: false }) }}
                        >
                            <Text uppercase={false} style={[btn__text, { color: '#fff', fontSize: 15 }]}>Đóng</Text>
                            <Icon style={[btn__icon, { color: '#fff' }]} type='FontAwesome' name='times-circle' />
                        </Button>
                    </View>
                    <View style={modalBody}>
                        <View style={{ padding: 10 }}>
                            {this.state.provinceList ? (
                                <Item picker>
                                    <Label style={{ fontSize: 14, width: 90 }}>Tỉnh/ TP</Label>
                                    <Picker
                                        block
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={{ width: undefined }}
                                        iosHeader='Tỉnh/ TP'
                                        placeholder="Chọn Tỉnh/ Thành phố"
                                        placeholderStyle={{ color: Colors.blackColor }}
                                        placeholderIconColor={Colors.blackColor}
                                        selectedValue={this.state.filter.province}
                                        onValueChange={async (val) => {
                                            this.setState({ isSelect: true });
                                            await getDistrictByProvinceID(val, res => {
                                                this.setState({
                                                    districtList: res.data,
                                                    filter: {
                                                        ...this.state.filter,
                                                        province: val,
                                                        district: res.data[0].maqh,
                                                    }
                                                });
                                            });
                                            await getWardByDistrictID(this.state.districtList[0].maqh, res => {
                                                if (res.success) {
                                                    this.setState({
                                                        wardList: res.data,
                                                        filter: {
                                                            ...this.state.filter,
                                                            ward: res.data[0].xaid
                                                        },
                                                    });
                                                }
                                            });
                                            this.setState({ isSelect: false });
                                        }}
                                    >
                                        {this.state.provinceList.map(item => {
                                            return <Picker.Item label={item.name} key={item.matp} value={item.matp} />
                                        })}
                                    </Picker>
                                </Item>
                            ) : <Text>Không có dữ liệu Tỉnh/ TP</Text>
                            }
                            {this.state.districtList ? (
                                <Item picker style={{ marginBottom: 10 }}>
                                    <Label style={{ fontSize: 14, width: 90 }}>Quận/ Huyện</Label>
                                    <Picker
                                        enabled={!this.state.isSelect}
                                        block
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={{ width: undefined }}
                                        iosHeader='Quận/ Huyện'
                                        placeholder="Chọn Quận/ Huyện"
                                        placeholderStyle={{ color: Colors.blackColor }}
                                        placeholderIconColor={Colors.blackColor}
                                        selectedValue={this.state.filter.district}
                                        onValueChange={async (val) => {
                                            this.setState({ isSelect: true });
                                            await getWardByDistrictID(val, res => {
                                                this.setState({
                                                    wardList: res.data,
                                                    filter: {
                                                        ...this.state.filter,
                                                        district: val,
                                                        ward: res.data[0].xaid,
                                                    }
                                                });
                                            });
                                            this.setState({ isSelect: false });
                                        }}
                                    >
                                        {this.state.districtList.map(item => {
                                            return <Picker.Item label={item.name} key={item.maqh} value={item.maqh} />
                                        })}
                                    </Picker>
                                </Item>
                            ) : (
                                    <Text>Không có dữ liệu Quận/ Huyện</Text>
                                )}
                            {this.state.wardList ? (
                                <Item picker style={{ marginBottom: 10 }}>
                                    <Label style={{ fontSize: 14, width: 90 }}>Phường/ Xã</Label>
                                    <Picker
                                        enabled={!this.state.isSelect}
                                        block
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={{ width: undefined }}
                                        iosHeader='Phường/ Xã'
                                        placeholder="Chọn Phường/ Xã"
                                        placeholderStyle={{ color: Colors.blackColor }}
                                        placeholderIconColor={Colors.blackColor}
                                        selectedValue={this.state.filter.ward}
                                        onValueChange={val => {
                                            this.setState({
                                                filter: {
                                                    ...this.state.filter,
                                                    ward: val
                                                }
                                            })
                                        }}
                                    >
                                        {this.state.wardList.map(item => {
                                            return <Picker.Item label={item.name} key={item.xaid} value={item.xaid} />
                                        })}
                                    </Picker>
                                </Item>
                            ) : (
                                    <Text>Không có dữ liệu Phường/ Xã</Text>
                                )}
                            <Button
                                disabled={this.state.isSelect}
                                block
                                success
                                style={btnSubmit}
                                onPress={() => this.filterOrder()}
                            >
                                <Text style={btn__text}>{this.state.isSelect ? 'Wait...' : 'Áp dụng'}</Text>
                            </Button>
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
    render() {
        const {
            fontFace,
            content,
            btn, btn__icon, btn__text
        } = Styles;
        const {
            dataList,
            refreshing,
            isLoading
        } = this.state;
        if (isLoading) {
            return <Loading />
        }
        return (
            <Container style={fontFace}>
                <TopBarOnlyTitle title='Danh sách đơn hàng' />
                {this.renderFilter()}
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-end',
                        paddingVertical: 3,
                        paddingHorizontal: 10,
                        backgroundColor: '#fff',
                        borderBottomWidth: 2,
                        borderBottomColor: '#eee'
                    }}
                >
                    <Button
                        success
                        transparent
                        style={btn}
                        onPress={() => this.setState({ modalVisible: true })}
                    >
                        <Text style={[btn__text, { fontSize: 16 }]} uppercase={false}>Tìm Kiếm</Text>
                        <Icon style={btn__icon} name='search' type='FontAwesome' />
                    </Button>
                </View>
                <View style={[content, { flex: 1, padding: 10 }]}>
                    {(dataList.length === 0 || refreshing) ? (
                        <View>
                            <Text style={{ fontSize: 15, textAlign: 'center', }}>
                                {this.state.refreshing ? 'Đang tải đơn hàng...' : 'Không có đơn hàng nào...'}
                            </Text>
                        </View>
                    ) : null}
                    <FlatList
                        style={{ flex: 1 }}
                        const
                        data={dataList}
                        renderItem={({ item }) =>
                            (<OrderItem
                                navigation={this.props.navigation}
                                order={item}
                                parent={this}
                            />)
                        }
                        keyExtractor={item => item.id.toString()}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this._onRefresh}
                            />
                        }
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={0.1}
                    />
                    {this.state.isLoadMore ? <ActivityIndicator size='small' color={Colors.mainColor} /> : null}
                </View>
            </Container>
        );
    }
}

export default StockOrders;
