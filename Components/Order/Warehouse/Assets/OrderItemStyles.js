import { StyleSheet } from 'react-native';
import Colors from '../../../Assets/Colors';

export default StyleSheet.create({
    //order
    orderItem: {
        marginBottom: 10,
        padding: 10,
        borderRadius: 5,
        overflow: 'hidden',
        backgroundColor: '#fff',
        borderTopColor: Colors.subColor,
        borderTopWidth:5
    },
    orderItemCode: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 3,

    },
    orderItemCodeIcon: {
        width: 35,
        height: 35,
        resizeMode: 'contain',
        marginRight: 10,
    },
    orderItemCodeText: {
        fontWeight: 'bold',
        fontSize: 15,
        lineHeight: 20,
    },
    orderItemText: {
        fontSize: 14,
        lineHeight: 22,
        marginBottom: 4
    },
    boldStyle: {
        fontWeight: 'bold'
    },
    line: {
        borderTopWidth: 1,
        borderTopColor: '#dad9d7',
        paddingTop: 4
    },
    orderItemControl: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
    },
    btnOIC: {
        marginRight: 5,
        width: 'auto',
        height: 'auto',
        fontSize: 13,
        paddingLeft: 0,
        paddingRight: 0,
    },
    btnOICIcon: {
        marginLeft: 0,
        color: Colors.subColor,
        fontSize: 14
    },
    btnOICText: {
        color: Colors.subColor,
        paddingLeft: 5,
        paddingRight: 5,
        fontSize: 14
    },
    noneMarginRight: {
        marginRight: 0
    },
});
