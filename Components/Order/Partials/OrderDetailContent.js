import React, { Component } from 'react';
import {
    View,
    FlatList,
    Modal,
    StyleSheet,
    ActivityIndicator,
    Image
} from 'react-native';
import Swiper from 'react-native-swiper';
import { SafeAreaView } from 'react-navigation';
import { Icon, Text, Spinner } from 'native-base';
// import { RNCamera } from 'react-native-camera';
import { Collapse, CollapseHeader, CollapseBody } from 'accordion-collapse-react-native';
import OrderDetailStyles from '../Assets/OrderDetailStyles';
import FormatCurrency from '../../Assets/FormatCurrency';
import Colors from '../../Assets/Colors';
// import { Order } from '../../../Networking/order';

class OrderDetailContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            uploading: false,
            modalGalleryVisible: false,
            isLoading: true,
        };
    }
    // async requestCameraPermission() {
    //     if(Platform.OS === 'android') {
    //         try {
    //             const granted = await PermissionsAndroid.request(
    //                 PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
    //                 {
    //                     title: 'Truy cập thư viện ảnh',
    //                     message: 'GHSR muốn truy cập thư viện ảnh để chụp hình và lưu trữ hình ảnh đơn hàng',
    //                     buttonNeutral: 'Hỏi lại sau',
    //                     buttonNegative: 'Cancel',
    //                     buttonPositive: 'OK',
    //                 },
    //             );
    //             if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    //                 return true;
    //             } else {
    //                 return false;
    //             }
    //         } catch (err) {
    //             console.warn(err);
    //         }
    //     } else {
    //         return true;
    //     }
    // }
    // takePicture = async function () {
    //     const checkStoragePer = this.requestCameraPermission();
    //     if (this.camera) {
    //         const options = {
    //             quality: 0.25,
    //             base64: true,
    //             orientation: "portrait",
    //             fixOrientation: true,
    //             forceUpOrientation: true
    //         };
    //         const data = await this.camera.takePictureAsync(options);
    //         if (checkStoragePer) {
    //             CameraRoll.saveToCameraRoll(data.uri);
    //         } else {
    //             alert('Vui lòng cho phép truy cập kho lưu trữ để lưu lại hình ảnh đơn hàng!');
    //         }
    //         const imageUrl = Platform.OS === "android" ? data.uri : data.uri.split('//')[1];
    //         const imageName = imageUrl.substring(imageUrl.lastIndexOf('/')+1);
    //         const params = {
    //             id: this.props.dataSource.id,
    //             image_url: imageUrl,
    //             image_name: imageName 
    //         }
    //         this.setState({uploading: true});
    //         await Order.updateOrderImages(params, (res) => {
    //             if (res.success){
    //                 this.setState({uploading: false});
    //                 Alert.alert(
    //                     'Thông báo',
    //                     'Đã cập nhật hình ảnh đơn hàng!',
    //                     [
    //                         {
    //                             text: 'OK', onPress: () => {
    //                                 this._setModalVisible(false);
    //                                 this.props.afterTakePhoto();
    //                             }
    //                         },
    //                     ],
    //                     {cancelable: false},
    //                 );
    //             } else {
    //                 this.setState({uploading: false}); 
    //                 Alert.alert(
    //                     'Lỗi',
    //                     res.message,
    //                     [
    //                         {text: 'OK', onPress: () => this._setModalVisible(false)},
    //                     ],
    //                     {cancelable: false},
    //                 );
    //             }
    //         }, (e) => {
    //             this.setState({uploading: false}); 
    //             Alert.alert(
    //                 'Lỗi',
    //                 e.toString(),
    //                 [
    //                     {text: 'OK', onPress: () => this._setModalVisible(false)},
    //                 ],
    //                 {cancelable: false},
    //             );
    //         });
    //     }
    // };
    // _setModalVisible(visible) {
    //     this.setState({ modalVisible: visible });
    // }

    render_collapseTimeline() {
        const {
            collapseHeader, timelineBox, timelineDot, verticalLine,
            timelineItem, timelineItemTitle, timelineItemDesc, collapseBody
        } = OrderDetailStyles;
        const { dataSource, dataReceiptNote, dataDeliveryNote, dataRefundNote } = this.props;
        return (
            <View>
                <Collapse>
                    <CollapseHeader>
                        <View style={collapseHeader}>
                            <Text style={{ fontWeight: 'bold', color: '#fff' }}>
                                NỘI DUNG CẬP NHẬT
                            </Text>
                            <Icon
                                style={{ fontWeight: 'bold', color: '#fff' }}
                                type='FontAwesome'
                                name='angle-down'
                            />
                        </View>
                    </CollapseHeader>
                    <CollapseBody>
                        {typeof dataSource === 'undefined' ?
                            (<Spinner />) : (
                                <View style={collapseBody}>
                                    <View style={timelineBox}>
                                        <View style={verticalLine} />
                                        <View style={timelineItem}>
                                            <View style={timelineDot} />
                                            <Text style={timelineItemTitle}>Tạo đơn hàng</Text>
                                            <Text style={timelineItemDesc}>
                                                {dataSource.created_at || ''}: Đã tạo đơn hàng
                                            </Text>
                                        </View>
                                        <View style={timelineItem}>
                                            <View style={timelineDot} />
                                            <Text style={timelineItemTitle}>Lấy hàng</Text>
                                            {dataSource.phieu_nhan_hang === null ? (
                                                <View />
                                            ) : (
                                                    <View>
                                                        <Text style={timelineItemDesc}>
                                                            {dataReceiptNote.created_at || ''}:
                                                            Đã tạo phiếu nhận hàng
                                                </Text>
                                                        <Text style={timelineItemDesc}>
                                                            Ghi chú: {dataReceiptNote.ghichu}
                                                        </Text>
                                                    </View>
                                                )}
                                        </View>
                                        <View style={timelineItem}>
                                            <View style={timelineDot} />
                                            <Text style={timelineItemTitle}>Giao hàng</Text>
                                            {dataSource.phieu_giao_hang === null ?
                                                (<View />) : (
                                                    <View>
                                                        <Text
                                                            style={timelineItemDesc}
                                                        >
                                                            {dataSource.phieu_giao_hang.created_at || ''}: Đã tạo phiếu giao hàng
                                                        </Text>
                                                        <Text style={timelineItemDesc}>
                                                            Ghi chú: {dataSource.phieu_giao_hang.ghichu}
                                                        </Text>
                                                    </View>
                                                )
                                            }

                                            {typeof dataDeliveryNote === 'object' && typeof dataDeliveryNote.ghichu_giaohang === 'object' && dataDeliveryNote.ghichu_giaohang !== null ? (
                                                <View>
                                                    <FlatList
                                                        data={dataDeliveryNote.ghichu_giaohang}
                                                        renderItem={({ item }) => (
                                                            <Text style={timelineItemDesc}>
                                                                {item.created_at}: {item.ten_trangthai}
                                                            </Text>
                                                        )}
                                                        keyExtractor={(item, index) => index.toString()}
                                                    />
                                                </View>
                                            ) : null}

                                        </View>
                                        <View style={timelineItem}>
                                            <View style={timelineDot} />
                                            <Text style={timelineItemTitle}>Trả hàng</Text>
                                            {dataSource.phieu_tra_hang === null ? (
                                                <View />
                                            ) : (
                                                    <View>
                                                        <Text
                                                            style={timelineItemDesc}
                                                        >
                                                            {dataRefundNote.created_at} : {dataRefundNote.tinhtrang === "" ? 'Đã tạo phiếu trả hàng' : dataRefundNote.tinhtrang}
                                                        </Text>
                                                        <Text
                                                            style={timelineItemDesc}
                                                        >
                                                            Ghi chú: {dataRefundNote.ghichu}
                                                        </Text>
                                                    </View>
                                                )}
                                        </View>

                                    </View>
                                </View>
                            )}
                    </CollapseBody>
                </Collapse>
            </View>
        );
    }
    render() {
        const { line, collapseHeader, collapseBody, bold } = OrderDetailStyles;
        const { dataSource } = this.props;
        if (typeof dataSource.diachi_nguoigui === 'object') {
            var diachi_nguoigui = dataSource.diachi_nguoigui;
        }

        if (typeof dataSource.diachi_nguoinhan === 'object') {
            var diachi_nguoinhan = dataSource.diachi_nguoinhan;
        }
        
        return (
            <View>
                {/* <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        console.log('close');
                    }}>
                    <SafeAreaView style={{ flex: 1 }}>
                        <View style={styles.container}>
                            {this.state.uploading ? (
                                <View style={{
                                    position: 'absolute',
                                    backgroundColor: 'rgba(0, 0, 0, 0.8)',
                                    flex: 1,
                                    top: 0,
                                    bottom: 0,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    zIndex: 20,
                                    width: '100%'
                                }}>
                                    <ActivityIndicator size='small' color='#fff' />
                                    <Text style={{ color: '#fff' }}>Cập nhật hình ảnh đơn hàng...</Text>
                                </View>
                            ) : null}
                            <RNCamera
                                ref={ref => {
                                    this.camera = ref;
                                }}
                                style={styles.preview}
                                type={RNCamera.Constants.Type.back}
                                flashMode={RNCamera.Constants.FlashMode.off}
                                captureAudio={false}
                                permissionDialogTitle={'Truy cập camera'}
                                permissionDialogMessage={'Cho phép ứng dụng sử dụng camera của điện thoại'}
                                onGoogleVisionBarcodesDetected={({ barcodes }) => {
                                    console.log(barcodes);
                                }}
                            >
                                {({ status }) => {
                                    if (status !== 'READY') return <Text style={{ textAlign: 'center', marginBottom: 5 }}>Đang tải...</Text>;
                                    return(
                                        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingBottom: 10 }}>
                                            <Text onPress={() => this._setModalVisible(false)} style={{ color: '#fff', flex: 2, textAlign: 'center' }}>Cancel</Text>
                                            <View style={{ flex: 8 }}>
                                                <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
                                                    <Icon type='FontAwesome' name='camera' />
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{ flex: 2 }} />
                                        </View>
                                    );
                                }}
                            </RNCamera>
                        </View>
                    </SafeAreaView>
                </Modal> */}
                <Modal
                    animationType="slide"
                    transparent
                    visible={this.state.modalGalleryVisible}
                    onRequestClose={() => {
                        console.log('close');
                    }}
                >
                    <SafeAreaView style={{ flex: 1 }}>
                        <View style={styles.containerGallery}>
                            <View style={{ backgroundColor: '#fff', flex: 1 }}>
                                <View style={{ backgroundColor: Colors.subColor, padding: 5 }}>
                                    <Text style={{ color: '#fff' }} onPress={() => this.setState({ modalGalleryVisible: false })}>
                                        Đóng{' '}
                                        <Icon type='FontAwesome' name='times-circle' style={{ color: '#fff', fontSize: 16 }} />
                                    </Text>
                                </View>
                                <View style={{ padding: 5, flex: 1, overflow: 'hidden' }}>
                                    <View
                                        style={{
                                            position: 'absolute',
                                            flex: 1,
                                            top: 0,
                                            bottom: 0,
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            width: '100%'
                                        }}
                                    >
                                        <ActivityIndicator size='small' color={Colors.subColor} />
                                    </View>

                                    {typeof dataSource.thuvien_donhang === 'object' && dataSource.thuvien_donhang.length > 0 ? (
                                        <Swiper>
                                            {dataSource.thuvien_donhang.map((item) => {
                                                return (
                                                    <View style={{ flex: 1 }} key={item.id}>
                                                        <Image
                                                            style={{ flex: 1, width: '100%', height: '100%', resizeMode: 'contain' }}
                                                            source={{ uri: item.duongdan }}
                                                        />
                                                    </View>
                                                )
                                            })}
                                        </Swiper>
                                    ) : null}
                                </View>
                            </View>
                        </View>
                    </SafeAreaView>
                </Modal>
                <View>
                    <Collapse isCollapsed>
                        <CollapseHeader>
                            <View style={collapseHeader}>
                                <Text style={{ fontWeight: 'bold', color: '#fff' }}>
                                    THÔNG TIN CHUNG
                                </Text>
                                <Icon
                                    style={{ fontWeight: 'bold', color: '#fff' }}
                                    type='FontAwesome'
                                    name='angle-down'
                                />
                            </View>
                        </CollapseHeader>
                        <CollapseBody>
                            {typeof diachi_nguoigui === 'undefined' ?
                                (
                                    <Spinner />
                                ) : (
                                    <View style={collapseBody}>
                                        <Text style={{ fontWeight: 'bold', marginBottom: 2 }}>Người gửi</Text>
                                        <Text>{diachi_nguoigui[0].hoten_nguoinhan} / {diachi_nguoigui[0].so_dienthoai}</Text>
                                        <Text>
                                            {diachi_nguoigui[0].chitiet},{' '}
                                            {diachi_nguoigui[0].ten_phuong},{' '}
                                            {diachi_nguoigui[0].ten_huyen},{' '}
                                            {diachi_nguoigui[0].ten_tinh}
                                        </Text>
                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                borderTopWidth: 1,
                                                borderTopColor: '#dad9d7',
                                                marginTop: 5,
                                                paddingTop: 5,
                                                marginBottom: 2
                                            }}
                                        >
                                            Người nhận
                                        </Text>
                                        <Text>{diachi_nguoinhan[0].hoten_nguoinhan} / {diachi_nguoinhan[0].so_dienthoai}</Text>
                                        <Text>
                                            {diachi_nguoinhan[0].chitiet},{' '}
                                            {diachi_nguoinhan[0].ten_phuong},{' '}
                                            {diachi_nguoinhan[0].ten_huyen},{' '}
                                            {diachi_nguoinhan[0].ten_tinh}
                                        </Text>
                                    </View>
                                )}
                        </CollapseBody>
                    </Collapse>
                </View>
                <View>
                    <Collapse isCollapsed>
                        <CollapseHeader>
                            <View style={collapseHeader}>
                                <Text style={{ fontWeight: 'bold', color: '#fff' }}>ĐƠN HÀNG</Text>
                                <Icon
                                    style={{ fontWeight: 'bold', color: '#fff' }}
                                    type='FontAwesome' name='angle-down'
                                />
                            </View>
                        </CollapseHeader>
                        <CollapseBody>
                            {(
                                typeof this.props.dataSource !== 'object'
                            ) ?
                                (
                                    <Spinner />
                                ) : (
                                    <View style={collapseBody}>
                                        {typeof this.props.dataSource.hoantrahang !== 'undefined' 
                                                && this.props.dataSource.hoantrahang === 1 ?
                                            <Text style={{ marginBottom: 3, color: '#ca0e00' }}>
                                                <Icon type='FontAwesome' name='refresh' style={{ color: '#ca0e00', fontSize: 17 }} />
                                                {' '}Khách yêu cầu hoàn trả hàng
                                            </Text> 
                                        : null }
                                        <Text style={{ marginBottom: 3 }}>
                                            <Text style={bold}>Mã vận đơn: </Text>
                                            <Text
                                                style={{
                                                    color: '#436AD4', fontWeight: 'bold'
                                                }}
                                            >
                                                #{dataSource.id}
                                            </Text>
                                        </Text>
                                        <Text style={{ marginBottom: 3 }}>
                                            <Text style={bold}>Ngày tạo: </Text>
                                            {dataSource.created_at || ''}
                                        </Text>
                                        <Text style={{ marginBottom: 3 }}>
                                            <Text style={bold}>Cập nhật: </Text>
                                            {dataSource.updated_at}
                                        </Text>
                                        <Text style={{ marginBottom: 3 }}>
                                            <Text style={bold}>Cân nặng: </Text>
                                            {dataSource.trongluong} kg
                                        </Text>
                                        <Text style={{ marginBottom: 3 }}>
                                            <Text style={bold}>Phí ship: </Text>
                                            {FormatCurrency(dataSource.gui_tinhphi)} đ
                                        </Text>
                                        <Text style={{ marginBottom: 3 }}>
                                            <Text style={bold}>Giá trị kiện hàng: </Text>
                                            {FormatCurrency(dataSource.giatri_kienhang)} đ
                                        </Text>
                                        <Text style={{ marginBottom: 3 }}>
                                            <Text style={bold}>Tiền thu hộ: </Text>
                                            {dataSource.thuho === 1 ? FormatCurrency(dataSource.giatri_kienhang) + ' đ' : 'Không thu hộ'}
                                        </Text>
                                        <Text style={{ marginBottom: 3 }}>
                                            <Text style={bold}>Tổng tiền: </Text>
                                            {FormatCurrency(dataSource.tongtien)} đ
                                        </Text>
                                        <Text style={{ marginBottom: 3 }}>
                                            <Text style={bold}>Trạng thái: </Text>
                                            <Text style={{ color: 'red' }}>
                                                {dataSource.trangthai === '' ? 'Đơn hàng mới' : dataSource.tt_trangthai[0]['ten_trangthai']}
                                            </Text>
                                        </Text>
                                        <View style={line}>
                                            <Text style={{ marginBottom: 3 }}>
                                                <Text style={bold}>Ảnh đơn hàng: </Text>
                                                Đã có ({typeof dataSource.thuvien_donhang === 'object' ? dataSource.thuvien_donhang.length : '0'}) ảnh.
                                                {typeof dataSource.thuvien_donhang === 'object' && dataSource.thuvien_donhang.length > 0 ? (
                                                    <Text style={{ color: Colors.subColor }} onPress={() => this.setState({ modalGalleryVisible: true })}> Xem ảnh</Text>
                                                ) : null}
                                            </Text>
                                            {typeof dataSource.thuvien_donhang === 'object' 
                                                    && dataSource.trangthai !== '4'
                                                        && dataSource.trangthai !== '12'
                                                            && dataSource.trangthai !== '13'
                                                                && ((this.props.typeUser === 'client' && dataSource.thuvien_donhang.length < 3) 
                                                                    || (this.props.typeUser === 'shipper' && dataSource.thuvien_donhang.length < 5)) ? (
                                                <Text
                                                    style={{ paddingVertical: 3, fontSize: 16, backgroundColor: '#f1f1f1', textAlign: 'center', marginTop: 5 }}
                                                    onPress={() => this.props.navigation.navigate('OrderCameraScreen', {
                                                        id_donhang: this.props.dataSource.id,
                                                        afterTakePhoto: this.props.afterTakePhoto,
                                                    })}
                                                >
                                                    (Bấm để đăng thêm ảnh)
                                                </Text>
                                            ) : null}
                                        </View>
                                        <View style={line}>
                                            <Text style={{ marginBottom: 3 }}>
                                                <Text style={bold}>Tên kiện hàng: </Text>
                                                {dataSource.ten_kienhang}
                                            </Text>
                                            <Text style={{ marginBottom: 3 }}>
                                                <Text style={bold}>Loại hàng hóa: </Text>
                                                {dataSource.loai_hanghoa}
                                            </Text>
                                            <Text style={{ marginBottom: 3 }}>
                                                <Text style={bold}>Ghi chú: </Text>
                                                {dataSource.ghichu}
                                            </Text>
                                        </View>
                                    </View>
                                )}
                        </CollapseBody>
                    </Collapse>
                </View>
                {this.render_collapseTimeline()}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 10,
        alignSelf: 'center',
        margin: 10,
    },
    containerGallery: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        padding: 10
    }
});
export default OrderDetailContent;
