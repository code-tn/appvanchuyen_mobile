import React, { Component } from 'react';
import {
    View,
    Text,
    PermissionsAndroid,
    CameraRoll,
    Alert,
    SafeAreaView,
    TouchableOpacity,
    StyleSheet,
    Platform,
    ActivityIndicator,
    StatusBar
} from 'react-native';
import { Icon } from 'native-base';
import { RNCamera } from 'react-native-camera';
import { Order } from '../../../Networking/order';

class OrderCamera extends Component {
    constructor(props) {
        super(props);
        this.state = {
            uploading: false,
        };
    }
    async requestCameraPermission() {
        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    {
                        title: 'Truy cập thư viện ảnh',
                        message: 'GHSR muốn truy cập thư viện ảnh để chụp hình và lưu trữ hình ảnh đơn hàng',
                        buttonNeutral: 'Hỏi lại sau',
                        buttonNegative: 'Cancel',
                        buttonPositive: 'OK',
                    },
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    return true;
                } else {
                    return false;
                }
            } catch (err) {
                console.warn(err);
            }
        } else {
            return true;
        }
    }
    takePicture = async function () {
        if (this.camera) {
            this.setState({ uploading: true });
            const options = {
                quality: 0.25,
                base64: true,
                orientation: 'portrait',
                fixOrientation: true,
                forceUpOrientation: true
            };
            const data = await this.camera.takePictureAsync(options);
            const checkStoragePer = this.requestCameraPermission();
            if (checkStoragePer) {
                CameraRoll.saveToCameraRoll(data.uri);
            } else {
                Alert.alert(
                    'Thông báo',
                    'Vui lòng cho phép truy cập kho lưu trữ để lưu lại hình ảnh đơn hàng!',
                    [
                        {
                            text: 'OK'
                        },
                    ],
                    { cancelable: false },
                );
            }
            const imageUrl = Platform.OS === "android" ? data.uri : data.uri.split('//')[1];
            const imageName = imageUrl.substring(imageUrl.lastIndexOf('/') + 1);
            const params = {
                id: this.props.navigation.getParam('id_donhang'),
                image_url: imageUrl,
                image_name: imageName
            }
            await Order.updateOrderImages(params, (res) => {
                if (res.success) {
                    this.setState({ uploading: false });
                    Alert.alert(
                        'Thông báo',
                        'Đã cập nhật hình ảnh đơn hàng!',
                        [
                            {
                                text: 'OK', onPress: () => {
                                    this.props.navigation.goBack();
                                    this.props.navigation.state.params.afterTakePhoto();
                                }
                            },
                        ],
                        { cancelable: false },
                    );
                } else {
                    this.setState({ uploading: false });
                    Alert.alert(
                        'Lỗi',
                        res.message,
                        [
                            { text: 'OK', onPress: () => this.props.navigation.goBack() },
                        ],
                        { cancelable: false },
                    );
                }
            }, (e) => {
                this.setState({ uploading: false });
                Alert.alert(
                    'Lỗi',
                    e.toString(),
                    [
                        { text: 'OK', onPress: () => this.props.navigation.goBack() },
                    ],
                    { cancelable: false },
                );
            });
        }
    };
    render() {
        return (
            <View style={{ flex: 1 }}>
                <StatusBar barStyle="dark-content" backgroundColor='#fff' animated />
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={styles.container}>
                        {this.state.uploading ? (
                            <View style={{
                                position: 'absolute',
                                backgroundColor: 'rgba(0, 0, 0, 0.8)',
                                flex: 1,
                                top: 0,
                                bottom: 0,
                                alignItems: 'center',
                                justifyContent: 'center',
                                zIndex: 20,
                                width: '100%'
                            }}>
                                <ActivityIndicator size='small' color='#fff' />
                                <Text style={{ color: '#fff' }}>Cập nhật hình ảnh đơn hàng...</Text>
                            </View>
                        ) : null}
                        <RNCamera
                            ref={ref => {
                                this.camera = ref;
                            }}
                            style={styles.preview}
                            type={RNCamera.Constants.Type.back}
                            flashMode={RNCamera.Constants.FlashMode.off}
                            captureAudio={false}
                            permissionDialogTitle={'Truy cập camera'}
                            permissionDialogMessage={'Cho phép ứng dụng sử dụng camera của điện thoại'}
                            onGoogleVisionBarcodesDetected={({ barcodes }) => {
                                console.log(barcodes);
                            }}
                        >
                            {({ status }) => {
                                if (status !== 'READY') return <Text style={{ textAlign: 'center', marginBottom: 5 }}>Đang tải...</Text>;
                                return (
                                    <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingBottom: 10 }}>
                                        <View style={{ flex: 2, paddingLeft: 2 }}>
                                            <Text
                                                onPress={() => this.props.navigation.goBack()}
                                                style={{ color: '#fff', backgroundColor: '#000', paddingVertical: 4, borderRadius: 3, textAlign: 'center' }}
                                            >
                                                Cancel
                                            </Text>
                                        </View>
                                        <View style={{ flex: 8 }}>
                                            <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
                                                <Icon type='FontAwesome' name='camera' />
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ flex: 2 }} />
                                    </View>
                                );
                            }}
                        </RNCamera>
                    </View>
                </SafeAreaView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 10,
        alignSelf: 'center',
        margin: 10,
    },
    containerGallery: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        padding: 10
    }
});
export default OrderCamera;
