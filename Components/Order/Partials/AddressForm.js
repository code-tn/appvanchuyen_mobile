import React, { Component } from 'react';
import { Modal, View, TextInput, Dimensions, StyleSheet, ScrollView, TouchableOpacity
} from 'react-native';
import { Collapse, CollapseHeader, CollapseBody } from 'accordion-collapse-react-native';
import { Button, Text, Icon } from 'native-base';
import Styles from '../../../Assets/Styles';
import Colors from '../../../Assets/Colors';

const Screen = Dimensions.get('screen');

class AddressForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mainAddress: '',
            modalVisible: false,
            dataSource: {
                chitiet: '',

            }
        };
    }

    render() {
        const { input, btnCloseModal, modalDialog, modalBody, modalHeader, fontFace } = Styles;
        const {
            desc, mainLocation, collapseHeader, collapseHeaderBox, collapseHeaderText,
            collapseHeaderIcon, collapseBody, collapseBodyText } = addresFormStyles;
        return (
            <View style={fontFace}>
                <TextInput
                    value={this.inputValue('chitiet')}
                    onChangeText={text => this._onInputChange({chitiet: text})}
                    placeholder='Số nhà, hẻm, ngõ, ngách, tòa nhà'
                    style={input}
                />

            </View>
        );
    }

    /*
    | ACTION METHODS -------------------------------------------------------------------------------
     */

    _onInputChange(data) {
        this.setState({dataSource: Object.assign(this.state.dataSource, data)});
        if(typeof this.props.onFormChanged ==='function') this.props.onFormChanged(this.state);
    }

    inputValue(name, defaultValue = '') {
        if (typeof this.state.dataSource[name] === 'undefined') return defaultValue;
        return this.state.dataSource[name];
    }

    _setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    _chooseMainAddress(strProvince, strDistrict, strWard) {
        this.setState({
            mainAddress: strWard + ', ' + strDistrict + ', ' + strProvince
        });
        this._onInputChange({tinh: strProvince});
        this._onInputChange({huyen: strDistrict});
        this._onInputChange({phuong: strWard});
        this._setModalVisible(false);
    }
}

const addresFormStyles = StyleSheet.create({
    desc: {
        color: '#838383',
        fontSize: 13,
        fontStyle: 'italic',
        paddingBottom: 10
    },
    mainLocation: {
        fontSize: 13,
        backgroundColor: Colors.mainColor,
        color: '#fff',
        padding: 5,
    },
    collapseHeader: {
        fontSize: 14,
        lineHeight: 20,
        color: Colors.blackColor,
        fontWeight: '600',
        paddingVertical: 3,
        borderBottomWidth: 1,
        borderBottomColor: '#eee',
    },
    collapseHeaderBox: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        fontWeight: 'normal',
        color: '#333'
    },
    collapseHeaderIcon: {
        fontSize: 14,
        lineHeight: 20,
        fontWeight: 'normal',
        color: '#333'
    },
    collapseHeaderText: {
        fontSize: 14,
        lineHeight: 20,
        color: '#333',
        fontWeight: 'normal',
    },
    collapseBody: {
        paddingLeft: 8,
    },
    collapseBodyText: {
        fontSize: 13,
        lineHeight: 19,
        color: '#333',
        paddingVertical: 2,
        borderBottomWidth: 1,
        borderBottomColor: '#eee',
    }
});
export default AddressForm;
