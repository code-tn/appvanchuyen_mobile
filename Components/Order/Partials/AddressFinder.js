import React, { Component } from 'react';
import { View, Alert, Modal, StyleSheet, TouchableOpacity } from 'react-native';
import { Button, List, ListItem, Text } from "native-base";
import { getCustomerBookmark } from '../../../Data/userRepo';
import Styles from '../../Assets/Styles';

export default class AddressFinder extends Component {

    constructor(props) {
        super(props);
        const options = {
            loai_diachi: 'nguoinhan'
        };
        if (typeof props.options.loai_diachi !== undefined)
            options.loai_diachi = 'nguoinhan'

        this.state = {
            user: props.options.user,
            loai_diachi: props.options.loai_diachi,
            modal_addressfinder: false,
            list: '',
            loading: true,
        }
    }

    componentWillMount() {

    }

    componentDidMount() {
        this.loadData();
    }

    loadData() {
        setTimeout(async () => {
            await getCustomerBookmark({
                id_khachhang: this.state.user.id,
                loai_diachi: this.state.loai_diachi
            }, (res) => {
                if (res.success) {
                    this.setState({
                        list: res.data,
                        loading: false,
                    });
                } else {
                    this.setState({
                        loading: false,
                    });
                }
            }, error => {
                Alert.alert(
                    'Lỗi',
                    error.toString(),
                    [
                        { text: 'OK'},
                    ],
                    { cancelable: false },
                );
                this.setState({
                    loading: false,
                });
            });
        })
    }

    render_content() {

        const AddressType = this.state.loai_diachi == 'nguoinhan' ? 'lấy hàng' : 'nhận hàng';

        if (this.state.loading) {
            return <Text>Đang tải...</Text>
        }

        return (
            
            <View>
                {this.state.list !== undefined && this.state.list.length > 0 ? (
                    <View>
                        <Text>Tìm địa chỉ {AddressType}</Text>
                        <List>
                            {
                                this.state.list.map((item, key) => {
                                    return (
                                        <ListItem key={key}>
                                            <TouchableOpacity onPress={() => this.onSelect(item)}>
                                                <Text>{item.ten_goinho}</Text>
                                            </TouchableOpacity>
                                        </ListItem>
                                    )
                                })
                            }
                        </List>
                    </View>
                ) : (<Text style={{ marginBottom: 10 }}>Không có dữ liệu...</Text>)}
                <Button success block onPress={() => {
                    this.props.onClose();
                }}>
                    <Text>Đóng</Text>
                </Button>
                
            </View>
        )
    }

    onSelect(item) {
        this.props.onSelect(item);
        this.props.onClose();
    }

    render() {

        const modalVisible = this.props.visible;
        const { modalDialog, modalHeader, btnCloseModal, modalBody } = Styles;
        return (
            <View>
                <Modal
                    transparent={true}
                    visible={modalVisible}
                    animationType="fade"
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                    }}>
                    <View style={modalDialog}>
                        <View style={Style.ComponentContainer}>
                            <View style={Style.ComponentContent}>
                                {this.render_content()}
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}

const Style = StyleSheet.create({
    ComponentContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    ComponentContent: {
        width: '90%',
        height: '90%',
        backgroundColor: '#FFF',
        padding: 10,
        borderRadius: 4,
    }
});
