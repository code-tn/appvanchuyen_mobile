import React, { Component } from 'react';
import { Container, Content, Fab, Icon, Text, Button } from 'native-base';
import { RefreshControl, SafeAreaView, View, Alert } from 'react-native';
import { phonecall, text } from 'react-native-communications';
import TopBar from '../../Shared/TopBar';
import Styles from '../../Assets/Styles';
import OrderDetailContent from '../Partials/OrderDetailContent';
import Loading from '../../Shared/Loading';
import {
    getOrderReceiptList,
    getOrderDeliveryBillList,
    getOrderDetail,
    getOrderRefundBill,
    confirmRevertOrder
} from '../../../Data/orderRepo';
import { Order } from '../../../Networking/order';
import { Auth } from '../../../Networking/auth';

class UserOrderDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: '',
            orderID: '',
            dataSource: '',
            dataReceiptNote: '',
            dataDeliveryNote: '',
            dataRefundNote: '',
            isLoading: true,
            fabsContact: false,
            refreshing: false
        };
    }
    async componentDidMount() {
        const user = await Auth.currentUser();
        await this.setState({ ...this.state, currentUser: user.id, orderID: this.props.navigation.getParam('idOrder') });
        await this.loadData();
    }
    _notice(title, content) {
        return (
            Alert.alert(
                title,
                content.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            )
        );
    }
    async _loadOrderDetail(idOrder) {
        await getOrderDetail(idOrder, res => {
            this.setState({
                dataSource: (res.success && typeof res.data === 'object') ? res.data : undefined,
                isLoading: false
            });
        }, error => {
            this._notice('error', error.toString());
            this.setState({ isLoading: false });
        });
    }
    async _loadReceiptBills(idOrder) {
        await getOrderReceiptList(idOrder, res => {
            this.setState({
                dataReceiptNote: (res.success && typeof res.data === 'object') ? res.data : null
            });
        }, error => this._notice('error', error));
    }
    async _loadDeliveryBills(idOrder) {
        await getOrderDeliveryBillList(idOrder, res => {
            this.setState({
                dataDeliveryNote: (res.success && typeof res.data === 'object') ? res.data : null
            });
        }, error => this._notice('error', error));
    }
    async _loadRefundBills(idOrder) {
        await getOrderRefundBill(idOrder, res => {
            this.setState({
                dataRefundNote: (res.success && typeof res.data === 'object') ? res.data : null
            });
        }, error => this._notice('error', error));
    }
    async loadData() {
        await this._loadOrderDetail(this.state.orderID);
        await this._loadReceiptBills(this.state.orderID);
        await this._loadDeliveryBills(this.state.orderID);
        await this._loadRefundBills(this.state.orderID);
    }
    _onRefresh = async () => {
        this.setState({ refreshing: true });
        await this.loadData();
        this.setState({ refreshing: false });
    };
    _refreshOrderDetail = async () => {
        this.setState({ refreshing: true });
        await this._loadOrderDetail(this.state.orderID);
        this.setState({ refreshing: false });
    }
    _cancelOrder() {
        Alert.alert(
            'Xác nhận',
            'Bạn muốn hủy đơn hàng này?',
            [
                { text: 'Hủy', style: 'cancel' },
                { text: 'OK', onPress: () => this._confirmCancelOrder() },
            ]
        );
    }
    async _confirmCancelOrder() {
        this.setState({ isLoading: true });
        await Order.confirm({
            id_vanchuyen: 0,
            id_donhang: this.state.orderID,
            trangthai: 13,
        }, (responseJson) => {
            if (responseJson.success) {
                this.props.navigation.navigate('NoticeScreen', {
                    msg: 'Đã hủy đơn hàng có mã đơn #' + this.state.orderID,
                    callback: () => this.props.navigation.navigate('OdersScreen', {
                        refresh: true
                    })
                });
            } else {
                Alert.alert(
                    'Thông báo',
                    responseJson.message,
                    [{ text: 'Ok', style: 'cancel' }],
                    { cancelable: false }
                );
            }
        }, e => {
            Alert.alert(
                'Thông báo',
                e.toString(),
                [{ text: 'Ok', style: 'cancel' }],
                { cancelable: false }
            );
        });
        this.setState({ isLoading: false });
    }
    _revertOrder() {
        Alert.alert(
            'Xác nhận',
            'Bạn muốn hoàn trả đơn hàng này?',
            [
                { text: 'Hủy', style: 'cancel' },
                { text: 'OK', onPress: () => this._confirmRevertOrder() },
            ]
        );
    }
    async _confirmRevertOrder() {
        this.setState({ isLoading: true });
        await confirmRevertOrder(
            this.state.orderID,
            this.state.currentUser,
            (responseJson) => {
            if (responseJson.success) {
                this.props.navigation.navigate('NoticeScreen', {
                    msg: 'Đã xác nhận hoàn trả đơn hàng có mã đơn #' + this.state.orderID + '',
                    callback: () => this.props.navigation.navigate('OdersScreen', {
                        refresh: true
                    })
                });
            } else {
                Alert.alert(
                    'Lỗi',
                    responseJson.message,
                    [{ text: 'Ok', style: 'cancel' }],
                    { cancelable: false }
                );
            }
        }, e => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [{ text: 'Ok', style: 'cancel' }],
                { cancelable: false }
            );
        });
        this.setState({ isLoading: false });
    }
    render() {
        const { dataSource, dataReceiptNote, dataDeliveryNote, dataRefundNote, isLoading } = this.state;
        const { content } = Styles;
        const { navigation } = this.props;
        if (isLoading) return <Loading loadingText={'Đang tải thông tin đơn hàng...'} />;
        return (
            <Container>
                <TopBar title='Chi tiết đơn hàng' navigation={navigation} />
                {dataSource.duocnhan_boi === 0 && dataSource.trangthai === '' ? (
                    <View 
                        style={{
                            paddingVertical: 5, 
                            paddingHorizontal: 10, 
                            backgroundColor: '#fff', 
                            borderBottomWidth: 2, 
                            borderBottomColor: '#eee', 
                            justifyContent: 'flex-end', 
                            flexDirection: 'row', 
                            flexWrap: 'wrap' 
                        }}
                    >
                        <Button
                            danger
                            iconRight
                            style={{ height: 'auto', marginVertical: 5 }}
                            onPress={() => this._cancelOrder()}
                        >
                            <Text uppercase={false} style={{ paddingLeft: 12, paddingRight: 12 }}>Hủy đơn</Text>
                            <Icon
                                type='FontAwesome'
                                name='times-circle'
                                style={{ color: '#fff', fontSize: 18, marginRight: 12, }}
                            />
                        </Button>
                        <Button
                            success
                            iconRight
                            style={{ marginLeft: 10, height: 'auto', marginVertical: 5 }}
                            onPress={() => this.props.navigation.navigate('EditOrderScreen', {
                                id_donhang: dataSource.id
                            })}
                        >
                            <Text uppercase={false} style={{ paddingLeft: 12, paddingRight: 12 }}>Chỉnh sửa</Text>
                            <Icon
                                type='FontAwesome'
                                name='pencil-square-o'
                                style={{ color: '#fff', fontSize: 18, marginRight: 12, }}
                            />
                        </Button>
                    </View>
                ) : null}
                {(this.state.dataSource.trangthai === '3'
                    || this.state.dataSource.trangthai === '14'
                        || this.state.dataSource.trangthai === '15'
                            || this.state.dataSource.trangthai === '17')
                                && this.state.dataSource.phieu_giao_hang === null
                                    && this.state.dataSource.hoantrahang === 0 ? (
                    <View 
                        style={{
                            paddingVertical: 5, 
                            paddingHorizontal: 10, 
                            backgroundColor: '#fff', 
                            borderBottomWidth: 2, 
                            borderBottomColor: '#eee', 
                            justifyContent: 'flex-end', 
                            flexDirection: 'row', 
                            flexWrap: 'wrap' 
                        }}
                    >
                        <Button
                            info
                            iconRight
                            style={{ marginLeft: 10, height: 'auto', marginVertical: 5 }}
                            onPress={() => this._revertOrder()}
                        >
                            <Text uppercase={false} style={{ paddingLeft: 12, paddingRight: 12 }}>Hoàn trả hàng</Text>
                            <Icon
                                type='FontAwesome'
                                name='refresh'
                                style={{ color: '#fff', fontSize: 18, marginRight: 12, }}
                            />
                        </Button>
                    </View>
                ) : null}
                <SafeAreaView style={{ flex: 1 }}>
                    <Content 
                        padder 
                        style={content}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this._onRefresh}
                            />
                        }
                    >
                        {
                            (isLoading
                                || typeof dataSource === 'undefined'
                                || typeof dataReceiptNote === 'undefined'
                                || typeof dataDeliveryNote === 'undefined'
                                || typeof dataRefundNote === 'undefined'
                            ) ?
                                (<Loading />) : (
                                    <OrderDetailContent
                                        dataSource={dataSource}
                                        dataReceiptNote={dataReceiptNote}
                                        dataDeliveryNote={dataDeliveryNote}
                                        dataRefundNote={dataRefundNote}
                                        navigation={this.props.navigation}
                                        afterTakePhoto={() => this._refreshOrderDetail()}
                                        typeUser='client'
                                    />
                                )
                        }
                    </Content>
                    {dataSource.duocnhan_boi !== 0 ? (
                        <Fab
                            active={this.state.fabsContact}
                            direction="up"
                            containerStyle={{}}
                            style={{ backgroundColor: '#eb9e3e' }}
                            position="bottomRight"
                            onPress={() => this.setState({ fabsContact: !this.state.fabsContact })}
                        >
                            <Icon name="person" />
                            <Button
                                title="Gọi cho shipper"
                                style={{ backgroundColor: '#34A34F' }}
                                onPress={() => {
                                    phonecall(dataSource.sodienthoai_shipper.toString(), true);
                                }}
                            >
                                <Icon name="logo-whatsapp" />
                            </Button>
                            <Button
                                title="Nhắn tin cho shipper"
                                style={{ backgroundColor: '#DD5144' }}
                                onPress={() => {
                                    text(dataSource.sodienthoai_shipper.toString(), 'Chào bạn');
                                }}
                            >
                                <Icon name="mail" />
                            </Button>
                        </Fab>
                    ) : null}
                </SafeAreaView>
            </Container>
        );
    }
}

export default UserOrderDetail;
// export default withNavigationFocus(UserOrderDetail);
