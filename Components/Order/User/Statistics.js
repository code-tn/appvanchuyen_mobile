import React, { Component } from 'react';
import { View, Image, TouchableOpacity, StyleSheet, ActivityIndicator, RefreshControl, Alert, Platform, StatusBar } from 'react-native';
import { Container, Content, Icon, Text, Button } from 'native-base';
import TopBarOnlyTitle from '../../Shared/TopBarOnlyTitle';
import Styles from '../../Assets/Styles';
import AppColors from '../../Assets/Colors';
import StatisticsStyles from './Assets/StatisticsStyles';
import newsIcon from './Assets/Images/newsIcon.png';
import bgTotal from './Assets/Images/bgTotal.png';

// import icon1 from './Assets/Images/Statistics/icon-1.png';
// import icon2 from './Assets/Images/Statistics/icon-2.png';
// import icon3 from './Assets/Images/Statistics/icon-3.png';
// import icon4 from './Assets/Images/Statistics/icon-4.png';
// import icon5 from './Assets/Images/Statistics/icon-5.png';
// import icon6 from './Assets/Images/Statistics/icon-6.png';

import { getCustomerStatislics } from '../../../Data/userRepo';
import { Auth } from '../../../Networking/auth';
// import Popup from '../../User/Popup';

class Statistics extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            tongdonhang: 0,
            dataSource: undefined,
            error: '',
            errors: []
        };
    }
    componentWillMount = async () => {
        await this.loadData();
    }
    
    _onRefresh = async () => {
        this.setState({ refreshing: true });
        await this.loadData();
        this.setState({ refreshing: false });
    }
    loadData = async () => {
        const user = await Auth.currentUser();
        await getCustomerStatislics({
            id_khachhang: user.id
        }, (res) => {
            if (res.success) {
                this.setState({
                    tongdonhang: res.data.tongdonhang,
                    dataSource: Array.isArray(res.data.trangthai) ? res.data.trangthai : null
                });
            } else {
                Alert.alert(
                    'Lỗi',
                    res.message,
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
            }
        }, error => {
            Alert.alert(
                'Lỗi',
                error.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
        });
    }

    renderStatistic() {
        const {
            // iconStatistics,
            itemStatistics, titleStatistics, countStatistics, 
        } = StatisticsStyles;
        if (this.state.dataSource == null) {
            return <Text style={{ textAlign: 'center', marginTop: 10 }}>Không có dữ liệu thống kê</Text>
        }
        const statisticItem = (item, index) => {
            return (
                <View key={index} style={[itemStatistics, { backgroundColor: item.mau }]}>
                    {/* <Image style={iconStatistics} source={icon1}/> */}
                    <Text style={titleStatistics}>{item.ten_trangthai || 'Không rõ'}</Text>
                    <Text style={countStatistics}>{item.soluong || 0}</Text>
                </View>
            );
        };
        return this.state.dataSource.map((item, index) => statisticItem(item, index));
    }

    render() {
        const { content, fontFace } = Styles;
        const {
            topContent, topIcon, topTitle, sectionTitle,
            // desc, 
            totalOrders, imgTotal, totalText,
            // itemStatistics
        } = StatisticsStyles;
        return (
            <Container style={fontFace}>
                <TopBarOnlyTitle title='Thông báo' />
                <Content
                    style={content}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                >
                    {/* <Popup /> */}
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('NewsScreen')}>
                        <View style={topContent}>
                            <Image source={newsIcon} style={topIcon} />
                            <Text style={topTitle}>TIN TỨC</Text>
                            <Icon type='FontAwesome' name='angle-right' />
                        </View>
                    </TouchableOpacity>
                    <View style={{ backgroundColor: '#fff', padding: 10 }}>
                        <Text style={sectionTitle}>BÁO CÁO VẬN HÀNH</Text>
                        {/* <Text style={desc}>Cập nhật 09:40 ngày 10/01/2019</Text> */}
                    </View>
                    <View style={styles.flexcontent}>
                        <Button 
                            success iconRight style={styles.btnMyOrder}
                            onPress={() => this.props.navigation.navigate('OdersScreen')}>
                            <Text uppercase={false} style={styles.btnText}>Đơn hàng của tôi</Text>
                            <Icon
                                style={{ color: '#fff', fontSize: 15, marginRight: 12 }}
                                type='FontAwesome'
                                name='clipboard'
                            />
                        </Button>

                        <Button 
                            success iconRight
                            style={styles.btnCreateOrder}
                            onPress={() => this.props.navigation.navigate('CreateOrderScreen')}>
                            <Text uppercase={false} style={styles.btnText}>Tạo đơn hàng</Text>
                            <Icon
                                style={{ color: '#fff', fontSize: 15, marginRight: 12 }}
                                type='FontAwesome'
                                name='plus'
                            />
                        </Button>
                    </View>
                    <View style={{ padding: 10, paddingBottom: 20 }}>
                        <View style={totalOrders}>
                            <Image source={bgTotal} style={imgTotal} />
                            <Text style={totalText}>{this.state.tongdonhang || 0}</Text>
                        </View>
                        {
                            (
                                typeof this.state.dataSource === 'undefined'
                            ) ?
                                (
                                    <View style={styles.loadingBar}>
                                        <ActivityIndicator
                                            style={styles.loadingBar_spinner}
                                            color={AppColors.mainColor}
                                            size='small'
                                        />
                                        <Text style={styles.loadingBar_text}>Đang tải dữ liệu thống kê...</Text>
                                    </View>
                                ) : this.renderStatistic()
                        }
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    flexcontent: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        paddingVertical: 3
    },
    loadingBar: {
        marginTop: 15,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
    },
    loadingBar_text: {
        color: AppColors.mainColor,
        paddingLeft: 15
    },
    btnMyOrder: {
        textAlign: 'center',
        marginTop: 10,
        backgroundColor: '#28D46C',
        color: '#fff',
    },
    btnCreateOrder: {
        textAlign: 'center',
        marginTop: 10,
        backgroundColor: '#FF592E',
        color: '#fff',
    },
    btnText: {
        fontSize: 15,
        paddingLeft: 12,
        paddingRight: 5,
        color: '#fff'
    }
});
export default Statistics;
