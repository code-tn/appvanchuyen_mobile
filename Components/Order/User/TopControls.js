import React, { Component } from 'react';
import { View, TextInput, Modal, Alert } from 'react-native';
import { Button, Icon, Text, Picker } from 'native-base';
import DatePicker from 'react-native-datepicker';
import { Order } from '../../../Networking/order';
import TopControlsStyles from './Assets/TopControlsStyles';
import Styles from '../../Assets/Styles';

class TopControls extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword: '',
            modalVisible: false,
            orderStatus: []
        };
    }
    
    async componentDidMount() {
        await this.getOrderStatus('phieudatcho');
        await this.getOrderStatus('phieunhanhang');
        await this.getOrderStatus('phieugiaohang');
        await this.getOrderStatus('phieutrahang');
        // await this.getOrderStatus('phieunhapkho');
        // this._onInputChange({trang_thai_don_hang: this.state.orderStatus[0].id});
    }
    async getOrderStatus(type) {
        await Order.getOrderStatusList(type, (responseJson) => {
            if (responseJson.success) {
                this.setState({ 
                    orderStatus: [...this.state.orderStatus, ...responseJson.data],
                });
            } else {
                Alert.alert(
                    'Lỗi',
                    responseJson.message,
                    [
                        {text: 'OK'},
                    ],
                    {cancelable: false},
                );
            }
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    {text: 'OK'},
                ],
                {cancelable: false},
            );
        });
    }
    _setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }
    _notice(title, content) {
        return (
            Alert.alert(
                title,
                content.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            )
        );
    }
    _onInputChange(data) {
        if (typeof this.props.parent === 'undefined') return '';
        if (typeof this.props.parent.state.filter === 'undefined') return '';
        let parent = this.props.parent;
        let dataFilter = parent.state.filter;
        let new_filter = Object.assign(dataFilter, data);
        parent.setState({ dataFilter: new_filter });
    }
    _inputValue(name, defaultValue = '') {
        if (typeof this.props.parent === 'undefined') return defaultValue;
        if (typeof this.props.parent.state.filter === 'undefined') return defaultValue;
        let parent = this.props.parent;
        let dataFilter = parent.state.filter;
        if (typeof dataFilter[name] === 'undefined') return defaultValue;
        return dataFilter[name];
    }
    _submit() {
        const currentDate = new Date().toISOString().slice(0, 10);
        if (this.state.fromDate > this.state.toDate) {
            this._notice('Lỗi', 'Khoảng thời gian không hợp lệ. \nNgày bắt đầu phải nhỏ hơn ngày kết thúc.');
            return;
        }
        if (this.state.toDate > currentDate) {
            this._notice('Lỗi', 'Ngày kết thúc phải nhỏ hơn hoặc bằng thời gian hiện tại.');
            return;
        }
        this.props.filterOrder();
    }
    _renderDatePicker() {
        return (
            <View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 5 }}>
                    <Text style={{ width: '40%' }}>Thời gian tạo đơn từ</Text>
                    <DatePicker
                        style={{ width: undefined, flexGrow: 1, marginLeft: 10 }}
                        // date={this.state.fromDate}
                        date={this._inputValue('tu_ngay')}
                        mode="date"
                        placeholder="Từ ngày"
                        // format="DD/MM/YYYY"
                        confirmBtnText="OK"
                        cancelBtnText="Hủy"
                        showIcon={false}
                        customStyles={{
                            dateInput: {
                                ...Styles.input,
                                height: 40
                            }
                        }}
                        onDateChange={(date) => { this._onInputChange({ tu_ngay: date }) }}
                    />
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ width: '40%' }}>Đến</Text>
                    <DatePicker
                        style={{ width: undefined, flexGrow: 1, marginLeft: 10 }}
                        // date={this.state.toDate}
                        date={this._inputValue('den_ngay')}
                        mode="date"
                        placeholder="Đến ngày"
                        // format="DD/MM/YYYY"
                        confirmBtnText="Xác nhận"
                        cancelBtnText="Đóng"
                        showIcon={false}
                        customStyles={{
                            dateInput: {
                                ...Styles.input,
                            }
                        }}
                        onDateChange={(date) => { this._onInputChange({ den_ngay: date }); }}
                    />
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ width: '40%' }}>Tên kiện hàng</Text>
                    <TextInput
                        style={[Styles.input, { width: undefined, flexGrow: 1, marginLeft: 10, textAlign: 'center' }]}
                        placeholder='Tên kiện hàng'
                        onChangeText={(text) => this._onInputChange({ ten_kien_hang: text })}
                    />
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 5 }}>
                    <Text style={{ width: '40%' }}>Trạng thái</Text>
                    <View style={{ width: undefined, flexGrow: 1, marginLeft: 10, textAlign: 'center' }}>
                        {this.state.orderStatus !== '' ? (
                            <Picker
                                mode="dropdown"
                                iosHeader='Tình trạng'
                                iosIcon={<Icon name="arrow-down" />}
                                style={{ width: undefined, height: 40 }}
                                selectedValue={this._inputValue('trang_thai_don_hang')}
                                onValueChange={value => this._onInputChange({ trang_thai_don_hang: value })}
                                itemTextStyle={{ fontSize: 13, color: '#666' }}
                            >
                                <Picker.Item label='Đơn hàng mới' value='' key='0' />
                                {this.state.orderStatus.map((item) => {
                                    return (<Picker.Item label={item.ten_trangthai} value={item.id} key={item.id} />);
                                })}
                            </Picker>
                        ) : null}
                    </View>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ width: '50%', paddingRight: 1 }}>
                        <Button
                            block
                            style={[Styles.btnSubmit, { justifyContent: 'center' }]}
                            onPress={() => this._setModalVisible(false)}
                        >
                            <Text>Đóng</Text>
                        </Button>
                    </View>
                    <View style={{ width: '50%', paddingLeft: 1 }}>
                        <Button
                            block
                            style={[Styles.btnSubmit, { justifyContent: 'center' }]}
                            onPress={() => this._submit()}
                        >
                            <Text>Áp dụng</Text>
                        </Button>
                    </View>
                </View>
            </View>
        );
    }
    render() {
        const {
            controlBox,
            btnColtrol,
            btnText,
            btnIcon,
            header,

            btnIsSearch,
            btnSearchText,
            btnSearchIcon
        } = TopControlsStyles;
        const {
            modalDialog,
            modalBody,
            modalHeader
        } = Styles;
        const { navigation } = this.props;
        return (
            <View style={header}>

                <Modal
                    animationType="slide"
                    transparent
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        console.log('close');
                    }}
                >
                    <View style={modalDialog}>
                        <View style={[modalHeader, { justifyContent: 'center' }]}>
                            <Text style={{ paddingHorizontal: 10, paddingVertical: 5, color: '#fff' }} uppercase>Tìm kiếm đơn hàng</Text>
                        </View>
                        <View style={modalBody}>
                            <View style={{ padding: 10 }}>
                                {this._renderDatePicker()}
                            </View>
                        </View>
                    </View>
                </Modal>

                <View
                    style={
                        {
                            width: '100%',
                            flexDirection: 'column',
                            alignItems: 'center',
                            justifyContent: 'center',
                            shadowColor: 'black',
                            shadowOffset: { width: 0, height: 2 },
                            shadowOpacity: 0.2,
                            shadowRadius: 1.2,
                        }
                    }
                >
                    <View style={controlBox}>

                        <View style={{ flex: 1 }}>
                            <Button
                                disabled={this.state.orderStatus !== '' ? false : true}
                                transparent
                                iconRight
                                style={btnIsSearch}
                                onPress={() => this._setModalVisible(true)}
                            >
                                <Text style={btnSearchText} uppercase={false}>Tìm kiếm</Text>
                                <Icon style={btnSearchIcon} name='search' type='Ionicons' />
                            </Button>
                        </View>

                        <Button
                            iconRight
                            transparent
                            style={btnColtrol}
                            onPress={() => navigation.navigate('CreateOrderScreen', {
                                user: this.props.user,
                                onGoBack: this.props.onGoBack
                            })}
                        >
                            <Text uppercase={false} style={btnText}>Tạo đơn</Text>
                            <Icon
                                type='FontAwesome'
                                name='plus'
                                style={btnIcon}
                            />
                        </Button>
                    </View>
                </View>
            </View>
        );
    }
}

export default TopControls;
