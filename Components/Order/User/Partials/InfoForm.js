import React, { Component } from 'react';
import { View, TextInput } from 'react-native';
import { Icon, Picker, Item, Label } from 'native-base';
import Styles from '../../../Assets/Styles';

class InfoForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mainAddress: '',
            modalVisible: false,
            dataSource: {
                hoten: '',
                so_dienthoai: '',
                quocgia: '',
            }
        };
    }

    componentDidMount() {
        if(typeof this.props.onInit ==='function'){
            this.props.onInit(this);
        }
    }

    render() {
        const { input, fontFace } = Styles
        return (
            <View style={fontFace}>
                <TextInput
                    style={input}
                    value={this.inputValue('hoten')}
                    onChangeText={text => this._onInputChange({ hoten: text })}
                    placeholder='Họ và tên...'
                />
                <TextInput
                    style={input}
                    value={this.inputValue('so_dienthoai')}
                    onChangeText={text => this._onInputChange({ so_dienthoai: text })}
                    placeholder='Số điện thoại...'
                    dataDetectorTypes='phoneNumber'
                />
                <Item picker>
                    <Label>Quốc tịch</Label>
                    <Picker mode="dropdown"
                        iosHeader='Quốc tịch'
                        iosIcon={<Icon name="arrow-down"/>}
                        style={{ width: undefined, height: 40 }}
                        selectedValue={this.inputValue('quocgia')}
                        onValueChange={value => this._onInputChange({ quocgia: value })}
                        itemTextStyle={{ fontSize: 14 }}
                    >
                        <Picker.Item label="Việt Nam" value="VN"/>
                    </Picker>
                </Item>
            </View>
        );
    }

    /*
    | ACTION METHODS -------------------------------------------------------------------------------
     */

    _onInputChange(data) {
        this.setState({dataSource: Object.assign(this.state.dataSource, data)});
        if(typeof this.props.onFormChanged ==='function') this.props.onFormChanged(this.state);
    }

    inputValue(name, defaultValue = '') {
        if (typeof this.state.dataSource[name] === 'undefined') return defaultValue;
        return this.state.dataSource[name];
    }

    _setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }
}

export default InfoForm;
