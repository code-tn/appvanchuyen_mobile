import React, { Component } from 'react';
import {
    Modal,
    View,
    TextInput,
    Dimensions,
    StyleSheet,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import {
    Collapse,
    CollapseHeader,
    CollapseBody,
} from 'accordion-collapse-react-native';
import { Button, Text, Icon } from 'native-base';
import Styles from '../../../Assets/Styles';
import Colors from '../../../Assets/Colors';

const Screen = Dimensions.get('screen');

class AddressForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mainAddress: '',
            modalVisible: false,
            dataSource: {
                chitiet: '',
                tinh: '',
                huyen: '',
                phuong: '',
            }
        };
    }

    render() {
        const { input, btnCloseModal, modalDialog, modalBody, modalHeader, fontFace } = Styles;
        const {
            desc, mainLocation, collapseHeader, collapseHeaderBox, collapseHeaderText,
            collapseHeaderIcon, collapseBody, collapseBodyText
        } = addresFormStyles;
        return (
            <View style={fontFace}>
                <TextInput
                    value={this.inputValue('chitiet')}
                    onChangeText={text => this._onInputChange({ chitiet: text })}
                    placeholder='Số nhà, hẻm, ngõ, ngách, tòa nhà'
                    style={input}
                />
                <TouchableOpacity onPress={() => this.setState({modalVisible:true})}>
                    <TextInput style={input} editable={false}
                        placeholder='Tỉnh/thành phố, quận/huyện, phường/xã'
                        value={this.state.mainAddress}
                    />
                </TouchableOpacity>
                <Modal transparent animationType="slide" visible={this.state.modalVisible}
                       presentationStyle='overFullScreen' onRequestClose={() => {
                }}
                >
                    <View style={modalDialog}>
                        <View style={modalBody}>
                            <View style={modalHeader}>
                                <Button
                                    transparent style={btnCloseModal}
                                    onPress={() => this.setState({modalVisible:false})}
                                >
                                    <Text style={{ color: '#fff', fontSize: 12, paddingLeft: 5
                                    }}>Đóng</Text>
                                </Button>
                            </View>
                            <View style={{ padding: 10, backgroundColor: '#eee' }}>
                                <Text style={desc}>
                                    Lưu ý: Khu vực chưa có trong danh sách này Chúng tôi chưa có
                                    tuyến,
                                    sẽ phải hoàn trả hàng về mất thời gian của shop. Xin vui lòng
                                    lưu ý
                                </Text>

                                <ScrollView style={{
                                    height: Screen.height * 0.65, backgroundColor: '#fff'
                                }}>
                                    <Text style={mainLocation}>Miền Nam</Text>
                                    <View style={{ paddingVertical: 7, paddingHorizontal: 10 }}>
                                        <Collapse>
                                            <CollapseHeader style={collapseHeader}>
                                                <View style={collapseHeaderBox}>
                                                    <Text style={collapseHeaderText}>Bà Rịa - Vũng
                                                        tàu</Text>
                                                    <Icon style={collapseHeaderIcon}
                                                          type='FontAwesome' name='angle-right'/>
                                                </View>
                                            </CollapseHeader>
                                            <CollapseBody style={collapseBody}>
                                                <Collapse>
                                                    <CollapseHeader style={collapseHeader}>
                                                        <View style={collapseHeaderBox}>
                                                            <Text style={collapseHeaderText}>TP Vũng
                                                                Tàu</Text>
                                                            <Icon style={collapseHeaderIcon}
                                                                  type='FontAwesome'
                                                                  name='angle-right'/>
                                                        </View>
                                                    </CollapseHeader>
                                                    <CollapseBody style={collapseBody}>
                                                        <Text
                                                            style={collapseBodyText}
                                                            onPress={() => this._chooseMainAddress('Bà Rịa Vũng Tàu', 'TP Vũng Tàu', 'Phường Thắng Tam')}
                                                        >
                                                            Phường Thắng Tam
                                                        </Text>
                                                        <Text
                                                            style={collapseBodyText}
                                                            onPress={() => this._chooseMainAddress('Bà Rịa Vũng Tàu', 'TP Vũng Tàu', 'Xã Long Sơn')}
                                                        >
                                                            Xã Long Sơn
                                                        </Text>
                                                    </CollapseBody>
                                                </Collapse>
                                                <Collapse>
                                                    <CollapseHeader style={collapseHeader}>
                                                        <View style={collapseHeaderBox}>
                                                            <Text
                                                                style={collapseHeaderText}
                                                            >
                                                                TP Bà Rịa
                                                            </Text>
                                                            <Icon style={collapseHeaderIcon}
                                                                  type='FontAwesome'
                                                                  name='angle-right'/>
                                                        </View>
                                                    </CollapseHeader>
                                                    <CollapseBody style={collapseBody}>
                                                        <Text
                                                            style={collapseBodyText}
                                                            onPress={() => this._chooseMainAddress('Bà Rịa Vũng Tàu', 'TP Bà Rịa', 'Phường 1')}
                                                        >
                                                            Phường 1
                                                        </Text>
                                                        <Text
                                                            style={collapseBodyText}
                                                            onPress={() => this._chooseMainAddress('Bà Rịa Vũng Tàu', 'TP Bà Rịa', 'Phường 2')}
                                                        >
                                                            Phường 2
                                                        </Text>
                                                    </CollapseBody>
                                                </Collapse>
                                            </CollapseBody>
                                        </Collapse>
                                    </View>
                                </ScrollView>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }

    /*
    | ACTION METHODS -------------------------------------------------------------------------------
     */

    _onInputChange(data) {
        this.setState({ dataSource: Object.assign(this.state.dataSource, data) });
        if (typeof this.props.onFormChanged === 'function') this.props.onFormChanged(this.state);
    }

    inputValue(name, defaultValue = '') {
        if (typeof this.state.dataSource[name] === 'undefined') return defaultValue;
        return this.state.dataSource[name];
    }

    _chooseMainAddress(strProvince, strDistrict, strWard) {
        this.setState({
            mainAddress: strWard + ', ' + strDistrict + ', ' + strProvince
        });
        this._onInputChange({ tinh: strProvince });
        this._onInputChange({ huyen: strDistrict });
        this._onInputChange({ phuong: strWard });
        this.setState({modalVisible: false});
    }
}

const addresFormStyles = StyleSheet.create({
    desc: {
        color: '#838383',
        fontSize: 13,
        fontStyle: 'italic',
        paddingBottom: 10
    },
    mainLocation: {
        fontSize: 13,
        backgroundColor: Colors.mainColor,
        color: '#fff',
        padding: 5,
    },
    collapseHeader: {
        fontSize: 14,
        color: Colors.blackColor,
        fontWeight: '600',
        paddingVertical: 3,
        borderBottomWidth: 1,
        borderBottomColor: '#eee',
    },
    collapseHeaderBox: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        fontWeight: 'normal',
        color: '#333'
    },
    collapseHeaderIcon: {
        fontSize: 14,
        fontWeight: 'normal',
        color: '#333'
    },
    collapseHeaderText: {
        fontSize: 14,
        color: '#333',
        fontWeight: 'normal',
    },
    collapseBody: {
        paddingLeft: 8,
    },
    collapseBodyText: {
        fontSize: 13,
        color: '#333',
        paddingVertical: 2,
        borderBottomWidth: 1,
        borderBottomColor: '#eee',
    }
});
export default AddressForm;
