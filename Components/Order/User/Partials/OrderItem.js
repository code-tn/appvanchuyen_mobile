import React, { PureComponent } from 'react';
import { View, Image } from 'react-native';
import { Text, Button, Icon } from 'native-base';
import truckIcon from '../Assets/Images/truck.png';
import OrderItemStyles from '../Assets/OrderItemStyles';

class OrderItem extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        const {
            boldStyle,
            line,
            orderItem,
            orderItemCode,
            orderItemCodeIcon,
            orderItemCodeText,
            orderItemText,
            orderItemControl,
            btnOIC,
            btnOICIcon,
            btnOICText,
            noneMarginRight,
        } = OrderItemStyles;
        const { id, created_at, ten_kienhang } = this.props.order;
        let trangthai_formated = typeof this.props.order.tt_trangthai[0] !== 'undefined' ? this.props.order.tt_trangthai[0].ten_trangthai : false;
        const ttNguoinhan = this.props.order.diachi_nguoinhan[0];
        return (
            <View style={orderItem}>
                <View style={orderItemCode}>
                    <Image source={truckIcon} style={orderItemCodeIcon} />
                    <Text style={orderItemCodeText}>#{id}</Text>
                </View>
                {typeof this.props.order.hoantrahang !== 'undefined' && this.props.order.hoantrahang === 1 ?
                <View style={[orderItemText, { flexDirection: 'row', alignItems: 'center' }]}>
                    <Icon type='FontAwesome' name='refresh' style={{ color: '#ca0e00', fontSize: 17, marginRight: 10 }}></Icon>
                    <Text style={{ color: '#ca0e00' }}>Đã yêu cầu hoàn trả</Text>
                </View> : null}
                <Text style={orderItemText}>Tên: {ttNguoinhan.hoten_nguoinhan} / {ttNguoinhan.so_dienthoai}</Text>
                <Text style={orderItemText}>Địa chỉ: {ttNguoinhan.chitiet}, {ttNguoinhan.xaphuong[0].name}, {ttNguoinhan.quanhuyen[0].name}, {ttNguoinhan.tinhthanhpho[0].name} </Text>
                <Text style={orderItemText}>Ngày tạo: {created_at}</Text>
                <Text style={[orderItemText, line]}>
                    Tên kiện hàng: {ten_kienhang}
                </Text>
                <Text style={[orderItemText, boldStyle, line]}>
                    Trạng thái: {trangthai_formated ? trangthai_formated : 'Đang chờ'}
                </Text>
                <View style={[orderItemControl, line]}>
                    {this.props.order.trangthai === "" ? (
                        <Button
                            iconLeft
                            transparent
                            style={[btnOIC, { marginRight: 'auto' }]}
                            onPress={() => this.props.navigation.navigate('EditOrderScreen', {
                                id_donhang: id
                            })}
                        >
                            <Icon type='FontAwesome' name="file-text-o" style={btnOICIcon} />
                            <Text uppercase={false} style={btnOICText}>Chỉnh sửa</Text>
                        </Button>
                    ) : null}
                    <Button
                        iconLeft
                        transparent
                        style={[btnOIC, noneMarginRight, { alignSelf: 'flex-end' }]}
                        onPress={() => this.props.navigation.navigate('UserOrderDetailScreen', {
                            idOrder: id
                        })}
                    >
                        <Icon type='FontAwesome' name="eye" style={btnOICIcon} />
                        <Text uppercase={false} style={btnOICText}>Chi tiết</Text>
                    </Button>
                </View>
            </View>
        );
    }
}

export default OrderItem;
