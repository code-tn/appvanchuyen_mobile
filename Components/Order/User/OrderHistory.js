import React, { Component } from 'react';
import { FlatList, RefreshControl, ActivityIndicator, Alert } from 'react-native';
import {
    Container,
    Content,
    View,
    Text
} from 'native-base';
import Styles from '../../Assets/Styles';
import OrderHistoryStyles from './Assets/OrderHistoryStyles';
import TopBarOnlyTitle from '../../Shared/TopBarOnlyTitle';
import OrderItem from './Partials/OrderItem';
import TopControls from './TopControls';

import Loading from '../../Shared/Loading';
import { Auth } from '../../../Networking/auth';

import { getOrderListByUserID } from '../../../Data/orderRepo';
import Colors from '../../Assets/Colors';
import { ApiConfig } from '../../../config/api';

class OrderHistory extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            keyword: '',
            isLoading: true,
            refreshing: false,
            idUser: '',
            user: '',
            loadmore: false,
            page: 1,
            total: '',
            nextPageURL: '',
            debug: true,
            datarq: '',
            isFilter: false,
            filter: {
                tu_ngay: new Date().toISOString().slice(0, 10),
                den_ngay: new Date().toISOString().slice(0, 10),
                ten_kien_hang: '',
                trang_thai_don_hang: '',
                id_khachhang: ''
            }
        };
    }

    async componentDidMount() {
        const user = await Auth.currentUser();
        this.setState({ idUser: user.id, user: user, filter: { ...this.state.filter, id_khachhang: user.id } });
        await this.loadData();
    }
    // -----------------------------------------------------------------------------------------------------------------

    async loadData(params = null, request_url = null) {
        const req_params = params == null ? { id_khachhang: this.state.user.id } : null;
        const requestUrl = request_url !== null ? request_url : undefined;
        await getOrderListByUserID(req_params, requestUrl, rJson => {
            if (rJson.success) {
                this.setState({
                    data: [...this.state.data, ...rJson.data.data],
                    isLoading: false,
                    refreshing: false,
                    total: rJson.data.total,
                    nextPageURL: typeof rJson.data.next_page_url !== "undefined" ? rJson.data.next_page_url : '',
                    loadmore: false,
                    datarq: rJson.data
                });
            } else {
                Alert.alert(
                    'Lỗi',
                    rJson.message,
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
            }
        });
    }

    _onRefresh = async () => {
        this.setState({ refreshing: true, isFilter: false, data: [] });
        await this.loadData();
    };

    onEndReached = async () => {
        if (this.state.datarq.current_page < this.state.datarq.last_page) {
            let req_params;
            if (this.state.isFilter) {
                req_params = this.state.filter;
            } else {
                req_params = { id_khachhang: this.state.user.id };
            }
            const request_url = this.state.nextPageURL;
            this.setState({ loadmore: true });
            await getOrderListByUserID(req_params, request_url, rJson => {
                if (rJson.success) {
                    this.setState({
                        data: [...this.state.data, ...rJson.data.data],
                        total: rJson.data.total,
                        nextPageURL: typeof rJson.data.next_page_url !== "undefined" ? rJson.data.next_page_url : '',
                        loadmore: false,
                        datarq: rJson.data
                    });
                } else {
                    this.setState({ loadmore: false });
                }
            });
        }
    }
    async filterOrder() {
        this.UserTopControl._setModalVisible(false);
        const req_url = ApiConfig.api_url + 'api/v1/search-customers-order';
        let params = this.state.filter;
        this.setState({ refreshing: true, isFilter: true, data: [] });
        await getOrderListByUserID(params, req_url, rJson => {
            if (rJson.success) {
                this.setState({
                    data: [...this.state.data, ...rJson.data.data],
                    refreshing: false,
                    total: rJson.data.total,
                    nextPageURL: typeof rJson.data.next_page_url !== "undefined" ? rJson.data.next_page_url : '',
                    loadmore: false,
                    datarq: rJson.data
                });
            } else {
                Alert.alert(
                    'Lỗi',
                    rJson.message,
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
            }
        });
    }
    render() {
        const { fontFace } = Styles;
        const { data } = this.state;
        const { content } = OrderHistoryStyles;
        const { navigation } = this.props;
        if (this.state.isLoading) {
            return (
                <Loading />
            );
        }
        return (
            <Container style={[fontFace, { backgroundColor: '#eee' }]}>
                <TopBarOnlyTitle title='Đơn hàng' />
                <TopControls
                    ref={ref => this.UserTopControl = ref}
                    navigation={navigation}
                    user={this.state.user}
                    parent={this}
                    filterOrder={() => this.filterOrder()}
                />
                <View style={[content, { flex: 1, padding: 10, }]}>
                    {
                        (this.state.data === undefined ||
                            this.state.refreshing ||
                            this.state.data.length === 0) ?
                            (
                                <View>
                                    <Text style={{ fontSize: 15, textAlign: 'center' }}>
                                        {this.state.refreshing ?
                                            'Đang tải danh sách...' : 'Không có đơn hàng nào...'}
                                    </Text>
                                </View>
                            ) : null
                    }
                    <FlatList
                        style={{ flex: 1 }}
                        const
                        data={data}
                        renderItem={({ item }) =>
                            <OrderItem navigation={navigation} order={item} />
                        }
                        keyExtractor={(item) => item.id.toString()}
                        onEndReached={this.onEndReached.bind(this)}
                        onEndReachedThreshold={0.1}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this._onRefresh}
                            />
                        }
                    />
                    {this.state.loadmore ?
                        <View style={{ paddingTop: 5 }}>
                            <ActivityIndicator size='small' color={Colors.mainColor} />
                        </View>
                        : null}
                </View>
            </Container>
        );
    }
}

export default OrderHistory;
