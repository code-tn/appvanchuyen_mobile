import React, { Component } from 'react';
import { View, TextInput, SafeAreaView, TouchableOpacity, Alert, ActivityIndicator } from 'react-native';
import {
    Root, Container, Tabs, Tab, TabHeading, Toast,
    Button, Content, CheckBox, Picker, Icon, Item, Label, Text,
} from 'native-base';
import { TextInputMask } from 'react-native-masked-text';
//
import TopBar from '../../Shared/TopBar';
import Colors from '../../Assets/Colors';
import Styles from '../../Assets/Styles';
import ComLoading from '../../Shared/Loading';
import { Auth } from './../../../Networking/auth';
import { Order } from './../../../Networking/order';
import AddressFinder from '../Partials/AddressFinder';
import FormatCurrency from '../../Assets/FormatCurrency';
import CreateOrderStyles from './Assets/CreateOrderStyles';
import { getProvinceList, getDistrictByProvinceID, getWardByDistrictID } from '../../../Data/locationRepo';
import { getCustomerBookmark } from '../../../Data/userRepo';

class CreateOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSubmit: false,
            isCalc: false,
            shopList: '',
            id_khachhang: '',
            dataUser: null,
            dataUser_status: false,
            modal_addressfinder: false,
            modal_addressfinder2: false,
            provinceList: '',
            districtListSender: '',
            districtListReceiver: '',
            wardListSender: '',
            wardListReceiver: '',
            duocthanhtoan_boi: '',
            /*
            + ghichu: text
            + morong: extra fields
            + thanhtoan_ship_boi: nguoigui/nguoinhan
             */
            dataOrder: {
                id_khachhang: '',
                ten_kienhang: '',
                mota: '',
                loai_hanghoa: 'chatran',
                trongluong: '0',
                giatri_kienhang: '',
                thuho: false,
                gui_tinhphi: '',
                ghichu: '',
                // id_diachi_nguoigui: null,
                morong: '',
            },
            /*
            + Tên người nhận
            + Số điện thoại
            + Quốc gia
            + Số nhà, hẻm, ngõ, ngách, tòa nhà
            + Tỉnh/ thành phố
             */
            dataReceiver: {
                ten_goinho: '',
                hoten_nguoinhan: '',
                so_dienthoai: '',
                tinh: '77',
                huyen: '',
                phuong: '',
                chitiet: '',
                id_taikhoan: '',
            },
            dataSender: {
                ten_goinho: '',
                hoten_nguoinhan: '',
                so_dienthoai: '',
                tinh: '77',
                huyen: '',
                phuong: '',
                chitiet: '',
                id_taikhoan: '',
            },
            currentTab: 0,
            dataReceiver_save: false,
            dataSender_save: false,
            nguoigui_diachi: [],
            nguoigui_diachi_status: false,
            isLoading: false,

            // UNUSED
            cur_nguoigui_diachi: '',
            cur_nguoigui_dienthoai: '',
            cur_nguoigui_email: '',
            cur_giatri_kienhang: '',
            cur_order_tongtien: '',
            cur_order_tongtien_text: 0,
            cur_order_shipcost_text: 0,
            accept_term: false,
            toasts: {
                success: { buttonText: 'OK', type: 'success', duration: 5000, position: 'top' },
                warning: { buttonText: 'OK', type: 'warning', duration: 5000, position: 'top' },
                error: { buttonText: 'OK', type: 'error', duration: 5000, position: 'top' },
            },
            actionText: 'Đang tải...'
        };
        this._onSubmitForm.bind(this);
    }

    async componentWillMount() {
        this.loading();
        let user = this.props.navigation.getParam('user', false);
        if (!user) {
            user = await Auth.currentUser();
        }
        let currentUser = user;

        await getProvinceList(res => {
            this.setState({ provinceList: res.data });
        });
        await getCustomerBookmark({
            id_khachhang: currentUser.id,
            loai_diachi: 'nguoigui'
        }, (rJson) => {
            if (rJson.success) {
                this.setState({
                    shopList: rJson.data,
                });
            }
        });
        await getDistrictByProvinceID(this.state.dataReceiver.tinh, res => {
            this.setState({
                districtListSender: res.data,
                districtListReceiver: res.data,
                dataReceiver: {
                    ...this.state.dataReceiver,
                    huyen: res.data[0].maqh
                },
                dataSender: {
                    ...this.state.dataSender,
                    huyen: res.data[0].maqh
                },
            });
        });
        await getWardByDistrictID(this.state.districtListReceiver[0].maqh, res => {
            this.setState({
                wardListSender: res.data,
                wardListReceiver: res.data,
                dataReceiver: {
                    ...this.state.dataReceiver,
                    phuong: res.data[0].xaid
                },
                dataSender: {
                    ...this.state.dataSender,
                    phuong: res.data[0].xaid
                },
            });
        });
        if (typeof this.state.shopList !== 'string' && this.state.shopList !== '') {
            const addressList = this.state.shopList[0];
            this.setState({
                dataSender: {
                    hoten_nguoinhan: addressList.hoten_nguoinhan,
                    so_dienthoai: addressList.so_dienthoai,
                    tinh: addressList.tinh,
                    huyen: addressList.huyen,
                    phuong: addressList.phuong,
                    chitiet: addressList.chitiet,
                    id_taikhoan: currentUser.id,
                },
            });
            await getDistrictByProvinceID(this.state.dataSender.tinh, res => {
                this.setState({
                    districtListSender: res.data,
                });
            });
            await getWardByDistrictID(this.state.dataSender.huyen, res => {
                this.setState({
                    wardListSender: res.data,
                });
            });
        }
        this.setState({
            id_khachhang: currentUser.id,
            dataUser: currentUser,
            dataUser_status: currentUser !== null,
            duocthanhtoan_boi: currentUser.id,
            dataOrder: { ...this.state.dataOrder, id_khachhang: currentUser.id },
            isLoading: false,
        });
    }

    render_content() {
        if (!this.state.dataUser_status) {
            return (<ComLoading loadingText={'Đang tải...'} />);
        }
        return (
            <View style={{ flex: 1 }}>
                {this.state.isLoading ?
                    <ComLoading loadingText={this.state.actionText} /> : this.render_tabs()}
            </View>
        );
    }
    upButtonHandler = () => {
        this._content._root.scrollToPosition(0, 0, true);
    };
    render_tabs() {
        const { tabContainerStyle } = CreateOrderStyles;
        return (
            <View style={{ flex: 1 }} >
                <Content
                    style={Styles.content}
                    ref={ref => { this._content = ref }}
                >
                    <Tabs
                        tabContainerStyle={tabContainerStyle}
                        tabBarUnderlineStyle={{
                            backgroundColor: Colors.mainColor, height: 5,
                        }}
                        ref="createordertabs"
                        onChangeTab={({ i }) => this.setState({ currentTab: i })}
                    >
                        {this.render_tab_nguoigui()}
                        {this.render_tab_nguoinhan()}
                        {this.render_tab_buukien()}
                    </Tabs>
                </Content>
                <View style={{ padding: 10, backgroundColor: '#eee' }}>
                    {this.render_bottomStepBar()}
                </View>
            </View>
        );
    }

    // TAB -----------------------------------------------------------------------------------------
    render_tab_nguoigui() {
        const {
            numberStep, titleStep, tabHeading, wrapper, tabStyle, activeIconStyle, checkbox
        } = CreateOrderStyles;
        const { input } = Styles;
        const dataSender = this.state.dataSender;

        return (
            <Tab
                heading={
                    <TabHeading style={{ backgroundColor: '#fff' }}>
                        <View style={tabHeading}>
                            <View style={[numberStep, this.state.currentTab === 0 ? activeIconStyle : null]}>
                                <Text style={{ color: this.state.currentTab === 0 ? '#fff' : '#838383', }}>1</Text>
                            </View>
                            <Text style={titleStep}>Người gửi</Text>
                        </View>
                    </TabHeading>
                } style={tabStyle}
            >
                <View style={wrapper}>
                    <View style={{ flexDirection: 'row' }}>
                        <TextInput
                            autoCapitalize='words'
                            style={[input, { flexGrow: 1 }]}
                            value={this.state.dataSender.hoten_nguoinhan}
                            onChangeText={text => this.updateDataSender({ hoten_nguoinhan: text })}
                            placeholder='Họ và tên...'
                        />
                        <Button
                            onPress={() => {
                                this.setState({ modal_addressfinder: true })
                            }}
                            style={{ height: 40, marginLeft: 5, backgroundColor: Colors.subColor }}
                        >
                            <Icon type='FontAwesome' name='address-book-o' style={{ fontSize: 22 }} />
                        </Button>
                    </View>
                    <AddressFinder visible={this.state.modal_addressfinder}
                        options={{
                            loai_diachi: 'nguoigui',
                            user: this.state.dataUser
                        }}
                        onClose={() => {
                            this.setState({ modal_addressfinder: false })
                        }}
                        onSelect={async (item) => {
                            await getDistrictByProvinceID(item.tinh, res => {
                                this.setState({
                                    districtListSender: res.data,
                                });
                            });
                            await getWardByDistrictID(item.huyen, res => {
                                this.setState({
                                    wardListSender: res.data,
                                });
                            });
                            this.setState({
                                dataSender: {
                                    ...this.state.dataSender,
                                    ten_goinho: '',
                                    chitiet: item.chitiet,
                                    so_dienthoai: item.so_dienthoai,
                                    hoten_nguoinhan: item.hoten_nguoinhan,
                                    tinh: item.tinh,
                                    huyen: item.huyen,
                                    phuong: item.phuong
                                }
                            });
                        }}
                    />


                    <TextInput
                        style={input}
                        value={this.state.dataSender.so_dienthoai}
                        onChangeText={text => this.updateDataSender({ so_dienthoai: text })}
                        placeholder='Số điện thoại...'
                        dataDetectorTypes='phoneNumber'
                        keyboardType='phone-pad'
                    />
                    {this.state.provinceList ? (
                        <Item picker>
                            <Label style={{ fontSize: 14, width: 90 }}>Tỉnh/ TP</Label>
                            <Picker
                                block
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                style={{ width: undefined }}
                                iosHeader='Tỉnh/ TP'
                                placeholder="Chọn Tỉnh/ Thành phố"
                                placeholderStyle={{ color: Colors.blackColor }}
                                placeholderIconColor={Colors.blackColor}
                                selectedValue={this.state.dataSender.tinh}
                                onValueChange={async (val) => {
                                    await getDistrictByProvinceID(val, res => {
                                        this.setState({
                                            districtListSender: res.data,
                                            dataSender: {
                                                ...this.state.dataSender,
                                                tinh: val,
                                                huyen: res.data[0].maqh,
                                            }
                                        });
                                    });
                                    await getWardByDistrictID(this.state.districtListSender[0].maqh, res => {
                                        this.setState({
                                            wardListSender: res.data,
                                            dataSender: {
                                                ...this.state.dataSender,
                                                phuong: res.data[0].xaid,

                                            }
                                        });
                                    });
                                }}
                            >
                                {this.state.provinceList.map(item => {
                                    return <Picker.Item label={item.name} key={item.matp} value={item.matp} />
                                })}
                            </Picker>
                        </Item>
                    ) : <Text>Không có dữ liệu Tỉnh/ TP</Text>
                    }
                    {this.state.districtListSender ? (
                        <Item picker style={{ marginBottom: 10 }}>
                            <Label style={{ fontSize: 14, width: 90 }}>Quận/ Huyện</Label>
                            <Picker
                                block
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                style={{ width: undefined }}
                                iosHeader='Quận/ Huyện'
                                placeholder="Chọn Quận/ Huyện"
                                placeholderStyle={{ color: Colors.blackColor }}
                                placeholderIconColor={Colors.blackColor}
                                selectedValue={this.state.dataSender.huyen}
                                onValueChange={async (val) => {
                                    await getWardByDistrictID(val, res => {
                                        this.setState({
                                            wardListSender: res.data,
                                            dataSender: {
                                                ...this.state.dataSender,
                                                huyen: val,
                                                phuong: res.data[0].xaid,
                                            }
                                        });
                                    })
                                }}
                            >
                                {this.state.districtListSender.map(item => {
                                    return <Picker.Item label={item.name} key={item.maqh} value={item.maqh} />
                                })}
                            </Picker>
                        </Item>
                    ) : (
                            <Text>Không có dữ liệu Quận/ Huyện</Text>
                        )}
                    {this.state.wardListSender ? (
                        <Item picker style={{ marginBottom: 10 }}>
                            <Label style={{ fontSize: 14, width: 90 }}>Phường/ Xã</Label>
                            <Picker
                                block
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                style={{ width: undefined }}
                                iosHeader='Phường/ Xã'
                                placeholder="Chọn Phường/ Xã"
                                placeholderStyle={{ color: Colors.blackColor }}
                                placeholderIconColor={Colors.blackColor}
                                selectedValue={this.state.dataSender.phuong}
                                onValueChange={val => {
                                    this.setState({
                                        dataSender: {
                                            ...this.state.dataSender, phuong: val
                                        }
                                    })
                                }}
                            >
                                {this.state.wardListSender.map(item => {
                                    return <Picker.Item label={item.name} key={item.xaid} value={item.xaid} />
                                })}
                            </Picker>
                        </Item>
                    ) : (
                            <Text>Không có dữ liệu Phường/ Xã</Text>
                        )}

                    <View>
                        <TextInput
                            value={this.state.dataSender.chitiet}
                            onChangeText={v => this.updateDataSender({ chitiet: v })}
                            placeholder='Số nhà, hẻm, ngõ, ngách, tòa nhà'
                            style={Styles.input}
                        />
                    </View>

                    <TouchableOpacity style={checkbox}
                        onPress={() => this.setState({
                            dataSender_save: !this.state.dataSender_save
                        })}
                    >
                        <CheckBox
                            checked={this.state.dataSender_save}
                            style={{ left: 0, marginRight: 5 }}
                            color={Colors.subColor}
                            onPress={() => this.setState({
                                dataSender_save: !this.state.dataSender_save
                            })}
                        />
                        <Text>Thêm vào danh bạ</Text>
                    </TouchableOpacity>
                    {this.state.dataSender_save ? (
                        <TextInput 
                            autoCapitalize='words'
                            style={input} 
                            value={dataSender.ten_goinho}
                            onChangeText={text => this.updateDataSender({ ten_goinho: text })}
                            placeholder='Tên gợi nhớ'
                        />
                    ) : null}

                </View>
            </Tab>
        );
    }
    render_tab_nguoinhan() {
        const {
            numberStep, titleStep, tabHeading, wrapper, tabStyle, activeIconStyle, checkbox
        } = CreateOrderStyles;
        const { input } = Styles;
        const dataReceiver = this.state.dataReceiver;
        return (
            <Tab
                heading={
                    <TabHeading style={{ backgroundColor: '#fff' }}>
                        <View style={tabHeading}>
                            <View
                                style={[numberStep, this.state.currentTab === 1 ? activeIconStyle : null]}
                            >
                                <Text style={{ color: this.state.currentTab === 1 ? '#fff' : '#838383', }}>2</Text>
                            </View>
                            <Text style={titleStep}>Người nhận</Text>
                        </View>
                    </TabHeading>
                }
                style={tabStyle}
            >
                <View style={wrapper}>
                    <View style={{ flexDirection: 'row' }}>
                        <TextInput
                            autoCapitalize='words'
                            style={[input, { flexGrow: 1 }]}
                            value={dataReceiver.hoten_nguoinhan}
                            onChangeText={text => this.updatedataReceiver({ hoten_nguoinhan: text })}
                            placeholder='Tên người nhận'
                        />
                        <Button
                            style={{ marginLeft: 5, height: 40, backgroundColor: Colors.subColor }}
                            onPress={() => {
                                this.setState({ modal_addressfinder2: true })
                            }}>
                            <Icon name='address-book-o' type='FontAwesome' style={{ fontSize: 22 }} />
                        </Button>
                    </View>
                    <TextInput
                        style={input}
                        value={dataReceiver.so_dienthoai}
                        onChangeText={text => this.updatedataReceiver({ so_dienthoai: text })}
                        placeholder='Số điện thoại'
                        dataDetectorTypes='phoneNumber'
                        keyboardType='phone-pad'
                    />

                    {this.state.provinceList ? (
                        <Item picker>
                            <Label style={{ fontSize: 14, width: 90 }}>Tỉnh/ TP</Label>
                            <Picker
                                block
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                style={{ width: undefined }}
                                placeholder="Chọn Tỉnh/ Thành phố"
                                placeholderStyle={{ color: Colors.blackColor }}
                                placeholderIconColor={Colors.blackColor}
                                selectedValue={this.state.dataReceiver.tinh}
                                onValueChange={async (val) => {
                                    await getDistrictByProvinceID(val, res => {
                                        this.setState({
                                            districtListReceiver: res.data,
                                            dataReceiver: {
                                                ...this.state.dataReceiver,
                                                tinh: val,
                                                huyen: res.data[0].maqh
                                            }
                                        });
                                    });
                                    await getWardByDistrictID(this.state.districtListReceiver[0].maqh, res => {
                                        this.setState({
                                            wardListReceiver: res.data,
                                            dataReceiver: {
                                                ...this.state.dataReceiver,
                                                phuong: res.data[0].xaid
                                            }
                                        });
                                    });
                                }}
                            >
                                {this.state.provinceList.map(item => {
                                    return <Picker.Item label={item.name} key={item.matp} value={item.matp} />
                                })}
                            </Picker>
                        </Item>
                    ) : <Text>Không có dữ liệu Tỉnh/ TP</Text>
                    }

                    {this.state.districtListReceiver ? (
                        <Item picker style={{ marginBottom: 10 }}>
                            <Label style={{ fontSize: 14, width: 90 }}>Quận/ Huyện</Label>
                            <Picker
                                block
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                style={{ width: undefined }}
                                placeholder="Chọn Quận/ Huyện"
                                placeholderStyle={{ color: Colors.blackColor }}
                                placeholderIconColor={Colors.blackColor}
                                selectedValue={this.state.dataReceiver.huyen}
                                onValueChange={async (val) => {
                                    await getWardByDistrictID(val, res => {
                                        this.setState({
                                            wardListReceiver: res.data,
                                            dataReceiver: {
                                                ...this.state.dataReceiver,
                                                huyen: val,
                                                phuong: res.data[0].xaid
                                            }
                                        });
                                    });
                                }}
                            >
                                {this.state.districtListReceiver.map(item => {
                                    return <Picker.Item label={item.name} key={item.maqh} value={item.maqh} />
                                })}
                            </Picker>
                        </Item>
                    ) : (
                            <Text>Không có dữ liệu Quận/ Huyện</Text>
                        )}
                    {this.state.wardListReceiver ? (
                        <Item picker style={{ marginBottom: 10 }}>
                            <Label style={{ fontSize: 14, width: 90 }}>Phường/ Xã</Label>
                            <Picker
                                block
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                style={{ width: undefined }}
                                iosHeader='Phường/ Xã'
                                placeholder="Chọn Phường/ Xã"
                                placeholderStyle={{ color: Colors.blackColor }}
                                placeholderIconColor={Colors.blackColor}
                                selectedValue={this.state.dataReceiver.phuong}
                                onValueChange={val => {
                                    this.setState({
                                        dataReceiver: {
                                            ...this.state.dataReceiver,
                                            phuong: val
                                        }
                                    })
                                }}
                            >
                                {this.state.wardListReceiver.map(item => {
                                    return <Picker.Item label={item.name} key={item.xaid} value={item.xaid} />
                                })}
                            </Picker>
                        </Item>
                    ) : (
                            <Text>Không có dữ liệu Phường/ Xã</Text>
                        )}

                    <View>
                        <TextInput
                            value={this.state.dataReceiver.chitiet}
                            onChangeText={v => this.updatedataReceiver({ chitiet: v })}
                            placeholder='Số nhà, hẻm, ngõ, ngách, tòa nhà'
                            style={Styles.input}
                        />
                    </View>

                    <TouchableOpacity
                        style={checkbox}
                        onPress={() => this.setState({
                            dataReceiver_save: !this.state.dataReceiver_save
                        })}
                    >
                        <CheckBox
                            checked={this.state.dataReceiver_save}
                            style={{ left: 0, marginRight: 5 }}
                            color={Colors.subColor}
                            onPress={() => this.setState({
                                dataReceiver_save: !this.state.dataReceiver_save
                            })}
                        />
                        <Text>Thêm vào danh bạ</Text>
                    </TouchableOpacity>

                    <AddressFinder
                        visible={this.state.modal_addressfinder2}
                        options={{
                            loai_diachi: 'nguoinhan', user: this.state.dataUser
                        }}
                        onClose={() => {
                            this.setState({ modal_addressfinder2: false })
                        }}
                        onSelect={async (item) => {
                            await getDistrictByProvinceID(item.tinh, res => {
                                this.setState({
                                    districtListReceiver: res.data,
                                });
                            });
                            await getWardByDistrictID(item.huyen, res => {
                                this.setState({
                                    wardListReceiver: res.data,
                                });
                            });
                            this.setState({
                                dataReceiver: {
                                    ...this.state.dataReceiver,
                                    ten_goinho: '',
                                    chitiet: item.chitiet,
                                    so_dienthoai: item.so_dienthoai,
                                    hoten_nguoinhan: item.hoten_nguoinhan,
                                    tinh: item.tinh,
                                    huyen: item.huyen,
                                    phuong: item.phuong
                                }
                            });
                        }}
                    />

                    {this.state.dataReceiver_save ? (
                        <TextInput 
                            autoCapitalize='words'
                            style={input} 
                            value={dataReceiver.ten_goinho}
                            onChangeText={text => this.updatedataReceiver({ ten_goinho: text })}
                            placeholder='Tên gợi nhớ'
                        />) : null}
                </View>
            </Tab>
        );
    }
    render_tab_buukien() {
        const {
            numberStep, titleStep, tabHeading, wrapper, tabStyle, activeIconStyle,
            checkbox, totalPrice
        } = CreateOrderStyles;
        const { input, link, textarea } = Styles;
        const dataOrder = this.state.dataOrder;
        let weightList = [];
        for (let i = 0; i < 50.1;) {
            weightList.push(<Picker.Item label={parseFloat(i).toFixed(1).toString()} value={parseFloat(i).toFixed(1).toString()} key={parseFloat(i).toFixed(1).toString()} />);
            i = i + 0.1;
        }
        return (
            <Tab
                heading={
                    <TabHeading style={{ backgroundColor: '#fff' }}>
                        <View style={tabHeading}>
                            <View style={[numberStep,
                                this.state.currentTab === 2 ? activeIconStyle : null]}
                            >
                                <Text style={{ color: this.state.currentTab === 2 ? '#fff' : '#838383', }}>3</Text>
                            </View>
                            <Text style={titleStep}>Bưu kiện</Text>
                        </View>
                    </TabHeading>
                } style={tabStyle}
            >
                <View style={wrapper}>
                    <View>
                        <Label style={{ fontSize: 14, paddingBottom: 4 }}>Tên kiện hàng</Label>
                        <TextInput style={input} value={dataOrder.ten_kienhang}
                            onChangeText={text => this.uDO({ ten_kienhang: text })}
                            placeholder='Tên kiện hàng'
                        />
                        <Item picker style={{ borderBottomWidth: 0, marginBottom: 10 }}>
                            <Label style={{ fontSize: 14, width: 120 }}>Trọng lượng(kg)</Label>
                            {/* <TextInput keyboardType='numeric' style={[input, {width: '100%'}]}
                                value={String(dataOrder.trongluong)}
                                onChangeText={text => {
                                    this.uDO({ trongluong: text });
                                    this._onCalculating();
                                }}
                                placeholder='Trọng lượng(kg)'
                                keyboardType='numeric'
                            /> */}
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                iosHeader="Trọng lượng"
                                style={{ width: undefined, height: 40, }}
                                placeholder="Trọng lượng"
                                placeholderStyle={{ color: Colors.blackColor }}
                                placeholderIconColor={Colors.blackColor}
                                selectedValue={dataOrder.trongluong}
                                onValueChange={value => {
                                    this.uDO({ trongluong: value });
                                    this._onCalculating();
                                }}
                            >
                                {weightList}
                            </Picker>
                        </Item>
                        {/* <TextInput keyboardType='numeric' style={input}
                            value={String(dataOrder.giatri_kienhang)}
                            onChangeText={text => this.uDO({ giatri_kienhang: text })}
                            placeholder='Giá trị kiện hàng(đ)'
                        /> */}
                        <Label style={{ fontSize: 14, paddingBottom: 4 }}>Giá trị kiện hàng</Label>
                        <TextInputMask
                            type={'money'}
                            options={{
                                precision: 0,
                                separator: ',',
                                delimiter: '.',
                                suffixUnit: '',
                                unit: '',
                            }}
                            placeholder='Giá trị kiện hàng(đ)'
                            style={input}
                            value={this.state.cur_giatri_kienhang}
                            onChangeText={text => this._checkOrderValue(text)}
                            ref={(ref) => this.moneyField = ref}
                        />
                        <Label style={{ fontSize: 14, paddingBottom: 4 }}>Ghi chú</Label>
                        <TextInput
                            style={textarea}
                            multiline
                            numberOfLines={4}
                            value={dataOrder.ghichu}
                            onChangeText={text => this.uDO({ ghichu: text })}
                            placeholder='Ghi chú'
                            textAlignVertical={'top'}
                        />

                        <Item picker style={{ marginBottom: 10 }}>
                            <Label style={{ fontSize: 14, width: 90 }}>Loại hàng</Label>
                            <Picker
                                block
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                iosHeader="Loại hàng"
                                style={{ width: undefined }}
                                placeholder="Loại hàng"
                                placeholderStyle={{ color: Colors.blackColor }}
                                placeholderIconColor={Colors.blackColor}
                                selectedValue={dataOrder.loai_hanghoa}
                                onValueChange={value => {
                                    this.updateDataOrder({ loai_hanghoa: value });
                                    this._onCalculating();
                                }}

                            >
                                <Picker.Item label="Bình thường" value="chatran" />
                                <Picker.Item label="Chất lỏng" value="chatlong" />
                                <Picker.Item label="Dễ vỡ" value="devo" />
                            </Picker>
                        </Item>
                        <Item picker style={{ marginBottom: 10 }}>
                            <Label style={{ fontSize: 14, width: 90 }}>Bên trả phí</Label>
                            <Picker
                                block
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                iosHeader="Trả phí"
                                style={{ width: undefined }}
                                placeholder="Trả phí"
                                placeholderStyle={{ color: Colors.blackColor }}
                                placeholderIconColor={Colors.blackColor}
                                selectedValue={this.state.duocthanhtoan_boi}
                                onValueChange={value => {
                                    this.setState({ duocthanhtoan_boi: value });
                                }}
                            >
                                <Picker.Item label="Shop Trả Phí" value={this.state.id_khachhang} />
                                <Picker.Item label="Khách Trả Phí" value="0" />
                            </Picker>
                        </Item>
                        <Text>Phí vận chuyển: {this.state.cur_order_shipcost_text} đ</Text>
                        <Text style={totalPrice}>
                            Tổng tiền: {this.state.cur_order_tongtien_text.toString()} đ
                        </Text>
                        <TouchableOpacity
                            style={checkbox}
                            onPress={() => this._checkEncashment()}

                        >
                            <CheckBox checked={dataOrder.thuho}
                                style={{ left: 0, marginRight: 5 }}
                                color={Colors.subColor}
                                onPress={() => this._checkEncashment()}

                            />
                            <Text>Thu hộ</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={checkbox}
                            onPress={() =>
                                this.setState({
                                    accept_term: !this.state.accept_term
                                })}
                        >
                            <CheckBox checked={this.state.accept_term}
                                style={{ left: 0, marginRight: 5 }}
                                color={Colors.subColor}
                                onPress={() =>
                                    this.setState({
                                        accept_term: !this.state.accept_term
                                    })}
                            />
                            <Text>Tôi đã đọc và đồng ý <Text style={link}>Điều khoản và quy định</Text></Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Tab>
        );
    }

    render_bottomStepBar() {
        var current_step = this.state.currentTab;
        switch (current_step) {
            case 0:
                return (
                    <Button
                        block
                        style={Styles.btnSubmit}
                        onPress={() => {
                            this.refs.createordertabs.goToPage(current_step + 1);
                            this.upButtonHandler();
                        }
                        }>
                        <Text style={{ color: '#fff' }}>Bước tiếp theo</Text>
                    </Button>
                );
                break;
            case 1:
                return (
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Button style={Styles.btnSubmit}
                            onPress={() => { this.refs.createordertabs.goToPage(current_step - 1); this.upButtonHandler(); }}
                        >
                            <Text style={{ color: '#fff' }}>Quay lại</Text>
                        </Button>
                        <Button style={Styles.btnSubmit}
                            onPress={() => { this.refs.createordertabs.goToPage(current_step + 1); this.upButtonHandler(); }}>
                            <Text style={{ color: '#fff' }}>Tiếp theo</Text>
                        </Button>
                    </View>
                );
            case 2:
                return (
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Button
                            style={Styles.btnSubmit}
                            onPress={() => {
                                this.refs.createordertabs.goToPage(1);
                                this.upButtonHandler();
                            }
                            }
                        >
                            <Text style={{ color: '#fff' }}>Quay lại</Text>
                        </Button>
                        <Button
                            style={Styles.btnSubmit}
                            disabled={this.state.isCalc}
                            onPress={() => this._onSubmitForm()}
                        >
                            {this.state.isSubmit ? <ActivityIndicator size='small' color='#fff' /> : null}
                            <Text style={{ color: '#fff' }}>{this.state.isCalc ? 'Đang tính...' : 'Tạo đơn hàng'}</Text>
                        </Button>
                    </View>
                );
        }
    }

    // END TAB -------------------------------------------------------------------------------------

    async _onCalculating() {
        this.setState({ isCalc: true });
        const giaTriKienHang = this.moneyField.getRawValue()
        let total = null;
        this.setState({ cur_order_tongtien_text: 'đang tính...' });
        await Order.getShipCost({
            trongluong: this.state.dataOrder.trongluong,
            loai_hanghoa: this.state.dataOrder.loai_hanghoa
        }, (responseJson) => {
            if (responseJson.success) {
                this.updateDataOrder({ gui_tinhphi: parseFloat(responseJson.data.value).toFixed(0) });
                if (this.state.dataOrder.thuho) {
                    total = responseJson.data.value + parseInt(giaTriKienHang);
                } else {
                    total = parseFloat(responseJson.data.value).toFixed(0);
                }
                this.setState({
                    cur_order_tongtien_text: FormatCurrency(total) + '',
                    cur_order_tongtien: total,
                    cur_order_shipcost_text: responseJson.data.format,
                    isCalc: false
                });

            }
        });
    }
    _checkEncashment() {
        const giaTriKienHang = this.moneyField.getRawValue();
        if (parseInt(giaTriKienHang) > 0) {
            this.updateDataOrder({ thuho: !this.state.dataOrder.thuho });
            this._onCalculating();
        } else {
            this.sendMessage('Vui lòng nhập giá trị đơn hàng!', this.state.toasts.warning);
            return;
        }
    }
    _checkOrderValue(text) {
        let giaTriKienHang = this.moneyField.getRawValue();
        if (parseInt(giaTriKienHang) > 100000000) {
            this.sendMessage('Giá trị kiện hàng tối đa 100.000.000 đồng', this.state.toasts.warning);
            this.setState({ cur_giatri_kienhang: '100000000' });
            giaTriKienHang = 100000000;
        } else {
            this.setState({ cur_giatri_kienhang: text });
        }
        this.uDO({ giatri_kienhang: giaTriKienHang });
        if (this.state.dataOrder.thuho) {
            this._onCalculating();
        }
    }
    _onPickerSenderAddressChanged(id) {
        this.uDO({ id_diachi_nguoigui: id });
        var tmp = this.state.nguoigui_diachi.find(x => x.value === id);
        this.setState({
            cur_nguoigui_diachi: tmp.label,
            cur_nguoigui_dienthoai: tmp.extra.so_dienthoai,
            cur_nguoigui_email: ''
        });
    }
    updatedataReceiver(row) {
        let cdata = this.state.dataReceiver;
        this.setState({ dataReceiver: Object.assign(cdata, row) });
    }
    // updateDataOrder -----------------------------------------------------------------------------
    uDO(row) {
        this.updateDataOrder(row);
    }
    updateDataOrder(row) {
        let cdata = this.state.dataOrder;
        this.setState({ dataOrder: Object.assign(cdata, row) });
    }
    updateDataSender(row) {
        let cdata = this.state.dataSender;
        this.setState({ dataSender: Object.assign(cdata, row) });
    }
    sendMessage(msg, options) {
        if (typeof options === 'undefined') {
            options = {
                buttonText: 'Okay', type: warning, duration: 3000
            };
        }
        options = Object.assign({ text: msg }, options);
        Toast.show(options);
    }
    async _onSubmitForm() {
        if (this.state.isCalc) {
            this.sendMessage('Đang tính...', this.state.toasts.warning);
            return;
        }
        if (!this.state.accept_term) {
            this.sendMessage('Bạn chưa đồng ý điều khoản & quy định!',
                this.state.toasts.warning);
            return;
        }

        if (this.state.dataOrder.trongluong <= 0) {
            this.sendMessage('Trọng lượng không hợp lệ!', this.state.toasts.warning);
            return;
        }

        if (this.state.dataOrder.giatri_kienhang <= 0) {
            this.sendMessage('Giá trị kiện hàng không hợp lệ!', this.state.toasts.warning);
            return;
        }

        if (
            this.state.dataSender.hoten_nguoinhan.length === 0 ||
            this.state.dataSender.so_dienthoai.length === 0 ||
            this.state.dataSender.chitiet.length === 0
        ) {
            this.sendMessage('Vui lòng điền đầy đủ thông tin người gửi!',
                this.state.toasts.warning);
            return;
        }

        if (
            this.state.dataReceiver.hoten_nguoinhan.length === 0 ||
            this.state.dataReceiver.so_dienthoai.length === 0 ||
            this.state.dataReceiver.chitiet.length === 0
        ) {
            this.sendMessage('Vui lòng điền đầy đủ thông tin người nhận!',
                this.state.toasts.warning);
            return;
        }
        this.setState({ isSubmit: true });
        let request_data = Object.assign(this.state.dataOrder, {
            diachi_nguoinhan: this.state.dataReceiver,
            diachi_nguoigui: this.state.dataSender,
            duocthanhtoan_boi: this.state.duocthanhtoan_boi,
        });

        var navigation = this.props.navigation;

        this.setState({ actionText: 'Đang tạo đơn hàng...' });
        await Order.create(request_data, (response) => {
            if (response.success) {
                let order_id = response.data.id;
                navigation.navigate('NoticeScreen', {
                    msg: 'Tạo đơn hàng thành công, mã đơn hàng #' + order_id.toString()
                        + '\n Vui lòng ghi mã đơn hàng lên gói/kiện hàng.',
                    callback: () => this.props.navigation.navigate('OdersScreen', { refresh: true })
                });
            } else {
                Alert.alert(
                    'Lỗi',
                    response.message,
                    [
                        { text: 'OK', onPress: () => this.setState({ isSubmit: false }) },
                    ],
                    { cancelable: false },
                );
            }
        });
        this.setState({
            actionText: 'Đang tải...',
            isLoading: false,
            isSubmit: false,
        });
    }
    loading(status = true) {
        this.setState({ isLoading: status });
    }
    render() {
        const { navigation } = this.props;
        return (
            <Root>
                <Container>
                    <TopBar navigation={navigation} title='Tạo đơn hàng' />
                    <SafeAreaView style={{ flex: 1 }}>
                        {this.render_content()}
                    </SafeAreaView>
                </Container>
            </Root>
        );
    }
}

export default CreateOrder;
