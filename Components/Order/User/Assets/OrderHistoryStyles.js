import { StyleSheet } from 'react-native';

const OrderHistoryStyles = StyleSheet.create({
    content: {
        backgroundColor: '#eee'
    },
    mainTitle: {
        width: '100%',
        fontSize: 15,
        lineHeight: 22,
        color: '#fff',
        fontWeight: 'bold',
        flexDirection: 'row'
    }
});
export default OrderHistoryStyles;
