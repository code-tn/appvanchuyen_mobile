import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    topContent: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderColor: '#dad9d7',
        paddingHorizontal: 10,
        paddingVertical: 5,
    },
    topIcon: {
        marginLeft: 0,
        width: 36,
        height: 30,
        resizeMode: 'contain'
    },
    topTitle: {
        paddingLeft: 5,
        flexGrow: 1,
        fontSize: 14
    },
    sectionTitle: {
        color: '#333',
        fontSize: 14
    },
    desc: {
        fontSize: 12,
        fontStyle: 'italic',
        fontWeight: '300',
    },
    totalOrders: {
        position: 'relative'
    },
    imgTotal: {
        height: 200,
        width: '100%',
        resizeMode: 'contain',
    },
    totalText: {
        position: 'absolute',
        top: '50%',
        left: 0,
        right: 0,
        //translateY: -25,
        marginTop: -25,
        textAlign: 'center',
        fontSize: 45,
        fontWeight: 'bold',
        height: 50,
        lineHeight: 50,
    },
    itemStatistics: {
        borderRadius: 5,
        overflow: 'hidden',
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        paddingVertical: 3
    },
    iconStatistics: {
        height: 32,
        width: 32,
        resizeMode: 'contain',
        paddingVertical: 5,
    },
    titleStatistics: {
        flexGrow: 1,
        fontSize: 14,
        color: '#fff',
        paddingVertical: 5,
        marginLeft: 7
    },
    countStatistics: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#fff',
        alignSelf: 'flex-end',
    },
    backgroundColor1: {
        backgroundColor: '#5ca0ed'
    },
    backgroundColor2: {
        backgroundColor: '#4ec2e7'
    },
    backgroundColor3: {
        backgroundColor: '#47cec0'
    },
    backgroundColor4: {
        backgroundColor: '#42cb6f'
    },
    backgroundColor5: {
        backgroundColor: '#ed5f55'
    },
    backgroundColor6: {
        backgroundColor: '#f87d51'
    },
    backgroundColor7: {
        backgroundColor: '#fab153'
    },
    backgroundColor8: {
        backgroundColor: '#fcce54'
    },
    backgroundColor9: {
        backgroundColor: '#ec87bf'
    },
    backgroundColor10: {
        backgroundColor: '#cb93ec'
    },
    backgroundColor11: {
        backgroundColor: '#9398ec'
    },
    backgroundColor12: {
        backgroundColor: '#99d468'
    },
    backgroundColor13: {
        backgroundColor: '#000'
    }
});
