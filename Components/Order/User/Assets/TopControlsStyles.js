import { StyleSheet } from 'react-native';
import Colors from '../../../Assets/Colors';

const { mainColor, subColor } = Colors;
export default StyleSheet.create({
    controlBox: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    btnColtrol: {
        marginLeft: 5,
        width: 'auto',
        paddingLeft: 0,
        paddingRight: 0,
        height: 32,
        backgroundColor: Colors.subColor,
        marginLeft: 'auto'
    },
    btnText: {
        color: '#fff',
        paddingRight: 10,
        paddingLeft: 10,
        fontSize: 17
    },
    btnIcon: {
        fontSize: 13,
        marginRight: 10,
        color: '#fff'
    },
    header: {
        paddingVertical: 5,
        paddingHorizontal: 10,
        backgroundColor: '#fff',
        height: 'auto',
    },
    btnIsSearch: {
        height: 32,
    },
    btnSearchText: {
        color: Colors.subColor,
        paddingRight: 10,
        paddingLeft: 0,
        fontSize: 17
    },
    btnSearchIcon: {
        fontSize: 20,
        marginRight: 10,
        color: Colors.subColor,
    },
    //search
    formSearch: {
        flexDirection: 'row',
    },
    searchInput: {
        flex: 1,
        borderWidth: 1,
        borderColor: '#eee',
        height: 32,
        paddingVertical: 0,
        paddingHorizontal: 8,
    },
    btnSearch: {
        height: 32,
        backgroundColor: subColor
    },
    btnIconSearch: {
        fontSize: 14,
        marginLeft: 10,
        marginRight: 10,
    }
});
