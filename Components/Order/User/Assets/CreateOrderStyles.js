import { StyleSheet } from 'react-native';
import Colors from '../../../Assets/Colors'; 

export default StyleSheet.create({
    tabHeading: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        height: 'auto',
        paddingTop: 5,
        fontWeight: 'normal',
    },
    numberStep: {
        width: 40,
        height: 40,
        borderRadius: 20,
        // lineHeight: 40,
        textAlign: 'center',
        backgroundColor: '#fff',
        borderWidth: 1,
        fontSize: 18,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        color: '#838383',
        overflow: 'hidden',
        borderColor: '#dad9d7'
    },
    activeIconStyle: {
        backgroundColor: Colors.mainColor,
        color: '#fff',
        borderColor: Colors.mainColor
    },
    titleStep: {
        color: '#838383',
        paddingBottom: 10
    },
    tabStyle: {
        backgroundColor: '#eee',
        flex: 1,
        flexGrow: 1,
    },
    tabContainerStyle: {
        height: 'auto', 
        backgroundColor: '#000'
    },
    wrapper: {
        backgroundColor: '#eee',
        padding: 10,
    },
    checkbox: {
        paddingLeft: 0,
        margin: 0,
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
    },
    totalPrice: {
        paddingTop: 10,
        marginTop: 10,
        marginBottom: 10,
        borderTopWidth: 1,
        borderColor: '#838383',
        color: '#e4352c',
        fontSize: 18,
        lineHeight: 24,
    }
});
