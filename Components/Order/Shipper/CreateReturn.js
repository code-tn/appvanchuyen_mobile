import React, { Component } from 'react';
import { TextInput, ActivityIndicator, Alert } from 'react-native';
import { Container, Content, Button, Text } from 'native-base';
import TopBar from '../../Shared/TopBar';
import Styles from '../../Assets/Styles';
import { ApiConfig } from '../../../config/api';
import Colors from '../../Assets/Colors';
import FormatCurrency from '../../Assets/FormatCurrency';

class CreateReturn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id_vanchuyen: '',
            id_donhang: '',
            sotiennhan: '0',
            tinhtrang: '0',
            ghichu: '',
            isLoading: false,
            // isChecked: false
        };
    }
    componentDidMount() {
        this.setState({
            id_vanchuyen: this.props.navigation.getParam('id_vanchuyen'),
            id_donhang: this.props.navigation.getParam('id_donhang'),
            sotiennhan: this.props.navigation.getParam('tongtien'),
        });
    }
    async _submit() {
        this.setState({ isLoading: true });
        await fetch(ApiConfig.api_url + 'api/v1/create-refund-note', {
            method: 'POST',
            headers: ApiConfig.xhr_header,
            body: JSON.stringify({
                id_donhang: this.state.id_donhang,
                id_vanchuyen: this.state.id_vanchuyen,
                sotiennhan: this.state.sotiennhan,
                tinhtrang: 0,
                ghichu: this.state.ghichu,
            })
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false
                });
                if (responseJson.success === true) {
                    this.setState({ isLoading: false });
                    Alert.alert(
                        'Thông báo',
                        'Tạo phiếu trả hàng thành công',
                        [
                            {
                                text: 'OK', onPress: () => {
                                    this.props.navigation.state.params.onNavigateBack();
                                    this.props.navigation.goBack();
                                }
                            },
                        ],
                        { cancelable: false }
                    );
                } else {
                    Alert.alert(
                        'Thông báo',
                        responseJson.message,
                        [
                            { text: 'Ok', style: 'cancel' }
                        ],
                        { cancelable: false }
                    );
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
    // _chkboxCheck = (checked) => {
    //     this.setState({
    //         isChecked: checked
    //     });
    // }
    render() {
        const {
            textarea,
            fontFace,
            btnSubmit
        } = Styles;
        const {
            sotiennhan,
            ghichu,
            tinhtrang,
        } = this.state;
        return (
            <Container style={fontFace}>
                <TopBar title='Tạo phiếu trả hàng' navigation={this.props.navigation} />
                <Content padder style={Styles.content}>
                    <Text style={{ marginBottom: 5 }}>Mã vận đơn: #{this.state.id_donhang}</Text>
                    <Text style={{ marginBottom: 5 }}>Tổng tiền: {FormatCurrency(this.state.sotiennhan)} đ</Text>
                    <TextInput
                        style={textarea}
                        multiline
                        numberOfLines={4}
                        value={ghichu}
                        onChangeText={(text) => this.setState({ ghichu: text })}
                        placeholder='Ghi chú'
                        textAlignVertical={'top'}
                    />

                    <Button
                        block
                        style={btnSubmit}
                        onPress={() => this._submit()}
                    >
                        {this.state.isLoading ? <ActivityIndicator size='small' color='#fff' /> : null}
                        <Text>Xác nhận</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

export default CreateReturn;
