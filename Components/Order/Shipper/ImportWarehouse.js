import React, { Component } from 'react';
import { TextInput, View, ActivityIndicator, Alert, TouchableOpacity } from 'react-native';
import { Container, Content, Button, Text, CheckBox } from 'native-base';
import TopBar from '../../Shared/TopBar';
import Styles from '../../Assets/Styles';
import { Order } from '../../../Networking/order';
import Colors from '../../Assets/Colors';

class ImportWarehouse extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id_vanchuyen: '',
            id_donhang: '',
            ghichu: '',
            isLoading: false,
            isChecked: false
        };
    }
    componentDidMount() {
        this.setState({
            id_vanchuyen: this.props.navigation.getParam('id_vanchuyen'),
            id_donhang: this.props.navigation.getParam('id_donhang'),
            isChecked: this.props.navigation.getParam('backToStock') ? true : false
        });
    }
    _createStockInputNote = async () => {
        const { navigation } = this.props;
        this.setState({ isLoading: true });
        await Order.createStockInputNote({
            id_vanchuyen: this.state.id_vanchuyen,
            id_donhang: this.state.id_donhang,
            id_kho: 1,
            tinhtrang: this.state.isChecked ? 18 : 14,
            ghichu: this.state.ghichu,
        }, (responseJson) => {
            if (responseJson.success) {
                Alert.alert('Thông báo',
                    this.state.isChecked === true ? 'Tạo phiếu trả hàng về kho thành công!' : 'Tạo phiếu nhập kho thành công!',
                    [
                        {
                            text: 'Ok',
                            onPress: () => {
                                navigation.state.params.onNavigateBack();
                                navigation.goBack();
                            }
                        }
                    ],
                    { cancelable: false }
                );
            } else {
                Alert.alert(
                    'Thông báo',
                    responseJson.message,
                    [{ text: 'Ok', style: 'cancel' }],
                    { cancelable: false }
                );
            }
        }, e => {
            Alert.alert(
                'Thông báo',
                e.toString(),
                [{ text: 'Ok', style: 'cancel' }],
                { cancelable: false }
            );
        });
        this.setState({ isLoading: false });
    }

    render() {
        const {
            textarea,
            fontFace,
            btnSubmit
        } = Styles;

        const {
            id_donhang,
            ghichu,
        } = this.state;
        return (
            <Container style={fontFace}>
                <TopBar title='Tạo phiếu nhập kho' navigation={this.props.navigation} />
                <Content padder style={Styles.content}>
                    <Text style={{ marginBottom: 5 }}>Mã vận đơn: #{id_donhang}</Text>
                    <TextInput
                        style={textarea}
                        multiline
                        numberOfLines={4}
                        value={ghichu}
                        onChangeText={(text) => this.setState({ ghichu: text })}
                        placeholder='Ghi chú'
                        textAlignVertical={'top'}
                    />
                    {
                        this.props.navigation.getParam('backToStock') ? (
                            <View
                                style={{
                                    marginBottom: 10,
                                    flex: 1,
                                    justifyContent: "flex-start",
                                    flexDirection: 'row',
                                }}
                            >
                                <CheckBox
                                    checked={this.state.isChecked}
                                    style={{ left: 0, marginRight: 5 }}
                                    color={Colors.subColor}
                                    disabled
                                />
                                <Text>Trả hàng về kho</Text>

                            </View>
                        ) : null
                    }
                    <Button
                        block
                        style={btnSubmit}
                        onPress={() => this._createStockInputNote()}
                    >
                        {this.state.isLoading ? <ActivityIndicator size='small' color='#fff' /> : null}
                        <Text>Xác nhận</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

export default ImportWarehouse;
