import React, { Component } from 'react';
import { View, Modal, ActivityIndicator, Alert, FlatList } from 'react-native';
import { Tabs, Tab, ScrollableTab, TabHeading, Text, Button, Icon, Picker } from 'native-base';
import ShipperBillStyles from './Assets/ShipperBillStyles';
import Colors from '../../Assets/Colors';
import Styles from '../../Assets/Styles';
import { Order } from '../../../Networking/order';
import { ApiConfig } from '../../../config/api';
import FormatCurrency from '../../Assets/FormatCurrency';
import { cancelImportStockBill } from '../../../Data/orderRepo';

class ShipperBills extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            tinhtrang: '',
            tinhtrangtrahang: '',
            modalVisible: false,
            // TRẠNG THÁI ĐƠN
            order_status_phieugiaohang: ''
        };
    }

    async componentWillMount() {
        await Order.getOrderStatusList('phieugiaohang', (responseJson) => {
            if (responseJson.success && typeof responseJson.data !== 'undefined') {
                this.setState({
                    order_status_phieugiaohang: responseJson.data,
                    tinhtrang: responseJson.data[0].id
                });
            } else {
                Alert.alert(
                    'Lỗi',
                    responseJson.message,
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
            }
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
        });
    }

    componentDidMount() {
    }
    warningAlert(message) {
        return (
            Alert.alert(
                'Lỗi',
                message,
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            )
        );
    }

    _setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }
    async _updateDeliverynote() {
        this.setState({ isLoading: true });
        await fetch(ApiConfig.api_url + 'api/v1/update-delivery-note', {
            method: 'POST',
            headers: ApiConfig.xhr_header,
            body: JSON.stringify({
                id: this.props.dataSource.phieu_giao_hang.id,
                id_vanchuyen: this.props.id_vanchuyen,
                trangthai: this.state.tinhtrang,
                id_donhang: this.props.dataSource.id
            })
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false
                });
                if (responseJson.success === true) {
                    Alert.alert(
                        'Thông báo',
                        'Cập nhật phiếu giao hàng thành công',
                        [
                            {
                                text: 'OK',
                                onPress: () => {
                                    this.props.afterUpdated();
                                    this._setModalVisible(false);

                                }
                            },
                        ],
                        { cancelable: false }
                    );
                    this.setState({
                        isLoading: false,
                    })
                } else {
                    Alert.alert(
                        'Thông báo',
                        responseJson.message,
                        [
                            { text: 'Ok', style: 'cancel' }
                        ],
                        { cancelable: false }
                    );
                }
            })
            .catch((error) => {
                this.warningAlert(error.toString());
            });

    }
    _confirmRefundSuccess() {
        Alert.alert(
            'Thông báo',
            'Xác nhận đã trả hàng thành công?',
            [
                {
                    text: 'Hủy',
                    style: 'cancel',
                },
                { text: 'OK', onPress: () => this._updateRefundNote() },
            ],
            { cancelable: false },
        );
    }
    async _updateRefundNote() {
        this.setState({ isLoading: true });
        await fetch(ApiConfig.api_url + 'api/v1/update-refund-note', {
            method: 'POST',
            headers: ApiConfig.xhr_header,
            body: JSON.stringify({
                id: this.props.dataSource.phieu_tra_hang.id,
                tinhtrang: 12,
                id_donhang: this.props.dataSource.id
            })
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false
                });
                if (responseJson.success === true) {
                    Alert.alert(
                        'Thông báo',
                        'Cập nhật phiếu trả hàng thành công',
                        [
                            {
                                text: 'OK',
                                onPress: () => {
                                    this.props.afterUpdated();

                                }
                            },
                        ],
                        { cancelable: false }
                    );
                    this.setState({
                        isLoading: false,
                    })
                } else {
                    Alert.alert(
                        'Thông báo',
                        responseJson.message,
                        [
                            { text: 'Ok', style: 'cancel' }
                        ],
                        { cancelable: false }
                    );
                }
            })
            .catch((error) => {
                Alert.alert(
                    'Thông báo',
                    error.toString(),
                    [
                        { text: 'Ok', style: 'cancel' }
                    ],
                    { cancelable: false }
                );
            });
    }
    confirmCancelImportStockBill = async (bill_id) => {
        await cancelImportStockBill(bill_id, 21, responseJson => {
            if (responseJson.success) {
                this.props.parent.afterUpdateImportBill();
            } else {
                Alert.alert(
                    'Lỗi',
                    responseJson.message,
                    [
                        {
                            text: 'OK'
                        },
                    ],
                    { cancelable: false }
                );
            }
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    {
                        text: 'OK'
                    },
                ],
                { cancelable: false }
            );
        });
    }
    cancelImportStockBill = (bill_id) => {
        Alert.alert(
            'Thông báo',
            'Bạn muốn hủy phiếu nhập kho này?',
            [
                {
                    text: 'Hủy'
                },
                {
                    text: 'OK',
                    onPress: () => { this.confirmCancelImportStockBill(bill_id) }
                },
            ],
            { cancelable: false }
        );
    }
    render() {
        const {
            billBox,
            billItem,
            billText,
            btnBillUpdate
        } = ShipperBillStyles;
        const {
            modalDialog,
            modalHeader,
            modalBody,
            btnCloseModal,
            input,
            btnSubmit
        } = Styles;
        const { dataSource, dataReceiptNote, dataDeliveryNote, dataRefundNote, dataStockInputNote } = this.props;
        if (typeof this.state.order_status_phieugiaohang !== 'object') {
            return (<View />);
        }
        let trangthai_phieugiaohang = this.state.order_status_phieugiaohang.map((item) => {
            return (<Picker.Item label={item.ten_trangthai} value={item.id} key={item.id} />);
        });
        let StyleShiperBills = {
            closeButton: {
                color: '#fff', fontSize: 15,
                padding: 5, paddingLeft: 8, paddingRight: 8
            },
            icon: { fontWeight: 'normal', fontSize: 15, color: '#fff' }
        };
        return (
            <View style={{ marginTop: 10 }}>
                <Modal
                    transparent
                    animationType="slide"
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                    }}
                >
                    <View style={modalDialog}>
                        <View style={modalHeader}>
                            <Button iconRight transparent style={btnCloseModal}
                                onPress={() => {
                                    this._setModalVisible(!this.state.modalVisible);
                                }}
                            >
                                <Text style={StyleShiperBills.closeButton}>Đóng</Text>
                                <Icon type='FontAwesome' name='times-circle'
                                    style={StyleShiperBills.icon} />
                            </Button>
                        </View>
                        <View style={modalBody}>

                            <View style={{ padding: 10 }}>
                                <Text style={{ marginBottom: 5 }}>Mã vận đơn: #{dataSource.id}</Text>
                                <View style={[input, {
                                    height: 'auto', paddingLeft: 2, paddingRight: 2,
                                }]}>
                                    <Picker
                                        mode="dropdown"
                                        iosHeader='Tình trạng'
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={{ width: undefined, height: 40 }}
                                        selectedValue={this.state.tinhtrang}
                                        onValueChange={value => this.setState({ tinhtrang: value })}
                                        itemTextStyle={{ fontSize: 13, color: '#666' }}
                                    >
                                        {trangthai_phieugiaohang}
                                    </Picker>
                                </View>
                                <Button
                                    block
                                    style={btnSubmit}
                                    onPress={() => this._updateDeliverynote()}
                                >

                                    {this.state.isLoading ?
                                        <ActivityIndicator size='small' color='#fff' /> : null
                                    }
                                    <Text>Xác nhận</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                </Modal>
                <Tabs
                    renderTabBar={() => <ScrollableTab style={{ backgroundColor: 'white' }} />}
                    tabBarUnderlineStyle={{ backgroundColor: Colors.mainColor }}
                >
                    <Tab
                        heading={
                            <TabHeading style={{ backgroundColor: '#fff' }} >
                                <Text style={{ color: Colors.blackColor, fontWeight: 'normal' }}>Phiếu nhận hàng</Text>
                            </TabHeading>
                        }
                        style={{ backgroundColor: '#eee' }}
                    >
                        {dataSource.phieu_nhan_hang !== null ?
                            (
                                <View style={billBox}>
                                    <View style={billItem}>
                                        <Text style={billText}>NV giao hàng: {dataReceiptNote.tenvanchuyen}</Text>
                                        <Text style={billText}>Số tiền nhận:  {FormatCurrency(parseFloat(dataReceiptNote.sotiennhan))} đ</Text>
                                        <Text style={billText}>Ngày tạo: {dataReceiptNote.created_at}</Text>
                                        <Text style={billText}>Ghi chú: {dataReceiptNote.ghichu}</Text>
                                    </View>
                                </View>
                            ) : (
                                <View style={{ paddingTop: 10 }}>
                                    <Text style={{ textAlign: 'center' }}>Không có dữ liệu...</Text>
                                </View>
                            )
                        }
                    </Tab>
                    <Tab
                        heading={
                            <TabHeading style={{ backgroundColor: '#fff' }}>
                                <Text style={{ color: Colors.blackColor, fontWeight: 'normal' }}>Phiếu giao hàng</Text>
                            </TabHeading>
                        }
                        style={{ backgroundColor: '#eee' }}
                    >
                        {dataSource.phieu_giao_hang !== null ?
                            (
                                <View style={billBox}>
                                    <View style={billItem}>
                                        <Text style={billText}>NV giao hàng: {dataDeliveryNote.tenvanchuyen}</Text>
                                        <Text style={billText}>Số tiền nhận: {FormatCurrency(parseFloat(dataDeliveryNote.sotiennhan))} đ</Text>
                                        <Text style={billText}>Ngày tạo: {dataDeliveryNote.created_at}</Text>
                                        <Text style={billText}>Tình trạng: {dataDeliveryNote.tinhtrang === '' ? 'Chưa giao' : dataDeliveryNote.tinhtrang}</Text>
                                        <Text style={billText}>Ngày cập nhật: {dataDeliveryNote.updated_at}</Text>
                                        <Text style={billText}>Ghi chú: {dataDeliveryNote.ghichu}</Text>
                                        {dataSource.trangthai !== '12'
                                            && dataSource.trangthai !== '4'
                                            && dataSource.phieu_tra_hang === null
                                            && dataSource.trangthai !== '18'
                                            //&& dataSource.trangthai !== '17' 
                                            ? (
                                                <View
                                                    style={{
                                                        paddingTop: 5,
                                                        flexDirection: 'row',
                                                        justifyContent: 'flex-end',
                                                        borderTopWidth: 1,
                                                        borderTopColor: '#eee',
                                                        marginTop: 5
                                                    }}
                                                >
                                                    <Button
                                                        transparent
                                                        success
                                                        style={Styles.btn}
                                                        iconRight
                                                        onPress={() => this._setModalVisible(!this.state.modalVisible)}
                                                    >
                                                        <Text uppercase={false}>Cập nhật</Text>
                                                        <Icon style={{ fontSize: 15 }} name='refresh' type='FontAwesome' />
                                                    </Button>
                                                </View>
                                            ) : null}

                                    </View>
                                </View>
                            ) : (
                                <View style={{ paddingTop: 10 }}>
                                    <Text style={{ textAlign: 'center' }}>Không có dữ liệu...</Text>
                                </View>
                            )
                        }
                    </Tab>
                    <Tab
                        heading={
                            <TabHeading style={{ backgroundColor: '#fff' }}>
                                <Text style={{ color: Colors.blackColor, fontWeight: 'normal' }}>Phiếu trả hàng</Text>
                            </TabHeading>
                        }
                        style={{ backgroundColor: '#eee' }}
                    >
                        {dataSource.phieu_tra_hang !== null ?
                            (
                                <View style={billBox}>
                                    <View style={billItem}>
                                        <Text style={billText}>NV giao hàng: {dataRefundNote.tenvanchuyen}</Text>
                                        <Text style={billText}>Số tiền nhận: {FormatCurrency(parseFloat(dataRefundNote.sotiennhan))} đ</Text>
                                        <Text style={billText}>Ngày tạo: {dataRefundNote.created_at}</Text>
                                        <Text style={billText}>Ghi chú: {dataRefundNote.ghichu}</Text>
                                        <Text style={billText}>Tình trạng: {dataRefundNote.tinhtrang === '' ? 'Đang chờ' : dataRefundNote.tinhtrang}</Text>
                                        <Text style={billText}>Ngày cập nhật: {dataRefundNote.updated_at}</Text>
                                        {dataRefundNote.tinhtrang !== 'Trả hàng thành công' ? (
                                            <View
                                                style={{
                                                    paddingTop: 5,
                                                    flexDirection: 'row',
                                                    justifyContent: 'flex-end',
                                                    borderTopWidth: 1,
                                                    borderTopColor: '#eee',
                                                    marginTop: 5
                                                }}
                                            >
                                                <Button
                                                    transparent
                                                    success
                                                    style={Styles.btn}
                                                    iconRight
                                                    onPress={() => this._confirmRefundSuccess()}
                                                >
                                                    <Text uppercase={false} style={{ paddingRight: 5 }}>Trả hàng thành công</Text>
                                                    <Icon style={{ fontSize: 15 }} name='check-circle' type='FontAwesome' />
                                                </Button>
                                            </View>
                                        ) : null}
                                    </View>
                                </View>
                            ) : (
                                <View style={{ paddingTop: 10 }}>
                                    <Text style={{ textAlign: 'center' }}>Không có dữ liệu...</Text>
                                </View>
                            )
                        }
                    </Tab>
                    <Tab
                        heading={
                            <TabHeading style={{ backgroundColor: '#fff' }}>
                                <Text style={{ color: Colors.blackColor, fontWeight: 'normal' }}>Phiếu nhập kho</Text>
                            </TabHeading>
                        }
                        style={{ backgroundColor: '#eee' }}
                    >
                        {dataSource.phieu_nhap_kho !== null && dataStockInputNote.length > 0 ? (
                            <FlatList
                                data={dataStockInputNote}
                                renderItem={({ item }) => (
                                    <View style={billBox}>
                                        <View style={billItem}>
                                            <Text style={billText}>NV giao hàng: {item.ten_vanchuyen}</Text>
                                            <Text style={billText}>Thủ kho: {item.tenthukho}</Text>
                                            <Text style={billText}>Tình trạng: {item.tinhtrang}</Text>
                                            <Text style={billText}>Ghi chú: {item.ghichu}</Text>
                                            <Text style={billText}>Ngày tạo: {item.created_at}</Text>
                                            {typeof dataSource.hoantrahang !== 'undefined' && dataSource.hoantrahang === 1 && item.tinhtrang === 'Đang chờ nhập kho' ?
                                                <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginTop: 10, borderTopWidth: 1, borderTopColor: '#eee' }}>
                                                    <Button
                                                        icon-right
                                                        transparent
                                                        danger
                                                        onPress={() => this.cancelImportStockBill(item.id)}
                                                        style={Styles.btn}
                                                    >
                                                        <Text uppercase={false} style={Styles.btn__text}>Hủy phiếu</Text>
                                                        <Icon name='times-circle' type='FontAwesome' style={Styles.btn__icon} />
                                                    </Button>
                                                </View> : null}
                                        </View>
                                    </View>
                                )}
                                keyExtractor={(item) => item.id.toString()}
                            />
                        ) : (
                                <View style={{ paddingTop: 10 }}>
                                    <Text style={{ textAlign: 'center' }}>Không có dữ liệu...</Text>
                                </View>
                            )
                        }
                    </Tab>
                </Tabs>
            </View>
        );
    }
}

export default ShipperBills;
