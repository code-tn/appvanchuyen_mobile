import React, { Component } from 'react';
import { RefreshControl, SafeAreaView, Alert } from 'react-native';
import { Container, Content } from 'native-base';
import TopBar from '../../Shared/TopBar';
import Styles from '../../Assets/Styles';
import OrderDetailContent from '../Partials/OrderDetailContent';
import ShipperTopControl from './ShipperTopControl';
import ShipperBills from './ShipperBills';
import Loading from '../../Shared/Loading';
import { Order } from '../../../Networking/order';
import { AuthShiper } from '../../../Networking/auth';

class ShipperOrderDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            _order_id: null,
            dataSource: '',
            dataReceiptNote: '',
            dataDeliveryNote: '',
            dataRefundNote: '',
            dataStockInputNote: '',
            userType: '',
            idShipper: '',
            isLoading: true,
            fab_status: false,
            id_vanchuyen: '',
            refreshing: false,
        };
    }

    async componentDidMount() {
        await this.setState({
            _order_id: this.props.navigation.getParam('idOrder')
        });
        //this.setState({ isLoading: true });
        // LOAD ORDER DETAIL
        await this._loadOrderDetail();
        // LOAD RECEIPT BILLS
        await this._loadOrderReceiptBills();
        // LOAD DELIVER BILLS
        await this._loadOrderDeliveryBills();
        // LOAD REFUND BILLS
        await this._loadOrderRefundBills();
        // LOAD STOCK INPUT BILLS
        await this._loadStockInputBills();
        //get id shipper
        await this._getShipperID();
    }

    async _loadOrderDetail() {
        await Order.getDetail(this.state._order_id, (responseJson) => {
            if (responseJson.success && typeof responseJson.data !== 'undefined') {
                this.setState({ dataSource: responseJson.data });
            }
        });
    }

    async _loadOrderReceiptBills() {
        await Order.getOrderReceiptBills(this.state._order_id, (responseJson) => {
            if (responseJson.success && typeof responseJson.data !== 'undefined') {
                this.setState({ dataReceiptNote: responseJson.data });
            }
        });
    }

    async _loadOrderDeliveryBills() {
        await Order.getOrderDeliveryBills(this.state._order_id, (responseJson) => {
            if (responseJson.success && typeof responseJson.data !== 'undefined') {
                this.setState({ dataDeliveryNote: responseJson.data });
            }
        });
    }

    async _loadOrderRefundBills() {
        await Order.getOrderRefundBills(this.state._order_id, (responseJson) => {
            if (responseJson.success && typeof responseJson.data !== 'undefined') {
                this.setState({ dataRefundNote: responseJson.data });
            }
        });
    }

    async _loadStockInputBills() {
        await Order.getStockInputNote(this.state._order_id, (responseJson) => {
            if (responseJson.success && typeof responseJson.data !== 'undefined') {
                this.setState({ dataStockInputNote: responseJson.data });
            }
            this.setState({ isLoading: false });
        });
    }

    async _getShipperID() {
        const currentUser = await AuthShiper.currentUser();
        if (currentUser) {
            this.setState({ id_vanchuyen: currentUser.id });
        }
    }
    _onRefresh = async () => {
        this.setState({ refreshing: true });
        await this._loadOrderDetail();
        await this._loadOrderReceiptBills();
        await this._loadOrderRefundBills();
        await this._loadStockInputBills();
        await this.setState({ refreshing: false });
    };
    afterBillUpdated = () => {
        this.setState({ refreshing: true });
        this._loadOrderDetail();
        this._loadOrderReceiptBills();
        this._loadOrderDeliveryBills();
        this._loadOrderRefundBills();
        this._loadStockInputBills();
        setTimeout(() => {
            this.setState({ refreshing: false });
        }, 2000);
    }
    afterBackAction = () => {
        this.setState({ refreshing: true });
        this._loadOrderDetail();
        this._loadOrderReceiptBills();
        this._loadOrderDeliveryBills();
        this._loadOrderRefundBills();
        this._loadStockInputBills();
        setTimeout(() => {
            this.setState({ refreshing: false });
        }, 2000);
    }
    afterTakePhoto = async () => {
        this.setState({ refreshing: true });
        await this._loadOrderDetail();
        this.setState({ refreshing: false });
    }
    afterUpdateImportBill = async () => {
        this.setState({ refreshing: true });
        await this._loadOrderDetail();
        await this._loadStockInputBills();
        this.setState({ refreshing: false });
    }
    afterReceive = () => {
        return (
            Alert.alert(
                'Thông báo',
                'Chụp ảnh hàng hóa sau khi đã nhận hàng!',
                [
                    { text: 'Để sau' },
                    { text: 'OK', onPress: () => this.props.navigation.navigate('OrderCameraScreen', {
                        id_donhang: this.state.dataSource.id,
                        afterTakePhoto: this.afterTakePhoto,
                    }) },
                ],
                { cancelable: false },
            )
        );
    }
    renderContent() {
        return (
            <Content
                padder
                style={Styles.content}
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh}
                    />
                }
            >
                <OrderDetailContent
                    dataSource={this.state.dataSource}
                    dataReceiptNote={this.state.dataReceiptNote}
                    dataDeliveryNote={this.state.dataDeliveryNote}
                    dataRefundNote={this.state.dataRefundNote}
                    typeUser='shipper'
                    navigation={this.props.navigation}
                    afterTakePhoto={() => this.afterTakePhoto()}
                />
                <ShipperBills
                    dataSource={this.state.dataSource}
                    dataReceiptNote={this.state.dataReceiptNote}
                    dataDeliveryNote={this.state.dataDeliveryNote}
                    dataRefundNote={this.state.dataRefundNote}
                    dataStockInputNote={this.state.dataStockInputNote}
                    id_vanchuyen={this.state.id_vanchuyen}

                    afterUpdated={() => this.afterBillUpdated()}
                    parent={this}
                />
            </Content>
        );
    }

    render() {
        // SHOW LOADING SPINNER
        if (this.state.isLoading) {
            return (<Loading />);
        }
        return (
            <Container>
                <TopBar title='Chi tiết đơn hàng' navigation={this.props.navigation} />
                <ShipperTopControl
                    navigation={this.props.navigation}
                    dataSource={this.state.dataSource}
                    idOrder={this.props.navigation.getParam('idOrder')}
                    orderStatus={this.state.dataSource.trangthai}
                    backAction={() => this.afterBackAction()}
                    afterReceive={() => this.afterReceive()}
                />
                <SafeAreaView style={{ flex: 1 }}>
                    {this.renderContent()}
                </SafeAreaView>
            </Container>
        );
    }
}

export default ShipperOrderDetail;
