import React, { PureComponent } from 'react';
import { View, Image } from 'react-native';
import { Text, Button, Icon } from 'native-base';
import { phonecall } from 'react-native-communications';
import truckIcon from '../Assets/Images/truck.png';
import OrderItemStyles from '../Assets/OrderItemStyles';
import Styles from '../../../Assets/Styles';

class OrderItem extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
    }

    render() {
        const {
            boldStyle,
            line,
            orderItem,
            orderItemCode,
            orderItemCodeIcon,
            orderItemCodeText,
            orderItemText,
            orderItemControl,
            btnOIC,
            btnOICIcon,
            btnOICText,
            noneMarginRight,
        } = OrderItemStyles;
        const {
            id,
            thuho,
            trangthai,
            tt_trangthai,
            ghichu,
            created_at,
        } = this.props.order;
        const ttNguoiGui = this.props.order.diachi_nguoigui[0];
        const chitiet = ttNguoiGui.chitiet;
        const tinh = ttNguoiGui.tinhthanhpho[0].name;
        const huyen = ttNguoiGui.quanhuyen[0].name;
        const phuong = ttNguoiGui.xaphuong[0].name;
        const { navigation } = this.props;
        return (
            <View style={orderItem}>
                <View style={orderItemCode}>
                    <Image source={truckIcon} style={orderItemCodeIcon} />
                    <Text style={orderItemCodeText}>#{id}</Text>
                </View>
                {typeof this.props.order.hoantrahang !== 'undefined' && this.props.order.hoantrahang === 1 ?
                <Text style={[orderItemText, { color: '#ca0e00' }]}>
                    <Icon type='FontAwesome' name='refresh' style={{ color: '#ca0e00', fontSize: 17 }} />
                    {' '}Khách yêu cầu hoàn trả
                </Text> : null}
                <Text style={orderItemText}>Tên: {ttNguoiGui.hoten_nguoinhan} / {ttNguoiGui.so_dienthoai}</Text>
                <Text style={orderItemText}>Địa chỉ: {chitiet}, {phuong}, {huyen}, {tinh}</Text>
                <Text style={orderItemText}>Thu hộ: {thuho ? 'Có' : 'Không'} </Text>
                <Text style={orderItemText}>Ngày tạo: {created_at}</Text>
                <Text style={orderItemText}>Ghi chú: {ghichu}</Text>
                <Text style={[orderItemText, boldStyle, line]}>
                    Trang thái: {trangthai === "" ? 'Đơn hàng mới' : null}
                    {typeof tt_trangthai !== 'undefined' && tt_trangthai !== null ? tt_trangthai[0]["ten_trangthai"] : null}
                </Text>

                <View
                    style={
                        [
                            orderItemControl, line,
                            {
                                flexDirection: 'row',
                                justifyContent: 'flex-end'
                            }
                        ]
                    }
                >
                    <Button
                        success
                        transparent 
                        iconRight
                        style={Styles.btn}
                        onPress={() => phonecall(ttNguoiGui.so_dienthoai.toString(), true)}
                    >                    
                        <Text uppercase={false} style={Styles.btn__text}>Gọi điện</Text>
                        <Icon type='FontAwesome' name="phone" style={Styles.btn__icon} />
                    </Button>
                    <View style={{ marginHorizontal: 10, borderLeftWidth: 1, borderLeftColor: '#dad9d7' }} />
                    <Button
                        iconLeft
                        transparent
                        success
                        style={[btnOIC, noneMarginRight]}
                        onPress={() => navigation.navigate('ShipperOrderDetailScreen', {
                            idOrder: id
                        })}
                    >
                        <Icon type='FontAwesome' name="eye" style={btnOICIcon} />
                        <Text uppercase={false} style={btnOICText}>Xem chi tiết</Text>
                    </Button>
                </View>
            </View>
        );
    }
}

export default OrderItem;
