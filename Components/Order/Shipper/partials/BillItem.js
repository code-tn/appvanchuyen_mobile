import React, { Component } from 'react';
import { View, Alert, ActivityIndicator } from 'react-native';
import { Text, Button, Icon } from 'native-base';
import BillItemStyles from '../Assets/BillItemStyles';
import Colors from '../../../Assets/Colors';
import { Order } from '../../../../Networking/order';

class BillItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
        };
    }
    componentDidMount() {
    }
    
    _confirmSubmit = () => {
        Alert.alert(
            'Thông báo',
            'Xác nhận yêu cầu xuất kho',
            [
                { text: 'OK', onPress: () => this._submit() },
                {
                    text: 'Hủy',
                    style: 'cancel',
                },
            ],
            { cancelable: false },
        );
    }
    _submit = async() => {
        this.setState({ isLoading: true });
        const data = {
            tinhtrang: 17
        };
        await Order.confirmShipperExportRequest(this.props.order.id, data, (res) => {
            if(res.success && res.data !== undefined) {
                Alert.alert(
                    'Xác nhận',
                    'Xác nhận xuất kho thành công',
                    [
                        { text: 'OK', onPress: () => 
                            {
                                this.props.afterAction();
                                this.setState({ isLoading: false });
                            }
                        },
                    ],
                    { cancelable: false },
                );
            } else {
                Alert.alert('Thông báo',
                    res.message,
                    [{ text: 'Ok', style: 'cancel' }],
                    { cancelable: false }
                );
                this.setState({ isLoading: false });
            }
        }, (e) => {
            Alert.alert('Thông báo',
                e.toString(),
                [{ text: 'Ok', style: 'cancel' }],
                { cancelable: false }
            );
            this.setState({ isLoading: false });
        });
    }
    render() {
        const {
            id,
            id_donhang,
            ten_trangthai,
            ghichu,
            tinhtrang,
            updated_at,
        } = this.props.order;
        const {
            billItem,
            idStyle,
            text,
            line
        } = BillItemStyles;
        return (
            <View style={billItem}>
                <Text style={text}> Mã phiếu: <Text style={idStyle}>#{id}</Text></Text>
                <Text style={text}> Mã vận đơn: <Text style={{ color: Colors.subColor }}>#{id_donhang}</Text></Text>
                <Text style={text}> Ghi chú: {ghichu} </Text>
                <Text style={[text, line]}> Tình trạng: <Text style={[text, { color: Colors.subColor, fontWeight: 'bold' }]}>{ten_trangthai}</Text> </Text>
                <Text style={[text, line]}> Cập nhật: {updated_at}</Text>                
                <View style={[line, { flexDirection: 'row', justifyContent: 'space-between' }]}>
                    <Button
                        transparent
                        iconRight
                        style={{ height: 'auto' }}
                        onPress={() => this.props.navigation.navigate('ShipperOrderDetailScreen', {
                            idOrder: id_donhang
                        })}
                    >   
                        <Text uppercase={false} style={{ fontSize: 15, paddingRight: 4, paddingLeft: 0, color: Colors.mainColor }}>
                            Xem đơn hàng
                        </Text>
                        <Icon type='FontAwesome' name='eye' style={{ fontSize: 15, marginRight: 10, color: Colors.mainColor }} />
                    </Button>
                    {tinhtrang === '16' || tinhtrang === '18' ? (
                        <Button
                            success
                            iconRight
                            style={{ height: 'auto' }}
                            onPress={() => this._confirmSubmit()}
                        >   
                            {this.state.isLoading ? <ActivityIndicator size='small' color='#fff' /> : null}
                            <Text uppercase={false} style={{ fontSize: 15, paddingRight: 4, paddingLeft: 10 }}>
                                Xác nhận
                            </Text>
                            <Icon type='FontAwesome' name='check-circle' style={{ fontSize: 15, marginRight: 10, }} />
                        </Button>
                    ) : null}
                </View>
            </View>
        );
    }
}

export default BillItem;
