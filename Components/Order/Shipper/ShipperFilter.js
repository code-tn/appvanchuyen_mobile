import React, { Component } from 'react';
import { View, Modal, Alert } from 'react-native';
import { Button, Icon, Text, Picker } from 'native-base';
import DatePicker from 'react-native-datepicker';
import { Order } from '../../../Networking/order';
import Styles from '../../Assets/Styles';
import Colors from '../../Assets/Colors';

class ShipperFilter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            orderStatus: []
        };
    }
    _setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }
    _notice(title, content) {
        return (
            Alert.alert(
                title,
                content.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            )
        );
    }
    _submit() {
        const currentDate = new Date().toISOString().slice(0, 10);
        if (this.state.fromDate > this.state.toDate) {
            this._notice('Lỗi', 'Khoảng thời gian không hợp lệ. \nNgày bắt đầu phải nhỏ hơn ngày kết thúc.');
            return;
        }
        if (this.state.toDate > currentDate) {
            this._notice('Lỗi', 'Ngày kết thúc phải nhỏ hơn hoặc bằng thời gian hiện tại.');
            return;
        }
        this.props.parent.filterOrder();
    }
    _onInputChange(data) {
        if (typeof this.props.parent === 'undefined') return '';
        if (typeof this.props.parent.state.filter === 'undefined') return '';
        let parent = this.props.parent;
        let dataFilter = parent.state.filter;
        let new_filter = Object.assign(dataFilter, data);
        parent.setState({ dataFilter: new_filter });
    }
    _inputValue(name, defaultValue = '') {
        if (typeof this.props.parent === 'undefined') return defaultValue;
        if (typeof this.props.parent.state.filter === 'undefined') return defaultValue;
        let parent = this.props.parent;
        let dataFilter = parent.state.filter;
        if (typeof dataFilter[name] === 'undefined') return defaultValue;
        return dataFilter[name];
    }
    _submit() {
        const currentDate = new Date().toISOString().slice(0, 10);
        if (this.state.fromDate > this.state.toDate) {
            this._notice('Lỗi', 'Khoảng thời gian không hợp lệ. \nNgày bắt đầu phải nhỏ hơn ngày kết thúc.');
            return;
        }
        if (this.state.toDate > currentDate) {
            this._notice('Lỗi', 'Ngày kết thúc phải nhỏ hơn hoặc bằng thời gian hiện tại.');
            return;
        }
        this.props.parent.filterOrder();
    }
    _renderDatePicker() {
        return (
            <View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 5 }}>
                    <Text style={{ width: '40%' }}>Thời gian tạo đơn từ</Text>
                    <DatePicker
                        style={{ width: undefined, flexGrow: 1, marginLeft: 10 }}
                        date={this._inputValue('tu_ngay')}
                        mode="date"
                        placeholder="Từ ngày"
                        confirmBtnText="OK"
                        cancelBtnText="Hủy"
                        showIcon={false}
                        customStyles={{
                            dateInput: {
                                ...Styles.input,
                                height: 40
                            }
                        }}
                        onDateChange={(date) => { this._onInputChange({ tu_ngay: date }) }}
                    />
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ width: '40%' }}>Đến</Text>
                    <DatePicker
                        style={{ width: undefined, flexGrow: 1, marginLeft: 10 }}
                        date={this._inputValue('den_ngay')}
                        mode="date"
                        placeholder="Đến ngày"
                        confirmBtnText="Xác nhận"
                        cancelBtnText="Đóng"
                        showIcon={false}
                        customStyles={{
                            dateInput: {
                                ...Styles.input,
                            }
                        }}
                        onDateChange={(date) => { this._onInputChange({ den_ngay: date }) }}
                    />
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 5 }}>
                    <Text style={{ width: '40%' }}>Trạng thái</Text>
                    <View style={{ width: undefined, flexGrow: 1, marginLeft: 10, textAlign: 'center' }}>
                        {this.state.orderStatus !== '' ? (
                            <Picker
                                mode="dropdown"
                                iosHeader='Tình trạng'
                                iosIcon={<Icon name="arrow-down" />}
                                style={{ width: undefined, height: 40 }}
                                selectedValue={this._inputValue('trang_thai_don_hang')}
                                onValueChange={value => this._onInputChange({ trang_thai_don_hang: value })}
                                itemTextStyle={{ fontSize: 13, color: '#666' }}
                            >
                                {this.state.orderStatus.map((item) => {
                                    return (<Picker.Item label={item.ten_trangthai} value={item.id} key={item.id} />);
                                })}
                            </Picker>
                        ) : null}
                    </View>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ width: '50%', paddingRight: 1 }}>
                        <Button
                            block
                            style={[Styles.btnSubmit, { justifyContent: 'center' }]}
                            onPress={() => this._setModalVisible(false)}
                        >
                            <Text>Đóng</Text>
                        </Button>
                    </View>
                    <View style={{ width: '50%', paddingLeft: 1 }}>
                        <Button
                            block
                            style={[Styles.btnSubmit, { justifyContent: 'center' }]}
                            onPress={() => this._submit()}
                        >
                            <Text>Áp dụng</Text>
                        </Button>
                    </View>
                </View>
            </View>
        );
    }
    async getOrderStatus(type) {
        await Order.getOrderStatusList(type, (responseJson) => {
            if (responseJson.success) {
                this.setState({
                    orderStatus: [...this.state.orderStatus, ...responseJson.data],
                });
            } else {
                Alert.alert(
                    'Lỗi',
                    responseJson.message,
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
            }
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
        });
    }
    async componentDidMount() {
        await this.getOrderStatus('phieudatcho');
        await this.getOrderStatus('phieunhanhang');
        await this.getOrderStatus('phieugiaohang');
        await this.getOrderStatus('phieutrahang');
        await this.getOrderStatus('phieunhapkho');
        this._onInputChange({ trang_thai_don_hang: this.state.orderStatus[0].id });
    }
    render() {
        const {
            modalDialog,
            modalBody,
            modalHeader
        } = Styles;
        return (
            <View>
                <Modal
                    animationType="slide"
                    transparent
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        console.log('close');
                    }}
                >
                    <View style={modalDialog}>
                        <View style={[modalHeader, { justifyContent: 'center' }]}>
                            <Text style={{ paddingHorizontal: 10, paddingVertical: 5, color: '#fff' }} uppercase>Tìm kiếm đơn hàng</Text>
                        </View>
                        <View style={modalBody}>
                            <View style={{ padding: 10 }}>
                                {this._renderDatePicker()}
                            </View>
                        </View>
                    </View>
                </Modal>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end', backgroundColor: '#fff', paddingVertical: 5, paddingHorizontal: 10 }}>
                    <Button
                        disabled={this.state.orderStatus !== '' ? false : true}
                        transparent
                        iconRight
                        style={{ height: 'auto' }}
                        onPress={() => this._setModalVisible(true)}
                    >
                        <Text style={{ color: Colors.mainColor, paddingLeft: 12, paddingRight: 12, fontSize: 17 }} uppercase={false}>Tìm kiếm</Text>
                        <Icon style={{ color: Colors.mainColor, paddingLeft: 0, marginRight: 5, fontSize: 20 }} name='search' type='Ionicons' />
                    </Button>
                </View>
            </View>
        );
    }
}

export default ShipperFilter;
