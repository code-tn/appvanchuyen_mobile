import React, { Component } from 'react';
import { FlatList, RefreshControl, ActivityIndicator, View, Alert } from 'react-native';
import {
    Container,
    Text
} from 'native-base';
import Styles from '../../Assets/Styles';
import OrderHistoryStyles from './Assets/NewOrdersStyles';
import TopBarOnlyTitle from '../../Shared/TopBarOnlyTitle';
import OrderItem from './partials/OrderItem';
import Loading from '../../Shared/Loading';
import { AuthShiper } from '../../../Networking/auth';
import { Order } from '../../../Networking/order';
import Colors from '../../Assets/Colors';

class NewOrders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: '',
            dataList: [],
            id_vanchuyen: '',
            isLoading: true,
            refreshing: false,
            isLoadmore: false
        };
    }

    async componentDidMount() {
        const currentUser = await AuthShiper.currentUser();
        if (currentUser) {
            this.setState({ id_vanchuyen: currentUser.id });
        }
        //load data
        await this._loadUserOrders();
    }
    async _loadUserOrders(request_url = null) {
        await Order.findOrderNearest(this.state.id_vanchuyen, request_url, (responseJson) => {
            if (responseJson.success) {
                this.setState({
                    data: responseJson.data,
                    dataList: request_url === null ? responseJson.data.data : [...this.state.dataList, ...responseJson.data.data],
                    isLoading: false,
                    refreshing: false,
                    isLoadmore: false
                });
            } else {
                this.setState({
                    isLoading: false,
                    refreshing: false,
                    isLoadmore: false
                });
            }
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
            this.setState({
                isLoading: false,
                refreshing: false,
                isLoadmore: false
            });
        });
    }
    _onRefresh = async () => {
        this.setState({ refreshing: true, data: '', dataList: [] });
        await this._loadUserOrders();
    }
    _onHandleLoadmore = async () => {
        if (this.state.data.current_page < this.state.data.last_page) {
            this.setState({ isLoadmore: true });
            await this._loadUserOrders(this.state.data.next_page_url);
        }
    }
    render() {
        const { fontFace } = Styles;
        const { dataList } = this.state;
        const { content } = OrderHistoryStyles;
        const { navigation } = this.props;
        if (this.state.isLoading) {
            return (<Loading />);
        }
        return (
            <Container style={fontFace}>
                <TopBarOnlyTitle title='Đơn hàng mới' />
                <View style={[content, { flex: 1, padding: 10 }]}>
                    {this.state.dataList === undefined || this.state.dataList.length === 0 || this.state.refreshing ?
                        <Text style={{ fontSize: 15, textAlign: 'center', }}>{this.state.refreshing ? 'Đang tải đơn hàng...' : 'Không có đơn hàng nào...'}</Text>
                        : null}
                    <FlatList
                        style={{ flex: 1 }}
                        data={dataList}
                        renderItem={({ item }) => <OrderItem navigation={navigation} order={item} />}
                        keyExtractor={(item) => item.id.toString()}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this._onRefresh}
                            />
                        }
                        onEndReached={this._onHandleLoadmore}
                        onEndReachedThreshold={0.1}
                    />
                    {this.state.isLoadmore ?
                        <View style={{ paddingTop: 10, backgroundColor: '#eee' }}>
                            <ActivityIndicator size='small' color={Colors.mainColor} />
                        </View> : null}
                </View>
            </Container>
        );
    }
}
export default NewOrders;
