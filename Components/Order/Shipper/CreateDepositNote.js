import React, { Component } from 'react';
import { TextInput, ActivityIndicator, Alert } from 'react-native';
import { Container, Content, Button, Text, } from 'native-base';
import { TextInputMask } from 'react-native-masked-text';
import Styles from '../../Assets/Styles';
import TopBar from '../../Shared/TopBar';
import { Order } from '../../../Networking/order';
import FormatCurrency from '../../Assets/FormatCurrency';

class CreateDepositNote extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id_vanchuyen: '',
            id_thuquy: 0,
            sotien: '',
            ghichu: '',
            tinhtrang: 9,
            isLoading: false,
            cur_so_tien: ''
        };
    }
    componentDidMount() {
        this.setState({
            id_vanchuyen: this.props.navigation.getParam('id_vanchuyen')
        });
    }
    _submit() {
        let sodu = parseInt(this.props.navigation.getParam('sodu_taikhoan'));
        this.setState({ sotien: this.moneyField.getRawValue() })
        if (this.state.sotien === '') {
            Alert.alert('Thông báo',
                'Vui lòng nhập số tiền nạp',
                [{ text: 'Ok', style: 'cancel' }],
                { cancelable: false }
            );
        } else if (parseInt(this.state.sotien) > sodu) {
            Alert.alert('Thông báo',
                'Số tiền nạp phải nhỏ hơn hoặc bằng số dư tài khoản của bạn!',
                [{ text: 'Ok', style: 'cancel' }],
                { cancelable: false }
            );
        } else {
            this._confirmSubmit();
        }
    }
    _confirmSubmit = async () => {
        this.setState({ isLoading: true });
        await Order.createDepositNote({
            id_vanchuyen: this.state.id_vanchuyen,
            id_thuquy: 0,
            sotien: this.state.sotien,
            ghichu: this.state.ghichu,
            tinhtrang: 9,
        }, (responseJson) => {
            if (responseJson.success) {
                this.setState({ isLoading: false });
                Alert.alert(
                    'Thông báo',
                    'Tạo phiếu nạp tiền thành công!',
                    [
                        {
                            text: 'Ok',
                            onPress: () => {
                                this.props.navigation.goBack();
                            }
                        }
                    ],
                    { cancelable: false }
                );
            } else {
                Alert.alert('Thông báo',
                    responseJson.message,
                    [{ text: 'Ok', style: 'cancel' }],
                    { cancelable: false }
                );
            }
        }, e => {
            Alert.alert(
                'Thông báo',
                e.toString(),
                [{ text: 'Ok', style: 'cancel' }],
                { cancelable: false }
            );
        });
        this.setState({ isLoading: false });
    }
    _changeMoney(text) {
        this.setState({ cur_so_tien: text, sotien: this.moneyField.getRawValue() });
    }
    render() {
        const {
            fontFace,
            content,
            input,
            textarea,
            btnSubmit
        } = Styles;
        return (
            <Container style={fontFace}>
                <TopBar title='Tạo phiếu đóng tiền' navigation={this.props.navigation} />
                <Content style={content} padder>
                    <Text style={{ marginBottom: 10 }}>Số dư hiện tại: {FormatCurrency(this.props.navigation.getParam('sodu_taikhoan'))} đ</Text>
                    {/* <TextInput
                        onChangeText={(text) => this.setState({ sotien: text })}
                        placeholder='Số tiền nạp'
                        keyboardType='numeric'
                        style={input}
                    /> */}
                    <TextInputMask
                        type={'money'}
                        options={{
                            precision: 0,
                            separator: ',',
                            delimiter: '.',
                            suffixUnit: '',
                            unit: '',
                        }}
                        placeholder='Số tiền nạp'
                        style={input}
                        value={this.state.cur_so_tien}
                        onChangeText={(text) => this._changeMoney(text)}
                        ref={(ref) => this.moneyField = ref}
                    />
                    <TextInput
                        style={textarea}
                        multiline
                        numberOfLines={4}
                        textAlignVertical='top'
                        onChangeText={(text) => this.setState({ ghichu: text })}
                        placeholder='Ghi chú'
                    />
                    <Button
                        block
                        style={btnSubmit}
                        onPress={() => this._submit()}
                    >
                        {this.state.isLoading ? <ActivityIndicator size="small" color="#fff" /> : null}
                        <Text>Xác nhận</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

export default CreateDepositNote;
