import React, { Component } from 'react';
import { View, Alert } from 'react-native';
import { Button, Text, Icon } from 'native-base';
import { AuthShiper } from '../../../Networking/auth';
import { Order } from '../../../Networking/order';
import ReceiveOrder from './topcontrols/ReceiveOrder';
import ShipperIsComing from './topcontrols/ShipperIsComing';
import ReceiptOrder from './topcontrols/ReceiptOrder';

class ShipperTopControl extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //dataSource: '',
            ghichu: '',
            modalVisible: false,
            modalCreateVisible: false,
            id_vanchuyen: '',
            id_donhang: '',
            trangthai: '',
        };
    }

    async componentDidMount() {
        const currentUser = await AuthShiper.currentUser();
        if (currentUser) {
            this.setState({ id_vanchuyen: currentUser.id });
        }
    }

    _setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    _setCreateExportVisible(visible) {
        this.setState({ modalCreateVisible: visible });
    }

    _receiveOrder() {
        Alert.alert(
            'Xác nhận',
            'Bạn muốn nhận đơn này?',
            [
                { text: 'Hủy', style: 'cancel' },
                { text: 'OK', onPress: () => this._confirmReceiveOrder() },
            ]
        );
    }

    // XÁC NHẬN ĐƠN HÀNG
    async _confirmReceiveOrder() {
        this.setState({ isLoading: true });
        await Order.confirm({
            id_vanchuyen: this.state.id_vanchuyen,
            id_donhang: this.props.idOrder, 
            trangthai: 1,
        }, (responseJson) => {
            if (responseJson.success) {
                this.props.navigation.navigate('NoticeScreen', {
                    msg: 'Nhận đơn hàng thành công, mã đơn hàng #' + this.props.idOrder + '',
                    callback: () => this.props.navigation.navigate('ShipperListOrderScreen', {
                        refresh: true
                    })
                });
            } else {
                Alert.alert('Thông báo',
                    responseJson.message,
                    [{ text: 'Ok', style: 'cancel' }],
                    { cancelable: false }
                );
            }
        }, e => {
            Alert.alert('Thông báo',
                'Không thể kết nối tới máy chủ, vui lòng thử lại sau!',
                [{ text: 'Ok', style: 'cancel' }],
                { cancelable: false }
            );
        });
        this.setState({ isLoading: false });
    }
    _check() {
        const { dataSource } = this.props;
        let check = false;
        // da nhan hang, chua giao
        if (dataSource.trangthai === '3' && dataSource.phieu_tra_hang === null) {
            check = true;
            return check;
        }
        // dan cho nhap kho, da huy phieu nhap kho
        if (dataSource.phieu_nhap_kho !== null && dataSource.phieu_nhap_kho.tinhtrang === '21' && dataSource.phieu_tra_hang === null) {
            check = true;
            return check;
        }
        // da xuat kho, duoc yeu cau hoan tra
        if (dataSource.phieu_xuat_kho !== null && dataSource.phieu_xuat_kho.tinhtrang === '17' 
            && dataSource.phieu_nhap_kho !== null && dataSource.phieu_nhap_kho.tinhtrang === '15' 
                && dataSource.phieu_tra_hang === null) {
            check = true;
            return check;
        }
        // doi tra hang ve kho
        if (dataSource.phieu_xuat_kho !== null && dataSource.phieu_nhap_kho !== null && dataSource.phieu_nhap_kho.tinhtrang === '18' && dataSource.phieu_tra_hang === null) {
            check = false;
            return check;
        }
        // nhan hang da tra ve kho
        if (dataSource.phieu_xuat_kho !== null && dataSource.phieu_nhap_kho !== null && dataSource.phieu_nhap_kho.tinhtrang === '19' && dataSource.phieu_tra_hang === null) {
            check = true;
            return check;
        }
        return check;

    }
    render() {
        const {
            idOrder,
            dataSource,
            navigation,
        } = this.props;
        let topControls = null;
        let shippingFee = 0;
        if ( dataSource.duocthanhtoan_boi === 0 && dataSource.thuho ) {
            shippingFee = dataSource.tongtien;
        } else if(dataSource.duocthanhtoan_boi === 0 && !dataSource.thuho) {
            shippingFee = dataSource.gui_tinhphi;
        } else if(dataSource.duocthanhtoan_boi > 0 && dataSource.thuho ) {
            shippingFee = dataSource.giatri_kienhang;
        }
        switch (this.props.orderStatus) {
            case "":
                topControls = (
                    <ReceiveOrder
                        id_vanchuyen={this.state.id_vanchuyen}
                        id_donhang={idOrder}
                        navigation={navigation}
                    />
                );
                break;
            case "1":
                topControls = (
                    <ShipperIsComing
                        id_vanchuyen={this.state.id_vanchuyen}
                        id_donhang={idOrder}
                        navigation={navigation}
                    />
                );
                break;
            case "2":
                topControls = (
                    <ReceiptOrder
                        id_vanchuyen={this.state.id_vanchuyen}
                        id_donhang={idOrder}
                        navigation={navigation}
                        dataSource={dataSource}
                        backAction={() => this.props.backAction()}
                        afterReceive={() => this.props.afterReceive()}
                    />
                );
                break;
            case "3":
                topControls = (
                    <View
                        style={{
                            backgroundColor: '#fff',
                            borderBottomColor: '#eee',
                            borderBottomWidth: 2,
                            paddingHorizontal: 10
                        }}
                    >
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'flex-end'
                        }}>
                            {dataSource.phieu_giao_hang === null && dataSource.phieu_nhap_kho === null ? (
                                <Button
                                    iconLeft
                                    success
                                    onPress={() => {
                                        navigation.navigate('CreateDeliveryBill', {
                                            id_vanchuyen: this.state.id_vanchuyen,
                                            idOrder: idOrder,
                                            orderTotal: shippingFee,
                                            onNavigateBack: this.props.backAction
                                        });
                                    }}
                                    style={{ 
                                        height: 'auto',
                                        marginLeft: 5,
                                        marginVertical: 5
                                    }}
                                >
                                    <Icon name='add' style={{ fontSize: 18, marginLeft: 10 }} />
                                    <Text style={{ fontSize: 14, paddingLeft: 5, paddingRight: 10 }}>Giao hàng</Text>
                                </Button>
                            ) : null}
                            {dataSource.phieu_nhap_kho === null && dataSource.phieu_giao_hang === null ? (
                                <Button
                                    iconLeft
                                    primary
                                    onPress={() => {
                                        this._setModalVisible(false);
                                        navigation.navigate('CreateImportWarehouseBill', {
                                            id_donhang: idOrder,
                                            id_vanchuyen: this.state.id_vanchuyen,
                                            onNavigateBack: this.props.backAction,
                                            backToStock: dataSource.phieu_giao_hang !== null ? true : false
                                        });
                                    }}
                                    style={{ 
                                        height: 'auto', 
                                        marginLeft: 5,
                                        marginVertical: 5
                                    }}
                                >
                                    <Icon name='add' style={{ fontSize: 18, marginLeft: 10 }} />
                                    <Text style={{ fontSize: 14, paddingLeft: 5, paddingRight: 10 }}>
                                        Nhập kho
                                    </Text>
                                </Button>
                            ) : null}
                        </View>
                    </View>
                );
                break;
            //case "4": //Giao hang thanh cong
            case "5":
            case "6":
            case "7":
            case "8":
            case "11":
                topControls = (
                    <View
                        style={{
                            backgroundColor: '#fff',
                            borderBottomColor: '#eee',
                            borderBottomWidth: 2,
                            paddingHorizontal: 10
                        }}
                    >
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'flex-end'
                        }}>
                            {dataSource.phieu_nhap_kho !== null && dataSource.phieu_giao_hang !== null && dataSource.phieu_xuat_kho !== null ? (
                                <Button
                                    iconLeft
                                    primary
                                    onPress={() => {
                                        this._setModalVisible(false);
                                        navigation.navigate('CreateImportWarehouseBill', {
                                            id_donhang: idOrder,
                                            id_vanchuyen: this.state.id_vanchuyen,
                                            onNavigateBack: this.props.backAction,
                                            backToStock: dataSource.phieu_giao_hang !== null ? true : false
                                        });
                                    }}
                                    style={{ 
                                        height: 'auto',
                                        marginLeft: 5,
                                        marginVertical: 5
                                    }}
                                >
                                    <Icon name='add' style={{ fontSize: 18, marginLeft: 10 }} />
                                    <Text style={{ fontSize: 14, paddingLeft: 5, paddingRight: 10 }}>
                                        Trả hàng về kho
                                    </Text>
                                </Button>
                            ) : null}
                            {dataSource.phieu_tra_hang === null && dataSource.phieu_giao_hang !== null && dataSource.phieu_nhap_kho === null ? (
                                <Button
                                    iconLeft
                                    danger
                                    onPress={() => {
                                        navigation.navigate('CreateReturnOrderScreen', {
                                            id_vanchuyen: this.state.id_vanchuyen,
                                            id_donhang: idOrder,
                                            tongtien: dataSource.duocthanhtoan_boi !== 0 ? 0 : dataSource.gui_tinhphi,
                                            onNavigateBack: this.props.backAction
                                        });
                                    }
                                    }
                                    style={{
                                        height: 'auto',
                                        marginLeft: 5,
                                        marginVertical: 5

                                    }}
                                >
                                    <Icon name='times-circle' type='FontAwesome' style={{ fontSize: 18, marginLeft: 10 }} />
                                    <Text style={{ fontSize: 14, paddingLeft: 5, paddingRight: 10 }}>Trả hàng</Text>
                                </Button>
                            ) : null}
                        </View>
                    </View>
                );
                break;
            case '17':
                topControls = (
                    <View
                        style={{
                            backgroundColor: '#fff',
                            borderBottomColor: '#eee',
                            borderBottomWidth: 2,
                            paddingHorizontal: 10
                        }}
                    >
                        {dataSource.phieu_giao_hang === null ? (
                            <View style={{
                                flexDirection: 'row',
                                justifyContent: 'flex-end'
                            }}>
                                <Button
                                    iconLeft
                                    success
                                    onPress={() => {
                                        navigation.navigate('CreateDeliveryBill', {
                                            id_vanchuyen: this.state.id_vanchuyen,
                                            idOrder: idOrder,
                                            orderTotal: shippingFee,
                                            onNavigateBack: this.props.backAction
                                        });
                                    }}
                                    style={{ 
                                        height: 'auto',
                                        marginLeft: 5,
                                        marginVertical: 5
                                    }}
                                >
                                    <Icon name='add' style={{ fontSize: 18, marginLeft: 10 }} />
                                    <Text style={{ fontSize: 14, paddingLeft: 5, paddingRight: 10 }}>Giao hàng</Text>
                                </Button>
                            </View>
                        ) : (
                            dataSource.phieu_giao_hang !== null && dataSource.phieu_giao_hang.tinhtrang !=='0' && dataSource.phieu_tra_hang === null ? (
                                <View style={{
                                    flexDirection: 'row',
                                    justifyContent: 'flex-end'
                                }}>
                                    <Button
                                        iconLeft
                                        danger
                                        onPress={() => {
                                            navigation.navigate('CreateReturnOrderScreen', {
                                                id_vanchuyen: this.state.id_vanchuyen,
                                                id_donhang: idOrder,
                                                tongtien: dataSource.duocthanhtoan_boi !== 0 ? 0 : dataSource.gui_tinhphi,
                                                onNavigateBack: this.props.backAction
                                            });
                                        }
                                        }
                                        style={{
                                            height: 'auto',
                                            marginLeft: 5,
                                            marginVertical: 5 
                                        }}
                                    >
                                        <Icon name='times-circle' type='FontAwesome' style={{ fontSize: 18, marginLeft: 10 }} />
                                        <Text style={{ fontSize: 14, paddingLeft: 5, paddingRight: 10 }}>Trả hàng</Text>
                                    </Button>
                                </View>
                            ) : null
                        )}
                    </View>    
                );
                break;      
            default:
                break;
        }
        const check = this._check();
        return (
            <View>
                {typeof dataSource.hoantrahang !== 'undefined' && dataSource.hoantrahang === 1 && check ?
                    <View 
                        style={{
                            paddingVertical: 2, 
                            flexDirection: 'row',
                            justifyContent: 'flex-end',
                            paddingHorizontal: 10, 
                            borderBottomColor: '#eee', 
                            borderBottomWidth: 2 
                        }}
                    >
                        {dataSource.phieu_nhap_kho !== null && dataSource.phieu_nhap_kho.tinhtrang !== '19'
                            && dataSource.phieu_xuat_kho !== null && dataSource.phieu_giao_hang === null  ? (
                            <Button
                                iconLeft
                                primary
                                onPress={() => {
                                    navigation.navigate('CreateImportWarehouseBill', {
                                        id_donhang: idOrder,
                                        id_vanchuyen: this.state.id_vanchuyen,
                                        onNavigateBack: this.props.backAction,
                                        backToStock: true
                                    });
                                }}
                                style={{ 
                                    height: 'auto',
                                    marginLeft: 5,
                                    marginVertical: 5
                                }}
                            >
                                <Icon name='add' style={{ fontSize: 18, marginLeft: 10 }} />
                                <Text style={{ fontSize: 14, paddingLeft: 5, paddingRight: 10 }}>
                                    Trả hàng về kho
                                </Text>
                            </Button>
                        ) : (
                            <Button
                                iconLeft
                                danger
                                onPress={() => {
                                    navigation.navigate('CreateReturnOrderScreen', {
                                        id_vanchuyen: this.state.id_vanchuyen,
                                        id_donhang: idOrder,
                                        tongtien: dataSource.duocthanhtoan_boi !== 0 ? 0 : dataSource.gui_tinhphi,
                                        onNavigateBack: this.props.backAction
                                    });
                                }
                                }
                                style={{
                                    height: 'auto',
                                    marginLeft: 5,
                                    marginVertical: 5 
                                }}
                            >
                                <Icon name='times-circle' type='FontAwesome' style={{ fontSize: 18, marginLeft: 10 }} />
                                <Text style={{ fontSize: 14, paddingLeft: 5, paddingRight: 10 }}>Trả hàng</Text>
                            </Button>
                        )}
                    </View>
                : null}
                {typeof dataSource.hoantrahang !== 'undefined' && dataSource.hoantrahang !== 1 ? topControls : null }
            </View>
        );
    }
}

export default ShipperTopControl;
