import React, { Component } from 'react';
import { View, Alert } from 'react-native';
import { Button, Text, Icon } from 'native-base';
import Colors from '../../../Assets/Colors';
import { Order } from '../../../../Networking/order';


class ReceiptOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // tongTien: '',
        };
    }

    // componentDidMount() {
    //     if (this.props.dataSource.duocthanhtoan_boi > 0) {
    //         this.setState({ tienPhaiThu: 0 });
    //     } else {
    //         this.setState({ tienPhaiThu: gui_tinhphi });
    //     }
    // }
    _cancelOrder() {
        Alert.alert(
            'Xác nhận',
            'Bạn muốn hủy đơn hàng này?',
            [
                { text: 'Hủy', style: 'cancel' },
                { text: 'OK', onPress: () => this._confirmCancelOrder() },
            ]
        );
    }

    // XÁC NHẬN ĐƠN HÀNG
    async _confirmCancelOrder() {
        this.setState({ isLoading: true });
        await Order.confirm({
            id_vanchuyen: this.props.id_vanchuyen,
            id_donhang: this.props.id_donhang,
            trangthai: 13,
        }, (responseJson) => {
            if (responseJson.success) {
                this.props.navigation.navigate('NoticeScreen', {
                    msg: 'Đã hủy đơn hàng có mã đơn #' + this.props.id_donhang + '',
                    callback: () => this.props.navigation.navigate('NewOrdersScreen', {
                        refresh: true
                    })
                });
            } else {
                Alert.alert(
                    'Thông báo',
                    responseJson.message,
                    [{ text: 'Ok', style: 'cancel' }],
                    { cancelable: false }
                );
            }
        }, e => {
            Alert.alert(
                'Thông báo',
                e.toString(),
                [{ text: 'Ok', style: 'cancel' }],
                { cancelable: false }
            );
        });
        this.setState({ isLoading: false });
    }
    render() {
        return (
            <View style={{
                flexDirection: 'row',
                backgroundColor: '#fff',
                justifyContent: 'flex-end',
                paddingVertical: 5,
                paddingHorizontal: 10,
                borderBottomColor: '#eee',
                borderBottomWidth: 2,
            }}>
                <Button
                    iconRight
                    success
                    onPress={() => {
                        this.props.navigation.navigate('CreateReceiptBill', {
                            idOrder: this.props.id_donhang,
                            orderTotal: this.props.dataSource.duocthanhtoan_boi > 0 ? this.props.dataSource.gui_tinhphi : 0,
                            id_vanchuyen: this.props.id_vanchuyen,
                            onNavigateBack: this.props.backAction,
                            afterReceive: this.props.afterReceive,
                        });
                    }}
                    style={{
                        height: 'auto'
                    }}
                >
                    <Text style={{ fontSize: 14, paddingLeft: 10, paddingRight: 5 }}>Nhận hàng</Text>
                    <Icon
                        type='FontAwesome'
                        name='check'
                        style={{
                            fontWeight: 'normal',
                            fontSize: 15,
                            marginRight: 10
                        }}
                    />
                </Button>
                <Button
                    iconRight
                    danger
                    onPress={() => {
                        this._cancelOrder()
                    }}
                    style={{ height: 'auto', marginLeft: 5 }}
                >
                    <Text style={{ fontSize: 14, paddingLeft: 10, paddingRight: 5 }}>Hủy đơn</Text>
                    <Icon
                        type='FontAwesome'
                        name='times-circle'
                        style={{
                            fontWeight: 'normal',
                            fontSize: 15,
                            marginRight: 10,
                        }}
                    />
                </Button>
            </View>
        );
    }
}

export default ReceiptOrder;
