import React, { Component } from 'react';
import { View, Alert } from 'react-native';
import { Button, Text, Icon } from 'native-base';
import { Order } from '../../../../Networking/order';

class ShipperIsComing extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    
    _isComing() {
        Alert.alert(
            'Xác nhận',
            'Xác nhận đang tới lấy hàng?',
            [
                { text: 'Hủy', style: 'cancel' },
                { text: 'OK', onPress: () => this._confirmIsComing() },
            ]
        );
    }

    // XÁC NHẬN ĐƠN HÀNG
    async _confirmIsComing() {
        this.setState({ isLoading: true });
        await Order.confirm({
            id_vanchuyen: this.props.id_vanchuyen,
            id_donhang: this.props.id_donhang, 
            trangthai: 2,

        }, (responseJson) => {
            if (responseJson.success) {
                
                this.props.navigation.navigate('NoticeScreen', {
                    msg: 'Xác nhận đang tới nhận hàng thành công, mã đơn hàng #' + this.props.id_donhang + '',
                    callback: () => this.props.navigation.navigate('ShipperListOrderScreen', {
                        refresh: true
                    })
                });
            } else {
                Alert.alert('Thông báo',
                    responseJson.message,
                    [{ text: 'Ok', style: 'cancel' }],
                    { cancelable: false }
                );
            }
        }, e => {
            Alert.alert(
                'Thông báo',
                e.toString(),
                [{ text: 'Ok', style: 'cancel' }],
                { cancelable: false }
            );
        });
        this.setState({ isLoading: false });
    }
    render() {
        return (
            <View style={{
                flexDirection: 'row', 
                backgroundColor: '#fff', 
                justifyContent: 'flex-end',
                paddingHorizontal: 10,
                paddingVertical: 5,
                borderBottomColor: '#eee',
                borderBottomWidth: 2,
            }}>
                <Button 
                    iconRight 
                    success 
                    onPress={() => this._isComing()}
                    style={{
                        height: 'auto'
                    }}
                >
                    <Text style={{ fontSize: 13, paddingRight: 5, paddingLeft: 10 }}>Tới lấy hàng</Text>
                    <Icon
                        type='FontAwesome'
                        name='truck'
                        style={{
                            fontWeight: 'normal',
                            fontSize: 15,
                            marginRight: 10,
                        }}
                    />
                </Button>
            </View>
        );
    }
}

export default ShipperIsComing;
