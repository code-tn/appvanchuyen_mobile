import React, { Component } from 'react';
import { FlatList, RefreshControl, ActivityIndicator, View, Alert } from 'react-native';
import { Container, Text } from 'native-base';
import Styles from '../../Assets/Styles';
import TopBarOnlyTitle from '../../Shared/TopBarOnlyTitle';
import OrderItem from './partials/OrderItem';
import ComLoading from '../../Shared/Loading';
import { AuthShiper } from '../../../Networking/auth';
import { Order } from '../../../Networking/order';
import Colors from '../../Assets/Colors';
import ShipperFilter from './ShipperFilter';

class ShipperListOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFilter: false,
            id_vanchuyen: '',
            data: '',
            dataList: [],
            isLoading: true,
            refreshing: false,
            isLoadMore: false,
            test: true,
            filter: {
                tu_ngay: new Date().toISOString().slice(0, 10),
                den_ngay: new Date().toISOString().slice(0, 10),
                trang_thai_don_hang: '',
                id_vanchuyen: ''
            }
        };
    }

    async componentDidMount() {
        const currentUser = await AuthShiper.currentUser();
        if (currentUser === null) {
            this.props.navigation.navigate('StaffLoginScreen');
        }
        if (currentUser) {
            this.setState({ id_vanchuyen: currentUser.id, filter: { ...this.state.filter, ...{ id_vanchuyen: currentUser.id } } });
        }
        await this._loadUserOrders();
    }

    async _loadUserOrders(request_url = null) {
        await Order.getShiperOrders(this.state.id_vanchuyen, request_url, (responseJson) => {
            if (responseJson.success) {
                this.setState({
                    data: responseJson.data,
                    dataList: request_url === null ? responseJson.data.data : [...this.state.dataList, ...responseJson.data.data],
                    isLoading: false,
                    refreshing: false,
                    isLoadMore: false
                });
            }
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
            this.setState({
                isLoading: false,
                refreshing: false,
                isLoadMore: false
            });
        });
        this.setState({
            isLoading: false,
            refreshing: false
        });
    }

    _onRefresh = async () => {
        this.setState({
            refreshing: true,
            data: '',
            dataList: [],
            isFilter: false
        });
        await this._loadUserOrders();
    };
    _onHandleLoadmore = async () => {
        if (this.state.data.current_page < this.state.data.last_page) {
            this.setState({ isLoadMore: true });
            if (this.state.isFilter) {
                await this.loadMoreFilterOrder(this.state.data.next_page_url);
            } else {
                await this._loadUserOrders(this.state.data.next_page_url);
            }
        }
    }
    async filterOrder(request_url = null) {
        this.UserTopControl._setModalVisible(false);
        const params = this.state.filter;
        this.setState({
            refreshing: true,
            isFilter: true,
            data: '',
            dataList: []
        });
        await Order.getShiperFilterOrders(params, request_url, (res) => {
            if (res.success) {
                this.setState({
                    data: res.data,
                    dataList: request_url === null ? res.data.data : [...this.state.dataList, ...res.data.data],
                    refreshing: false,
                    isLoadMore: false
                });
            } else {
                Alert.alert(
                    'Lỗi',
                    res.message,
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
            }
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
            this.setState({ refreshing: false, isFilter: false });
        });
    }
    async loadMoreFilterOrder(request_url) {
        this.UserTopControl._setModalVisible(false);
        const params = this.state.filter;
        await Order.getShiperFilterOrders(params, request_url, (res) => {
            if (res.success) {
                this.setState({
                    data: res.data,
                    dataList: [...this.state.dataList, ...res.data.data],
                    isLoadMore: false
                });
            } else {
                Alert.alert(
                    'Lỗi',
                    res.message,
                    [
                        { text: 'OK' },
                    ],
                    { cancelable: false },
                );
            }
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    { text: 'OK' },
                ],
                { cancelable: false },
            );
            this.setState({ isLoadMore: false });
        });
    }
    render() {
        const { fontFace, content } = Styles;
        if (this.state.isLoading) {
            return (
                <ComLoading />
            );
        }
        if (this.state.id_vanchuyen == '') {
            return (
                <ComLoading />
            );
        }
        return (
            <Container style={fontFace}>
                <TopBarOnlyTitle title='Đơn hàng của bạn' />
                <ShipperFilter
                    id_vanchuyen={this.state.id_vanchuyen}
                    parent={this}
                    ref={ref => this.UserTopControl = ref}
                />
                <View style={[content, { flex: 1, padding: 10 }]}>
                    {(this.state.data === undefined || this.state.dataList.length === 0 || this.state.refreshing) ? (
                        <Text style={{ fontSize: 15, textAlign: 'center', }}>
                            {this.state.refreshing ? 'Đang tải đơn hàng...' : 'Không có đơn hàng nào...'}
                        </Text>
                    ) : null}
                    <FlatList
                        style={{ flex: 1 }}
                        const
                        data={this.state.dataList}
                        renderItem={({ item }) =>
                            <OrderItem navigation={this.props.navigation} order={item} />
                        }
                        keyExtractor={(item) => item.id.toString()}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this._onRefresh}
                            />
                        }
                        onEndReached={this._onHandleLoadmore}
                        onEndReachedThreshold={0.1}
                    />
                    {this.state.isLoadMore ?
                        <View style={{ paddingTop: 10, backgroundColor: '#eee' }}>
                            <ActivityIndicator size='small' color={Colors.mainColor} />
                        </View> : null}
                </View>
            </Container>
        );
    }
}

export default ShipperListOrder;
