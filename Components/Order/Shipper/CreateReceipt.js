import React, { Component } from 'react';
import { TextInput, ActivityIndicator, Alert, } from 'react-native';
import { Container, Content, Button, Text } from 'native-base';
import TopBar from '../../Shared/TopBar';
import Styles from '../../Assets/Styles';
import { ApiConfig } from '../../../config/api';
import FormatCurrency from '../../Assets/FormatCurrency';

class CreateReceipt extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sotiennhan: '',
            ghichu: '',
            id_vanchuyen: '',
            id_donhang: '',
            ghichu_nhanhang: '',
            isLoading: false
        };
    }

    componentDidMount() {
        this.setState({
            id_vanchuyen: this.props.navigation.getParam('id_vanchuyen'),
            id_donhang: this.props.navigation.getParam('idOrder'),
            sotiennhan: this.props.navigation.getParam('orderTotal')
        });
    }

    async _submit() {
        this.setState({ isLoading: true });
        await fetch(ApiConfig.api_url + 'api/v1/create-receipt-note', {
            method: 'POST',
            headers: ApiConfig.xhr_header,
            body: JSON.stringify({
                sotiennhan: this.state.sotiennhan,
                ghichu: this.state.ghichu,
                id_vanchuyen: this.state.id_vanchuyen,
                id_donhang: this.state.id_donhang,
                ghichu_nhanhang: this.state.ghichu_nhanhang
            })
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false
                });
                if (responseJson.success === true) {
                    Alert.alert(
                        'Thông báo',
                        'Tạo phiếu nhận hàng thành công',
                        [
                            {
                                text: 'OK',
                                onPress: () => {
                                    this.props.navigation.state.params.onNavigateBack();
                                    this.props.navigation.state.params.afterReceive();
                                    this.props.navigation.goBack();
                                }
                            },
                        ],
                        { cancelable: false }
                    );
                } else {
                    Alert.alert(
                        'Thông báo',
                        responseJson.message,
                        [
                            { text: 'Cancel', style: 'cancel' }
                        ],
                        { cancelable: false }
                    );
                }
            })
            .catch((error) => {
                // console.error(error);
                Alert.alert(
                    'Thông báo',
                    error.toString(),
                    [
                        { text: 'Cancel', style: 'cancel' }
                    ],
                    { cancelable: false }
                );
            });
    }

    render() {
        const {
            textarea,
            fontFace,
            btnSubmit
        } = Styles;
        const {
            sotiennhan,
            ghichu
        } = this.state;
        return (
            <Container style={fontFace}>
                <TopBar title='Tạo phiếu nhận hàng' navigation={this.props.navigation} />
                <Content padder style={Styles.content}>
                    <Text style={{ marginBottom: 5 }}>Mã vận đơn: #{this.state.id_donhang}</Text>
                    <Text style={{ marginBottom: 5 }}>Số tiền nhận: {FormatCurrency(sotiennhan)} đ</Text>
                    <TextInput
                        style={textarea}
                        multiline
                        numberOfLines={4}
                        value={ghichu}
                        onChangeText={(text) => this.setState({ ghichu: text })}
                        placeholder='Ghi chú'
                        textAlignVertical={'top'}
                    />
                    <Button
                        block
                        style={btnSubmit}
                        onPress={() => this._submit()}
                    >
                        {this.state.isLoading ?
                            <ActivityIndicator size='small' color='#fff' /> : null}
                        <Text>Xác nhận</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

export default CreateReceipt;
