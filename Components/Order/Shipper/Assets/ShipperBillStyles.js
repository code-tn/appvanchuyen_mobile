import { StyleSheet } from 'react-native';
import Colors from '../../../Assets/Colors';

export default StyleSheet.create({
    billBox: {
        backgroundColor: '#eee',
        paddingTop: 10,
    },
    billItem: {
        marginBottom: 10,
        borderRadius: 5,
        backgroundColor: '#fff',
        padding: 10
    },
    billText: {
        fontSize: 14,
        lineHeight: 20,
        marginBottom: 4,
        color: Colors.blackColor,
    },
    btnBillUpdate: {
        backgroundColor: Colors.subColor,
        height: 'auto'
    }
});
