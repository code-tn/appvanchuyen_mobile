import { StyleSheet } from 'react-native';
import Colors from '../../../Assets/Colors';

const {
    mainColor,
    subColor
} = Colors;
const NewOrders = StyleSheet.create({
    content: {
        backgroundColor: '#eee'
    },
    mainTitle: {
        width: '100%',
        fontSize: 15,
        lineHeight: 22,
        color: '#fff',
        fontWeight: 'bold',
        flexDirection: 'row'
    },
    controlBox: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    btnColtrol: {
        marginLeft: 5,
        width: 'auto',
        marginTop: 3,
        fontSize: 13,
        lineHeight: 19,
        paddingLeft: 0,
        paddingRight: 0,
        color: mainColor,
        height: 32,
    },
    btnText: {
        color: mainColor,
        paddingRight: 3,
        paddingLeft: 5,
    },
    btnIcon: {
        fontSize: 13,
        lineHeight: 19,
        marginRight: 3,
        color: mainColor
    },
    header: {
        paddingVertical: 5,
        paddingHorizontal: 10,
        backgroundColor: '#fff',
        height: 'auto',
    },
    //order
    orderItem: {
        marginBottom: 10,
        padding: 10,
        borderRadius: 5,
        overflow: 'hidden',
        backgroundColor: '#fff'
    },
    orderItemCode: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 3,
    },
    orderItemCodeIcon: {
        width: 35,
        height: 35,
        resizeMode: 'contain',
        marginRight: 10,
    },
    orderItemCodeText: {
        fontWeight: 'bold',
        fontSize: 15,
        lineHeight: 20,
    },
    orderItemText: {
        fontSize: 14,
        lineHeight: 20,
        marginBottom: 4
    },
    boldStyle: {
        fontWeight: 'bold'
    },
    line: {
        borderTopWidth: 1,
        borderTopColor: '#dad9d7',
        paddingTop: 4
    },
    orderItemControl: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
    },
    btnOIC: {
        marginRight: 5,
        width: 'auto',
        height: 'auto',
        marginTop: 3,
        fontSize: 13,
        lineHeight: 19,
        paddingLeft: 0,
        paddingRight: 0,
    },
    btnOICIcon: {
        marginLeft: 0,
        color: subColor,
        fontSize: 14,
        lineHeight: 20,
    },
    btnOICText: {
        color: subColor,
        paddingLeft: 5,
        paddingRight: 5,
        fontSize: 14,
        lineHeight: 20,
    },
    noneMarginRight: {
        marginRight: 0
    },
    //search
    formSearch: {
        flexDirection: 'row',
    },
    searchInput: {
        flex: 1,
        borderWidth: 1,
        borderColor: '#eee',
        height: 32,
        paddingVertical: 0,
        paddingHorizontal: 8,
    },
    btnSearch: {
        height: 32,
        backgroundColor: subColor
    },
    btnIconSearch: {
        fontSize: 14,
        lineHeight: 20,
        marginLeft: 10,
        marginRight: 10,
    }
});
export default NewOrders;
