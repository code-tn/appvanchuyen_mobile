import { StyleSheet } from 'react-native';
import Colors from '../../../Assets/Colors';

export default StyleSheet.create({
    billItem: {
        backgroundColor: '#fff',
        borderRadius: 5,
        marginBottom: 10,
        padding: 5,
        borderTopWidth: 4,
        borderTopColor: Colors.subColor
    },
    idStyle: {
        color: Colors.mainColor
    },
    text: {
        fontSize: 14,
        lineHeight: 20,
        marginBottom: 3
    },
    line: {
        borderTopWidth: 1,
        borderTopColor: '#ddd',
        paddingTop: 4,
    }
});
