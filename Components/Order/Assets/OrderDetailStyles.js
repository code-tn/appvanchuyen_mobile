import { StyleSheet } from 'react-native';
import Colors from '../../Assets/Colors';

export default StyleSheet.create({
    collapseHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: Colors.mainColor,
        padding: 10,
        paddingVertical: 5,
        borderTopWidth: 1,
        borderTopColor: '#dad9d7',
        color:'#fff'
    },
    collapseBody: {
        padding: 10,
        backgroundColor: '#fff',
        borderTopWidth: 1,
        borderTopColor: '#dad9d7',
    },
    timelineBox: {
        position: 'relative',
        paddingLeft: 15,
    },
    verticalLine: {
        position: 'absolute',
        left: 5,
        top: 0,
        bottom: 0,
        borderLeftWidth: 1,
        borderLeftColor: '#dad9d7',
    },
    timelineItemTitle: {
        color: Colors.mainColor,
        marginBottom: 4
    },
    timelineDot: {
        width: 11,
        height: 11,
        borderRadius: 5.5,
        backgroundColor: '#dad9d7',
        position: 'absolute',
        left: -15,
        top: 6
    },
    timelineItemDesc: {
        fontSize: 15,
        marginBottom: 2,
    },
    line: {
        borderTopWidth: 1,
        borderTopColor: '#dad9d7',
        paddingTop: 10,
        marginTop: 10,
    },
    bold: {
        fontWeight: '600'
    }
});
