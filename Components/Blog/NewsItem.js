import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import { Icon } from 'native-base';
import NewsItemStyles from './assets/NewsItemStyles';
import Colors from '../Assets/Colors';

class NewsItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const item = this.props.item;
        const {
            itemBox,
            title
        } = NewsItemStyles;
        return (
            <View style={itemBox}>
                <Text 
                    style={title}
                    onPress={() => this.props.navigation.navigate('PostScreen', {
                        title: item.title,
                        slug: item.slug,
                        content: item.content_html
                    })}
                >
                    {item.title}
                </Text>
                <Text style={{ marginBottom: 2 }}><Icon type='FontAwesome' name='calendar' style={{ fontSize: 16 }}  /> {item.published_at}</Text>
                <Text style={{ fontSize: 15, lineHeight: 22 }}>{item.excerpt}...
                    <Text
                        style={{ color: Colors.subColor }}
                        onPress={() => this.props.navigation.navigate('PostScreen', {
                            title: item.title,
                            slug: item.slug,
                            content: item.content_html
                        })}
                    > 
                        [Chi tiết]
                    </Text>
                </Text>
            </View>
        );
    }
}
export default NewsItem;
