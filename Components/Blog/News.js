import React, { Component } from 'react';
import { View, Text, FlatList, RefreshControl, ActivityIndicator, SafeAreaView, Alert } from 'react-native';
import { Container, Content } from 'native-base';
import TopBar from '../Shared/TopBar';
import Styles from '../Assets/Styles';
import Loading from '../Shared/Loading';
import { Blog } from '../../Networking/blog';
import NewsItem from './NewsItem';
import Colors from '../Assets/Colors';
class News extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            currentPage: '',
            lastPage: '',
            nextPageUrl: '',

            isLoading: true,
            refreshing: false,
            isLoadmore: false,
        };
    }
    componentDidMount() {
        this._loadBlog();
        setTimeout(() => {
            this.isLoadmore = false;
        }, 2000);
    }
    _loadBlog = async (resquest_url = null) => {
        await Blog.getNews(resquest_url, (res) => {
            if (res.success) {
                this.setState({
                    data: resquest_url === null ? res.data.data : [...this.state.data, ...res.data.data],
                    currentPage: res.data.current_page,
                    lastPage: res.data.last_page,
                    nextPageUrl: res.data.next_page_url,
                    isLoading: false,
                    refreshing: false,
                    isLoadmore: false
                });
            } else {
                Alert.alert(
                    'Lỗi',
                    res.message,
                    [
                        {text: 'OK'},
                    ],
                    {cancelable: false},
                );
                this.setState({ isLoading: false, refreshing: false, isLoadmore: false });
            }
        }, (e) => {
            Alert.alert(
                'Lỗi',
                e.toString(),
                [
                    {text: 'OK'},
                ],
                {cancelable: false},
            );
            this.setState({ isLoading: false, refreshing: false, isLoadmore: false });
        })
    }
    _onRefresh = async () => {
        this.setState({ refreshing: true, data: [] });
        await this._loadBlog();
    }
    _handleLoadmore = async() => {
        if(this.state.currentPage < this.state.lastPage) {
            this.setState({ isLoadmore: true });
            await this._loadBlog(this.state.nextPageUrl);
        }
    }
    render() {
        const {
            FontFace,
            content
        } = Styles;
        if (this.state.isLoading) {
            return <Loading />
        }
        return (
            <Container style={FontFace}>
                <TopBar title='Tin tức' navigation={this.props.navigation} />
                {/* <Content 
                    padder
                    style={content}
                    contentContainerStyle={{ flex: 1 }}
                > */}
                <View style={[content, { flex: 1, padding: 10 }]}>
                    <SafeAreaView style={{ flex: 1 }}>
                        {this.state.data.length === 0 || this.state.refreshing ? (
                            <Text style={{ textAlign: 'center' }}>{this.state.refreshing ? 'Đang tải tin tức...' : 'Không có tin tức nào...'}</Text>
                        ) : null}
                        <FlatList
                            data={this.state.data}
                            renderItem={({ item }) => <NewsItem navigation={this.props.navigation} item={item} />}
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh}
                                />
                            }
                            keyExtractor={(item) => item.id.toString()}
                            onEndReached={this._handleLoadmore}
                        />
                        {
                            this.isLoadmore ? <ActivityIndicator style={{ marginTop: 10 }} size="small" color={Colors.mainColor} /> : null
                        }
                    </SafeAreaView>
                </View>
                {/* </Content> */}
            </Container>
        );
    }
}

export default News;
