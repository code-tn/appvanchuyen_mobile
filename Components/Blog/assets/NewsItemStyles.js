import { StyleSheet } from 'react-native';
import Colors from '../../Assets/Colors';

export default StyleSheet.create({
    itemBox: {
        backgroundColor: '#fff',
        borderTopWidth: 4,
        borderTopColor: Colors.subColor,
        padding: 10,
        marginBottom: 10,
    },
    title: {
        fontWeight: 'bold',
        fontSize: 18,
        lineHeight: 24,
        color: '#000',
        marginBottom: 10
    }
});