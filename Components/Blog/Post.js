import React, { Component } from 'react';
import { Text, Dimensions, SafeAreaView } from 'react-native';
import { Container, Content } from 'native-base';
import TopBar from '../Shared/TopBar';
import HTML from 'react-native-render-html';
import Colors from '../Assets/Colors';
import NewsItemStyles from './assets/NewsItemStyles';

class Post extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    
    render() {
        const postTitle = this.props.navigation.getParam('title', 'Chi tiết bài viết');
        const content = this.props.navigation.getParam('content');
        const {
            title
        } = NewsItemStyles;
        return (
            <Container>
                <TopBar navigation={this.props.navigation} title={postTitle}/>
                <Content style={{ padding: 10, color: Colors.blackColor }}>
                    <SafeAreaView style={{ flex: 1 }}>
                        <Text style={title}>{postTitle}</Text>
                        <HTML html={content} imagesMaxWidth={Dimensions.get('window').width - 20} />
                    </SafeAreaView>
                </Content>
            </Container>
        );
    }
}

export default Post;
