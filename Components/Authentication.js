import React from 'react';
import { Auth, AuthShiper } from '../Networking/auth';
import { ScreenManager } from '../System/Screen';
import Loading from './Shared/Loading';

export default class Authentication extends React.Component {

    constructor(props) {
        super(props);
        this._bootstrapAsync();
    }

    // Fetch the token from storage then navigate to our appropriate place
    _bootstrapAsync = async () => {
        const lastScreen = await ScreenManager.getInitScreen();
        if (lastScreen == 'ConfirmResetPasswordScreen') {
            this.props.navigation.navigate('ConfirmResetPasswordScreen');
        } else if (lastScreen == 'StaffLoginScreen') {
            let shipperSession = await AuthShiper.currentUser();
            if (shipperSession != null && shipperSession.nhiemvu === 'shipper') {
                this.props.navigation.navigate('Shipper');
            } else if (shipperSession != null && shipperSession.nhiemvu === 'stocker') {
                this.props.navigation.navigate('Stock');
            } 
            else {
                this.props.navigation.navigate('AuthShipper');
            }
        } else {
            let userSession = await Auth.currentUser();
            if (userSession != null) {
                this.props.navigation.navigate('App');
            } else {
                this.props.navigation.navigate('Auth');
            }
        }
    };

    // Render any loading content that you like here
    render() {
        let loadingText = 'Giao hàng nhanh BRVT';
        return (
            <Loading loadingText={loadingText} />
        );
    }
}
