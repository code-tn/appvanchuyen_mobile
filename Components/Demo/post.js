import React, { Component } from 'react';
import { View, TextInput } from 'react-native';

export default class PostData extends Component {

    constructor(props) {
        super(props);
        this.state = {
            txt_title: '',
            responseText: ''
        };
    }

    render() {
        <View>
            <TextInput
                onChangeText={(txt_title) => this.setState({ txt_title })}
                value={this.state.txt_title}
            />
            <Text>{this.state.responseText}</Text>
        </View>;
    }
}
