import React, { Component } from 'react';
import { View, Text, ListView } from 'react-native';

export default class DemoListView extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            dataSource: new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
        };
    }

    componentDidMount() {
        const responseData = ['Tin tức', 'Thể thao', 'Thời trang'];
        this.setState({
           dataSource: this.state.dataSource.cloneWithRows(responseData),
        });
    }

    render() {
        return (
            <View>
                <Text>Component: ListView</Text>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={(rowData) =>
                        (<View style={{ padding: 20, borderWidth: 1 }}>
                            <Text>{rowData}</Text>
                        </View>)
                    }
                />
            </View>
        );
    }
}
