import { ApiConfig } from '../config/api';

/**
 *
 * @param user_id
 * @param callback
 * @returns {Promise<void>}
 */
export async function getOrderListByUserID(params, request_url, callback) {
    await fetch(
        typeof request_url === 'undefined' ? ApiConfig.api_url + 'api/v1/get-order-list-by-customer' : request_url
        , {
            method: 'POST',
            headers: ApiConfig.xhr_header,
            body: JSON.stringify(params)
        })
        .then((response) => response.json())
        .then((responseJson) => {
            if (typeof callback === 'function') callback(responseJson);
            return responseJson;
        })
        .catch((error) => {
            return error;
        });
}
/**
 *
 * @param order_id
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function getOrderReceiptList(order_id, callback, error_handle) {
    await fetch(ApiConfig.api_url + 'api/v1/get-receipt-note/' + order_id, {
        method: 'GET', headers: ApiConfig.xhr_header,
    })
        .then((r) => r.json())
        .then((rJson) => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch((e) => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}
/**
 *
 * @param order_id
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function getOrderDeliveryBillList(order_id, callback, error_handle) {
    await fetch(ApiConfig.api_url + 'api/v1/get-delivery-note/' + order_id, {
        method: 'GET', headers: ApiConfig.xhr_header,
    })
        .then(r => r.json())
        .then(rJson => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch(e => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}
/**
 *
 * @param order_id
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function getOrderRefundBill(order_id, callback, error_handle) {
    await fetch(ApiConfig.api_url + 'api/v1/get-refund-note/' + order_id, {
        method: 'GET', headers: ApiConfig.xhr_header,
    })
        .then(r => r.json())
        .then(rJson => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch(e => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}
/**
 * 
 * @param order_id
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function getOrderDetail(order_id, callback, error_handle) {
    await fetch(ApiConfig.api_url + 'api/v1/get-order-detail/' + order_id, {
        method: 'GET',
        headers: ApiConfig.xhr_header,
    })
        .then((r) => r.json())
        .then((rJson) => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch(e => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}
/**
 * 
 * @param order_id
 * @param user_id
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function confirmRevertOrder(order_id, user_id, callback, error_handle) {
    await fetch(ApiConfig.api_url + 'api/v1/returns-by-customer', {
        method: 'POST',
        headers: ApiConfig.xhr_header,
        body: JSON.stringify({
            id_donhang: order_id,
            id_khachhang: user_id,
            hoantrahang: 1
        })
    })
        .then((r) => r.json())
        .then((rJson) => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch(e => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}
/**
 * 
 * @param id_phieunhapkho
 * @param trang_thai
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function cancelImportStockBill(id_phieunhapkho, trang_thai, callback, error_handle) {
    await fetch(ApiConfig.api_url + 'api/v1/update-input-note-by-shipper', {
        method: 'POST',
        headers: ApiConfig.xhr_header,
        body: JSON.stringify({
            id_phieunhapkho: id_phieunhapkho,
            trang_thai: trang_thai,
        })
    })
        .then((r) => r.json())
        .then((rJson) => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch(e => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}
/**
 * 
 * @param params
 * @param request_url
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function searchStockerOrder(params, request_url, callback, error_handle) {
    const requestUrl = request_url !== null ? request_url : ApiConfig.api_url + 'api/v1/search-stocker-order';
    await fetch(requestUrl, {
        method: 'POST',
        headers: ApiConfig.xhr_header,
        body: JSON.stringify(params)
    })
        .then((r) => r.json())
        .then((rJson) => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch(e => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}
