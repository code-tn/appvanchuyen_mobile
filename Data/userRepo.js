import { ApiConfig } from "../config/api";
import { AsyncStorage } from "react-native";
import { Auth } from '../Networking/auth';

export async function refreshUserStorage() {
    let curUser = await getUserStorage();
    await getUserInfo(curUser.id, res => {
        if (res.success) {
            setTimeout(async () => {
                await AsyncStorage.setItem('@User', JSON.stringify(res.data));
            }, 100);
        } else {
            alert(res.message);
        }
    }, error => {
        alert(error);
    });
}

/**
 *
 * @returns {Promise<Error>}
 */
export async function getUserStorage() {
    try {
        return await JSON.parse(await AsyncStorage.getItem('@User'));
    } catch (e) {
        return new Error(e);
    }
}

/**
 * URL: /api/v1/getcustomerdata/1
 * @param user_id
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function getUserInfo(user_id, callback, error_handle) {
    await fetch(ApiConfig.api_url + 'api/v1/getcustomerdata/' + user_id, {
        method: 'GET', headers: ApiConfig.xhr_header
    })
        .then((r) => r.json())
        .then((rJson) => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch((e) => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}

/**
 * URL: /api/v1/update-customer-banking
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function updateUserBankInfo(params, callback, error_handle) {
    if (typeof params.id_khachhang === "undefined") {
        const user = await Auth.currentUser();
        params.id_khachhang = user.id;
    }
    await fetch(ApiConfig.api_url + 'api/v1/update-customer-banking', {
        method: 'POST',
        headers: ApiConfig.xhr_header,
        body: JSON.stringify({
            id_khachhang: params.id_khachhang,
            so_taikhoan: params.so_taikhoan,
            chinhanh: params.chinhanh,
            nganhang: params.nganhang,
            chutaikhoan: params.chutaikhoan,
            lich_doisoat: params.lich_doisoat
        })
    })
        .then((r) => r.json())
        .then((rJson) => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch((e) => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}

/** 9:48 AM 25-02-2019
 * URL: /api/v1/get-list-of-order-statistics-by-customer
 * METHOD: POST
 * @param options
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function getCustomerStatislics(options, callback, error_handle) {
    if (typeof options.id_khachhang === undefined) {
        error_handle(new Error('Dữ liệu gửi đi không hợp lệ!'));
    }
    await fetch(ApiConfig.api_url + 'api/v1/get-list-of-order-statistics-by-customer', {
        method: 'POST', headers: ApiConfig.xhr_header, body: JSON.stringify({
            id_khachhang: options.id_khachhang
        })
    })
        .then((r) => r.json())
        .then((rJson) => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch((e) => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}

/**
 * /api/v1/get-customer-bookmark
 * @param options
 * @param callback
 * @param error
 * @returns {Promise<void>}
 */
export async function getCustomerBookmark(options, callback, error_handle) {

    let dOptions = {
        id_khachhang: null,
        loai_diachi: 'nguoigui'
    };

    let nOptions = Object.assign(dOptions, options);

    await fetch(ApiConfig.api_url + 'api/v1/get-customer-bookmark', {
        method: 'POST',
        headers: ApiConfig.xhr_header,
        body: JSON.stringify(nOptions)
    })
        .then((r) => r.json())
        .then((rJson) => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch((e) => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}

/**
 * URL: /api/v1/update-customer-bookmark/2
 * @param dataSource
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function updateCustomerBookmark(dataSource, callback, error_handle) {
    if (typeof dataSource.id === 'undefined') {
        if (typeof error_handle === 'function') error_handle('Dữ liệu cập nhật không hợp lệ!');
    }
    await fetch(ApiConfig.api_url + 'api/v1/update-customer-bookmark/' + dataSource.id, {
        method: 'POST', headers: ApiConfig.xhr_header,
        body: JSON.stringify({
            ten_goinho: dataSource.ten_goinho,
            hoten_nguoinhan: dataSource.hoten_nguoinhan,
            so_dienthoai: dataSource.so_dienthoai,
            tinh: dataSource.tinh,
            huyen: dataSource.huyen,
            chitiet: dataSource.chitiet,
            phuong: dataSource.phuong,
            loai_diachi: dataSource.loai_diachi,
        })
    })
        .then((r) => r.json())
        .then((rJson) => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch((e) => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}

export async function deleteCustomerBookmark(bookmark_id, callback, error_handle) {
    await fetch(ApiConfig.api_url + 'api/v1/delete-customer-bookmark/' + bookmark_id, {
        method: 'POST',
        headers: ApiConfig.xhr_header
    })
        .then((response) => response.json())
        .then((responseJson) => {
            if (typeof callback === 'function') callback(responseJson);
            return responseJson;
        })
        .catch((error) => {
            if (typeof error_handle === 'function') error_handle(error);
            return error;
        });
}

/**
 * URL: http://120.72.107.50:3535/api/v1/create-address-bookmark
 * @param data
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function createCustomerBookmark(data, callback, error_handle) {
    await fetch(ApiConfig.api_url + 'api/v1/create-address-bookmark', {
        method: 'POST', headers: ApiConfig.xhr_header,
        body: JSON.stringify(data)
    })
        .then((r) => r.json())
        .then((rJson) => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch((e) => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}
/**
 * URL: cross-checking/list/3
 * @param data
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function getCashFollow(id_shop, callback, error_handle) {
    await fetch(ApiConfig.api_url + 'api/v1/cross-checking/list/' + id_shop, {
        method: 'GET', headers: ApiConfig.xhr_header,
    })
        .then((r) => r.json())
        .then((rJson) => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch((e) => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}

/**
 * URL: api/v1/signup-facebook
 * @param data
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function signupFacebook(data, callback, callback_error) {
    try {
        let API_URL = ApiConfig.api_url + 'api/v1/signup-facebook';
        let API_HEADER = {
            method: 'POST',
            headers: ApiConfig.xhr_header,
            body: JSON.stringify(data)
        };
        await fetch(API_URL, API_HEADER)
            .then((responseJson) => responseJson.json())
            .then((res) => callback(res));
    } catch (e) {
        if (typeof callback_error === 'function') callback_error(e);
        return e;
    }
}

/**
 * URL: api/v1/request-forgot-password
 * @param user_id
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function requestForgotPassword(data, callback, error_handle) {
    await fetch(ApiConfig.api_url + 'api/v1/request-forgot-password', {
        method: 'POST',
        headers: ApiConfig.xhr_header,
        body: JSON.stringify(data)
    })
        .then((r) => r.json())
        .then((rJson) => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch((e) => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}

/**
 * URL: api/v1/change-forgot-password
 * @param user_id
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function changeForgotPassword(data, callback, error_handle) {
    await fetch(ApiConfig.api_url + 'api/v1/change-forgot-password', {
        method: 'POST',
        headers: ApiConfig.xhr_header,
        body: JSON.stringify(data)
    })
        .then((r) => r.json())
        .then((rJson) => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch((e) => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}
/**
 * URL: api/v1/verify-active-account-code
 * @param email
 * @param token
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function confirmEmailToken(data, callback, error_handle) {
    await fetch(ApiConfig.api_url + 'api/v1/verify-active-account-code', {
        method: 'POST',
        headers: ApiConfig.xhr_header,
        body: JSON.stringify(data)
    })
        .then((r) => r.json())
        .then((rJson) => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch((e) => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}

/**
 * URL: /api/v1/get-popups
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function getPopup(callback, error_handle) {
    await fetch(ApiConfig.api_url + 'api/v1/get-popups', {
        method: 'GET',
        headers: ApiConfig.xhr_header
    })
        .then((r) => r.json())
        .then((rJson) => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch((e) => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}

/**
 * URL: /api/v1/update-customer-profile
 * @param callback
 * @param params
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function updateUserPassword(params, callback, error_handle) {

    await fetch(ApiConfig.api_url + 'api/v1/update-customer-profile', {
        method: 'POST',
        headers: ApiConfig.xhr_header,
        body: JSON.stringify(params)
    })
        .then((r) => r.json())
        .then((rJson) => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch((e) => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}

/**
 * URL: api/v1/get-cross-checking-history
 * @param callback
 * @param params
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function getCrossCheckingHistory(params, reqUrl, callback, error_handle) {
    const requestUrl = reqUrl === null ? ApiConfig.api_url + 'api/v1/get-cross-checking-history' : reqUrl;
    await fetch(requestUrl, {
        method: 'POST',
        headers: ApiConfig.xhr_header,
        body: JSON.stringify(params)
    })
        .then((r) => r.json())
        .then((rJson) => {
            if (typeof callback === 'function') callback(rJson);
            return rJson;
        })
        .catch((e) => {
            if (typeof error_handle === 'function') error_handle(e);
            return e;
        });
}