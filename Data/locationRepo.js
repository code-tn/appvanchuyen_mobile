import {ApiConfig} from "../config/api";

/**
 * URL: http://120.72.107.50:3535/api/v1/get-list-of-province
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function getProvinceList(callback, error_handle) {
    await fetch(ApiConfig.api_url + 'api/v1/get-list-of-province', {
        method: 'GET', headers: ApiConfig.xhr_header
    })
        .then((response) => response.json())
        .then((responseJson) => {
            if (typeof callback === 'function') callback(responseJson);
            return responseJson;
        })
        .catch((error) => {
            if (typeof error_handle === 'function') error_handle(error);
            return error;
        });
}

/**
 * URL: http://120.72.107.50:3535/api/v1/get-list-of-district-by-province-id/01
 * @param province_id
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */
export async function getDistrictByProvinceID(province_id, callback, error_handle) {
    await fetch(ApiConfig.api_url + 'api/v1/get-list-of-district-by-province-id/' + province_id, {
        method: 'GET', headers: ApiConfig.xhr_header
    })
        .then((response) => response.json())
        .then((responseJson) => {
            if (typeof callback === 'function') callback(responseJson);
            return responseJson;
        })
        .catch((error) => {
            if (typeof error_handle === 'function') error_handle(error);
            return error;
        });
}

/**
 * URL: http://giaohangbrvt.com/api/v1/get-ward-by-district/001
 * @param district_id
 * @param callback
 * @param error_handle
 * @returns {Promise<void>}
 */

export async function getWardByDistrictID(district_id, callback, error_handle) {
    await fetch(ApiConfig.api_url + 'api/v1/get-ward-by-district/' + district_id, {
        method: 'GET', headers: ApiConfig.xhr_header
    })
        .then((response) => response.json())
        .then((responseJson) => {
            if (typeof callback === 'function') callback(responseJson);
            return responseJson;
        })
        .catch((error) => {
            if (typeof error_handle === 'function') error_handle(error);
            return error;
        });
}