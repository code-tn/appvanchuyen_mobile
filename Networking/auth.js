import {AsyncStorage} from 'react-native';
import {ApiConfig} from '../config/api';
import { refreshUserStorage } from '../Data/userRepo';

let AuthShiper = {

    login: async (login, password, callback, callback_error) => {
        await fetch(ApiConfig.api_url + 'api/v1/shipper-login', {
            method: 'POST', headers: ApiConfig.xhr_header,
            body: JSON.stringify({sodienthoai: login, password: password})
        })
            .then(responseJson => responseJson.json())
            .then(async (responseJson) => {
                if (responseJson.success) {
                    await AsyncStorage.setItem('@Shiper', JSON.stringify(responseJson.data));
                }
                await callback(responseJson, responseJson.success);
            })
            .catch(async (e) => {
                if (typeof callback_error === 'function') {
                    await callback_error(e);
                }
                return new Error(e);
            });
    },
    logout: async (callback, callback_error) => {
        try {
            await AsyncStorage.setItem('@Shiper', '');
            if (typeof callback === 'function') {
                callback();
            }
        } catch (e) {
            if (typeof callback_error === 'function') {
                callback_error(e);
            }
            return new Error(e.toString());
        }
    },
    currentUser: async () => {
        try {
            return await JSON.parse(await AsyncStorage.getItem('@Shiper'));
        } catch (e) {
            return new Error(e);
        }
    },
    getShipperInfo: async(idShipper, callback, callback_error) => {
        try {
            let API_URL = ApiConfig.api_url + 'api/v1/get-shipper-profile/'+idShipper;
            let API_HEADER = {
                method: 'GET',
                headers: ApiConfig.xhr_header,
            };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    }

};

/**
 *
 * @type {{currentUser: Auth.currentUser, getAddressList: Auth.getAddressList, login: Auth.login}}
 */
let Auth = {

    /**
     *
     * @param login
     * @param password
     * @param callback
     * @returns {Promise<void>}
     */
    login: async (login, password, callback, callback_error) => {
        await fetch(ApiConfig.api_url + 'api/v1/signin', {
            method: 'POST', headers: ApiConfig.xhr_header,
            body: JSON.stringify({email: login, password: password})
        })
            .then(responseJson => responseJson.json())
            .then(async (responseJson) => {
                // if (responseJson.success) {
                //     await AsyncStorage.setItem('@User', JSON.stringify(responseJson.data));
                // }
                callback(responseJson, responseJson.success);
            })
            .catch((e) => {
                if (typeof callback_error === 'function') {
                    callback_error(e);
                }
                return new Error(e.toString());
            });
    },

    logout: async (callback, callback_error) => {
        try {
            await AsyncStorage.setItem('@User', '');
            if (typeof callback === 'function') {
                callback();
            }
        } catch (e) {
            if (typeof callback_error === 'function') {
                callback_error(e);
            }
            return new Error(e.toString());
        }
    },

    /**
     *
     * @param callback
     * @returns {Promise<*>}
     */
    // reload: async (callback) => {
    //     let currentUser = await Auth.currentUser();
    //     if(typeof currentUser !== 'object')
    //         return callback(new Error('Chưa đăng nhập!'));
    //
    //
    // },

    /**
     *
     * @returns {Promise<*>}
     */
    currentUser: async () => {
        try {
            return await JSON.parse(await AsyncStorage.getItem('@User'));
        } catch (e) {
            return new Error(e);
        }
    },

    refreshAsyncData: async (data) => {
        await AsyncStorage.setItem('@User', JSON.stringify(data));
    },

    updateProfile: async (currentUser = null, data) => {
        try {
            if (currentUser === null) currentUser = await Auth.currentUser();
            if (typeof data !== 'object') {
                return new Error('Data is empty!');
            }
            let REQ_BODY = {
                id_khachhang: currentUser.id
            };

            let API_URL = ApiConfig.api_url + 'api/v1/update-customer-profile';
            let API_HEADER = {
                method: 'POST', headers: ApiConfig.xhr_header,
                body: JSON.stringify(Object.assign(REQ_BODY, data))
            };

            let request = await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then(async (res) => {
                    if (res.success) {
                        refreshUserStorage();
                        return res;
                    } else {
                        return new Error(res.message);
                    }
                })
                .catch((e) => new Error(e));
            return request;
        } catch (e) {
            return new Error(e);
        }
    },

    /**
     *
     * @param currentUser
     * @returns {Promise<void>}
     */
    getAddressList: async (currentUser = null) => {
        try {
            if (currentUser === null) currentUser = await Auth.currentUser();

            let API_URL = ApiConfig.api_url + 'api/v1/get-address-list/' + currentUser.id;
            let API_HEADER = {method: 'GET', headers: ApiConfig.xhr_header};

            let request = await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => {
                    if (res.success) {
                        var array_nguoigui_diachi = [];
                        res.data.map((item) => {
                            array_nguoigui_diachi.push({
                                label: item.chitiet + ',' + item.huyen + ',' + item.phuong
                                    + ',' + item.tinh + ',' + item.quocgia,
                                value: item.id,
                                extra: item
                            });
                        });
                        return array_nguoigui_diachi;
                    } else {
                        return new Error('Kết nối server chậm!, vui lòng thử lại sau.');
                    }
                })
                .catch((e) => new Error(e));
            return request;
        } catch (e) {
            return new Error(e);
        }
    },
    /**
     * @param id_fb
     * @param callback
     * @param callback_error
     * @returns {Promise<void>}
     */
    getUserInfoFB: async (id_fb, callback, callback_error) => {
        await fetch(ApiConfig.api_url + 'api/v1/get-customer-by-facebook-id', {
            method: 'POST', headers: ApiConfig.xhr_header,
            body: JSON.stringify({id_fb: id_fb})
        })
            .then(responseJson => responseJson.json())
            .then(async (responseJson) => {
                if (responseJson.success) {
                    await AsyncStorage.setItem('@User', JSON.stringify(responseJson.data));
                }
                callback(responseJson, responseJson.success);
                return responseJson;
            })
            .catch((e) => {
                if (typeof callback_error === 'function') {
                    callback_error(e);
                }
                return new Error(e.toString());
            });
    },



};

export {Auth, AuthShiper};
