import { ApiConfig } from '../config/api';

let Blog = {
    getNews: async (request_url, callback) => {
        let API_URL = request_url === null ? ApiConfig.api_url + 'api/v1/blog/list' : request_url;
        let API_HEADER = {
            method: 'GET', 
            headers: ApiConfig.xhr_header,
        };
        await fetch(API_URL, API_HEADER)
            .then((responseJson) => responseJson.json())
            .then((res) => {
                callback(res);
            });
    },
};

export { Blog };
