import { ApiConfig } from '../config/api';

const Order = {

    create: async (data, callback) => {
        let API_URL = ApiConfig.api_url + 'api/v1/create-order';
        let API_HEADER = {
            method: 'POST', headers: ApiConfig.xhr_header, body: JSON.stringify(data)
        };

        await fetch(API_URL, API_HEADER)
            .then((responseJson) => responseJson.json())
            .then((res) => {
                callback(res);
            });
    },
    // update-order
    updateOrder: async (data, callback) => {
        let API_URL = ApiConfig.api_url + 'api/v1/update-order';
        let API_HEADER = {
            method: 'POST', headers: ApiConfig.xhr_header, body: JSON.stringify(data)
        };

        await fetch(API_URL, API_HEADER)
            .then((responseJson) => responseJson.json())
            .then((res) => {
                callback(res);
            });
    },
    /**
     * GET ORDER SHIP COST
     * @param data
     * @param callback
     * @returns {Promise<void>}
     */
    getShipCost: async (data, callback) => {
        let API_URL = ApiConfig.api_url + 'api/v1/calculate-ship-fee';
        let API_HEADER = {
            method: 'POST',
            headers: ApiConfig.xhr_header,
            body: JSON.stringify(data)
        };
        await fetch(API_URL, API_HEADER)
            .then((responseJson) => responseJson.json())
            .then((res) => {
                callback(res);
            });
    },
    /**
     * FIND ORDER NEAREST
     * @param data
     * @param callback
     * @param callback_error
     * @returns {Promise<*>}
     */
    findOrderNearest: async (id_shiper, request_url, callback, callback_error) => {
        try {
            let API_URL = request_url === null ? ApiConfig.api_url + 'api/v1/get-order-list-shipper' : request_url;
            let API_HEADER = {
                method: 'POST', headers: ApiConfig.xhr_header, body: JSON.stringify({
                    id_vanchuyen: id_shiper
                })
            };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => {
                    callback(res);
                });
        } catch (e) {
            if (typeof callback_error === 'function') {
                callback_error(e);
            }
            return e;
        }
    },

    getShiperOrders: async (id_shiper, request_url, callback, callback_error) => {
        try {
            let API_URL = request_url === null ? ApiConfig.api_url + 'api/v1/get-old-order-list-shipper' : request_url;
            let API_HEADER = {
                method: 'POST', headers: ApiConfig.xhr_header, body: JSON.stringify({
                    id_vanchuyen: id_shiper
                })
            };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => {
                    callback(res);
                });
        } catch (e) {
            if (typeof callback_error === 'function') {
                callback_error(e);
            }
            return e;
        }
    },

    getShiperFilterOrders: async (params, request_url, callback, callback_error) => {
        try {
            let API_URL = request_url === null ? ApiConfig.api_url + 'api/v1/search-shipper-order' : request_url;
            let API_HEADER = {
                method: 'POST',
                headers: ApiConfig.xhr_header,
                body: JSON.stringify(params)
            };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => {
                    callback(res);
                });
        } catch (e) {
            if (typeof callback_error === 'function') {
                callback_error(e);
            }
            return e;
        }
    },

    getDetail: async (id_order, callback, callback_error) => {
        try {
            let API_URL = ApiConfig.api_url + 'api/v1/get-order-detail/' + id_order;
            let API_HEADER = { method: 'GET', headers: ApiConfig.xhr_header };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },

    getOrderReceiptBills: async (id_order, callback, callback_error) => {
        try {
            let API_URL = ApiConfig.api_url + 'api/v1/get-receipt-note/' + id_order;
            let API_HEADER = { method: 'GET', headers: ApiConfig.xhr_header };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },

    getOrderDeliveryBills: async (id_order, callback, callback_error) => {
        try {
            let API_URL = ApiConfig.api_url + 'api/v1/get-delivery-note/' + id_order;
            let API_HEADER = { method: 'GET', headers: ApiConfig.xhr_header };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },

    getOrderRefundBills: async (id_order, callback, callback_error) => {
        try {
            let API_URL = ApiConfig.api_url + 'api/v1/get-refund-note/' + id_order;
            let API_HEADER = { method: 'GET', headers: ApiConfig.xhr_header };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },

    getOrderStatusList: async (type, callback, callback_error) => {
        try {
            let API_URL = ApiConfig.api_url + 'api/v1/get-order-statuses/' + type.toString();
            let API_HEADER = { method: 'GET', headers: ApiConfig.xhr_header };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },

    getOrderDeliveryNote: async (id_order, callback, callback_error) => {
        try {
            let API_URL = ApiConfig.api_url + 'api/v1/get-delivery-note/' + id_order;
            let API_HEADER = { method: 'GET', headers: ApiConfig.xhr_header };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },
    createStockInputNote: async (data, callback, callback_error) => {
        try {
            let API_URL = ApiConfig.api_url + 'api/v1/create-stock-input-note';
            let API_HEADER = {
                method: 'POST', headers: ApiConfig.xhr_header,
                body: JSON.stringify(data)
            };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },
    getStockInputNote: async (id_order, callback, callback_error) => {
        try {
            let API_URL = ApiConfig.api_url + 'api/v1/get-stock-input-note/' + id_order;
            let API_HEADER = { method: 'GET', headers: ApiConfig.xhr_header };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },
    createDepositNote: async (data, callback, callback_error) => {
        try {
            let API_URL = ApiConfig.api_url + 'api/v1/create-deposit-note';
            let API_HEADER = {
                method: 'POST', headers: ApiConfig.xhr_header,
                body: JSON.stringify(data)
            };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },
    confirm: async (data, callback, callback_error) => {
        try {
            let API_URL = ApiConfig.api_url + 'api/v1/create-order-booking';
            let API_HEADER = {
                method: 'POST', headers: ApiConfig.xhr_header,
                body: JSON.stringify(data)
            };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },
    /*
     * STOCKER
     */
    // phieu yeu cau nhap kho moi
    getStockImportRequest: async (params, callback, callback_error) => {
        const page_url = params !== null ? params.request_url : null;
        try {
            let API_URL = page_url === null ? ApiConfig.api_url + 'api/v1/get-new-input-note' : page_url;
            let API_HEADER = {
                method: 'GET',
                headers: ApiConfig.xhr_header,
            };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },
    // xac nhan phieu yeu cau nhap kho
    confirmImportStockRequest: async (idBill, data, callback, callback_error) => {
        try {
            let API_URL = ApiConfig.api_url + 'api/v1/update-input-note/' + idBill;
            let API_HEADER = {
                method: 'POST',
                headers: ApiConfig.xhr_header,
                body: JSON.stringify(data)
            };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },
    // lay danh sach phieu nhap kho da xac nhan
    getStockImportedNote: async (params, callback, callback_error) => {
        const page_url = params !== null ? params.request_url : null;
        try {
            let API_URL = page_url === null ? ApiConfig.api_url + 'api/v1/get-imported-input-note' : page_url;
            let API_HEADER = {
                method: 'GET',
                headers: ApiConfig.xhr_header,
            };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },
    // lay danh sach phieu yeu cau xuat kho
    getStockExportNote: async (params, callback, callback_error) => {
        const page_url = params !== null ? params.request_url : null;
        try {
            let API_URL = page_url === null ? ApiConfig.api_url + 'api/v1/warehouse/delivery-note-list' : page_url;
            let API_HEADER = {
                method: 'GET',
                headers: ApiConfig.xhr_header,
            };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },
    //danh sach don hang trong kho 
    getStockOrders: async (params, callback, callback_error) => {
        const page_url = params !== null ? params.request_url : null;
        try {
            let API_URL = page_url === null ? ApiConfig.api_url + 'api/v1/warehouse/order-list' : page_url;
            let API_HEADER = {
                method: 'GET',
                headers: ApiConfig.xhr_header,
            };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },
    // tao phieu xuat kho
    getShipperList: async (callback) => {
        try {
            let API_URL = ApiConfig.api_url + 'api/v1/get-shippers';
            let API_HEADER = {
                method: 'GET',
                headers: ApiConfig.xhr_header,
            };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },
    // tao phieu xuat kho
    // api/v1/get-shippers-by-address-order/38
    getShippersByAddressOrder: async (idOrder, callback, callbackError) => {
        try {
            const apiUrl = ApiConfig.api_url + 'api/v1/get-shippers-by-address-order/' + idOrder;
            console.log(apiUrl);
            const apiHEADER = {
                method: 'GET',
                headers: ApiConfig.xhr_header,
            };
            await fetch(apiUrl, apiHEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callbackError === 'function') callbackError(e);
            return e;
        }
    },
    createStockExportRequest: async (data, callback, callback_error) => {
        try {
            let API_URL = ApiConfig.api_url + 'api/v1/warehouse/create-delivery-note';
            let API_HEADER = {
                method: 'POST',
                headers: ApiConfig.xhr_header,
                body: JSON.stringify(data)
            };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },
    // shipper export request
    getShipperExportRequest: async (id_shiper, params, callback, callback_error) => {
        let request_url = null;
        if (params !== null) {
            request_url = params.request_url;
        }
        try {
            let API_URL = request_url === null ? (ApiConfig.api_url + 'api/v1/warehouse/delivery-note-list/' + id_shiper) : request_url;
            let API_HEADER = {
                method: 'GET',
                headers: ApiConfig.xhr_header,
            };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => callback(res));
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },
    // confirm shipper export request
    confirmShipperExportRequest: async (idBill, data, callback, callback_error) => {
        try {
            let API_URL = ApiConfig.api_url + 'api/v1/warehouse/update-delivery-note/' + idBill;
            let API_HEADER = {
                method: 'POST',
                headers: ApiConfig.xhr_header,
                body: JSON.stringify(data)
            };
            await fetch(API_URL, API_HEADER)
                .then((responseJson) => responseJson.json())
                .then((res) => {
                    if (typeof callback === 'function') callback(res);
                    return res;
                });
        } catch (e) {
            if (typeof callback_error === 'function') callback_error(e);
            return e;
        }
    },
    // upload image to order 
    // url api/v1/update-order-images
    updateOrderImages: async (params, callback) => {
        const data = new FormData();
        data.append('id_donhang', params.id + '');
        data.append('image', {
            uri: params.image_url,
            type: 'image/jpeg',
            name: params.image_name
        });
        await fetch(ApiConfig.api_url + 'api/v1/update-order-images', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: data
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (typeof callback === 'function') callback(responseJson);
                return responseJson;
            })
            .catch((error) => {
                console.error(error);
            });
    },
};

export { Order };
