import React, { Component } from 'react';
import { AppRegistry, SectionList } from 'react-native';
import ApiConfig from '../config/api';

async function getUpdateInfo(url, options) {
    try {
        let response = await fetch(url, options);
        let responseJson = await response.json();
        return responseJson;
    } catch (e) {
        console.log(e);
    }
}

export { getUpdateInfo };
