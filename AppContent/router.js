import React from 'react';
import CreateOrder from '../Components/Order/User/CreateOrder';
import EditOrder from '../Components/Order/User/EditOrder';
import UpdatePassword from '../Components/User/Partials/UpdatePassword';
import Statistics from '../Components/Order/User/Statistics';
import iconNotice from '../Components/Shared/images/notice.png';
import OrderHistory from '../Components/Order/User/OrderHistory';
import iconList from '../Components/Shared/images/list.png';
import CashFlow from '../Components/User/CashFlow';
import iconMoneyBag from '../Components/Shared/images/money.png';
import iconUser from '../Components/Shared/images/user.png';

import { createBottomTabNavigator } from 'react-navigation';
import { Image } from 'react-native';
import Colors from '../Components/Assets/Colors';

let iconStyle = { width: 35, height: 35, resizeMode: 'contain' };

import {ScreenLogin, ScreenLoginStaff, ScreenSignup, ScreenResetPassword, ScreenConfirmEmail,
    ScreenChat,ScreenCreateOrder, ScreenEditOrder,ScreenEditUserInfo,ScreenEditBankInfo,
    ScreenEditAddress, ScreenConfirmToken, ScreenNewPassword, ScreenUpdatePassword, ScreenNotice,
    ScreenStatistics, ScreenOrderHistory, ScreenCashFlow, ScreenUserInfo} from 'screens';

export default {
    route: {
        LoginScreen: ScreenLogin,
        StaffLoginScreen: ScreenStaffLogin,
        SignupScreen: ScreenSignup,
        ResetScreen: ScreenResetPassword,
        ConfirmEmailScreen: ScreenConfirmEmail,
        ChatScreen: ScreenChat,
        CreateOrderScreen: ScreenCreateOrder,
        EditOrderScreen: ScreenEditOrder,
        UserOrderDetailScreen: ScreenUserOrderDetail,
        EditUserInfoScreen: ScreenEditUserInfo,
        EditBankInfoScreen: ScreenEditBankInfo,
        EditAddressScreen: ScreenEditAddress,
        ConfirmTokenScreen: ScreenConfirmToken,
        NewPasswordScreen: ScreenNewPassword,
        UpdatePasswordScreen: ScreenUpdatePassword,
        NoticeScreen: ScreenNotice,
        /*
        | SHIPER -----------------------------------------------------------------------------------
         */
        CreateReceiptBill: ScreenCreateReceipt,
        CreateDeliveryBill: {screen: ScreenCreateDelivery},
        CreateImportWarehouseBill: {screen: ScreenImportWarehouse},
        ShipperOrderDetailScreen: {screen: ScreenShipperOrderDetail},
        /*
        | WAREHOUSE --------------------------------------------------------------------------------
         */
        WarehouseOrderDetailScreen: ScreenWarehouseOrderDetail,
    },
    options: {
        initialRouteName: 'LoginScreen',
        headerMode: 'none'
    }
};
