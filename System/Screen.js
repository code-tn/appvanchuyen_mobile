import { AsyncStorage } from 'react-native';

let ScreenManager = {
    setInitScreen: async (screen) => {
        try {
            return await AsyncStorage.setItem('@InitScreen', String(screen.toString()));
        } catch (e) {
            return new Error(e.toString());
        }
    },
    getInitScreen: async () => {
        try {
            let res = await AsyncStorage.getItem('@InitScreen');
            return res;
        } catch (e) {
            return new Error(e.toString());
        }
    }
};

export { ScreenManager };
