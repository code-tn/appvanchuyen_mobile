import React from 'react';
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import { Image } from 'react-native';

// ICONS
import Colors from '../Components/Assets/Colors';
import iconNewImport from '../Components/Shared/images/list.png';
import iconInStock from '../Components/Shared/images/check-mark.png';
import iconImport from '../Components/Shared/images/import.png';
import iconExport from '../Components/Shared/images/export.png';
import iconUser from '../Components/Shared/images/user.png';
// screen
import StaffLogin from "../Components/User/Login/StaffLogin";
import StockImportRequest from '../Components/Order/Warehouse/StockImportRequest';
import StockOrders from '../Components/Order/Warehouse/StockOrders';
import StockImportBills from '../Components/Order/Warehouse/StockImportBills';
import StockExportBills from '../Components/Order/Warehouse/StockExportBills';
import StockerInfo from '../Components/User/Stocker/StockerInfo';
import StockOrderDetails from '../Components/Order/Warehouse/StockOrderDetails';
import StockCreateExportBill from '../Components/Order/Warehouse/StockCreateExportBill';

const iconStyle = {width: 27, height: 27, resizeMode: 'contain'};

const AuthStock = createStackNavigator({
    StaffLoginScreen: StaffLogin
}, {
    headerMode: 'none'
});

const StockStack = createStackNavigator({
    StockTabScreen: createBottomTabNavigator(
        {
            // danh sach yeu cau nhap kho moi
            StockImportRequestScreen: {
                screen: StockImportRequest,
                navigationOptions: {
                    tabBarIcon: (<Image source={iconNewImport} style={iconStyle}/>)
                },
            },
            // danh sach don hang trong kho
            StockOrdersScreen: {
                screen: StockOrders,
                navigationOptions: {
                    tabBarIcon: (<Image source={iconInStock} style={iconStyle}/>)
                },
            },
            // danh sach phieu nhap kho
            StockImportBillsScreen: {
                screen: StockImportBills,
                navigationOptions: {
                    tabBarIcon: (<Image source={iconImport} style={iconStyle}/>)
                },
            },
            // danh sach phieu xuat kho
            StockExportBillsScreen: {
                screen: StockExportBills,
                navigationOptions: {
                    tabBarIcon: (<Image source={iconExport} style={iconStyle}/>)
                },
            },
            // thong tin user
            StockerInfoScreen: {
                screen: StockerInfo,
                navigationOptions: {
                    tabBarIcon: (<Image source={iconUser} style={iconStyle}/>)
                },
            },
        },
        {
            tabBarPosition: 'bottom',
            tabBarOptions: {
                showLabel: false,
                activeBackgroundColor: Colors.activeColor,
                inactiveBackgroundColor: Colors.mainColor
            }
        }
    ),
    // tao phieu xuat kho
    StockCreateExportBillScreen: { screen: StockCreateExportBill },
    // chi tiet don hang
    StockOrderDetailScreen: { screen: StockOrderDetails }
}, { headerMode: 'none' });

export { StockStack, AuthStock };
