import React from 'react';
import { Image } from 'react-native';
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
// icons
import iconNotice from '../Components/Shared/images/notice.png';
import iconList from '../Components/Shared/images/list.png';
import iconMoneyBag from '../Components/Shared/images/money.png';
import iconUser from '../Components/Shared/images/user.png';
// screens
import Login from '../Components/User/Login/Login';
import Signup from '../Components/User/Login/Signup';
import ResetPassword from '../Components/User/Login/ResetPassword';
import ConfirmResetPassword from '../Components/User/Login/ConfirmResetPassword';
import ConfirmEmail from '../Components/User/Login/ConfirmEmail';
import ConfirmToken from '../Components/User/Partials/ConfirmToken';
import Statistics from '../Components/Order/User/Statistics';
import OrderHistory from '../Components/Order/User/OrderHistory';
import CrossChecking from '../Components/User/CrossChecking';
import UserInfo from '../Components/User/UserInfo';
import Colors from '../Components/Assets/Colors';

const iconStyle = {
    width: 27, 
    height: 27, 
    resizeMode: 'contain'
};

const AuthStack = createStackNavigator({
    LoginScreen: Login,
    SignupScreen: Signup,
    ResetScreen: ResetPassword,
    ConfirmResetPasswordScreen: ConfirmResetPassword,
    ConfirmEmailScreen: ConfirmEmail,
    ConfirmTokenScreen: ConfirmToken
}, {
    headerMode: 'none'
});

const UserNavigator = createBottomTabNavigator(
    {
        StatisticsScreen: {
            screen: Statistics,
            navigationOptions: {
                tabBarIcon: (<Image source={iconNotice} style={iconStyle} />)
            }
        },
        OdersScreen: {
            screen: OrderHistory,
            navigationOptions: {
                tabBarIcon: (<Image source={iconList} style={iconStyle} />)
            }
        },
        CashScreen: {
            screen: CrossChecking,
            navigationOptions: {
                tabBarIcon: (<Image source={iconMoneyBag} style={iconStyle} />)
            }
        },
        InfoScreen: {
            screen: UserInfo,
            navigationOptions: {
                tabBarIcon: (<Image source={iconUser} style={iconStyle} />)
            }
        },
    },
    {
        initialRouteName: 'StatisticsScreen',
        tabBarPosition: 'bottom',
        tabBarOptions: {
            showLabel: false,
            activeBackgroundColor: Colors.activeColor,
            inactiveBackgroundColor: Colors.mainColor
        }
    }
);

export {
    AuthStack, UserNavigator
};
