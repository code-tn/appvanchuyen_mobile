import React from 'react';
import { Image } from 'react-native';
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import Colors from '../Components/Assets/Colors';
// icons
import iconList from '../Components/Shared/images/list.png';
import iconCheckList from '../Components/Shared/images/check-mark.png';
import iconUser from '../Components/Shared/images/user.png';
import iconExport from '../Components/Shared/images/warehouse.png';
//
import ShipperListOrder from '../Components/Order/Shipper/ShipperListOrder';
import NewOrders from '../Components/Order/Shipper/NewOrders';
import ShipperInfo from '../Components/User/Shipper/ShipperInfo';
import CreateReceipt from '../Components/Order/Shipper/CreateReceipt';
import CreateDelivery from '../Components/Order/Shipper/CreateDelivery';
import ImportWarehouse from '../Components/Order/Shipper/ImportWarehouse';
import CreateReturn from '../Components/Order/Shipper/CreateReturn';
import ShipperOrderDetail from '../Components/Order/Shipper/ShipperOrderDetail';
import StaffLogin from '../Components/User/Login/StaffLogin';
import CreateDepositNote from '../Components/Order/Shipper/CreateDepositNote';
import ShipperExportRequest from '../Components/Order/Shipper/ShipperExportRequest';
import OrderCamera from '../Components/Order/Partials/OrderCamera';

const iconStyle = { width: 27, height: 27, resizeMode: 'contain' };

const AuthShipper = createStackNavigator({
    StaffLoginScreen: StaffLogin
}, {
        headerMode: 'none'
    });

const ShipperStack = createStackNavigator({
    ShipperTabScreen: createBottomTabNavigator({
        NewOrdersScreen: {
            screen: NewOrders,
            navigationOptions: {
                tabBarIcon: (<Image source={iconList} style={iconStyle} />)
            }
        },

        ShipperListOrderScreen: {
            screen: ShipperListOrder,
            navigationOptions: {
                tabBarIcon: (<Image source={iconCheckList} style={iconStyle} />)
            }
        },
        ShipperExportRequestScreen: {
            screen: ShipperExportRequest,
            navigationOptions: {
                tabBarIcon: (<Image source={iconExport} style={iconStyle} />)
            }
        },
        ShipperInfoScreen: {
            screen: ShipperInfo,
            navigationOptions: {
                tabBarIcon: (<Image source={iconUser} style={iconStyle} />)
            }
        }
    },
    {
        tabBarPosition: 'bottom',
        tabBarOptions: {
            showLabel: false,
            activeBackgroundColor: Colors.activeColor,
            inactiveBackgroundColor: Colors.mainColor
        }
    }),
    CreateReceiptBill: CreateReceipt,
    CreateDeliveryBill: { screen: CreateDelivery },
    CreateImportWarehouseBill: { screen: ImportWarehouse },
    CreateDepositNoteScreen: { screen: CreateDepositNote },
    CreateReturnOrderScreen: { screen: CreateReturn },
    ShipperOrderDetailScreen: { screen: ShipperOrderDetail },
    OrderCameraScreen: { screen: OrderCamera } ,
}, { headerMode: 'none' });

export { ShipperStack, AuthShipper };
