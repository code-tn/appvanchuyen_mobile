import { createAppContainer, createStackNavigator, createSwitchNavigator } from 'react-navigation';
/**
 * SCREEN LIST
 */
// import Chat from '../Components/User/Chat';
import CreateOrder from '../Components/Order/User/CreateOrder';
import EditOrder from '../Components/Order/User/UpdateOrder';
import UserOrderDetail from '../Components/Order/User/UserOrderDetail';
import EditUserInfo from '../Components/User/EditUserInfo';
import EditBankInfo from '../Components/User/EditBankInfo';
// import EditAddress from '../Components/User/EditAddress';

import AdderssListScreen from '../Components/User/AddressList';
import AddAdderssScreen from '../Components/User/Address/AddAddress';

import NewPassword from '../Components/User/Partials/NewPassword';
import UpdatePassword from '../Components/User/Partials/UpdatePassword';
import Notice from '../Components/Shared/Notice';
import ScreenAuthentication from '../Components/Authentication';
/**
 * STACK LIST
 */
import { AuthStack, UserNavigator } from './userStack';
import { AuthShipper, ShipperStack } from './shipperStack';
import { AuthStock, StockStack } from './stockStack';
import News from '../Components/Blog/News';
import Post from '../Components/Blog/Post';
import OrderCamera from '../Components/Order/Partials/OrderCamera';

const AppStack = createStackNavigator(
    {
        UserTabScreen: UserNavigator,
        CreateOrderScreen: CreateOrder,
        EditOrderScreen: EditOrder,
        UserOrderDetailScreen: UserOrderDetail,
        OrderCameraScreen: OrderCamera,
        EditUserInfoScreen: EditUserInfo,
        EditBankInfoScreen: EditBankInfo,
        NewPasswordScreen: NewPassword,
        UpdatePasswordScreen: UpdatePassword,
        AdderssListScreen,
        AddAdderssScreen,
        //news
        NewsScreen: News,
        PostScreen: Post
    },
    {
        headerMode: 'none'
    }
);

export default createAppContainer(
    createSwitchNavigator({
        AuthLoading: ScreenAuthentication,
        App: AppStack,
        Shipper: ShipperStack,
        Stock: StockStack,
        Auth: AuthStack,
        AuthShipper,
        AuthStock,
        NoticeScreen: Notice,
    }, {
        initialRouteName: 'AuthLoading',
        headerMode: 'none'
    })
);
